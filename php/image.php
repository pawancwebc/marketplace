<?php

include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');

$section = isset($_GET['section']) ? $_GET['section'] : "dashboard";
$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : "account";
$resend = isset($_GET['resend']) ? $_GET['resend'] : "";
$id = isset($_GET['id']) ? $_GET['id'] : "0";
$sid = isset($_GET['sid']) ? $_GET['sid'] : "0";
$service_id = isset($_GET['service_id']) ? $_GET['service_id'] : "";
$p = isset($_GET['p']) ? $_GET['p'] : "1";

$queryUser = new user();
$user_details = $queryUser->getUser($user_id);

if (!$logged_in) {
    Redirect(make_url('login'));
}

$query = new simage();
$imag = $query->getServiceImages($id);

$query = new simage();
$cimag = $query->getcoverimage($id);

if (isset($_GET['act']) && $_GET['act'] == 'makecoverimage') {
    $query = new simage();
    $query->update_field($sid, 'is_main', '1');

    $query = new simage();
    $query->update_field($cimag->id, 'is_main', '0');

    $admin_user->set_pass_msg('Cover image Has been set');
    Redirect(make_url('image', 'id=' . $id));
}
if (isset($_GET['act']) && $_GET['act'] == 'delete') {
    $query = new simage();
    $query->deleteServiceImage($sid);

    $admin_user->set_pass_msg('Image Has been deleted');
    Redirect(make_url('image', 'id=' . $id));
}

$query = new service();
$list_oneservice = $query->getService($id);

$content = add_metatags("My Account");
?>

