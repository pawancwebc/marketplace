<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
$token = isset($_GET['token']) ? $_GET['token'] : '';

//$query = new user;
//$user_by_token = $query->get_using_field('forgot_password_token', $token);

if (isset($_POST['change_password'])) {
    extract($_POST);
    if ($password == $confirm_password) {
	$query = new user;
	$query->update_reset_password($password, $token);
	Redirect(make_url('login'));
    }
}
?>