<?php
include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
if (!$logged_in) {
    Redirect(make_url('login'));
}
$section = isset($_GET['section']) ? $_GET['section'] : "dashboard";
$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : "account";
$resend = isset($_GET['resend']) ? $_GET['resend'] : "";
$id = isset($_GET['id']) ? $_GET['id'] : "0";
$p = isset($_GET['p']) ? $_GET['p'] : "1";

$queryUser = new user();
$user_details = $queryUser->getUser($user_id);

$query = new category();
$categories = $query->listCategory();

if (isset($_POST['submit'])) {
if ($_POST['name'] || $_POST['service_area'] || $_POST['price'] || $_POST['description'] ||  $_POST['position']){
   
    $query= new service();
    $new_id = $query->saveService($_POST,$user_id);

    /* Delete All Previous Service Rel */
    $query = new categoryService();
    $query->deleteServiceCat($new_id);

    /* Save Category Services Rel Services */
    if (!empty($_POST['catgory_id']) && !empty($_POST['catgory_id'])){
        $query = new categoryService();
        $query->saveServiceCat($new_id, $_POST['catgory_id']);
    }
    $admin_user->set_pass_msg('Service added successfully');
    Redirect(make_url('listservice'));
}
    $admin_user->set_error();
    $admin_user->set_pass_msg('Fields Required.');
    Redirect(make_url('addservice'));
   
}


$content = add_metatags("My Account");
?>

