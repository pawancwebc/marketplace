<?php
include_once('../include/config/config.php');
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/countryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/stateClass.php');
include_once(DIR_FS_SITE.'include/functionClass/cityClass.php');

isset($_POST['country_id'])?$country_id=$_POST['country_id']:$country_id='0';
isset($_POST['state_id'])?$state_id=$_POST['state_id']:$state_id='0';

$return=array();
if($country_id!='0'):
    /*get state with country id*/ 
    $QueryObj = new state();
    $return=$QueryObj->getStateNameByCountry($country_id);
endif;
if($state_id!='0'):
    /*get state with country id*/ 
    $QueryObj = new city();
    $return=$QueryObj->getCityNameByState($state_id);
endif;


echo json_encode($return);
die;
?>