<div class="form-group">
    <label class="text-dark" for="address">Address 1: <span class="warning">*</span></label>
    <textarea name="address"  id="address" class="form-control form-control validate[required]" placeholder="Address1" rows="2"></textarea>
</div>
<div class="form-group">
    <label class="text-dark" for="address1">Address 2: </label>
    <textarea name="address1" id="address1" class="form-control form-control" placeholder="Address2" rows="2"></textarea>
</div>
<div class="form-group">
    <label class="span2 control-label" for="country_id">Country</label>
    <div class="span4">
        <select name="country_id" id="country_id" class="form-control">
            <option value="">Select Country</option>
            <?php foreach (getCountries(true) as $kk => $vv): ?>
                <option value="<?php echo $vv['id']; ?>"><?php echo $vv['short_name']; ?></option>
            <?php endforeach; ?>
        </select>
        <span id="country_error" style="display:none;color:red"><i>Please select the country first.</i></span>
    </div>
</div>
<div class="form-group">
    <label class="span2 control-label" for="state_id">State</label>
    <div class="span4">
        <select name="state_id" id="state_id" class="form-control">
            <option value="">Select State</option>
        </select>
        <span id="state_error" style="display:none;color:red"><i>Please select the state.</i></span>
    </div>
</div>	
<div class="form-group">
    <label class="span2 control-label" for="city_id">City</label>
    <div class="span4">
        <select name="city_id" id="city_id" class="form-control">
            <option value="">Select City</option>
        </select>
        <span id="state_error" style="display:none;color:red"><i>Please select the city.</i></span>
    </div>
</div>
<div class="form-group">
    <label class="text-dark" for="zip_code">Post code: <span class="warning">*</span></label>
    <input type="text" name="zip_code"  id="zip_code" class="form-control form-control--contact validate[required]" placeholder="Zip Code" maxlength="7" size="10"  />
</div>

