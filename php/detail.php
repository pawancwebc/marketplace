<?php
include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
$p = isset($_GET['p']) ? $_GET['p'] : '1';
if (!$logged_in) {
    Redirect(make_url('login'));
}

$queryUser = new user();
$user_details = $queryUser->getUser($user_id);

if (isset($_GET['act']) && $_GET['act'] == 'detail') {
    $query = new trans();
    $new = $query->listorder($id);
   
   
    if ($new) {
        $admin_user->set_pass_msg('Result found');
    } else {
        $admin_user->set_error();
        $admin_user->set_pass_msg('No result found');
        Redirect(make_url('serviceorder'));
    }
}

/* SEO information */
$content = add_metatags("Services");
?>