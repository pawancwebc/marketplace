<?php

$redirect = (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != '') ? $_SERVER['HTTP_REFERER'] : "";
if (!validateUrl($redirect)):
    $redirect = make_url('account');
endif;
if ($logged_in):
    Redirect($redirect);
endif;

/* * *********** Practitioner Registration **************** */

if (isset($_POST['resigter']) && $_POST['resigter'] == 'new'):

    /* use server side validation */
    $validation = new user_validation();

    $validation->add('firstname', 'req');
    $validation->add('firstname', 'reg_words');

    $validation->add('lastname', 'req');
    $validation->add('lastname', 'reg_words');

    $validation->add('username', 'req');
    $validation->add('username', 'email');
    $validation->add('username', 'reg_words');

    $validation->add('password', 'req');
    $validation->add('password', 'minlength', '6');
    $validation->add('password', 'special_chars');

    $valid = new valid();
    $error = 0;
    if ($valid->validate($_POST, $validation->get())):
	$error = 0;
    else:
	$error = 1; /* set error */
	$error_obj->errorAddArray($valid->error);
    endif;


    if ($error != '1'): /* if there is no error */
	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	$email = $_POST['username'];
	$password = md5($_POST['password']);


	$user_obj = new user();
	$user = $user_obj->checkUserEmailExists($email);
	if (!is_object($user)):
	    $create_obj = new user();
	    $new_user_id = $create_obj->createUser($_POST);

	    if ($new_user_id):

		$code = randomString(10);

		$query11 = new user();
		$query11->updateConfirmationVerifyCode($new_user_id, $code);

		/* Send Email Here */
		$emailArray = array(
		    'firstname' => $_POST['firstname'],
		    'lastname' => $_POST['lastname'],
		    'confirmation_url' => make_url('verify', 'req=' . $code)
		);
		send_email_by_cron($email, 'Please click on the link to confirm signup in market place<a href="' . ('http://qtx.in/marketplace/') . '">http://qtx.in/marketplace</a>', 's');

		$login_session->pass_msg[] = show(MSG_ACCOUNT_CONFIRM_EMAIL_NEW_RESEND_SUCCESS, FALSE);
		Redirect(make_url('login'));

	    /*
	      user::loginUser($new_user_id);
	      $login_session->pass_msg[] = show(MSG_REGISTERSUCCESS, FALSE);
	      $login_session->set_pass_msg();
	      Redirect(make_url('account'));
	     */
	    else:
		$login_session->set_error();
		$login_session->pass_msg[] = show(MSG_REGISTR_FAILED, FALSE);
		$login_session->set_pass_msg();
	    endif;
	else:
	    $login_session->set_error();
	    $login_session->pass_msg[] = show(MSG_REGISTER_EMAIL_ALREADY_EXIST, FALSE);
	    $login_session->set_pass_msg();
	endif;
    endif;
    Redirect(make_url('login'));
endif;
/* * *********** Practitioner Registration end **************** */

/* SEO information */
$content = add_metatags("Register");
?>
