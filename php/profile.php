<?php

include_once(DIR_FS_SITE . 'include/functionClass/countryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/stateClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/cityClass.php');

$section = isset($_GET['section']) ? $_GET['section'] : "dashboard";
$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : "account";
$resend = isset($_GET['resend']) ? $_GET['resend'] : "";
$id = isset($_GET['id']) ? $_GET['id'] : "0";
$p = isset($_GET['p']) ? $_GET['p'] : "1";

if (!$logged_in) {
    Redirect(make_url('login'));
}

isset($_POST['country_id']) ? $country_id = $_POST['country_id'] : $country_id = '0';
isset($_POST['state_id']) ? $state_id = $_POST['state_id'] : $state_id = '0';

$queryUser = new user();
$user_details = $queryUser->getUser($user_id);


if (isset($_POST['submit'])) {
    $query = new user();
    $query->update_user($_POST, $user_id);
    $admin_user->set_pass_msg('Profile updated successfully');
    Redirect(make_url('profile'));
}



$query = new country();
$country = $query->getCountry($user_details->country_id);

$query = new state();
$state = $query->getState($user_details->state_id);

$query = new city();
$city = $query->getCity($user_details->city_id);


$Query_obj = new user();
$values = $Query_obj->getUser($user_id);


$states = array();
$QueryObj1 = new state();
$states = $QueryObj1->getStateNameByCountry($values->country_id);


/* get cities with country id */
$cities = array();
$QueryObj1 = new city();
$cities = $QueryObj1->getCityNameByState($values->state_id);


//$query = new country();
//$countryall = $query->listCountry();
//
//$query = new state();
//$state_all = $query->getStateNameByCountry($user_details->country_id);
//
//$query = new city();
//$city_all = $query->getCityNameByState($user_details->city_id);

if (isset($_POST['change_password'])) {
    extract($_POST);

    if ($user_details->password == md5($oldpassword)) {
        if ($password == $confirm_password) {
            $query = new user;
            $values = $user_details->username;
            $value = 'info@cwebconsultants.com';

            if ($values) {
                $Subject = 'Password Changed  successfully';
                $ToEmail = $values;
                $FromEmail = $value;
                $FromName = 'Marketpalce';
                $Message = 'password';
                $query = new user;
                $query->changePassword($user_id, $password);

                $send_email = send_email_by_cron($ToEmail, $Message, $Subject, $FromEmail);
                pr($send_email);
                if ($send_email) {
                    $admin_user->set_pass_msg('Password Changed Successfully');
                    Redirect(make_url('profile'));
                } else {
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Request not sent');
                    Redirect(make_url('profile'));
                }
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg('Email not found');
                Redirect(make_url('profile'));
            }
            $admin_user->set_pass_msg('Password changed successfully');
            Redirect(make_url('profile'));
        } else {
            $admin_user->set_error();
            $admin_user->set_pass_msg('Confirm password is incorrect');
            Redirect(make_url('profile'));
        }
    } else {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Old password is incorrect');
        Redirect(make_url('profile'));
    }
}
$content = add_metatags("My Account");
?>
