<?php
include_once(DIR_FS_SITE.'include/functionClass/userClass.php');
if ($logged_in):
    Redirect($redirect);
endif;

$code = isset($_GET['req'])?$_GET['req']:'';

/* check code exist or not */
if(!empty($code)):
	$user_obj = new user();
	$user = $user_obj->getUserByEmailToken($code);
	
	if (!is_object($user)):       	
		$login_session->pass_msg[]=show(MSG_SOMETHING_WENT_WRONG,FALSE);
		$login_session->set_error();	
		$login_session->set_pass_msg();
		Redirect(make_url('forgot_password','resend=1'));		
	endif;
	
	/* update verify */
	$user_obj = new user();
	$user_obj->setEmailVerified($user->id);
	
	$login_session->pass_msg[]=show(MSG_EMAIL_CONFIRMATION_SUCCESS,FALSE);
	$login_session->set_pass_msg();
	Redirect(make_url('login'));		
	
else:
	$login_session->pass_msg[]=show(MSG_SOMETHING_WENT_WRONG,FALSE);
	$login_session->set_error();	
    $login_session->set_pass_msg();
    Redirect(make_url('forgot_password','resend=1'));	
endif;


$content = add_metatags("Verify");

?>
