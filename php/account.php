<?php
$section = isset($_GET['section'])?$_GET['section']:"dashboard";
$redirect = isset($_GET['redirect'])?$_GET['redirect']:"account";
$resend = isset($_GET['resend'])?$_GET['resend']:"";
$id = isset($_GET['id'])?$_GET['id']:"0";
$p = isset($_GET['p'])?$_GET['p']:"1";

if (!$logged_in) {
    Redirect(make_url('login'));
}

$queryUser = new user();
$user_details = $queryUser ->getUser($user_id);

$content=add_metatags("My Account");
?>
