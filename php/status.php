<?php

include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');

$section = isset($_GET['section']) ? $_GET['section'] : "dashboard";
$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : "account";
$resend = isset($_GET['resend']) ? $_GET['resend'] : "";
$orderid = isset($_GET['orderid']) ? $_GET['orderid'] : "0";
$us_id = isset($_GET['us_id']) ? $_GET['us_id'] : "0";
$p = isset($_GET['p']) ? $_GET['p'] : "1";

if (!$logged_in) {
    Redirect(make_url('login'));
}

$queryUser = new user();
$user_details = $queryUser->getUser($user_id);

$query = new status();
$orderdetail = $query->getorderdetail($orderid);


$query = new orderstatus();
$orderstatus = $query->listmyservice();

if (isset($_POST['enter'])) {
    extract($_POST);
    $_POST['orderid'] = $orderid;
    $_POST['us_id'] = $us_id;
    $is_order_user_exit = status :: is_order_user_exit($orderid, $us_id);

    $queryUser = new user();
    $user_order = $queryUser->getUser($us_id);
    if (!$is_order_user_exit) {
        $query = new status();
        $query->update_status($_POST);
    } else {
        $query = new status();
        $query->update_field_custom($_POST, $orderid, $us_id);
    }
    if ($status_id == 3) {
        $query = new user;
        $values = $user_order->username;
        $value = 'info@cwebconsultants.com';
        

        if ($values) {
            $Subject = 'Order Delivered succesfully';
            $ToEmail = $values;
            $FromEmail = $value;
            $FromName = 'Marketpalce';
            $Message = ' order confirmation';
            $send_email = send_email_by_cron($ToEmail, $Message, $Subject, $FromEmail, $FromName);
            $send_email = send_email_by_cron($user_details->username, $Message, $Subject, $FromEmail, $FromName);
            $send_email = send_email_by_cron(ADMIN_EMAIL, $Message, $Subject, $FromEmail, $FromName);
            if ($send_email) {
                $admin_user->set_pass_msg('Order Is Delivered successfully');
                Redirect(make_url('status', 'orderid=' . $orderid . '&us_id=' . $us_id));
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg('Request not sent');
                Redirect(make_url('status', 'orderid=' . $orderid . '&us_id=' . $us_id));
            }
        } else {
            $admin_user->set_error();
            $admin_user->set_pass_msg('Email not found');
            Redirect(make_url('status', 'orderid=' . $orderid . '&us_id=' . $us_id));
        }
    }
    Redirect(make_url('status', 'orderid=' . $orderid . '&us_id=' . $us_id));
}

$content = add_metatags("My Account");
?>
