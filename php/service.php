<?php

include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
$p = isset($_GET['p']) ? $_GET['p'] : '1';

if (!$logged_in) {
    Redirect(make_url('login'));
}
if (isset($_POST['submit'])) {

    if (isset($_POST['searchby']) && $_POST['searchby'] != '') {
        if (isset($_POST['searchby'])) {
            $totalservices = 0;
            $services2 = service :: is_location_exists($id, $_POST['searchby']);
            if($services2){
                      Redirect(make_url('service'));
                   $admin_user->set_pass_msg('Result found');
            }else{
                $admin_user->set_error();
                   $admin_user->set_pass_msg('No result found');
            }
        }
    } else {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Please Write Something');
    }
} else {
      $QueryObj = new category();
    $categories = $QueryObj->listCategory();

    $QueryObj = new categoryService();
    $selected_cat = $QueryObj->getServiceCat($id);
    
    $QueryObj = new service();
    $services = $QueryObj->listActiveServiceWithDetails();
    $d = 4;
    $totalservices = count($services);
    $totalservices = ceil($totalservices / $d);

    $start = ($p - 1) * $d;
    $QueryObj = new service();
    $services2 = $QueryObj->listActiveServiceWithDetails2($start, $d);
}
/* SEO information */
$content = add_metatags("Services");
?>