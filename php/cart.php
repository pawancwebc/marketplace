<?php

//Set useful variables for paypal form
$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
$paypal_id = 'info@codexworld.com'; //Business Email

include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
$id = isset($_GET['id']) ? $_GET['id'] : '0';

$queryUser = new user();
$user_details = $queryUser->getUser($user_id);

$QueryObj = new service();
$service = $QueryObj->getServiceWithDetails($id);
$productid = $service->id;

if (isset($_POST['choose'])) {

    if (!$_POST['u_name'] || !$_POST['mobile'] || !$_POST['add']) {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Please fill the fields');
        Redirect(make_url('cart', 'id=' . $id));
    }
}

if (isset($_GET['act']) && $_GET['act'] == 'payed') {
    $new = explode(',', $_GET['cm']);
    $add = $new[3];
    $addres = $add;
    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($addres) . '&sensor=true');
    $coordinates = json_decode($coordinates);
    $lat = $coordinates->results[0]->geometry->location->lat;
    $longi = $coordinates->results[0]->geometry->location->lng;
    'Latitude:' . $coordinates->results[0]->geometry->location->lat;
    'Longitude:' . $coordinates->results[0]->geometry->location->lng;
    $ordervalue = array(
        'txn_id' => $_GET['tx'],
        'service_id' => $_GET['id'],
        'payment_gross' => $_GET['amt'],
        'currency_code' => $_GET['cc'],
        'payment_status' => $_GET['st'],
        'name' => $new['0'],
        'date_time' => $new['1'],
        'u_name' => $new['2'],
        'add' => $new['3'],
        'mobile' => $new['4'],
        'vendor_id' => $new['5'],
        'u_id' => $user_details->id,
        'lat' => $lat,
        'longi' => $longi,
    );
    $query = new trans();
    $query->saveServiceorder($ordervalue);
    $admin_user->set_pass_msg('Thanks For Your order,Your Payment is confirmed Successfully');
    Redirect(make_url('myorder'));
}

/* SEO information */
$content = add_metatags($service->name, $service->name);
?>
