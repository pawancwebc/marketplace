<?php

include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');

$section = isset($_GET['section']) ? $_GET['section'] : "dashboard";
$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : "account";
$resend = isset($_GET['resend']) ? $_GET['resend'] : "";
$id = isset($_GET['id']) ? $_GET['id'] : "0";
$sid = isset($_GET['sid']) ? $_GET['sid'] : "0";
$service_id = isset($_GET['service_id']) ? $_GET['service_id'] : "";
$p = isset($_GET['p']) ? $_GET['p'] : "1";

$queryUser = new user();
$user_details = $queryUser->getUser($user_id);

if (!$logged_in) {
    Redirect(make_url('login'));
}





if (isset($_POST['submit_images'])):
    if (isset($_FILES['image']) && $_FILES['image']['tmp_name']['0'] != ''):
        $files = serializeUploadArray($_FILES, 'image');
        $QueryObj = new simage();
        $QueryObj->uploadServiceImages($id, $files, 'image');
        $admin_user->set_pass_msg('Image uploaded successfully.');
        Redirect(make_url('image','id=' . $id));
    endif;

    $admin_user->set_error();
    $admin_user->set_pass_msg('Error occurred while uploading new image.');
    Redirect(make_url('addimage', 'id=' . $id));


endif;

$content = add_metatags("My Account");
?>

