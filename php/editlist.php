<?php

include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');

$section = isset($_GET['section']) ? $_GET['section'] : "dashboard";
$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : "account";
$resend = isset($_GET['resend']) ? $_GET['resend'] : "";
$id = isset($_GET['id']) ? $_GET['id'] : "0";
$p = isset($_GET['p']) ? $_GET['p'] : "1";

if (!$logged_in) {
    Redirect(make_url('login'));
}

$queryUser = new user();
$user_details = $queryUser->getUser($user_id);

$query = new service();
$list_oneservice = $query->getService($id);

$QueryObj = new category();
$categories = $QueryObj->listCategory();

$QueryObj = new categoryService();
$selected_cat = $QueryObj->getServiceCat($id);

if (isset($_POST['submit'])) {
    if ($_POST['name'] || $_POST['service_area'] || $_POST['price'] || $_POST['description'] || $_POST['position']) {

        $query = new user();
        $query->update_user($_POST, $user_id);
        $admin_user->set_pass_msg('Profile updated successfully');
        Redirect(make_url('profile'));
    }
    $admin_user->set_error();
    $admin_user->set_pass_msg('Error occurred adding service.');
    Redirect(make_url('addservice'));
}



if (isset($_POST['update'])) {
    $QueryObj = new service();
    $new_id = $QueryObj->saveService($_POST);

    /* Delete All Previous Service Rel */
    $QueryObj = new categoryService();
    $QueryObj->deleteServiceCat($new_id);

    /* Save Category Services Rel Services */
    if (!empty($_POST['catgory_id']) && !empty($_POST['catgory_id'])) {
        $QueryObj = new categoryService();
        $QueryObj->saveServiceCat($new_id, $_POST['catgory_id']);
    }
    $admin_user->set_pass_msg('Service updated successfully');
    Redirect(make_url('listservice'));
}




$content = add_metatags("My Account");
?>

