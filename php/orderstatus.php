<?php

include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');

$section = isset($_GET['section']) ? $_GET['section'] : "dashboard";
$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : "account";
$resend = isset($_GET['resend']) ? $_GET['resend'] : "";
$orderid = isset($_GET['orderid']) ? $_GET['orderid'] : "0";
$us_id = isset($_GET['us_id']) ? $_GET['us_id'] : "0";
$p = isset($_GET['p']) ? $_GET['p'] : "1";

if (!$logged_in) {
    Redirect(make_url('login'));
}

$queryUser = new user();
$user_details = $queryUser->getUser($user_id);

if (isset($_GET['act']) && $_GET['act'] == 'status') {
    $query = new status();
    $status = $query->list_status($orderid, $us_id);
}



$content = add_metatags("My Account");
?>
