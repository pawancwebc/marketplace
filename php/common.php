<?php
$id = isset($_GET['id'])?$_GET['id']:0;
/*
 * This file is used for adding the common codes for all the php files
 * like navigation and sliders
 */
 
/*  get classs  */
include_once(DIR_FS_SITE . 'include/functionClass/navigationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/bannerClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/countryClass.php');

/* to get current location */
include_once(DIR_FS_SITE . 'include/location/ip.codehelper.io.php');
include_once(DIR_FS_SITE . 'include/location/php_fast_cache.php');


/*  top navigation  */
$MAIN_NAVIGATION = array();
if (is_numeric(MAIN_NAVIGATION_ID)):
    $query_nav = new navigationItem();
    $MAIN_NAVIGATION = $query_nav->getFullNavigation(MAIN_NAVIGATION_ID, '', TRUE);
endif;


/* Get slides */
$photos = array();
if (SLIDER_ACTIVE):
    $query = new banner();
    $photos=$query->listBanner('1','object');
endif;
/* slider portion ends here */


/* check if user logged in or not */
$login_session = new user_session();
$logged_in = $user_id = 0;
$firstname = '';
if ($login_session->is_logged_in()):
    $logged_in = 1;
    $firstname = getUserFirstName();
    $user_id = $login_session->get_user_id();
endif;

?>
