<?php
if($logged_in):
	$redirect = make_url('account');
    Redirect($redirect);
endif;

if(isset($_POST['login_submit']) && $_POST['login_submit']=='login'):
   
    /* use server side validation */
    $validation = new user_validation();

    $validation->add('email', 'req');
    $validation->add('email', 'email');
    $validation->add('email', 'reg_words');

    $validation->add('password', 'req');
    $validation->add('password', 'special_chars'); 

    $valid = new valid();
    $error = 0;
    if ($valid->validate($_POST, $validation->get())):
        $error = 0;
    else:
        $error = 1; /* set error */
        $error_obj->errorAddArray($valid->error);
    endif;


    if ($error != '1'): /* if there is no error */
        
        $email = $_POST['email'];
        $password = md5($_POST['password']);
        $user_obj = new user();
        $user = $user_obj->checkUserExists($email,$password);
        if($user):
            if($user->is_email_verified=='1'):
                if($user->is_active=='1'):
                    user::loginUser($user->id);
                    Redirect(make_url('account'));
                else:
                    $login_session->pass_msg[]=show(MSG_ACCOUNT_DEACTIVATED,FALSE);
                    $login_session->set_error();
                    $login_session->set_pass_msg();
                endif;
                
            else:
                $login_session->pass_msg[]=show(MSG_EMAIL_NOT_VERIFIED,FALSE);
                $login_session->set_error();
                $login_session->set_pass_msg();
            endif;
             
        else:
		
			$login_session->pass_msg[]=MSG_LOGIN_INVALID_USERNAME_PASSWORD;
            $login_session->set_error();
            $login_session->set_pass_msg();
        endif;      
    endif;

    Redirect(make_url('login'));
endif;

/* SEO information */  
$content=add_metatags("Login");

?>
