<?php

include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');

$section = isset($_GET['section']) ? $_GET['section'] : "dashboard";
$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : "account";
$resend = isset($_GET['resend']) ? $_GET['resend'] : "";
$id = isset($_GET['id']) ? $_GET['id'] : "0";
$p = isset($_GET['p']) ? $_GET['p'] : "1";

if (!$logged_in) {
    Redirect(make_url('login'));
}

$queryUser = new user();
$user_details = $queryUser->getUser($user_id);

$query = new service();
$list_service = $query->listmyservice($user_id);


$query = new categoryService();
$list_cat = $query->getServiceCatNames($user_id);


if (isset($_POST['submit'])) {

    $query = new user();
    $query->update_user($_POST, $user_id);
    $admin_user->set_pass_msg('Profile updated successfully');
    Redirect(make_url('profile'));
}

if (isset($_POST['update'])){
    $QueryObj = new service();
    $new_id = $QueryObj->saveService($_POST);

    /* Delete All Previous Service Rel */
    $QueryObj = new categoryService();
    $QueryObj->deleteServiceCat($new_id);

    /* Save Category Services Rel Services */
    if (!empty($_POST['catgory_id']) && !empty($_POST['catgory_id'])){
        $QueryObj = new categoryService();
        $QueryObj->saveServiceCat($new_id, $_POST['catgory_id']);
    }
}

if (isset($_GET['act']) && $_GET['act'] == 'active') {
    $query = new service();
    $query->update_field($id, 'is_on','0');
    $admin_user->set_pass_msg('Service Stopped Succesfully');
    Redirect(make_url('listservice'));
}
if (isset($_GET['act']) && $_GET['act'] == 'stop') {
    $query = new service();
    $query->update_field($id, 'is_on','1');
    $admin_user->set_pass_msg('Service Started Succesfully');
    Redirect(make_url('listservice'));
}
if (isset($_GET['act']) && $_GET['act'] == 'start') {
    $query = new service();
    $query->update_field($id, 'is_active','1');
    $admin_user->set_pass_msg('Service Approved successfully');
    Redirect(make_url('listservice'));
}
if (isset($_GET['act']) && $_GET['act'] == 'delete') {
    $query = new service();
    $query->deleteService($id);
    $admin_user->set_pass_msg('deleted successfully');
    Redirect(make_url('listservice'));
}

$content = add_metatags("My Account");
?>

