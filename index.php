<?php
/*********** INCLUDE FILES ****************/
include_once("include/config/config.php");
//session_destroy();
/*include Mail class*/
require_once(DIR_FS_SITE_PHP."common.php");
				
/* WEBSITE STATISTICS */
if(!isset($_SESSION['visitor'])):
        $_SESSION['visitor']=1;
        $qu=new query('web_stat');
        $qu->Data['ip_address']=$_SERVER['REMOTE_ADDR'];
        $qu->Insert();
endif;


/************** INCLUDE BASIC FUNTIONS - VITAL FUNCTIONS ************/ 
$include_fucntions=array('http','image_manipulation','calender','url_rewrite','email','paging');
include_functions($include_fucntions);


/************ FOR URL REWRITE ****************/		      		 
load_url();

/************* website statistic recorded. ************************/
$page = isset($_GET['page']) ? $_GET['page']:'home';


// check site in maintance mode
// if admin logged in then open site even in maintance mode
if(MAINTANCE_MODE && !$admin_user->is_logged_in()):
    $page = 'maintance';
endif;

if(file_exists(DIR_FS_SITE_PHP."$page.php")):
	require_once(DIR_FS_SITE_PHP."$page.php");
else:
	require_once(DIR_FS_SITE_PHP."404.php");
endif;

		
if(file_exists(DIR_FS_SITE_HTML."$page.php")):
	require_once(DIR_FS_SITE_HTML."$page.php");
else: 
	require_once(DIR_FS_SITE_HTML."404.php");
endif;

?>
