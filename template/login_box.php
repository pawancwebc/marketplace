<?php  display_message(1); ?>
<div class="col-xs-12 col-sm-6">
<form action="<?php echo make_url('login');?>" method="post" id="form_validation" class='validation'>
    <div id="request_form" class='row'>
	<div class="col-xs-12 col-md-10">
		<h3 class="center">Return User</h3>
		<hr class="divider"/>
		<div class="form-group">
            <label class="text-dark" for="email">Username:<span class="warning">*</span></label>
            <input type="text" name="email" tabindex="1" id="email" class="form-control form-control--contact validate[required,custom[email]]" placeholder="USERNAME/EMAIL"/>
        </div>
        <div class="form-group">
            <label class="text-dark" for="password">Password:<span class="warning">*</span></label>
            <input type="password" name="password" tabindex="2" id="password" class="form-control form-control--contact validate[required]" placeholder="PASSWORD"/>
        </div>
        
		<a href="<?php echo make_url('forgot_password');?>" tabindex="4" class='pull-right'>Forgot Password? click here</a>
        
		<button type="submit" name="login_submit" tabindex="3" id="login_submit" class="btn btn-info custombtn btnblue pull-left" value="login">Sign In</button>
        <div class="clearfix"></div>
    </div>
	</div>
</form>
</div>

<!--<div class="columns-container">
				<div id="columns" class="container">
											
 Breadcrumb 
<div class="breadcrumb clearfix">
    
			<span class="navigation-pipe"><i class="icon-angle-right"></i></span>
						Authentication
			</div>
<hr>
 /Breadcrumb 

										<div id="slider_row" class="row">
																	</div>
					<div class="row">
																		<div id="center_column" class="center_column col-xs-12 col-sm-12">	


	
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			 <form action="<?= make_url('register'); ?>" class="push-down-15 validation" method="POST" id="form_validation">
			     <h3 class="page-subheading">CREATE AN ACCOUNT</h3>
				<hr>
            <div class="row" id="request_form">
                <div class="form-group">
                    <label class="text-dark" for="firstname">First Name:<span class="warning">*</span></label>
                    <input type="text" name="firstname" id="firstname" class="form-control form-control--contact validate[required]" placeholder="FIRST NAME" pattern="[a-zA-Z]+" oninvalid="setCustomValidity('Plz enter only Alphabets ')"/>
                </div>
                <div class="form-group">
                    <label class="text-dark" for="lastname">Last Name:<span class="warning">*</span></label>
                    <input type="text" name="lastname" id="lastname" class="form-control form-control--contact validate[required]" placeholder="LAST NAME" pattern="[a-zA-Z]+" oninvalid="setCustomValidity('Plz enter only Alphabets ')"/>
                </div>
                <div class="form-group">
                    <label class="text-dark" for="username">Email:<span class="warning">*</span></label>
                    <input type="email" name="username"  id="username" class="form-control form-control--contact validate[required,custom[email]]" placeholder="EMAIL"/>
                </div>
                <div class="form-group">
                    <label class="text-dark" for="phone">Phone:</label>
                    <input type="text" name="phone"  id="phone" class="form-control form-control--contact" placeholder="PHONE" maxlength="12" size="10"  />
                </div>
                <div class="form-group">
                    <label class="text-dark" for="password">Password:<span class="warning">*</span></label>
                    <input type="password" name="password"  id="password_1" class="form-control form-control--contact validate[required,minSize[6]]" placeholder="PASSWORD"/>
                </div>

                <div class="extra_for_vendor">

                </div>
                <div class="form-group">
                    <label class="radio-inline">
                        <input type='radio' name="user_type" class="change_user_type" checked value="user">
                        <strong>"I am Buyer"</strong>
                    </label>
                </div>
                <div class="form-group">
                    <label class="radio-inline">
                        <input type='radio' name="user_type" class="change_user_type" value="vendor">
                        <strong>"I am Vendor"</strong>
                    </label>
                </div>
                <div class="clearfix"></div>
                <button type="submit" name="resigter" id="login_submit" class="btn btn-info custombtn btnblue pull-left" value="new">Create An Account</button>
                <input type='hidden' id='is_city' value='1'/>
                <div class="clearfix"></div>
            </div>
        </form>
		</div>
	    
	    
		<div class="col-xs-12 col-sm-6">
			<form action="<?php //echo make_url('login');?>" method="post" id="login_form" class="box">
				<h3 class="page-subheading">Already registered?</h3>
				<hr>
				<div class="form_content clearfix">
					<div class="form-group">
						<label for="email">Email address</label>
						<input type="text" name="email" tabindex="1" id="email" class="form-control form-control--contact validate[required,custom[email]]" placeholder="USERNAME/EMAIL"/>
					</div>
					<div class="form-group">
						<label for="passwd">Password</label>
						<input type="password" name="password" tabindex="2" id="password" class="form-control form-control--contact validate[required]" placeholder="PASSWORD"/>
					</div>
					<p class="lost_password form-group"><a href="<?php //echo make_url('forgot_password');?>" title="Recover your forgotten password" rel="nofollow">Forgot your password?</a></p>
					<p class="submit">
						<button type="submit" name="login_submit" tabindex="3" id="login_submit" class="btn btn-info custombtn btnblue pull-left" value="login">Sign In</button>
							
						</button>
					</p>
				</div>
			</form>
		</div>
	</div>
						</div> #center_column 
									    </div> .row 
				</div> #columns 
			</div>-->