<?php if(!empty($photos)):?>
	<div class="page-slider margin-bottom-35">
      <!-- LayerSlider start -->
      <div id="layerslider" style="width: 100%; height: 400px; margin: 0 auto;">

		<?php foreach($photos as $key_photo=>$slider_val):
				$image_obj = new imageManipulation();	?>
			<!-- slide one start -->
			<div class="ls-slide ls-slide1" data-ls="offsetxin: right; slidedelay: 7000; transition2d: 24,25,27,28;">
			
			  <img src="<?php $image_obj->get_image('banner', 'large', $slider_val->image);?>" class="ls-bg" alt="Slide background">

			  <div class="ls-l ls-title" style="top: 96px; left: 35%; white-space: nowrap;" data-ls="
				fade: true; 
				fadeout: true; 
				durationin: 750; 
				durationout: 750; 
				easingin: easeOutQuint; 
				rotatein: 90; 
				rotateout: -90; 
				scalein: .5; 
				scaleout: .5; 
				showuntil: 4000;
			  "><strong><?php echo $slider_val->banner_title;?></strong></div>

			  <div class="ls-l ls-mini-text" style="top: 338px; left: 35%; white-space: nowrap;" data-ls="
			  fade: true; 
			  fadeout: true; 
			  durationout: 750; 
			  easingin: easeOutQuint; 
			  delayin: 300; 
			  showuntil: 4000; 
			  "><?php echo html_entity_decode($slider_val->text);?>.
			  </div>
			</div>
			<!-- slide one end -->
		<?php endforeach;?>
		
      </div>
      <!-- LayerSlider end -->
    </div>
<?php endif;?>    