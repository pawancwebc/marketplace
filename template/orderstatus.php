<?php display_message(1); ?>
<hr>
<style>
    .one, .two, .three, .four{
        position:absolute;
        margin-top:-10px;
        z-index:1;
        height:40px;
        width:40px;
        border-radius:25px;
        font-family: "Times New Roman", Times, serif;

    }
    .p{
        font-family: "Times New Roman", Times, serif;
    }
    .one{
        left:2%;
    }
    .two{
        left:50%;
    }
    .three{
        left:91%;
    }

    .primary-color{
        background-color:#4989bd;
    }
    .success-color{
        background-color:#5cb85c;
    }
    .danger-color{
        background-color:#d9534f;
    }
    .warning-color{
        background-color:#f0ad4e;
    }
    .info-color{
        background-color:#5bc0de;
    }
    .no-color{
        background-color:inherit;
    }



</style>
<div class="container g">
    <div class="row">
        <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a  href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li> <a class="linked" href="<?php echo make_url('serviceorder'); ?>">Service Orders</a></li>
                            <li><a href="<?php echo make_url('support'); ?>"> Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
            </div><?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul>
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a class="linked" href="<?php echo make_url('myorder'); ?>"> My order</a></li>
                            <li> <a href="<?php echo make_url('support'); ?>">Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
            </div>
        <?php } ?>
        <div class="col-md-7 single-grid-left">
            <h3></h3>
            <p>
            <div class="col-sm-9">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"><a href="#home" data-toggle="tab">Track your order</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                        <hr>
                        <?php if (!empty($status)) { ?>
                            <?php foreach ($status as $new) { ?>
                                <?php
                                if ($new) {
                                    $query = new orderstatus();
                                    $statu = $query->list_info($new['status_id']);
                                    $progressvalue = $statu ? $statu->value : '';
                                }
                                ?> 
                                <div class="row"><br />
                                    <div class="col-md-12">
                                        <?php if ($progressvalue == 'Delivered') { ?>
                                            <div class="progress">
                                                <div class="one success-color"><i  class="fa fa-check fa-2x prg"></i></div><div class="two success-color"><i class="fa fa-check fa-2x prg"></i></div><div class="three success-color"><i class="fa fa-check fa-2x prg"></i></div>
                                                <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                                            </div>
                                        <p class="detail3"><?php echo $progressvalue; ?></p>
                                            <hr />
                                        <?php } ?>
                                        <?php if ($progressvalue == 'In Process') { ?>
                                            <div class="progress">
                                                <div class="one warning-color"><i class="fa fa-check fa-2x prg"></i></div><div class="two warning-color"><i class="fa fa-check fa-2x prg"></i></div><div class="three no-color"></div>
                                                <div class="progress-bar progress-bar-warning" style="width: 60%"></div>
                                            </div>
                                                <p class="detail2"><?php echo $progressvalue; ?></p>
                                         
                                            <hr />
                                        <?php } ?>
                                        <?php if ($progressvalue == 'Confirm') { ?>
                                            <div class="progress">
                                                <div class="one info-color"><i class="fa fa-check fa-2x prg"></i></div><div class="two no-color"></div><div class="three no-color"></div>
                                                <div class="progress-bar progress-bar-info" style="width: 35%"></div>
                                            </div>
                                            <p class="detail"><?php echo $progressvalue; ?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                                <hr/>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="col-sm-12 text-center">
                                <h2>No Status updated yet</h2>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="tab-pane" id="messages">


                    </div>
                    <div class="tab-pane" id="settings">
                        <hr>

                    </div>
                    <div class="tab-pane" id="country">


                    </div><!--/tab-pane-->
                </div><!--/tab-pane-->
            </div>
            </p>
        </div>

        <!--/tab-content-->
    </div><!--/col-9-->
</div><!--/row-->

<!--<div class="form-group">

    <div class="col-xs-6">
        <label for="email"><h4>State</h4></label>
        <input type="text" <?php //if ($user_details->state_id != '') {                                        ?> value="<?php //echo $state->name;                                        ?>" <?php //}                                        ?> name="state_id" class="form-control" id="state_id">
    </div>
</div>
<div class="form-group">

    <div class="col-xs-6">
        <label for="email"><h4>City</h4></label>
        <input type="text" <?php //if ($user_details->city_id != '') {                                        ?> value="<?php //echo $city->name;                                        ?>" <?php //}                                        ?> name="city_id" class="form-control" id="zip_code">
    </div>
</div>
<div class="form-group">

    <div class="col-xs-6">
        <label for="email"><h4>Country</h4></label>
        <input type="text" <?php //if ($user_details->country_id != '') {                                        ?> value="<?php //echo $country->short_name;                                        ?>"<?php //}                                        ?> name="country_id" class="form-control" id="zip_code">
    </div>
</div>-->






<!--<form class="form" method="post" >
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="country_id"><h4>Country</h4></label>
                                <select name="country_id" id="country_id" class="form-control">
                                    <option value="">Select Country</option>
<?php //foreach (getCountries(true) as $kk => $vv):  ?>
                                        <option value="<?php //echo $vv['id'];                                   ?>"><?php //echo $vv['short_name'];                                   ?></option>
<?php //endforeach;  ?>
                                </select>
                                <spa n id="country_error" style="display:none;color:red"><i>Please select the country first.</i></span>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="state_id"><h4>State</h4></label>
                                <select name="state_id" id="state_id" class="form-control">
                                    <option value="">Select State</option>
<?php //foreach ($state_all as $st):  ?>
                                        <option value="<?php ///echo $st['id'];                                   ?>"><?php //echo $st['name'];                                   ?></option>
<?php //endforeach;  ?>
                                </select>
                                <span id="state_error" style="display:none;color:red"><i>Please select the state.</i></span>
                            </div>
                        </div>	
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="city_id"><h4>City</h4></label>
                                <select name="city_id" id="city_id" class="form-control">
                                    <option value="">Select City</option>
<?php //foreach ($city_all as $ct):  ?>
                                        <option value="<?php //echo $ct['id'];                                   ?>"><?php //echo $ct['name'];                                   ?></option>
<?php //endforeach;  ?>
                                </select>
                                <span id="state_error" style="display:none;color:red"><i>Please select the city.</i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
                                <input class="btn blue" type="hidden" name="id" value="<?php //echo $user_id                                    ?>" tabindex="7" /> 
                                <input class="btn btn-lg btn-success" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            </div>
                        </div>
                    </form>-->






