<?php display_message(1); ?>

<hr>
<div class="container g">
    <div class="row">
        <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a  href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a class="linked" href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li> <a href="<?php echo make_url('serviceorder'); ?>">Service Orders</a></li>
                            <li><a href="<?php echo make_url('support'); ?>"> Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                </div><?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a class="linked" href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('myorder'); ?>"> My order</a></li>
                            <li> <a href="<?php echo make_url('support'); ?>">Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                </div>
                <?php }?>
        
         <div class="col-md-7 single-grid-left">
                    <h3></h3>
                    <p>
                        <div class="col-sm-9">
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#home" data-toggle="tab">Profile</a></li>
                <li><a href="#settings" data-toggle="tab">Edit Profile</a></li>
                <li><a href="#messages" data-toggle="tab">Change Password</a></li>
                <li><a href="#country" data-toggle="tab">Edit Country</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <hr/>
                    <b>Name:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $user_details->firstname . ' ' . $user_details->lastname; ?>
                    <hr>
                    <b>Email:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $user_details->username; ?> 
                    <hr>
                    <b>Address:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $user_details->address; ?>
                    <hr>
                    <b>Address:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $user_details->address1; ?>
                    <hr>
                    <b>City:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if ($user_details->city_id != '') { ?><?php echo $city->name; ?><?php } ?>
                    <hr>
                    <b>State:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if ($user_details->city_id != '') { ?> <?php echo $state->name; ?> <?php } ?>
                    <hr>
                    <b>Country:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if ($user_details->city_id != '') { ?> <?php echo $country->short_name; ?><?php } ?>
                    <hr>
                    <b>Post code: </b> &nbsp;&nbsp;&nbsp;&nbsp;<?php echo $user_details->zip_code; ?>
                    <hr>
                    <b>Phone Number:</b> <?php echo $user_details->phone; ?>
                     <?php if ($user_details->user_type == 'vendor') { ?>
                    <hr>
                    <b>Firm Name:</b> &nbsp;&nbsp;<?php echo $user_details->firm_name; ?>
                    <hr>
                     <?php } ?>
                </div><!--/tab-pane-->
                <div class="tab-pane" id="messages">
                    <form class="form" method="post" >
                                            <hr/>

                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="Password"><h4>Old Password</h4></label>
                                <input type="password" name="oldpassword" id="old_password" class="form-control" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="New Password"><h4>New Password</h4></label>
                                <input type="password" name="password" id="new_password" class="form-control" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="Confirm Password"><h4>Confirm Password</h4></label>
                                <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
                                <input class="btn blue" type="hidden" name="id" value="<?php echo $user_id ?>" tabindex="7" /> 
                                <input class="btn btn-lg btn-success" id="change_password_pro"  type="submit" name="change_password" value="Submit" tabindex="7" /> 
                            </div>
                        </div>
                    </form>
                </div><!--/tab-pane-->
                <div class="tab-pane" id="settings">
                    <hr>
                    <form class="form"  method="post" id="registrationForm">
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="first_name"><h4>First name</h4></label>
                                <input type="text" value="<?php echo $user_details ? $user_details->firstname : ''; ?>" class="form-control" name="firstname" id="firstname" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="last_name"><h4>Last name</h4></label>
                                <input type="text" value="<?php echo $user_details ? $user_details->lastname : ''; ?>" class="form-control" name="lastname" id="lastname">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="phone"><h4>Phone</h4></label>
                                <input type="number" value="<?php echo $user_details ? $user_details->phone : ''; ?>" class="form-control" name="phone" id="phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="email"><h4>Address</h4></label>
                                <input type="text" value="<?php echo $user_details ? $user_details->address : ''; ?>" name="address" class="form-control" id="address">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="email"><h4>Address2</h4></label>
                                <input type="text" value="<?php echo $user_details ? $user_details->address1 : ''; ?>" name="address1" class="form-control" id="address">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="email"><h4>Post Code</h4></label>
                                <input type="number" value="<?php echo $user_details ? $user_details->zip_code : ''; ?>" name="zip_code" class="form-control" id="zip_code">
                            </div>
                        </div>
                        <?php if ($user_details->user_type == 'vendor') { ?>
                            <div class="form-group">
                                <div class="col-xs-6">
                                    <label for="email"><h4>Firm Name</h4></label>
                                    <input name="firm_name" value="<?php echo $user_details ? $user_details->firm_name : ''; ?>" type="text" class="form-control" id="firm_name">
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
                                <input class="btn blue" type="hidden" name="id" value="<?php echo $user_id ?>" tabindex="7" /> 
                                <input class="btn btn-lg btn-success" id="change_password_pro" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="country">

                    <form class="form" method="post" >
                                            <hr/>

                        <div class="form-group">
                            <label class="span2 control-label" for="country_id">Country</label>
                            <div class="span4">
                                <select name="country_id" id="country_id" class="form-control">
                                    <option value="">Select Country</option>
                                    <?php foreach (getCountries(true) as $kk => $vv): ?>
                                        <option value="<?php echo $vv['id']; ?>" <?php echo ($vv['id'] == $values->country_id) ? "selected" : ""; ?>><?php echo $vv['short_name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span id="country_error" style="display:none;color:red"><i>Please select the country first.</i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="span2 control-label" for="state_id">State</label>
                            <div class="span4">
                                <select name="state_id" id="state_id" class="form-control">
                                    <option value="">Select State</option>
                                    <?php if (!empty($states)): ?>
                                        <?php foreach ($states as $kk => $vv): ?>
                                            <option value="<?php echo $vv['id']; ?>" <?php echo ($vv['id'] == $values->state_id) ? "selected" : ""; ?>><?php echo $vv['name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                                <span id="state_error" style="display:none;color:red"><i>Please select the state.</i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="span2 control-label" for="city_id">City</label>
                            <div class="span4">
                                <select name="city_id" id="city_id" class="form-control">
                                    <option value="">Select City</option>
                                    <?php if (!empty($cities)): ?>
                                        <?php foreach ($cities as $kk => $vv): ?>
                                            <option value="<?php echo $vv['id']; ?>" <?php echo ($vv['id'] == $values->city_id) ? "selected" : ""; ?>><?php echo $vv['name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                                <span id="state_error" style="display:none;color:red"><i>Please select the city.</i></span>
                            </div>
                        </div>
                        <input type='hidden' id="is_city" value="1"/>
                        <div class="form-group" id="edit-country-profile-page">
                            <div class="col-xs-12">
                                <br>
                                <input class="btn blue" id="edit-cntry-sbt" type="hidden" name="id" value="<?php echo $user_id ?>" tabindex="7" /> 
                                <input class="btn btn-lg btn-success" id="change_password_pro" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            </div>
                        </div>
                    </form>
                </div><!--/tab-pane-->
            </div><!--/tab-pane-->
        </div>
                    </p>
                </div>
        
        <!--/tab-content-->
    </div><!--/col-9-->
</div><!--/row-->

<!--<div class="form-group">

    <div class="col-xs-6">
        <label for="email"><h4>State</h4></label>
        <input type="text" <?php //if ($user_details->state_id != '') {     ?> value="<?php //echo $state->name;     ?>" <?php //}     ?> name="state_id" class="form-control" id="state_id">
    </div>
</div>
<div class="form-group">

    <div class="col-xs-6">
        <label for="email"><h4>City</h4></label>
        <input type="text" <?php //if ($user_details->city_id != '') {     ?> value="<?php //echo $city->name;     ?>" <?php //}     ?> name="city_id" class="form-control" id="zip_code">
    </div>
</div>
<div class="form-group">

    <div class="col-xs-6">
        <label for="email"><h4>Country</h4></label>
        <input type="text" <?php //if ($user_details->country_id != '') {     ?> value="<?php //echo $country->short_name;     ?>"<?php //}     ?> name="country_id" class="form-control" id="zip_code">
    </div>
</div>-->






<!--<form class="form" method="post" >
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="country_id"><h4>Country</h4></label>
                                <select name="country_id" id="country_id" class="form-control">
                                    <option value="">Select Country</option>
                                    <?php //foreach (getCountries(true) as $kk => $vv): ?>
                                        <option value="<?php //echo $vv['id']; ?>"><?php //echo $vv['short_name']; ?></option>
                                    <?php //endforeach; ?>
                                </select>
                                <spa n id="country_error" style="display:none;color:red"><i>Please select the country first.</i></span>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="state_id"><h4>State</h4></label>
                                <select name="state_id" id="state_id" class="form-control">
                                    <option value="">Select State</option>
                                    <?php //foreach ($state_all as $st): ?>
                                        <option value="<?php ///echo $st['id']; ?>"><?php //echo $st['name']; ?></option>
                                    <?php //endforeach; ?>
                                </select>
                                <span id="state_error" style="display:none;color:red"><i>Please select the state.</i></span>
                            </div>
                        </div>	
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="city_id"><h4>City</h4></label>
                                <select name="city_id" id="city_id" class="form-control">
                                    <option value="">Select City</option>
                                    <?php //foreach ($city_all as $ct): ?>
                                        <option value="<?php //echo $ct['id']; ?>"><?php //echo $ct['name']; ?></option>
                                    <?php //endforeach; ?>
                                </select>
                                <span id="state_error" style="display:none;color:red"><i>Please select the city.</i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
                                <input class="btn blue" type="hidden" name="id" value="<?php //echo $user_id ?>" tabindex="7" /> 
                                <input class="btn btn-lg btn-success" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            </div>
                        </div>
                    </form>-->





<!--                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
			  <td><b>name:</b></td>
                        <td><?php echo $user_details->firstname . ' ' . $user_details->lastname; ?></td>
                      </tr>
                      <tr>
                        <td>Email:</td>
                        <td><?php echo $user_details->username; ?></td>
                      </tr>
                      <tr>
                        <td>Address:</td>
                        <td><?php echo $user_details->address; ?></td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td>Address:</td>
                        <td><?php echo $user_details->address1; ?></td>
                      </tr>
                        <tr>
                        <td>City:</td>
                        <td><?php if ($user_details->city_id != '') { ?><?php echo $city->name; ?><?php } ?></td>
                      </tr>
                      <tr>
                        <td>State:</td>
                        <td><?php if ($user_details->city_id != '') { ?> <?php echo $state->name; ?> <?php } ?></td>
                      </tr>
                       <tr>
                        <td>Country:</td>
                        <td><?php if ($user_details->city_id != '') { ?> <?php echo $country->short_name; ?><?php } ?></td>
                      </tr>
		      <tr>
                        <td>Post Code:</td>
                        <td><?php echo $user_details->zip_code; ?></td>
                      </tr>
		       <tr>
                        <td>Phone Number:</td>
                        <td><?php echo $user_details->phone; ?></td>
                      </tr>
		       <tr>
                        <td>Firm Name:</td>
                        <td><?php echo $user_details->firm_name; ?></td>
                      </tr>
                     
                    </tbody>
                  </table>
                  
               
                </div>
             -->
