<?php display_message(1); ?>
<hr>
<div class="container g">
    <div class="row">
        <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a  href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a  href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li><a  class="linked"  href="<?php echo make_url('serviceorder'); ?>"> Service Orders</a></li>
                            <li><a href="<?php echo make_url('support'); ?>">  Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
            </div><?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('myorder'); ?>"> My order</a></li>
                            <li><a href="<?php echo make_url('support'); ?>">Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
            </div>
        <?php } ?>
        <div class="col-md-7 single-grid-left">
            <h3></h3>
            <p>
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#home" data-toggle="tab">Order</a></li>
                <!--                                <li><a href="#status" data-toggle="tab">Status</a></li>
                                                <li><a href="#edit" data-toggle="tab">Edit</a></li>
                                                <li><a href="#image" data-toggle="tab">Image</a></li>-->
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <div class="table-responsive">          
                        <table class="table">
                            <thead>

                                <tr>
                                    <th style="text-align: center;">S.No</th>
                                    <th style="text-align: center;">Service Name</th>
                                    <th style="text-align: center;">Order id</th>
                                    <th style="text-align: center;">Buyer name</th>
                                    <th style="text-align: center;">Service date</th>
                                    <th style="text-align: center;">Payment status</th>
                                    <th style="text-align: center;">Controls</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($new as $key => $n) { ?>
                                    <tr>
                                        <td> <?php echo $key+=1; ?></td>
                                        <td> <?php echo $n['name']; ?></td>
                                        <td> <?php echo $n['id']; ?></td>
                                        <td> <?php echo $n['u_name']; ?></td>
                                        <td> <?php echo $n['date_time']; ?></td>
                                        <td> <?php echo $n['payment_status']; ?></td>
                                        <td style="text-align: center;">
                                            <a href="<?php echo make_url('track', 'id=' . $n['id'] . '&act=active&u_id='.$n['u_id'] ); ?>" title="Click here for more detail"> Detail </a>

                                        </td>




                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div><!--/tab-pane-->
                <div class="tab-pane " id="status">

                </div><!--/tab-pane-->
                <div class="tab-pane" id="edit">
                    <hr>
                </div>
            </div>
            <div class="tab-pane " id="image">
                <br>
                <br>
            </div><!--/tab-pane-->
            </p>
        </div>
    </div><!--/tab-pane-->
</div>
</div><!--/tab-content-->

</div><!--/col-9-->
</div><!--/row-->




