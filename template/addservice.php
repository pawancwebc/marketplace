<?php display_message(1); ?>
<hr>
<div class="container g">
    <div class="row">
        <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a  href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a  href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a class="linked" href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li> <a href="<?php echo make_url('serviceorder'); ?>">Service Orders</a></li>
                            <li> <a href="<?php echo make_url('support'); ?>">Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                </div><?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a class="linked" href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('myorder'); ?>">My order</a></li>
                            <li><a href="<?php echo make_url('support'); ?>"> Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                </div>
                <?php }?>
        
         <div class="col-md-7 single-grid-left">
                    <h3></h3>
                    <p>
        <div class="col-sm-9">
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#home" data-toggle="tab">Add New Service</a></li>
                <!--            <li><a href="#status" data-toggle="tab">Status</a></li>
                                <li><a href="#edit" data-toggle="tab">Edit</a></li>
                                <li><a href="#image" data-toggle="tab">Image</a></li>-->
            </ul>
            <div class="tab-content" id="add-nw-srvc">
                <div class="tab-pane active" enctype="multipart/form-data" id="home">
                    <a href="<?php echo make_url('listservice'); ?>" class="pull-right">Back</a>
                    <form class="form" method="post"  >
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-md-12" for="name">Service Title :<span class="required">*</span></label>
                                <input type="text" name="name"  value="" id="name"  class="form-control m-wrap validate[required]" />
                            </div>
                        </div><br /><br /><br /><br /><br />

                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-md-12" for="catgory_id">Category :<span class="required">*</span></label><br />
                                <select name="catgory_id[]" id='catgory_id' class="form-control m-wrap validate[required]">
                                    <?php
                                    if (!empty($categories)):
                                        foreach ($categories as $key => $value):
                                            ?>
                                            <option value="<?= $value->id ?>"><?= $value->name ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>	
                                </select>
                             
                            </div>
                        </div><br /><br /><br />
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-md-12" for="description">Description :</label>
                                <textarea name="description" rows='2' id="description" value="" class="form-control m-wrapckeditor ckeditor "></textarea>
                            </div>
                        </div></div><br /><br /><br /><br />

                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-md-12" for="service_area">Service Area :<span class="required">*</span></label>
                                <textarea name="service_area" rows='2' id="service_area" value="" class="form-control m-wrapckeditor validate[required]"></textarea>
                                <!--<div class="hint"><i>Enter (Postal Codes) / separated by commas</i></div>-->
                            </div>
                        </div><br /><br /><br /><br />
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-md-12" for="price">Price :<span class="required">*</span></label>
                                <input type="text" name="price"  value="" id="price" class="form-control m-wrap validate[required,custom[number]]" />
                            </div>
                        </div><br /><br /><br />
                        
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-md-12" for="position">Position</label>
                                <input type="number" name="position"  value="" id="position" class="form-control" />
                            </div>
                        </div>

<!--                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-md-12" for="is_active">Approve Status :</label>
                                <div class="radio-list">
                                    <label class="radio-inline">
                                        <input type="radio" name="is_active" id="is_active" value="1" checked/> Publish </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="is_active" id="is_active" value="0"/> Pending Review </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="is_active" id="is_active" value="2"/> Not Approved </label>
                                           <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>-->
                        <div class="clearfix"></div>
                        <input type="hidden" name="vendor_id" id="vendor_id" value="<?php echo $user_id ?>" />
			<br />
                        <div class="form-actions fluid">
                            <div class="offset2">
				
				    <input class="btn btn-lg btn-success" id="change_password_pro" type="submit" name="submit" value="Submit" tabindex="7" />
<!--                                <input type="hidden" name="id" id="service_id" value="<?php //echo $list_oneservice->id        ?>" />
                                <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button> -->
                            </div>
                        </div>
                    </form>
                </div><!--/tab-pane-->
                <div class="tab-pane " id="status">
                </div><!--/tab-pane-->


                <div class="tab-pane" id="edit">
                    <hr>

                </div>



            </div>
            <div class="tab-pane " id="image">

                <br>
                <br>


            </div><!--/tab-pane-->
        </div><!--/tab-pane-->
        </p></div>
    </div>
</div><!--/tab-content-->

</div><!--/col-9-->
</div><!--/row-->






