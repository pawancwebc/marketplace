<?php
include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');

$QueryObj = new service();
$services_list = $QueryObj->listService_homepag();
?>


<center><button class="button_srvc">OUR SERVICES</button></center><br /><br />
<div class="static-top ">
    <?php
    if (!empty($categories)) {
        foreach ($categories as $key => $value) {
            ?>
            <div class="home-banner-2 banner-top col-md-4 col-sm-12 col-xs-12">
                <div class="banner-content"><center><a title="banner1" href="#">
                            <img src="<?php echo ($value->image !== '') ? DIR_WS_SITE_UPLOAD_PHOTO . 'category/small/' . $value->image : DIR_WS_SITE_UPLOAD_PHOTO . 'category/small/service.png'; ?>" height="270" width="270" id="services_img">
                        </a></center>
                    <div class="bg_tran"></div>
                </div>
                <div class="service_heading">
                    <h4><?= $value->name ?></h4>
                    <p><?= $value->description ?></p>
                    <a href="<?php echo make_url('category', 'id=' . $value->id . '&act=show'); ?><?php (in_array($value->id, $selected_cat)) ? 'selected' : '' ?>"><button id="read_more">Read More...</button></a>
                </div>
                <div></div>
            </div>
        <?php } ?>
    <?php } ?>
</div>
