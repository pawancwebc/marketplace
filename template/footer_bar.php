<?php
include_once(DIR_FS_SITE . 'include/functionClass/contentClass.php');
$query_obj = new content();
$about_page = $query_obj->getPage(2);
?>

<div id="hr-upon-footer">

</div>
<div class="footer-container">
    <div id="footer">

        <div class="footer-center">
            <div class="container">
                <div class="row">
                    <div class="footer-static row-fluid">


                        <div class=" col-xs-12  col-sm-6 col-md-4">
                            <h4 class="title_font">
                                <?= html_entity_decode($about_page->page_name) ?>
                            </h4>
                            <br />
                            <center><a href="#"> <img class="footer-logo" src="upload/photo/banner/small/about.PNG" style="height:100px;" a=""></a></center>
                            <p><?= limit_text(html_entity_decode(strip_tags($about_page->page)), 400) ?>...</p>
                            <a href="<?= make_url('content', 'id=' . $about_page->id) ?>"><button style="color:#fff;" id="read_more">Read More</button></a>
                            <div class="toggle-footer">
                                <div>

                                </div>
                            </div></div>


                        <!-- MODULE One Cate Products -->
                        <section id="onecate_products_block" class="  col-sm-4 col-xs-12 col-md-4">

                            <h4 class="title_font">
                                <img src="upload/photo/banner/small/qu.PNG">
                                Quick View
                                <?php
                                include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');

                                $query = new service_detail;
                                $recent = $query->list_info($user_id);
                                ?>
                            </h4>
                            <div id="onecate_products" class="block_content toggle-footer carousel-grid owl-carousel owl-theme" style="display: block;">
                                <div class="owl-wrapper-outer">
                                    <div class="owl-item" style="width: 270px;">
                                        <?php if (!empty($recent)) { ?>
                                        <?php
                                        foreach ($recent as $rec) {
                                            $query = new service;
                                            $view = $query->getServicedetail($rec['service_id']);
                                            ?>
                                            <div class="item">
                                                <div class="item-content clearfix">
                                                    <div class="left-content">
                                                        <a href="<?= make_url('service_detail', 'id=' . $view->id) ?>">
                                                            <img src="<?php echo cwebc::getImage($view->image, 'service', 'big'); ?>" alt="" height="70" width="70" id="quick_img">
                                                        </a>
                                                    </div>
                                                    <div class="right-content pull-right">
                                                        <a href="<?= make_url('service_detail', 'id=' . $view->id) ?>">  <h4><?php echo $view->name; ?></h4>
                                                            <p><?= limit_text(html_entity_decode(strip_tags($view->description)), 30) ?></p></a>
                                                    </div>
                                                </div>
                                                <br />

                                            </div>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        No Recent Search Found.. 
                                        <?php }?>
                                    </div>
                                </div>
                            </div>

                    </div>
                    </section>
                    <!-- /MODULE One Cate Products -->

                    <div class=" custom-columns col-xs-12 col-sm-6 col-md-4" id="our_add_footer">
                        <h4 class="title_font"><img src="upload/photo/banner/small/ad.PNG">Our Address</h4>
                        <br />
                        <div class="columns-content toggle-footer" style="">
                            <p><i class="fa fa-phone" aria-hidden="true"></i> Call Us Now:  1234567890</p>
                            <p><i class="fa fa-map-marker"></i> Cweb-Co Phase-8 Ins Area Mohali</p>
                            <p><i class="fa fa-envelope" aria-hidden="true"></i> Cwebconsultants.com</p>
                        </div>
                    </div>

                    <!-- Block Newsletter module-->
                    <!--			<div id="newsletter_block_left" class=" custom-columns col-xs-12 col-sm-6 col-md-3">
                                                <center>
                                                <h4 class="title_font">NEWS LETTER</h4>
                                                </center
                                                  <br /> <br />
                                                <div class="block_content">
                                                    <form  method="post">
                                                        <div class="form-group">
                                                            <input class="inputNew form-control grey newsletter-input" id="newsletter-input" type="text" name="email" size="18" value="Your e-mail">
                                                            <div class="button-subscribe">
                                                                <button type="submit" name="submitNewsletter" class="btn btn-default button button-small" id="newsletter-submit-btn">
                                                                    <span>subscribe</span>
                                                                </button>
                                                            </div>
                                                            <input type="hidden" name="action" value="0">
                                                        </div>
                                                    </form>
                                                </div>
                    
                                            </div>-->
                    <!-- /Block Newsletter module-->

                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

        </div>
    </div>
</div>
</div>