<?php
include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
$p = isset($_GET['p']) ? $_GET['p'] : '1';

    $QueryObj = new category();
    $categories = $QueryObj->listCategory();

    $QueryObj = new categoryService();
    $selected_cat = $QueryObj->getServiceCat($id);
?>

<div class="pre-header">
    <div class="container">
        <div class="row">
            <!-- BEGIN TOP BAR LEFT PART -->
            <div class="col-md-4 col-sm-4 additional-shop-info">
                <div id="pre_header_logo">
                    <a href="<?php echo make_url('home') ?>"><img src="upload/photo/banner/small/marketing logo.PNG"></a>

                </div>
            </div>
            <!-- END TOP BAR LEFT PART -->
            <!-- BEGIN TOP BAR MENU -->
         
            <div class="col-md-8 col-sm-8 additional-nav">
                <ul class="list-unstyled list-inline pull-right">
                    <li >
                        <form method="POST" action="<?php echo make_url('search'); ?>" role="search">
                            <select name="catgory_id" id='catgory_id' class="select_box_pre " >
                                <?php
                                if (!empty($categories)) {
                                    foreach ($categories as $key => $value) {
                                        ?>
                                        <option value="<?= $value->id ?>" <?= (in_array($value->id, $selected_cat)) ? 'selected' : '' ?>><?= $value->name ?></option>
                                    <?php } ?>
                                <?php } ?>	
                            </select>
                            <input class="search_service_pre"  type="text" value=""  name="searchby" placeholder="Enter Service Area" required/>
                            <input class="newbtn" type="submit" name="submit" value="Search"  />
                            <!--<img src="upload/photo/banner/small/marketing search.PNG" id="search-image-pre">--> 
        <!--		     <input type="image" class="f" name="submit" value="Search" src="upload/photo/banner/small/marketing search.PNG" alt="Submit">-->
                        </form>
                    </li>
                </ul>
            </div>
            <!-- END TOP BAR MENU -->
        </div>
    </div>        
</div>