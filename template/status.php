<?php display_message(1); ?>
<hr>
<div class="container g">
    <div class="row">
        <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a  href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li> <a class="linked" href="<?php echo make_url('serviceorder'); ?>">Service Orders</a></li>
                            <li><a href="<?php echo make_url('support'); ?>"> Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
            </div><?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a class="linked" href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('myorder'); ?>"> My order</a></li>
                            <li> <a href="<?php echo make_url('support'); ?>">Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
            </div>
        <?php } ?>

        <div class="col-md-7 single-grid-left">
            <h3></h3>
            <p>
            <div class="col-sm-9">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"><a href="#home" data-toggle="tab">List of status</a></li>
                    <li><a href="#settings" data-toggle="tab">Add / Edit New Status </a></li>
                    <!--                <li><a href="#messages" data-toggle="tab">Change Password</a></li>
                                    <li><a href="#country" data-toggle="tab">Edit Country</a></li>-->
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                        <hr/>
                        <b>Order Status:</b> 
                        <?php
                        if ($orderdetail) {
                            $query = new orderstatus();
                            $status = $query->list_info($orderdetail->status_id);
                            echo $status ? $status->value : '';
                        }
                        ?> 
                        <hr>
                        <b>Details:</b> <?php echo $orderdetail ? $orderdetail->description : ''; ?> 
                        <hr>
                        <b>Date:</b> <?php echo $orderdetail ? $orderdetail->date : ''; ?>
                        <hr>
                    </div>
                    <div class="tab-pane" id="messages">
                    </div>
                    <div class="tab-pane" id="settings">
                        <hr>
                        <form class="form"  method="POST" >
                            <div class="form-group">
                                <div class="col-xs-6">
                                    <label for="first_name"><h4>Add Status </h4></label>
                                    <select class="form-control" name="status_id">

                                        <?php foreach ($orderstatus as $status) { ?>
                                            <option value="<?php echo $status['id'] ?>" <?php echo $orderdetail ? ($status['id'] == $orderdetail->status_id) ? 'selected' : '' : '' ?>><?php echo $status['value']; ?></option>

                                        <?php } ?>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-6">
                                    <label for="last_name"><h4>Description </h4></label>
                                    <input type="text" value="<?php echo $orderdetail ? $orderdetail->description : ''; ?>" class="form-control" name="description" id="lastname">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-6">
                                    <label for="phone"><h4>Date </h4></label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' class="form-control" name="date"  value="<?php echo $orderdetail ? $orderdetail->date : ''; ?>" required />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br>
                                    <input style="background: #29BEA1;" class="btn btn-lg btn-success" type="submit" name="enter" value="Submit" tabindex="7" /> 
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="country">


                    </div><!--/tab-pane-->
                </div><!--/tab-pane-->
            </div>
            </p>
        </div>

        <!--/tab-content-->
    </div><!--/col-9-->
</div><!--/row-->

<!--<div class="form-group">

    <div class="col-xs-6">
        <label for="email"><h4>State</h4></label>
        <input type="text" <?php //if ($user_details->state_id != '') {                       ?> value="<?php //echo $state->name;                       ?>" <?php //}                       ?> name="state_id" class="form-control" id="state_id">
    </div>
</div>
<div class="form-group">

    <div class="col-xs-6">
        <label for="email"><h4>City</h4></label>
        <input type="text" <?php //if ($user_details->city_id != '') {                       ?> value="<?php //echo $city->name;                       ?>" <?php //}                       ?> name="city_id" class="form-control" id="zip_code">
    </div>
</div>
<div class="form-group">

    <div class="col-xs-6">
        <label for="email"><h4>Country</h4></label>
        <input type="text" <?php //if ($user_details->country_id != '') {                       ?> value="<?php //echo $country->short_name;                       ?>"<?php //}                       ?> name="country_id" class="form-control" id="zip_code">
    </div>
</div>-->






<!--<form class="form" method="post" >
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="country_id"><h4>Country</h4></label>
                                <select name="country_id" id="country_id" class="form-control">
                                    <option value="">Select Country</option>
<?php //foreach (getCountries(true) as $kk => $vv):  ?>
                                        <option value="<?php //echo $vv['id'];                  ?>"><?php //echo $vv['short_name'];                  ?></option>
<?php //endforeach;  ?>
                                </select>
                                <spa n id="country_error" style="display:none;color:red"><i>Please select the country first.</i></span>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="state_id"><h4>State</h4></label>
                                <select name="state_id" id="state_id" class="form-control">
                                    <option value="">Select State</option>
<?php //foreach ($state_all as $st):  ?>
                                        <option value="<?php ///echo $st['id'];                  ?>"><?php //echo $st['name'];                  ?></option>
<?php //endforeach;  ?>
                                </select>
                                <span id="state_error" style="display:none;color:red"><i>Please select the state.</i></span>
                            </div>
                        </div>	
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="city_id"><h4>City</h4></label>
                                <select name="city_id" id="city_id" class="form-control">
                                    <option value="">Select City</option>
<?php //foreach ($city_all as $ct):  ?>
                                        <option value="<?php //echo $ct['id'];                  ?>"><?php //echo $ct['name'];                  ?></option>
<?php //endforeach;  ?>
                                </select>
                                <span id="state_error" style="display:none;color:red"><i>Please select the city.</i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
                                <input class="btn blue" type="hidden" name="id" value="<?php //echo $user_id                   ?>" tabindex="7" /> 
                                <input class="btn btn-lg btn-success" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            </div>
                        </div>
                    </form>-->






