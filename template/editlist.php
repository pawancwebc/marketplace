<?php display_message(1); ?>
<hr>
<div class="container">
    <div class="row">
        <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a  href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a  href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a class="linked" href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li> <a href="<?php echo make_url('support'); ?>"> Service Orders</a></li>
                            <li> Support</li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                </div><?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('myorder'); ?>">My order</a></li>
                            <li><a href="<?php echo make_url('support'); ?>">  Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                </div>
                <?php }?>
       <div class="col-md-7 single-grid-left">
                    <h3></h3>
                    <p>

        <?php if ($list_oneservice) { ?>
            <div class="col-sm-9" id="edit-form-srvc">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"><a href="#home" data-toggle="tab">Service</a></li>
                    <!--                <li><a href="#status" data-toggle="tab">Status</a></li>
                                    <li><a href="#edit" data-toggle="tab">Edit</a></li>
                                    <li><a href="#image" data-toggle="tab">Image</a></li>-->
                </ul>
                <a href="<?php echo make_url('listservice'); ?>" class="pull-right">Back</a>
		<br />
                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                        <div class="row-fluid">
                            <form class="form" method="POST" id="edit-service-page" >

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-12" for="name">Service Title :<span class="required">*</span></label>
                                        <input type="text" name="name"  value="<?php echo $list_oneservice->name ?>" id="name" class="form-control m-wrap validate[required]" />
                                    </div>
                                </div><br /><br />

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-12" for="catgory_id">Category :<span class="required">*</span></label>
                                        <select name="catgory_id[]" id='catgory_id' class="form-control m-wrap validate[required]" multiple >
                                            <?php
                                            if (!empty($categories)) {
                                                foreach ($categories as $key => $value) {
                                                    ?>
                                                    <option value="<?= $value->id ?>" <?= (in_array($value->id, $selected_cat)) ? 'selected' : '' ?>><?= $value->name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>	
                                        </select>
                                    </div>
                                </div>
                                <!--                            <div class="form-group">
                                                                <div class="col-md-6">
                                                                <label class="col-md-6" for="vendor_id">Vendor :<span class="required">*</span></label>
                                                                    <select class="form-control validate[required]" id="vendor_id" name="vendor_id">
                                                                        <option value="">Select Vendor</option>
                                <?php
                                /* if (!empty($allUser)):
                                  foreach ($allUser as $key => $value):
                                  ?>
                                  <option value="<?= $value->id ?>" <?= ($values->vendor_id == $value->id) ? 'selected' : '' ?>><?= $value->firstname . ' ' . $value->lastname ?></option>
                                  <?php
                                  endforeach;
                                  endif; */
                                ?>
                                                                    </select>
                                                                </div>
                                                            </div>-->

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-12" for="description">Description :</label>
                                        <textarea name="description" rows='5' id="description" value="" class="form-control m-wrapckeditor ckeditor "><?= $list_oneservice->description ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-12" for="service_area">Service Area :<span class="required">*</span></label>
                                        <textarea name="service_area" rows='2' id="service_area" value="" class="form-control m-wrapckeditor validate[required]"><?= $list_oneservice->service_area ?></textarea>
                                        <div class="hint"><i>Enter (Postal Codes) / separated by commas</i></div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-12" for="price">Price :<span class="required">*</span></label>
                                        <input type="text" name="price"  value="<?= $list_oneservice->price ?>" id="price" class="form-control m-wrap validate[required,custom[number]]" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-12" for="position">Position</label>
                                        <input type="number" name="position"  value="<?= $list_oneservice->position ?>" id="position" class="form-control" />
                                    </div>
                                </div>
                                        <input type="hidden" name="is_on"  value="<?= $list_oneservice->is_on ?>" id="position" class="form-control" />

                                <?php if ($list_oneservice->is_active == 1) { ?>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label class="col-md-12" for="is_active">Approve Status :</label>
                                            <div class="radio-list">
                                                <label class="radio-inline">
                                                    <input type="radio" name="is_active" id="is_active" value="1" <?= ($list_oneservice->is_active == 1) ? 'checked' : '' ?>/> Publish </label>
                                            </div>
                                        </div>
                                    </div>
					<br />
                                <?php } else { ?>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label class="col-md-12" for="is_active">Approve Status :</label>
                                            <div class="radio-list">
                                                <label class="radio-inline" id="radio_approve">
                                                    <input type="radio" name="is_active" id="is_active" value="0" <?= ($list_oneservice->is_active == 0) ? 'checked' : '' ?>/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Not Approved </label>
                                            </div>
					    <br />
                                        </div>
                                    </div>

                                <?php } ?>
                                <!--                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <label class="col-md-12" for="is_active">Approve Status :</label>
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline">
                                                                                <input type="radio" name="is_active" id="is_active" value="1" <?= ($list_oneservice->is_active == 1) ? 'checked' : '' ?>/> Publish </label>
                                                                            <label class="radio-inline">
                                                                                <input type="radio" name="is_active" id="is_active" value="0" <?= ($list_oneservice->is_active == 0) ? 'checked' : '' ?>/> Pending Review </label>
                                                                            <label class="radio-inline">
                                                                                <input type="radio" name="is_active" id="is_active" value="2" <?= ($list_oneservice->is_active == 2) ? 'checked' : '' ?>/> Not approved </label>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                <div class="form-actions fluid">
                                    <div class="offset2">
                                        <input class="btn blue" type="hidden" name="id" value="<?php echo $list_oneservice->id ?>" tabindex="7" /> 
                                        <input class="btn btn-lg btn-success" id="edit-service-sbt-btn" type="submit" name="update" value="Submit" tabindex="7" /> 
        <!--                                <input type="hidden" name="id" id="service_id" value="<?php //echo $list_oneservice->id    ?>" />
                                        <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button> -->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="tab-pane " id="status">
                </div><!--/tab-pane-->
                <div class="tab-pane" id="edit">
                    <hr>
                </div>
            </div>
                    </p></div>
        <?php } else { ?>
            No Record Found
        <?php } ?>
        <div class="tab-pane " id="image">
            <br>
            <br>
        </div><!--/tab-pane-->
    </div><!--/tab-pane-->
</div>
</div><!--/tab-content-->

</div><!--/col-9-->
</div><!--/row-->






