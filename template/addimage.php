<?php display_message(1); ?>
<hr>
<div class="container">
    <div class="row">
        <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a  href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a  href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a class="linked" href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li><a href="<?php echo make_url('serviceorder'); ?>"> Service Orders</a></li>
                            <li><a href="<?php echo make_url('support'); ?>"> Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                </div><?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('myorder'); ?>">My order</a></li>
                            <li><a href="<?php echo make_url('support'); ?>"> Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                </div>
                <?php }?>
       <div class="col-md-7 single-grid-left">
                    <h3></h3>
                    <p>

        <div class="col-sm-9">
            <!--            <ul class="nav nav-tabs" id="myTab">
                            <li class="active"><a href="#home" data-toggle="tab">Service</a></li>
                            <li><a href="#status" data-toggle="tab">Status</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#image" data-toggle="tab">Image</a></li>
                        </ul>-->
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="validation">
                        <div class="portlet-body form form-body">      
                            <div class="form-group">
                                <label class="span2 control-label">Upload Images</label>
                                <div class="span6">
                                    <input type="file" name="image[]" multiple class="form-control m-wrap" />
                                    <div class="clearfix"></div>
                                    <span class="help-block">add multiple files by using 'ctrl' key</span>
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <label class="span2 control-label">Upload from URL</label>
                                <div class="span6">
                                    <input type="text" name="image_url" placeholder="Image URL.." class="form-control" title="Image URL"/>
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label class="span2 control-label">&nbsp;</label>
                                <div class="span4">
                                    <input type="hidden" name="id" value="<?php echo $user_id; ?>"/>
                                    <input class="btn blue" id="change_password_pro" type="submit" name="submit_images" value="Submit"/>
                                </div>
                            </div>
                        </div>
                    </form>

                </div><!--/tab-pane-->
                <div class="tab-pane" id="edit">
                    <hr>
                </div>
            </div>
            <div class="tab-pane " id="image">
            </div><!--/tab-pane-->
        </div><!--/tab-pane-->
        </p></div>
    </div>
</div><!--/tab-content-->







