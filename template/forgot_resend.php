<div class="row"><br/>
    <div class="col-md-4 col-md-offset-4">
        <?php display_message(1); ?>

        <div class="panel panel-default">
            <div class="panel-body">
                    <div class="text-center">
                        <h3><i class="fa fa-lock fa-4x"></i></h3>
                        <h2 class="text-center">Forgot Password?</h2>
                        <p>You can reset your password here.</p>
                        <div class="panel-body">
                            <form action="<?= make_url('forgot_password'); ?>" class="push-down-15 validation" method="POST" id="form_validation">
                                <fieldset>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>

                                            <input id="emailInput" placeholder="email address" class="form-control validate[required,custom[email]]" type="email" name="email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="btn btn-lg btn-primary btn-block" name="submit" type="submit" value='Submit'><br/>
                                    </div>
                                </fieldset>
                            </form>

                        </div>
                    </div>
                				

            </div>
        </div>
    </div>
</div>