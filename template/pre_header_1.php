<?php
include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
$p = isset($_GET['p']) ? $_GET['p'] : '1';

if (isset($_POST['submit'])) {
    if (isset($_POST['searchby']) && $_POST['catgory_id'] != '') {
        if (isset($_POST['searchby'])) {
            $totalservices = 0;
            $services2 = service :: is_location_exist($id, $_POST['searchby'], $_POST['catgory_id']);
            if ($services2) {
                
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg('No result found');
            }
        }
    } else {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Please Write Something');
    }
} else {
    $QueryObj = new category();
    $categories = $QueryObj->listCategory();

    $QueryObj = new categoryService();
    $selected_cat = $QueryObj->getServiceCat($id);

    $QueryObj = new service();
    $services = $QueryObj->listActiveServiceWithDetails();

    $d = 4;
    $totalservices = count($services);
    $totalservices = ceil($totalservices / $d);

    $start = ($p - 1) * $d;
    $QueryObj = new service();
    $services2 = $QueryObj->listActiveServiceWithDetails2($start, $d);
}

/* SEO information */
$content = add_metatags("Services");
?>







<div class="header-container">
    <header id="header">

        <div class="header_bottom">
            <div class="container">
                <div class="row">
                    <div id="header_logo" class="col-xs-12 col-sm-12 col-md-3">
                        <a href="#" title="cClose">
                            <img class="logo img-responsive" src="upload/photo/banner/small/images.jpg" >
                        </a>
                    </div>
                    <div class="welcome-text" style="
                         ">
                        <p><em class="icon-phone"></em> Call +91-745 254 2659</p>
                    </div>

                    <div id="social_block_header">
                        <ul>

                            <li><a  target='_blank' href="<?= FACEBOOK_PAGE ?>"><span class="fa fa-facebook-square  fa-2x"></span></a></li>


                            <li><a  target='_blank' href="<?= TWITTER_PAGE ?>"><i class="fa fa-twitter-square fa-2x"></i></a></li>

                            <li ><a target="_blank" href="<?= RSS_PAGE ?>" <i class="fa fa-rss-square fa-2x"></i></a></li>

                            <li ><a target="_blank" href="<?= GOOGLE_PLUS ?>" <i class="fa fa-google-plus-square fa-2x"></i></a></li>
                        </ul>
                    </div>
                <!--							    <input class="search_query form-control ac_input" type="text" id="search_query_top" name="search_query" placeholder="Search" value="" autocomplete="off">-->

                    <div id="search_box">
                        <?php if ($page == 'home') { ?>

                            <?php include_once(DIR_FS_SITE . 'html/search.php'); ?>

                        <?php } ?>
                    </div>
                    <div class="navigation">
                        <nav class="navbar navbar-default" style="border:none;">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                                <nav class="cl-effect-13" id="cl-effect-13">
                                    <ul class="nav navbar-nav">
                                        <?php if (!empty($MAIN_NAVIGATION)) { ?>
                                            <?php foreach ($MAIN_NAVIGATION as $key => $nav) { ?>
                                                <?php //if ($nav->navigation_title != 'Services' || $logged_in) { ?>
                                                <li class=""><a <?= ($nav->open_in_new == '1') ? 'target="_blank"' : '' ?> href="<?php
                                                    if ($nav->is_external == '0'): echo make_url($nav->navigation_link, $nav->navigation_query);
                                                    else: echo $nav->navigation_link;
                                                    endif;
                                                    ?>"><?= $nav->navigation_title ?></a>
                                                </li>
                                            <?php } ?>			
                                        <?php } ?>
                                        <?php if ($logged_in) { ?>
                                            <li><a href="<?= make_url('account') ?>">Myaccount</a></li>
                                            <li><a href="<?= make_url('logout') ?>">Log Out</a></li>
                                        <?php } else { ?>
                                            <li><a href="<?= make_url('login') ?>">Log In</a></li>
                                            <li><a href="<?= make_url('login') ?>">Registration</a></li>
                                        <?php } ?>
                                    </ul>
                                </nav> 
                            </div>
                        </nav>
                    </div>                                               
                    </header>
                </div>
