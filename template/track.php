<?php display_message(1); ?>
<hr>
<div class="container g">
    <div class="row">
        <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li><a class="linked" href="<?php echo make_url('serviceorder'); ?>">Service Orders</a></li>
                            <li><a href="<?php echo make_url('support'); ?>">  Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
            </div><?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul>
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a class="linked" href="<?php echo make_url('myorder'); ?>">My order</a></li>
                            <li><a href="<?php echo make_url('support'); ?>">Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="col-md-7 single-grid-left">
            <h3></h3>
            <p>
                <a style="    background: #29BEA1;" class="btn btn-lg btn-success pull-right" href="<?php echo make_url('status', 'orderid=' . $id . '&act=status&us_id='.$u_id ); ?>">Status Update</a>
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#home" data-toggle="tab">Detail</a></li>
                <!--                                <li><a href="#status" data-toggle="tab">Status</a></li>
                                                <li><a href="#edit" data-toggle="tab">Edit</a></li>
                                                <li><a href="#image" data-toggle="tab">Image</a></li>-->
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <h1>Vendor Information :</h1>
                    Name: <?php echo $user_details->firstname . ' ' . $user_details->lastname; ?><br>
                    Email: <?php echo $user_details->username; ?><br>
                    Phone No.: <?php echo $user_details->phone; ?>

                    <?php foreach ($vendorinfo as $key => $vendor) { ?>
                        <h1>Buyer Information :</h1>
                        Name: <?php echo $vendor->firstname . ' ' . $vendor->lastname; ?><br>
                        Email: <?php echo $vendor->username; ?><br>
                        Phone No.: <?php echo $vendor->phone; ?>

                        <h1>Service Order Details :</h1>
                        Name of service: <?php echo $vendor->name; ?><br>
                        Price: <?php echo $vendor->payment_gross; ?><br>
                        Name of Person: <?php echo $vendor->u_name; ?><br>
                        Phone: <?php echo $vendor->mobile; ?><br>
                        Address: <?php echo $vendor->add; ?><br>
                        Payment: <?php echo $vendor->payment_status; ?><br>
                        Date on which order has to be placed: <?php echo $vendor->date_time; ?><br>
                    <?php } ?>

                </div><!--/tab-pane-->
                <div class="tab-pane " id="status">

                </div><!--/tab-pane-->
                <div class="tab-pane" id="edit">
                    <hr>
                </div>
            </div>
            <div class="tab-pane " id="image">
                <br>
                <br>
            </div><!--/tab-pane-->
            </p>
        </div>
    </div><!--/tab-pane-->
</div>
</div><!--/tab-content-->

</div><!--/col-9-->
</div><!--/row-->




