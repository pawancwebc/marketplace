<div class="col-xs-12  col-sm-6 g">
    <div id="request_form" class="col-md-10">
        <h3 class="center">New on <?= SITE_NAME ?>?</h3>
        <hr class="divider"/>	  
        <form action="<?= make_url('register'); ?>" class="push-down-15 validation" method="POST" id="form_validation">
            <div class="row" id="request_form">
                <div class="form-group">
                    <label class="text-dark" for="firstname">First Name:<span class="warning">*</span></label>
                    <input type="text" name="firstname" id="firstname" class="form-control form-control--contact validate[required]" placeholder="FIRST NAME" pattern="[a-zA-Z]+" oninvalid="setCustomValidity('Plz enter only Alphabets ')"/>
                </div>
                <div class="form-group">
                    <label class="text-dark" for="lastname">Last Name:<span class="warning">*</span></label>
                    <input type="text" name="lastname" id="lastname" class="form-control form-control--contact validate[required]" placeholder="LAST NAME" pattern="[a-zA-Z]+" oninvalid="setCustomValidity('Plz enter only Alphabets ')"/>
                </div>
                <div class="form-group">
                    <label class="text-dark" for="username">Email:<span class="warning">*</span></label>
                    <input type="email" name="username"  id="username" class="form-control form-control--contact validate[required,custom[email]]" placeholder="EMAIL"/>
                </div>
                <div class="form-group">
                    <label class="text-dark" for="phone">Phone:</label>
                    <input type="text" name="phone"  id="phone" class="form-control form-control--contact" placeholder="PHONE" maxlength="12" size="10"  />
                </div>
                <div class="form-group">
                    <label class="text-dark" for="password">Password:<span class="warning">*</span></label>
                    <input type="password" name="password"  id="password_1" class="form-control form-control--contact validate[required,minSize[6]]" placeholder="PASSWORD"/>
                </div>

                <div class="extra_for_vendor">

                </div>
                <div class="form-group">
                    <label class="radio-inline">
                        <input type='radio' name="user_type" class="change_user_type" checked value="user">
                        <strong>"I am Buyer"</strong>
                    </label>
                </div>
                <div class="form-group">
                    <label class="radio-inline">
                        <input type='radio' name="user_type" class="change_user_type" value="vendor">
                        <strong>"I am Vendor"</strong>
                    </label>
                </div>
                <div class="clearfix"></div>
                <button type="submit" name="resigter" id="login_submit" class="btn btn-info custombtn btnblue pull-left" value="new">Create An Account</button>
                <input type='hidden' id='is_city' value='1'/>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>
 <br/>
                <br/>

