<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
    <head>
        <title>Inventor a Real Estates and Builders Category Flat Bootstrap Responsive Website Template | Blog :: w3layouts</title>
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Inventor Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- //for-mobile-apps -->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!-- js -->
        <script src="js/jquery-1.11.1.min.js"></script>
        <!-- //js -->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Abril+Fatface' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <!-- bannner -->
        <div class="banner1">
            <div class="container">
                <div class="logo">
                    <a href="index.html">Inventor <span>Find Your Home</span></a>
                </div>
                <div class="navigation">
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                            <nav class="cl-effect-13" id="cl-effect-13">
                                <ul class="nav navbar-nav">
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="events.html">News & Events</a></li>
                                    <li><a href="short-codes.html">Short Codes</a></li>
                                    <li><a href="services.html">Services</a></li>
                                    <li><a href="blog.html" class="active">Blog</a></li>
                                    <li><a href="mail.html">Mail Us</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- /.navbar-collapse -->
                    </nav>
                </div>
            </div>
        </div>
        <!-- //bannner -->
        <!-- blog -->
        <div class="blog">
            <div class="container">
                <h3><span>Latest</span> Blog</h3>
                <p class="dolore">Consectetur adipiscing elit, sed do 
                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
                    minim veniam.</p>
                <div class="blog-grids">
                    <div class="blog-grid">
                        <div class="col-md-5 blog-grid-right">
                            <a href="single.html"><img src="images/10.jpg" alt=" " class="img-responsive" /></a>
                            <div class="blog-grid-right-pos">
                                <ul>
                                    <li><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> 100 Likes</li>
                                    <li><a href="#"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> 3200 Views</a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> 47 Comments</a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span> 1 Tag</a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-share" aria-hidden="true"></span> 389 Shares</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-7 blog-grid-left">
                            <h4><a href="single.html">incididunt ut labore et dolore magna</a></h4>
                            <p>By <a href="single.html">Admin</a> <span>- Jan 26, 2016.</span></p>
                            <div class="rem">
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium 
                                    doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore 
                                    veritatis et quasi Inventoro beatae.</p>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="blog-grid">
                        <div class="col-md-7 blog-grid-left">
                            <h4><a href="single.html">incididunt ut labore et dolore magna</a></h4>
                            <p>By <a href="single.html">Admin</a> <span>- Jan 30, 2016.</span></p>
                            <div class="rem">
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium 
                                    doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore 
                                    veritatis et quasi Inventoro beatae.</p>
                            </div>
                        </div>
                        <div class="col-md-5 blog-grid-right">
                            <a href="single.html"><img src="images/1.jpg" alt=" " class="img-responsive" /></a>
                            <div class="blog-grid-right-pos1">
                                <ul>
                                    <li><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> 100 Likes</li>
                                    <li><a href="#"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> 3200 Views</a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> 47 Comments</a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span> 1 Tag</a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-share" aria-hidden="true"></span> 389 Shares</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="blog-grid">
                        <div class="col-md-5 blog-grid-right">
                            <a href="single.html"><img src="images/10.jpg" alt=" " class="img-responsive" /></a>
                            <div class="blog-grid-right-pos">
                                <ul>
                                    <li><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> 100 Likes</li>
                                    <li><a href="#"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> 3200 Views</a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> 47 Comments</a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span> 1 Tag</a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-share" aria-hidden="true"></span> 389 Shares</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-7 blog-grid-left">
                            <h4><a href="single.html">incididunt ut labore et dolore magna</a></h4>
                            <p>By <a href="single.html">Admin</a> <span>- Jan 26, 2016.</span></p>
                            <div class="rem">
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium 
                                    doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore 
                                    veritatis et quasi Inventoro beatae.</p>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <nav>
                    <ul class="pagination pasge">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- //blog -->
        <!-- footer -->
        <div class="footer">
            <div class="container">
                <div class="footer-grids">
                    <div class="col-md-4 footer-grid-left">
                        <h3>twitter feed</h3>
                        <ul>
                            <li><a href="single.html">It is a long established fact that a reader will 
                                    be distracted by the readable content of a page when looking at 
                                    its layout.</a><span>15 minutes ago</span></li>
                            <li><a href="mailto:info@example.com" class="cols">@NASA</a> & <a href="mailto:info@example.com" class="cols">
                                    @orbital science</a> <a href="single.html">readable content of a page when looking at 
                                    its layout</a><span>45 minutes ago</span></li>
                        </ul>
                    </div>
                    <div class="col-md-4 footer-grid-left">
                        <h3>Newsletter</h3>
                        <form>
                            <input type="email" value="enter your email address" onfocus="this.value = '';" onblur="if (this.value == '') {
                                        this.value = 'enter your email address';
                                    }" required="">
                            <input type="submit" value="Submit" >
                        </form>
                    </div>
                    <div class="col-md-4 footer-grid-left">
                        <h3>about us</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                            eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
                            <span>But I must explain to you how all this mistaken idea of denouncing
                                pleasure and praising pain was born and I will give you a complete 
                                account of the system, and expound the actual teachings of the 
                                great explorer.</span>
                        </p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="footer-bottom">
                    <div class="footer-bottom-left">
                        <p>&copy 2016 Inventor. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts.</a></p>
                    </div>
                    <div class="footer-bottom-right">
                        <ul>
                            <li><a href="#" class="icon-button twitter"><i class="icon-twitter"></i><span></span></a></li>
                            <li><a href="#" class="icon-button google"><i class="icon-google"></i><span></span></a></li>
                            <li><a href="#" class="icon-button v"><i class="icon-v"></i><span></span></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!-- //footer -->
        <!-- for bootstrap working -->
        <script src="js/bootstrap.js"></script>
        <!-- //for bootstrap working -->
    </body>
</html>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="cache-control" content="no-cache" />
        <?php echo head(isset($content) ? $content : ''); ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
        <?php css($array = array('css/style', 'css/bootstrap', 'reset', 'components', 'style', 'style-responsive', 'font-awesome/css/font-awesome.min', 'bootstrap/css/bootstrap.min', 'fancybox/source/jquery.fancybox', 'slider-layer-slider/css/layerslider', 'style-layer-slider')); ?> 			
    </head>
    <body class="corporate">
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'top_nav.php'); ?>
        <?php display_message(1); ?>
        <?php if (!empty($services2)): $st = '1'; ?>
            <div class="container margin-bottom-35">
                <?php if (!isset($_POST['submit'])) { ?>
                    <div class="pull-right">
                        <form method="POST" role="search">
                            <select name="catgory_id" id='catgory_id' class="form-control m-wrap validate[required]" >
                                <?php
                                if (!empty($categories)) {
                                    foreach ($categories as $key => $value) {
                                        ?>
                                        <option value="<?= $value->id ?>" <?= (in_array($value->id, $selected_cat)) ? 'selected' : '' ?>><?= $value->name ?></option>
                                    <?php } ?>

                                <?php } ?>	
                            </select>
                            <input type="text" value="" name="searchby" class="form-control" placeholder="Search Services by Service-Code"/>
                            <input type="submit" name="submit" value="Search" class="iconsearchtitle" />
                        </form>
                    </div>
                <?php } ?>
                <h1>Services</h1>
                <?php foreach ($services2 as $key => $data): ?>
                    <div class="col-sm-6">
                        <?php if ($st != '1'): ?>
                            <div class="col-sm-3">
                                <div class="service_image">
                                    <a href="<?= make_url('service_detail', 'id=' . $data->id) ?>"><img src="<?php echo cwebc::getImage($data->image, 'service', 'big'); ?>" alt="<?php echo $data->name; ?>"/></a>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="service_title"><h2><?= $data->name ?></h2></div>
                                <div class="service_verndor"><strong>Vendor:</strong> <?= $data->firstname . " " . $data->lastname ?></div>
                                <div class="service_verndor"><strong>Category:</strong> 
                                    <?php
                                    $QueryObj = new categoryService();
                                    $rec = $QueryObj->getServiceCatNames($data->id);
                                    if (!empty($rec)):
                                        echo implode(', ', $rec);
                                    endif;
                                    ?>
                                </div>

                                <div class="service_verndor"><strong>Phone:</strong> <?= $data->phone ?></div>
                                <div class="service_price"><strong>Price:</strong> <?= CURRENCY_SYMBOL . number_format($data->price, 2) ?></div>
                                <div class="service_description"><?= limit_text(html_entity_decode(strip_tags($data->description)), 400) ?>...</div>
                                <div class="read_more text-right"><a href="<?= make_url('service_detail', 'id=' . $data->id) ?>">Read More..</a></div>
                            </div>
                        <?php else: ?>
                            <div class="col-sm-3">
                                <div class="service_image">
                                    <a href="<?= make_url('service_detail', 'id=' . $data->id) ?>"><img src="<?php echo cwebc::getImage($data->image, 'service', 'big'); ?>" alt="<?php echo $data->name; ?>"/></a>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="service_title"><h2><?= $data->name ?></h2></div>
                                <div class="service_verndor"><strong>Vendor:</strong> <?= $data->firstname . " " . $data->lastname ?></div>
                                <div class="service_verndor"><strong>Category:</strong> 
                                    <?php
                                    $QueryObj = new categoryService();
                                    $rec = $QueryObj->getServiceCatNames($data->id);
                                    if (!empty($rec)):
                                        echo implode(', ', $rec);
                                    endif;
                                    ?>
                                </div>
                                <div class="service_verndor"><strong>Phone:</strong> <?= $data->phone ?></div>
                                <div class="service_price"><strong>Price:</strong> <?= CURRENCY_SYMBOL . number_format($data->price, 2) ?></div>
                                <div class="service_description"><?= limit_text(html_entity_decode(strip_tags($data->description)), 400) ?>...</div>
                                <div class="read_more text-right"><a href="<?= make_url('service_detail', 'id=' . $data->id) ?>">Read More..</a></div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php
                    $st++;
                    if ($st == '3'):
                        $st = '1';
                    endif;
                endforeach;
                ?>
            </div>
            <!-- End Content -->	
        <?php else: ?>
            <div class="container margin-bottom-35">
                <p>Sorry, No Service Found..!</p>
            </div>
        <?php endif; ?>	
        <div class="text-center">
            <ul class="pagination">
                <li></li>
                <?php for ($x = 1; $x <= $totalservices; $x++) { ?>
                    <li <?php echo $x == $p ? 'class="active"' : '' ?>><a href="<?php echo make_url('service&p=' . $x); ?>"><?php echo $x; ?></a></li>
                <?php } ?>
                <li></li>
            </ul>
        </div>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'footer_bar.php'); ?>	
        <!-- Load javascripts at bottom, this will reduce page load time -->
        <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
        <?php
        js($array = array('jquery.min', 'jquery-migrate.min', 'bootstrap.min', 'back-to-top', 'jquery.fancybox.pack'
            , 'jquery.slimscroll.min', 'owl.carousel.min', 'slider/greensock', 'slider/layerslider.transitions', 'slider/layerslider.kreaturamedia.jquery', 'slider/layerslider-init', 'layout'));
        ?>
        <script type="text/javascript">
                                jQuery(document).ready(function () {
                                    Layout.init();
                                    LayersliderInit.initLayerSlider();
                                });
        </script>
        <!-- END PAGE LEVEL JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>




<!--new-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="cache-control" content="no-cache" />
        <?php echo head(isset($content) ? $content : ''); ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
        <?php css($array = array('css/style', 'css/bootstrap', 'font-awesome/css/font-awesome.min', 'bootstrap/css/bootstrap.min', 'fancybox/source/jquery.fancybox', 'slider-layer-slider/css/layerslider', 'style-layer-slider')); ?> 			
    </head>
    <body class="corporate">
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'top_nav.php'); ?>
        <?php display_message(1); ?>
        <?php if (!empty($services2)): $st = '1'; ?>
            <div class="container margin-bottom-35">
                <?php if (!isset($_POST['submit'])) { ?>
                    <div class="pull-right">
                        <form method="POST" role="search">
                            <select name="catgory_id" id='catgory_id' class="form-control m-wrap validate[required]" >
                                <?php
                                if (!empty($categories)) {
                                    foreach ($categories as $key => $value) {
                                        ?>
                                        <option value="<?= $value->id ?>" <?= (in_array($value->id, $selected_cat)) ? 'selected' : '' ?>><?= $value->name ?></option>
                                    <?php } ?>

                                <?php } ?>	
                            </select>
                            <input type="text" value="" name="searchby" class="form-control" placeholder="Search Services by Service-Code"/>
                            <input type="submit" name="submit" value="Search" class="iconsearchtitle" />
                        </form>
                    </div>
                <?php } ?>
                <h1>Services</h1>
                <?php foreach ($services2 as $key => $data): ?> 
                    <div class="blog-grids">
                        <?php if ($st != '1'): ?>
                            <div class="blog-grid">
                                <div class="col-md-5 blog-grid-right">
                                    <a href="<?= make_url('service_detail', 'id=' . $data->id) ?>"><img src="<?php echo cwebc::getImage($data->image, 'service', 'big'); ?>" alt=" " class="img-responsive" /></a>

                                </div>
                                <div class="col-md-7 blog-grid-left">
                                    <h4><a href="<?= make_url('service_detail', 'id=' . $data->id) ?>"><?= $data->name ?></a></h4>
                                    <p>By <a href="<?= make_url('service_detail', 'id=' . $data->id) ?>">Vendor</a> <span>- <?= $data->firstname . " " . $data->lastname ?>.</span></p>
                                    <p>By <a href="<?= make_url('service_detail', 'id=' . $data->id) ?>">Category</a> <span>- <?php
                                            $QueryObj = new categoryService();
                                            $rec = $QueryObj->getServiceCatNames($data->id);
                                            if (!empty($rec)):
                                                echo implode(', ', $rec);
                                            endif;
                                            ?>.</span></p>

                                    <p>By <a href="<?= make_url('service_detail', 'id=' . $data->id) ?>">Phone:</a> <span>- <?= $data->phone ?>.</span></p>
                                    <p>By <a href="<?= make_url('service_detail', 'id=' . $data->id) ?>">Price:</a> <span>- <?= CURRENCY_SYMBOL . number_format($data->price, 2) ?>.</span></p>
                                    <div class="rem">
                                        <p>"<?= limit_text(html_entity_decode(strip_tags($data->description)), 400) ?>..."</p>
                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        <?php else: ?>
                            <div class="blog-grid">
                                <div class="col-md-7 blog-grid-left">
                                    <h4><a href="<?= make_url('service_detail', 'id=' . $data->id) ?>"><?= $data->name ?></a></h4>
                                    <p>By <a href="<?= make_url('service_detail', 'id=' . $data->id) ?>">Vendor</a> <span>- <?= $data->firstname . " " . $data->lastname ?>.</span></p>
                                    <p>By <a href="<?= make_url('service_detail', 'id=' . $data->id) ?>">Category</a> <span>- <?php
                                            $QueryObj = new categoryService();
                                            $rec = $QueryObj->getServiceCatNames($data->id);
                                            if (!empty($rec)):
                                                echo implode(', ', $rec);
                                            endif;
                                            ?>.</span></p>

                                    <p>By <a href="<?= make_url('service_detail', 'id=' . $data->id) ?>">Phone:</a> <span>- <?= $data->phone ?>.</span></p>
                                    <p>By <a href="<?= make_url('service_detail', 'id=' . $data->id) ?>">Price:</a> <span>- <?= CURRENCY_SYMBOL . number_format($data->price, 2) ?>.</span></p>
                                    <div class="rem">
                                        <p>"<?= limit_text(html_entity_decode(strip_tags($data->description)), 400) ?>..."</p>
                                    </div>
                                </div>
                                <div class="col-md-5 blog-grid-right">
                                    <a href="<?= make_url('service_detail', 'id=' . $data->id) ?>"><img src="<?php echo cwebc::getImage($data->image, 'service', 'big'); ?>" alt=" " class="img-responsive" /></a>
                                    <!--                            <div class="blog-grid-right-pos1">
                                                                    <ul>
                                                                        <li><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> 100 Likes</li>
                                                                        <li><a href="#"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> 3200 Views</a></li>
                                                                        <li><a href="#"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> 47 Comments</a></li>
                                                                        <li><a href="#"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span> 1 Tag</a></li>
                                                                        <li><a href="#"><span class="glyphicon glyphicon-share" aria-hidden="true"></span> 389 Shares</a></li>
                                                                    </ul>
                                                                </div>-->
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        <?php endif; ?>
                    </div>


                    <?php
                    $st++;
                    if ($st == '3'):
                        $st = '1';
                    endif;
                endforeach;
                ?>
            </div>
            <!-- End Content -->	
        <?php else: ?>
            <div class="container margin-bottom-35">
                <p>Sorry, No Service Found..!</p>
            </div>
        <?php endif; ?>	
        <div class="text-center">
            <ul class="pagination">
                <li></li>
                <?php
                for ($x = 1; $x <= $totalservices; $x++) {
                    ?>
                    <li <?php echo $x == $p ? 'class="active"' : '' ?>><a href="<?php echo make_url('service&p=' . $x); ?>"><?php echo $x; ?></a></li>
                <?php } ?>
                <li></li>
            </ul>
        </div>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'footer_bar.php'); ?>	
        <!-- Load javascripts at bottom, this will reduce page load time -->
        <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
        <?php
        js($array = array('jquery.min', 'jquery-migrate.min', 'bootstrap.min', 'back-to-top', 'jquery.fancybox.pack'
            , 'jquery.slimscroll.min', 'owl.carousel.min', 'slider/greensock', 'slider/layerslider.transitions', 'slider/layerslider.kreaturamedia.jquery', 'slider/layerslider-init', 'layout'));
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                Layout.init();
                LayersliderInit.initLayerSlider();
            });
        </script>
        <!-- END PAGE LEVEL JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>


<?php
SELECT `cwebc_service`.`service_area`,
 `cwebc_service_cat_rel`.`id`,
 `cwebc_service_cat_rel`.`category_id`,
 `cwebc_service`.`id` FROM `cwebc_service_cat_rel` JOIN `cwebc_service` on `cwebc_service_cat_rel`.`service_id` = `cwebc_service`.`id` WHERE `cwebc_service`.`service_area` LIKE '%9%' AND `cwebc_service_cat_rel`.`category_id` LIKE '%1%'
?>'
. '






















<?php
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
$resend = isset($_GET['resend']) ? $_GET['resend'] : '0';
$req_id = isset($_GET['req']) ? $_GET['req'] : '0';
/* check if user logged in or not */

if ($logged_in):
    $login_session->logout_user();
endif;


if (isset($_POST['submit'])):
    if ($_POST['email'] != ''):
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)):

            $query = new user();
            $user_info = $query->checkUserEmailExists($_POST['email']);

            if ($user_info):

                $code = randomString(10);

                $query11 = new user();
                if ($query11->updateForgotPasswordVerifyCode($user_info->id, $code)):
                    /* send email */
                    $email_array = array(
                        'firstname' => $user_info->firstname,
                        'lastname' => $user_info->lastname,
                        'confirmation_url' => make_url('reset_password', 'req=' . $code)
                    );


                    /*                     * * user email * */
                    $QueryObj = new email();
                    $QueryObj->send_db_email(7, $user_info->username, $email_array);

                    $login_session->pass_msg[] = show(MSG_FORGOT_PASSWORD_SUCCESS, FALSE);
                else:

                    $login_session->pass_msg[] = show(MSG_SOMETHING_WENT_WRONG, FALSE);
                    $login_session->set_error();
                endif;
            else:
                $login_session->pass_msg[] = show(MSG_ACCOUNT_WITH_EMAIL_NOT_EXIST, FALSE);
                $login_session->set_error();
            endif;

        else:
            $login_session->pass_msg[] = show(MSG_WRONG_EMAIL_FORMAT, FALSE);
            $login_session->set_error();
        endif;

    else:
        $login_session->pass_msg[] = show(FILL_REQUIRED_FIELDS, FALSE);
        $login_session->set_error();
    endif;

    $login_session->set_pass_msg();
    Redirect(make_url('forgot_password'));
endif;


if (isset($_POST['resend'])):
    if ($_POST['email'] != ''):
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)):

            $query = new user();
            $user_info = $query->checkUserEmailExists($_POST['email']);

            if ($user_info):
                $code = randomString(10);
                $query11 = new user();
                if ($query11->updateConfirmationVerifyCode($user_info->id, $code)):
                    /* send email */
                    $email_array = array(
                        'firstname' => $user_info->firstname,
                        'lastname' => $user_info->lastname,
                        'confirmation_url' => make_url('verify', 'req=' . $code)
                    );
                    /*                     * * user email * */
                    $QueryObj = new email();
                    $QueryObj->send_db_email(30, $user_info->username, $email_array);
                    $login_session->pass_msg[] = show(MSG_ACCOUNT_RESEND, FALSE);
                else:

                    $login_session->pass_msg[] = show(MSG_SOMETHING_WENT_WRONG, FALSE);
                    $login_session->set_error();
                endif;
            else:
                $login_session->pass_msg[] = show(MSG_ACCOUNT_WITH_EMAIL_NOT_EXIST, FALSE);
                $login_session->set_error();
            endif;

        else:
            $login_session->pass_msg[] = show(MSG_WRONG_EMAIL_FORMAT, FALSE);
            $login_session->set_error();
        endif;

    else:
        $login_session->pass_msg[] = show(FILL_REQUIRED_FIELDS, FALSE);
        $login_session->set_error();
    endif;

    $login_session->set_pass_msg();
    Redirect(make_url('forgot_password', 'resend=1'));
endif;

/* SEO information */
if ($resend == 1):
    $content = add_metatags("Resend Confirmation Email");
else:
    $content = add_metatags("Forgot Password");
endif;
?>












<?php
//Set useful variables for paypal form
$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
$paypal_id = 'info@codexworld.com'; //Business Email

include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
$id = isset($_GET['id']) ? $_GET['id'] : '0';

$queryUser = new user();
$user_details = $queryUser->getUser($user_id);

$QueryObj = new service();
$service = $QueryObj->getServiceWithDetails($id);
$productid = $service->id;





if (isset($_POST['choose'])) {
    if (!$_POST['u_name'] || !$_POST['phone'] || !$_POST['address']) {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Please fill the fields');
        Redirect(make_url('cart', 'id=' . $id));
    }
}

if (isset($_GET['act']) && $_GET['act'] == 'payed') {
    $new = explode(',', $_GET['cm']);
    $address = $new[3];
    $addres = $address;
    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($addres) . '&sensor=true');
    $coordinates = json_decode($coordinates);
    $lat = $coordinates->results[0]->geometry->location->lat;
    $longi = $coordinates->results[0]->geometry->location->lng;
    'Latitude:' . $coordinates->results[0]->geometry->location->lat;
    'Longitude:' . $coordinates->results[0]->geometry->location->lng;
    $ordervalue = array(
        'txn_id' => $_GET['tx'],
        'service_id' => $_GET['id'],
        'payment_gross' => $_GET['amt'],
        'currency_code' => $_GET['cc'],
        'payment_status' => $_GET['st'],
        'name' => $new['0'],
        'date_time' => $new['1'],
        'u_name' => $new['2'],
        'address' => $new['3'],
        'phone' => $new['4'],
        'user_id' => $user_details->id,
        'lat' => $lat,
        'longi' => $longi,
    );

    $query = new user;
    $values = $user_details->username;
    $value = 'info@cwebconsultants.com';

    if ($values) {
        $Subject = 'payment succesfully done';
        $ToEmail = $values;
        $FromEmail = $value;
        $FromName = 'Marketpalce';
        $Message = ' order confirmation';
        $send_email = send_email_by_cron($ToEmail, $Message, $Subject, $FromEmail, $FromName);
        if ($send_email) {
            $admin_user->set_pass_msg('Thanks For Your order,Your Payment is confirmed Successfully');
            Redirect(make_url('myorder'));
        } else {
            $admin_user->set_error();
            $admin_user->set_pass_msg('Request not sent');
        }
    } else {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Email not found');
    }
}

if (!empty($txn_id) && $sid == $productid) {
    $query = new trans();
    $query->print = 1;
    $query->saveServiceorder($ordervalue);
    Redirect(make_url('myorder'));
}



/* SEO information */
$content = add_metatags($service->name, $service->name);
?>



















//olden





















<?php
//Set useful variables for paypal form
$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
$paypal_id = 'info@codexworld.com'; //Business Email

include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
$id = isset($_GET['id']) ? $_GET['id'] : '0';

$queryUser = new user();
$user_details = $queryUser->getUser($user_id);

$QueryObj = new service();
$service = $QueryObj->getServiceWithDetails($id);
$productid = $service->id;





if (isset($_POST['choose'])) {
    if (!$_POST['u_name'] || !$_POST['phone'] || !$_POST['address']) {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Please fill the fields');
        Redirect(make_url('cart', 'id=' . $id));
    }
}

if (isset($_GET['act']) && $_GET['act'] == 'payed') {
    $txn_id = $_GET['tx'];
    $sid = $_GET['id'];
    $payment_gross = $_GET['amt'];
    $currency_code = $_GET['cc'];
    $payment_status = $_GET['st'];
    $new = explode(',', $_GET['cm']);
    $name = $new[0];
    $date_time = $new[1];
    $u_name = $new[2];
    $address = $new[3];
    $addres = $address;
    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($addres) . '&sensor=true');
    $coordinates = json_decode($coordinates);
    pr($lat = $coordinates->results[0]->geometry->location->lat);
    pr($longi = $coordinates->results[0]->geometry->location->lng);
    echo 'Latitude:' . $coordinates->results[0]->geometry->location->lat;
    echo 'Longitude:' . $coordinates->results[0]->geometry->location->lng;

    $phone = $new[4];
    $uid = $user_details->id;

    $query = new user;
    $values = $user_details->username;

    if ($values) {
        $Subject = 'payment succesfully done';
        $ToEmail = $values;
        $FromEmail = $value;
        $FromName = 'Marketpalce';
        $Message = ' order confirmation';
        $send_email = send_email_by_cron($ToEmail, $Message, $Subject, $FromEmail);
        pr($send_email);
        if ($send_email) {
            $admin_user->set_pass_msg('Thanks For Your order,Your Payment is confirmed Successfully');
        } else {
            $admin_user->set_error();
            $admin_user->set_pass_msg('Request not sent');
        }
    } else {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Email not found');
    }
}

if (!empty($txn_id) && $sid == $productid) {
    $query = new trans();
    $query->saveServiceorder($txn_id, $sid, $name, $date_time, $u_name, $address, $lat, $longi, $phone, $payment_gross, $currency_code, $payment_status, $uid);
    Redirect(make_url('myorder'));
}



/* SEO information */
$content = add_metatags($service->name, $service->name);
?>






















<!--<div class="footer">
<div class="container">
<div class="footer-bottom">
    <div class="footer-bottom-left">
        <p><?//=date('Y')?> © <? //=SITE_NAME?>. All rights reserved </p>
    </div>
    <div class="footer-bottom-right">
        <ul>
<?php //if(TWITTER_PAGE){  ?>
            <li><a href="<?//=TWITTER_PAGE?>" class="icon-button twitter"><i class="icon-twitter"></i><span></span></a></li>
<?PHP //} ?>
<?php //if(GOOGLE_PLUS_PAGE){ ?>
            <li><a href="<?//=GOOGLE_PLUS_PAGE?>" class="icon-button google"><i class="icon-google"></i><span></span></a></li>
<?PHP //} ?>
            <li><a href="#" class="icon-button v"><i class="icon-v"></i><span></span></a></li>
        </ul>
    </div>
    <div class="clearfix"> </div>
</div>
</div>
</div>-->
<?php
include_once(DIR_FS_SITE . 'include/functionClass/contentClass.php');
$query_obj = new content();
$about_page = $query_obj->getPage(2);
?>
<div id="hr-upon-footer">
</div>
<div class="footer-container">
    <div id="footer">
        <div class="footer-center">
            <div class="container">
                <div class="row">
                    <div class="footer-static row-fluid">
                        <div class=" col-xs-12  col-sm-6 col-md-4">
                            <h4 class="title_font">
                                <?= html_entity_decode($about_page->page_name) ?>
                            </h4>
                            <br />
                            <center><a href="#"> <img class="footer-logo" src="upload/photo/banner/small/about.PNG" style="height:100px;" a=""></a></center>
                            <p>

                            <p> <?= limit_text(html_entity_decode(strip_tags($about_page->page)), 400) ?>...</p>
                            <a href="<?= make_url('content', 'id=' . $about_page->id) ?>"><button style="color:#fff;" id="read_more">Read More</button></a>
                            </p><div class="toggle-footer"><div>
                                </div>
                            </div></div>
                        <section id="onecate_products_block" class="  col-sm-4 col-xs-12 col-md-4">
                            <h4 class="title_font">
                                <img src="upload/photo/banner/small/qu.PNG">
                                Quick View
                            </h4>
                            <?php
                            include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');

                            $query = new service_detail;
                            $recent = $query->list_info($user_id);
                            ?>
                            <div id="onecate_products" class="block_content toggle-footer carousel-grid owl-carousel owl-theme" style="display: block;">
                                <div class="owl-wrapper-outer">
                                    <div class="owl-wrapper" style=" left: 0px; display: block;">
                                        <?php
                                        foreach ($recent as $rec) {
                                            $query = new service;
                                            $view = $query->getServiceWithDetails($rec['service_id']);
                                            ?>
                                            <div class="owl-item" style="width: 270px;">
                                                <div class="item first">
                                                    <div class="item-content clearfix">
                                                        <div class="left-content pull-left">
                                                            <a href="#" title="">
                                                                <img src="upload/photo/banner/small/bag_in_style4175.jpg" alt="" height="70" width="70" id="quick_img">
                                                            </a>
                                                        </div>
                                                        <div class="right-content pull-right">
                                                            <p>
                                                                <?php
                                                                echo $view->name;
                                                                ?>
                                                            </p>
                                                            <p><?= limit_text(html_entity_decode(strip_tags($view->description)), 100) ?></p>
                                                        </div>
                                                    </div>

                                                </div>
                                                <br />
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="owl-item" style="width: 270px;">
                                        <div class="item">
                                            <div class="item-content clearfix">
                                                <div class="left-content">
                                                    <a href="#" title="">
                                                        <img src="upload/photo/banner/small/bag_in_style4175.jpg" alt="" height="70" width="70" id="quick_img">
                                                    </a>
                                                </div>
                                                <div class="right-content pull-right">
                                                    <p>Call Us Now:  1234567890</p>
                                                    <p>Cweb-Co Phase-8 Ins Area Mohali</p>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="item-content clearfix">
                                                <div class="left-content">
                                                    <a href="#" title="SSM Andriod App">
                                                        <img src="upload/photo/banner/small/bag_in_style4175.jpg" alt="" height="70" width="70" id="quick_img">
                                                    </a>
                                                </div>
                                                <div class="right-content pull-right">
                                                    <p>Call Us Now:  1234567890</p>
                                                    <p>Cweb-Co Phase-8 Ins Area Mohali</p>
                                                </div>
                                            </div>
                                        </div></div></div></div>
                    </div>
                    </section>
                    <div class=" custom-columns col-xs-12 col-sm-6 col-md-4" id="our_add_footer">
                        <h4 class="title_font"><img src="upload/photo/banner/small/ad.PNG">Our Address</h4>
                        <br />
                        <div class="columns-content toggle-footer" style="">
                            <p><i class="fa fa-phone" aria-hidden="true"></i> Call Us Now:  1234567890</p>
                            <p><i class="fa fa-map-marker"></i> Cweb-Co Phase-8 Ins Area Mohali</p>
                            <p><i class="fa fa-envelope" aria-hidden="true"></i> Cwebconsultants.com</p>
                        </div>
                    </div>
                    <!-- Block Newsletter module-->
                    <!--			<div id="newsletter_block_left" class=" custom-columns col-xs-12 col-sm-6 col-md-3">
                                                <center>
                                                <h4 class="title_font">NEWS LETTER</h4>
                                                </center
                                                  <br /> <br />
                                                <div class="block_content">
                                                    <form  method="post">
                                                        <div class="form-group">
                                                            <input class="inputNew form-control grey newsletter-input" id="newsletter-input" type="text" name="email" size="18" value="Your e-mail">
                                                            <div class="button-subscribe">
                                                                <button type="submit" name="submitNewsletter" class="btn btn-default button button-small" id="newsletter-submit-btn">
                                                                    <span>subscribe</span>
                                                                </button>
                                                            </div>
                                                            <input type="hidden" name="action" value="0">
                                                        </div>
                                                    </form>
                                                </div>
                    
                                            </div>-->
                    <!-- /Block Newsletter module-->

                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

        </div>
    </div>
</div>
</div>
