<?php display_message(1); ?>

<div class="container margin-bottom-35">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="text-center">
                Order Service
            </h1>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-4">
                <h3>Service detail</h3>
                <div class="service_title"><strong>Service Name:</strong> <?= $service->name ?></div>
                <div class="service_verndor"><strong>Category:</strong> 
                    <?php
                    $QueryObj = new categoryService();
                    $rec = $QueryObj->getServiceCatNames($service->id);
                    if (!empty($rec)):
                        echo implode(', ', $rec);
                    endif;
                    ?>
                </div>
                <div class="service_verndor"><strong>Vendor:</strong> <?= $service->firstname . " " . $service->lastname ?></div>
                <div class="service_price"><strong>Price:</strong> <?= CURRENCY_SYMBOL . number_format($service->price, 2) ?></div>
            </div>

            <?PHP if (!isset($_POST['date_time'])) { ?>
                <form action="" method="POST">
                    <div class="col-sm-4">
                        <h3>Enter the fields where order has to be placed</h3>
                        <div class="form-group">
                            <label class="text-dark" for="username">Name:</label>
                            <input type="text" name="u_name"   id="firstname" class="form-control form-control--contact validate[required]"/>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="address">Address:</label>
                            <input type="text" name="add"   class="form-control form-control--contact validate[required,custom[email]]"/>
                        </div>
                        <div class="form-group">
                            <label class="text-dark" for="address">Phone:</label>
                            <input type="number" name="mobile"   id="phone" class="form-control form-control--contact" maxlength="12" size="10" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <h3>Choose the service Date / Time</h3>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control" name="date_time" required />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 text-center">
                        <input type="hidden" name="vendor_id" value=' <?= $service->vendor_id ?>'>

                        <input type='hidden' class="form-control" name="name" value="<?= $service->name ?>" />
                        <input type="submit" name="choose" class="btn btn-info" border="0" value="Go To Next Level">

                    </div>
                <?php } else { ?>
                    <div class="col-sm-4">
                        <h4>Make your payement now</h4>
                    </div>
                    <div class="col-sm-4">
                        <form action="<?php echo $paypal_url; ?>" method="POST">
                            <input type="hidden" name="business" value="<?php echo $paypal_id; ?>">
                            <input type="hidden" name="cmd" value="_xclick">
                            <input type="hidden" name="item_name" value=' <?= $service->name ?>'>
                            <input type="hidden" name="vendor_id" value=' <?= $service->vendor_id ?>'>
                            <input type="hidden" name="item_number" value="">
                            <input type="hidden" name="custom" value="<?php echo $_POST['name'] . ',' . $_POST['date_time']. ',' . $_POST['u_name']. ',' . $_POST['add']. ',' . $_POST['mobile']. ',' . $_POST['vendor_id']; ?>">
                            <input type="hidden" name="user_id" value="<?php echo $user_details->id; ?>">
                            <input type="hidden" name="amount" value="<?= CURRENCY_SYMBOL . number_format($service->price, 2) ?>">
                            <input type="hidden" name="currency_code" value="USD">
                            <input type='hidden' name='cancel_return' value='http://example.com/cancel.php'>
                            <input type='hidden' name='return' value='<?php echo make_url('cart', 'id=' . $service->id . '&act=payed'); ?>'>
                            <input type="submit" name="submit" class="btn btn-info text-right" border="0" value="Payment">
                        </form>
                    <?php } ?>
                </div>

        </div>


    </div>
</div>

