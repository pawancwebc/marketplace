<?php display_message(1); ?>
<hr>
<div class="container g">
    <div class="row">
        <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a  href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a  href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a class="linked" href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li><a href="<?php echo make_url('serviceorder'); ?>"> Service Orders</a></li>
                            <li><a href="<?php echo make_url('support'); ?>">  Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                </div><?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('myorder'); ?>"> My order</a></li>
                            <li><a href="<?php echo make_url('support'); ?>">Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                </div>
                <?php }?>
       <div class="col-md-7 single-grid-left">
                    <h3></h3>
                    <p>
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#home" data-toggle="tab">Service</a></li>
                <!--                                <li><a href="#status" data-toggle="tab">Status</a></li>
                                                <li><a href="#edit" data-toggle="tab">Edit</a></li>
                                                <li><a href="#image" data-toggle="tab">Image</a></li>-->
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <div class="table-responsive">          
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">S.No</th>
                                    <th style="text-align: center;">Name</th>
                                    <th style="text-align: center;">Status</th>
                                    <th style="text-align: center;">Orders</th>
                                    <th style="text-align: center;">Category</th>
                                    <th style="text-align: center;">Images</th>
                                    <th style="text-align: center;">Controls</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_service as $key => $listservice) { ?>
                                    <tr>
                                        <td> <?php echo $key+=1; ?></td>
                                        <td> <?php echo $listservice['name']; ?></td>

                                        <td>
                                            <?php
                                            if ($listservice['is_active'] == 1) {
                                                echo "ACTIVE";
                                            } else {
                                                echo "NOT APPROVED";
                                            }
                                            ?>
                                        </td>
                                        <td>Order</td>
                                        <td>    
                                            <?php
                                            $query = new categoryService();
                                            $list_cat = $query->getServiceCatNames($listservice['id']); {
                                                ?>

                                                <?php
                                                if (!empty($list_cat)) {
                                                    echo implode(', ', $list_cat);
                                                }
                                                ?>
                                            <?php } ?>
                                        </td>
                                        <td class='text-center' >
                                            <a href="<?php echo make_url('image', 'id=' . $listservice['id']); ?>">view image</a>
                                        </td>
                                        <td>
                                            <a href="<?php echo make_url('editlist', 'id=' . $listservice['id']); ?>" title="Click here to edit"><i class="fa fa-edit" id="edit_btn_service"></i></a> 

                                            <?php if ($listservice['is_active'] == 1) { ?>
                                                <?php if ($listservice['is_on'] == 1) { ?>
                                                    <a href="<?php echo make_url('listservice', 'id=' . $listservice['id'] . '&act=active'); ?>" title="Click to stop service"> <i class="fa fa-pause"></i>  </a>
                                                <?php } else {
                                                    ?>
                                                    <a href="<?php echo make_url('listservice', 'id=' . $listservice['id'] . '&act=stop'); ?>" title="Click to start service"> <i class="fa fa-caret-square-o-right "></i>   </a>
                                                <?php } ?>
                                                                                <!--<a href="<?php //echo make_url('listservice', 'id=' . $listservice['id'] . '&act=start');   ?>" title="click to Start service"> <i class="fa fa-close "></i>   </a>-->
                                                                                <!--<i class="fa fa-close "></i>--> 
                                            <?php } else { ?>
                                                            <!--<a href="<?php //echo make_url('listservice', 'id=' . $listservice['id'] . '&act=delete')   ?>" onclick="return confirm('Are you sure? You want  to move this service to trash.');"  title="click here to delete this service." > <i class="fa fa-trash"></i></a>-->
                                                <i class="fa fa-close" id="edit_btn_service" title="Not approved by admin"></i>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div><!--/tab-pane-->
                <div class="tab-pane " id="status">
                    <?php
                    $query = new simage();
                    $imag = $query->getServiceImages($listservice['id']);
                    ?>
                    <?php foreach ($imag as $im) { ?>

                        <img src="<?php echo DIR_WS_SITE_UPLOAD_PHOTO . 'service/thumb/' . $im['image']; ?>" alt=""/>

                    <?php } ?> 
                </div><!--/tab-pane-->
                <div class="tab-pane" id="edit">
                    <hr>
                </div>
            </div>
            <div class="tab-pane " id="image">
                <br>
                <br>
            </div><!--/tab-pane-->
       </p>
       </div>
        </div><!--/tab-pane-->
    </div>
</div><!--/tab-content-->

</div><!--/col-9-->
</div><!--/row-->




