<?php display_message(1); ?>

<hr>

    
        <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-4 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a  href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a  href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li><a href="<?php echo make_url('serviceorder'); ?>"> Service Orders</a></li>
                            <li> <a class="linked" href="<?php echo make_url('support'); ?>">Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
            </div><?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('myorder'); ?>">  My order</a></li>
                            <li><a class="linked" href="<?php echo make_url('support'); ?>"> Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
            </div>
        <?php } ?>

        <div class="col-md-8 single-grid-left" id="left-support-zone">
            
<!--            <div class="col-sm-5" id="free-cntct-support">
                <h2>GET IN TOUCH</h2><br />
		<p>If you have any further questions, please don’t hesitate to contact me.Please feel free to call me on (telephone) or at (email), if you require any further information.</p>
            </div>-->
<div class="col-sm-10" id="right-box-support-page">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"><a href="#home" data-toggle="tab">Contact Info</a></li>
                </ul>
    <hr>
                <div class="tab-content">
                    <div class="tab-pane active col-sm-5" id="home">
                        <h4>GET IN TOUCH</h4>
                        <p>If you have any further questions, please don’t hesitate to contact me.Please feel free to call me on (telephone) or at (email), if you require any further information.</p>
                    
                </div><!--/tab-pane-->
            </div>
	    <div class="col-sm-5" id="right-box-support-page">
               
                <div class="tab-content">
                    <div class="tab-pane active" id="home">
			
                        <b><p><i class="fa fa-phone" aria-hidden="true"></i> Call Us Now:</b><?= PHONE_NUMBER ?> </p>                  

                        
                        <p><i class="fa fa-envelope" aria-hidden="true"></i> Email: <?= ADMIN_EMAIL ?> </p>
                        
			
                        <p><i class="fa fa-map-marker"></i> Cweb-Co Phase-8 Ins Area Mohali</p>
                        
                    </div><!--/tab-pane-->
                    
                </div><!--/tab-pane-->
           
           
        </div>

        <!--/tab-content-->
    </div><!--/col-9-->
</div><!--/row-->

<!--<div class="form-group">

    <div class="col-xs-6">
        <label for="email"><h4>State</h4></label>
        <input type="text" <?php //if ($user_details->state_id != '') {      ?> value="<?php //echo $state->name;      ?>" <?php //}      ?> name="state_id" class="form-control" id="state_id">
    </div>
</div>
<div class="form-group">

    <div class="col-xs-6">
        <label for="email"><h4>City</h4></label>
        <input type="text" <?php //if ($user_details->city_id != '') {      ?> value="<?php //echo $city->name;      ?>" <?php //}      ?> name="city_id" class="form-control" id="zip_code">
    </div>
</div>
<div class="form-group">

    <div class="col-xs-6">
        <label for="email"><h4>Country</h4></label>
        <input type="text" <?php //if ($user_details->country_id != '') {      ?> value="<?php //echo $country->short_name;      ?>"<?php //}      ?> name="country_id" class="form-control" id="zip_code">
    </div>
</div>-->






<!--<form class="form" method="post" >
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="country_id"><h4>Country</h4></label>
                                <select name="country_id" id="country_id" class="form-control">
                                    <option value="">Select Country</option>
<?php //foreach (getCountries(true) as $kk => $vv): ?>
                                        <option value="<?php //echo $vv['id'];  ?>"><?php //echo $vv['short_name'];  ?></option>
<?php //endforeach; ?>
                                </select>
                                <spa n id="country_error" style="display:none;color:red"><i>Please select the country first.</i></span>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="state_id"><h4>State</h4></label>
                                <select name="state_id" id="state_id" class="form-control">
                                    <option value="">Select State</option>
<?php //foreach ($state_all as $st): ?>
                                        <option value="<?php ///echo $st['id'];  ?>"><?php //echo $st['name'];  ?></option>
<?php //endforeach; ?>
                                </select>
                                <span id="state_error" style="display:none;color:red"><i>Please select the state.</i></span>
                            </div>
                        </div>	
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="city_id"><h4>City</h4></label>
                                <select name="city_id" id="city_id" class="form-control">
                                    <option value="">Select City</option>
<?php //foreach ($city_all as $ct): ?>
                                        <option value="<?php //echo $ct['id'];  ?>"><?php //echo $ct['name'];  ?></option>
<?php //endforeach; ?>
                                </select>
                                <span id="state_error" style="display:none;color:red"><i>Please select the city.</i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
                                <input class="btn blue" type="hidden" name="id" value="<?php //echo $user_id  ?>" tabindex="7" /> 
                                <input class="btn btn-lg btn-success" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            </div>
                        </div>
                    </form>-->






