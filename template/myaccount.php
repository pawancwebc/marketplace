
<div class="container">
        <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
			<nav class="navbar navbar-default">
                           <div class="container-fluid">
                        <ul >
                            <li><a class="linked" href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li><a href="<?php echo make_url('serviceorder'); ?>"> Service Orders</a></li>
                            <li><a href="<?php echo make_url('support'); ?>"> Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
			   </div>
			</nav>
                    </div>

                </div>
		<br /><br /><br />
                <div class="col-md-7 single-grid-left">
                    <h3></h3>
                    <p>
                        <a href="<?php echo make_url('profile'); ?>">
                            <div class="col-md-6 col-sm-6">
                                <div class="servive-block servive-block-dark-blue">            
                                    <i class="icon-2x color-light fa fa-photo"></i>
                                    <h2 class="heading-md">Profile</h2>
                                </div>
                            </div>
                        </a>
                        <a href="<?php echo make_url('listservice'); ?>">
                            <div class="col-md-6 col-sm-6">
                                <div class="servive-block servive-block-dark-blue">            
                                    <i class="icon-2x color-light fa fa-gear"></i>
                                    <h2 class="heading-md">Service</h2>
                                </div>
                            </div>
                        </a>
                        <a href="<?php echo make_url('serviceorder'); ?>">
                            <div class="col-md-6 col-sm-6">
                                <div class="servive-block servive-block-dark-blue">            
                                    <i class="icon-2x color-light fa fa-gift"></i>
                                    <h2 class="heading-md">Service Order</h2>
                                </div>
                            </div>
                        </a>
                        <a href="<?php echo make_url('support'); ?>">
                            <div class="col-md-6 col-sm-6">
                                <div class="servive-block servive-block-dark-blue">            
                                    <i class="icon-2x color-light fa fa-support"></i>
                                    <h2 class="heading-md">Support</h2>
                                </div>
                            </div>
                        </a>
                    </p>
                </div>
                <div class="clearfix"> </div>
            </div>
        <?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a class="linked" href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('myorder'); ?>"> My order</a></li>
                            <li> <a href="<?php echo make_url('support'); ?>">Support</li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                <div class="col-md-7 single-grid-left">
                    <h3></h3>
                    <p>
                        <a href="<?php echo make_url('profile'); ?>">
                            <div class="col-md-6 col-sm-6">
                                <div class="servive-block servive-block-dark-blue">            
                                    <i class="icon-2x color-light fa fa-photo"></i>
                                    <h2 class="heading-md">Profile</h2>
                                </div>
                            </div>
                        </a>
                        <a href="<?php echo make_url('myorder'); ?>">
                            <div class="col-md-6 col-sm-6">
                                <div class="servive-block servive-block-dark-blue">            
                                    <i class="icon-2x color-light fa fa-gear"></i>
                                    <h2 class="heading-md">my order</h2>
                                </div>
                            </div>
                        </a>

                        <a href="<?php echo make_url('support'); ?>">
                            <div class="col-md-6 col-sm-6">
                                <div class="servive-block servive-block-dark-blue">            
                                    <i class="icon-2x color-light fa fa-support"></i>
                                    <h2 class="heading-md">Support</h2>
                                </div>
                            </div>
                        </a>
                    </p>
                </div>
                <div class="clearfix"> </div>
            </div>
        <?php } ?>	
    </div>

