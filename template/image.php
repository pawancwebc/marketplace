<?php display_message(1); ?>
<hr>
<div class="container">
    <div class="row">
         <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a  href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a  href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a class="linked" href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li> Service Orders</li>
                            <li><a href="<?php echo make_url('support'); ?>">  Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                </div><?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('myorder'); ?>">My order</a></li>
                            <li> <a href="<?php echo make_url('support'); ?>"> Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
                </div>
                <?php }?>
       <div class="col-md-7 single-grid-left">
                    <h3></h3>
                    <p>

        <div class="col-sm-9">
            <!--            <ul class="nav nav-tabs" id="myTab">
                            <li class="active"><a href="#home" data-toggle="tab">Service</a></li>
                            <li><a href="#status" data-toggle="tab">Status</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#image" data-toggle="tab">Image</a></li>
                        </ul>-->
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                                <a class="pull-right" href="<?php echo make_url('addimage', 'id=' . $id); ?>"><i class="fa fa-plus" ></i>Add Image</a>
                    <?php if ($imag) { ?>                    <div class="table-responsive">          
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th >S.No</th>
                                        <th >Image</th>
                                        <th >Cover Image</th>
                                        <th >Action</th>
                                        <!--<th >Position</th>-->

                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($imag as $key => $imagee) { ?>
                                    <tr>
                                        <td><?php echo $key+=1; ?></td>
                                        <td>  
                                            <?php if ($imagee['image'] != '') { ?>
                                                <div class="tab-pane " id="status">
                                                    <img src="<?php echo DIR_WS_SITE_UPLOAD_PHOTO . 'service/thumb/' . $imagee['image']; ?>" alt="" class="thumbnail"/>
                                                </div>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if ($imagee['is_main'] == 1) { ?>
                                                        <!--<a href="<?php echo make_url('image', 'id=' . $imagee['id'] . '&act=remove'); ?>"  title="Click Here To Remove It As Cover Image"><i class="fa fa-check"></i> </a>-->
                                                <i class="fa fa-check" id="edit_btn_service"></i> 

                                            <?php } else { ?>

                                                <a href="<?php echo make_url('image', 'id=' . $id . '&act=makecoverimage&sid=' . $imagee['id']); ?>"  title="Click Here To Make it Cover Image"> <i class="fa fa-ban"></i>   </a>
                                            <?php }
                                            ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo make_url('image', 'id='  .$id . '&act=delete&sid=' . $imagee['id']);?>" onclick="return confirm('Are you sure? You want to delete this Image.');"  title="click here to delete this Image." ><i class="fa fa-trash"></i></a>
                                            
                                        </td>
                                            <!--<input type="number" class="text-center" name="position[<?php echo $image['id']; ?>]" value="<?php echo $imagee['position']; ?>" size="5" style="width:50%;" />-->
                                       

                                    </tr>

                                <?php } ?>
                                </tbody>
<!--                                <tfoot>
                                        <tr class="odd gradeX hidden-480">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="left" valign="middle" width="20%"><input type="submit" class="btn blue mt15 middle" name="submit_image_position" value="Update" /></td>
                                            <td></td>
                                        </tr> 
                                    </tfoot>-->
                            </table>
                        <?php } else { ?>
                            No Record Found
                        <?php } ?>

                    </div>

                </div><!--/tab-pane-->
                <!--/tab-pane-->


                <div class="tab-pane" id="edit">
                    <hr>

                </div>



            </div>
            <div class="tab-pane " id="image">

                <br>
                <br>


            </div><!--/tab-pane-->
        </div><!--/tab-pane-->
        </p></div>
    </div>
</div><!--/tab-content-->

</div><!--/col-9-->
</div><!--/row-->






