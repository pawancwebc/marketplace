<?php if (!empty($services_2)) { ?>

     <?php display_message(1); ?>
    <div class="container margin-bottom-35">
        <?php foreach ($services_2 as $key => $data) { ?>
            <h1><?= $data->name ?></h1>
            <div class="row">
                <div class="col-sm-3">
                    <div class="service_image">
                        <img src="<?php echo cwebc::getImage($data->image, 'service', 'big'); ?>" alt="<?php echo $data->name; ?>"/>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="col-sm-10">
                        <div class="service_verndor"><strong>Category:</strong> 
                            <?php
                            $QueryObj = new categoryService();
                            $rec = $QueryObj->getServiceCatNames($data->id);
                            if (!empty($rec)):
                                echo implode(', ', $rec);
                            endif;
                            ?>
                        </div>
                        <div class="service_verndor"><strong>Vendor:</strong> <?= $data->firstname . " " . $data->lastname ?></div>
                        <div class="service_verndor"><strong>Phone:</strong> <?= $data->phone ?></div>
                        <div class="service_price"><strong>Price:</strong> <?= CURRENCY_SYMBOL . number_format($data->price, 2) ?></div>
                        <div class="service_description"><?= limit_text(html_entity_decode(strip_tags($data->description)), 400) ?>...</div>
                        <div class="read_more text-right"><a href="<?= make_url('service_detail', 'id=' . $data->id) ?>">Read More..</a></div>

                    </div>
                    <div class="col-sm-2 text-right">
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php }else { ?>
    <div class="container margin-bottom-35">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1>No service found</h1>
            </div>
        </div>
    </div>

<?php } ?>


