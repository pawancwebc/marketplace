<?php display_message(1); ?>
<hr>
<div class="container g">
    <div class="row">
        <?php if ($user_details->user_type == 'vendor') { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a  href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a  href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('listservice'); ?>"> My Services</a></li>
                            <li><a href="<?php echo make_url('addservice'); ?>">Add new Service</a> </li>
                            <li> <a class="linked" href="<?php echo make_url('serviceorder'); ?>">Service Orders</a></li>
                            <li><a href="<?php echo make_url('support'); ?>">  Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
            </div><?php } else { ?>
            <div class="single-grids">
                <div class="col-md-5 single-grid-right">
                    <div class="recent">
                        <h4></h4>
                        <ul >
                            <li><a href="<?php echo make_url('account'); ?>">Account</a></li>
                            <li ><a href="<?php echo make_url('profile'); ?>"> Profile</a></li>
                            <li><a href="<?php echo make_url('myorder'); ?>"> My order</a></li>
                            <li><a href="<?php echo make_url('support'); ?>">Support</a></li>
                            <li><a href="<?php echo make_url('logout'); ?>">Logout</a></li>
                        </ul> 
                    </div>

                </div>
            </div>
        <?php } ?>
        <div class="col-md-7 single-grid-left">
            <h3></h3>
            <p>
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#home" data-toggle="tab">Services</a></li>
                <!--                                <li><a href="#status" data-toggle="tab">Status</a></li>
                                                <li><a href="#edit" data-toggle="tab">Edit</a></li>
                                                <li><a href="#image" data-toggle="tab">Image</a></li>-->
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <h4>Select service to know the order : </h4>
                            <ul>
                                <?php foreach ($list_service as $key => $listservice) { ?>
                                    </br>
                                    <li style="list-style: none;" class="order_serviceorder_page">
                                        <a  class="btn grey"  href="<?php echo make_url('detail', 'id=' . $listservice['id'] . '&act=detail'); ?>"><h2><?php echo $listservice['name']; ?></h2></a>
                                    </li>
                                    </br>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="col-md-2"></div>



                    </div><!--/tab-pane-->
                    <!--/tab-pane-->
                    <div class="tab-pane" id="edit">
                        <hr>
                    </div>
                </div>
                <div class="tab-pane " id="image">
                    <br>
                    <br>
                </div><!--/tab-pane-->
                </p>
            </div>
        </div><!--/tab-pane-->
    </div>
</div><!--/tab-content-->

</div><!--/col-9-->
</div><!--/row-->




