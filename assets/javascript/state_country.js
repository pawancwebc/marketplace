
$('#country_id').live('change',function(){ 
    var Country_id = $(this).val();
    var url = "php/ajax_state_country.php";
    var data = $(this).serialize();
	
    $.ajax({
       type: "POST",
       url: url,
       data: data,   
       success: function(data)
       {
            var options="";
            data = $.parseJSON(data);
			
           /*fill address fields with json*/
           options+='<option value="">Select State</option>'; 
            $.each(data, function(i, item) {
               options+='<option value="'+item['id']+'">'+item['name']+'</option>'; 
            });
            $("#state_id").find('option').remove().end().append(options);
       }
   });
});
var is_city = $('#is_city').val();
if(is_city=='1'){
    $('#state_id').live('change',function(){ 
        var state_id = $(this).val();
        var url = "php/ajax_state_country.php";
        var data = $(this).serialize();
       $.ajax({
           type: "POST",
           url: url,
           data: data,   
           success: function(data)
           {
                var options="";
                data = $.parseJSON(data);
				
               /*fill address fields with json*/
                options+='<option value="">Select City</option>'; 
                $.each(data, function(i, item) {
                   options+='<option value="'+item['id']+'">'+item['name']+'</option>'; 
                });
                $("#city_id").find('option').remove().end().append(options);
           }
       });
    });
}
