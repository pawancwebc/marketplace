var LayersliderInit = function () {

    return {
        initLayerSlider: function () {
            $('#layerslider').layerSlider({
                skinsPath : 'assets/css/slider-layer-slider/skins/',
                skin : 'fullwidth',
                thumbnailNavigation : 'hover',
                hoverPrevNext : true,
                responsive : true,
                responsiveUnder : 960,
                layersContainer : 960
            });
        }
    };

}();