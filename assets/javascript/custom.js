(function ($) {
	$('.change_user_type').on("change", function(){
		var type=$(this).val();
		if(type=='vendor'){
			set_loading('.extra_for_vendor');
			
			var url = "php/ajax.php?type=vendor_extra_form";
			$.ajax({
				   type: "POST",
				   url: url,
				   data: $('.qstAnsForm').serialize(),  
				   success: function(response){ 
						remove_loading('.extra_for_vendor');
						$('.extra_for_vendor').html(response);
				   }
				});			
		}
		else{
			$('.extra_for_vendor').html('');
		}
	});
})(jQuery);	

function set_loading(id){
	$(id).addClass('laoding');
	$(id).html('<img src="assets/img/LoaderIcon.gif"/>');
}
function remove_loading(id){
	$(id).removeClass('laoding');	
}