<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="cache-control" content="no-cache" />
        <?php echo head(isset($content) ? $content : ''); ?>
        <!-- Fonts START -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
            <!-- Fonts END -->
            <?php css($array = array('css/style', 'css/bootstrap', 'reset', 'components', 'style', 'style-responsive', 'font-awesome/css/font-awesome.min', 'bootstrap/css/bootstrap.min', 'fancybox/source/jquery.fancybox', 'slider-layer-slider/css/layerslider', 'style-layer-slider')); ?> 			
    </head>
    <!-- Head END -->

    <!-- Body BEGIN -->
    <body class="corporate">

        <!-- BEGIN TOP BAR -->
        <!-- END TOP BAR -->


        <!-- BEGIN HEADER -->
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'pre_header.php'); ?>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'top_nav.php'); ?>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'track.php'); ?>
        <!-- Header END -->



        <!-- BEGIN Content -->
        

        <?php include_once(DIR_FS_SITE_TEMPLATE . 'footer_bar.php'); ?>	




        <!-- Load javascripts at bottom, this will reduce page load time -->

        <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->


        <?php js($array = array('jquery.min', 'jquery-migrate.min', 'bootstrap.min', 'back-to-top', 'jquery.fancybox.pack'
            , 'jquery.slimscroll.min', 'owl.carousel.min', 'slider/greensock', 'slider/layerslider.transitions', 'slider/layerslider.kreaturamedia.jquery', 'slider/layerslider-init', 'layout'));
        ?>


        <script type="text/javascript">
            jQuery(document).ready(function () {
                Layout.init();
                LayersliderInit.initLayerSlider();
            });
        </script>
        <!-- END PAGE LEVEL JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>