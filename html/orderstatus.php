<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="cache-control" content="no-cache" />
        <style>


        </style>
        <?php echo head(isset($content) ? $content : ''); ?>
        <!-- Fonts START -->

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
            <!-- Fonts END -->
            <?php css($array = array('css/style', 'css/bootstrap', 'font-awesome/css/font-awesome.min', 'bootstrap/css/bootstrap.min', 'fancybox/source/jquery.fancybox')); ?> 			

    </head>
    <!-- Head END -->

    <!-- Body BEGIN -->
    <body class="corporate">
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'pre_header.php'); ?>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'top_nav.php'); ?>
        <!-- Header END -->

        <!-- BEGIN Content -->
        <div class="container margin-bottom-35">
            <?php display_form_error(); ?>
            <?php validation_form_error(); ?>

            <?php include_once(DIR_FS_SITE_TEMPLATE . 'orderstatus.php'); ?>

        </div>
        <!-- End Content -->	

        <?php include_once(DIR_FS_SITE_TEMPLATE . 'footer_bar.php'); ?>	
	 <?php include_once(DIR_FS_SITE_TEMPLATE.'footer_bar_2.php');?>	

        <!-- Load javascripts at bottom, this will reduce page load time -->

        <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->


        <?php
        //js($array=array('jquery.min','jquery-migrate.min','bootstrap.min','back-to-top','jquery.fancybox.pack'
//,'jquery.slimscroll.min','owl.carousel.min','layout'));
        ?>

        <?php
        js($array = array('jquery.min', 'jquery-migrate.min', 'bootstrap.min', 'back-to-top', 'jquery.fancybox.pack', 'plugins/moment', 'plugins/datetime', 'plugins/datepicker'
            , 'jquery.slimscroll.min', 'owl.carousel.min', 'layout', 'validation/jquery.validationEngine', 'validation/languages/jquery.validationEngine-en', 'custom', 'state_country'));
        ?>

        <!-- END PAGE LEVEL JAVASCRIPTS -->
        <script>
            $(document).ready(function () {
                $('#rootwizard').bootstrapWizard({'tabClass': 'nav nav-tabs'});
            });
        </script>
        <script type="text/javascript">
            $('#datetimepicker1').datetimepicker({
                minDate: moment()
            });

        </script>


        <script>
            
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({
        'transform': 'scale('+scale+')',
        'position': 'absolute'
      });
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
})

        </script>
    </body>
    <!-- END BODY -->
</html>