<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="cache-control" content="no-cache" />
        <?php echo head(isset($content) ? $content : ''); ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
            <?php css($array = array('css/style', 'css/bootstrap', 'reset', 'components', 'style', 'style-responsive', 'font-awesome/css/font-awesome.min', 'bootstrap/css/bootstrap.min', 'fancybox/source/jquery.fancybox', 'slider-layer-slider/css/layerslider', 'style-layer-slider')); ?> 			
    </head>
    <body class="corporate">
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'pre_header.php'); ?>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'top_nav.php'); ?>
        <?php display_message(1); ?>

        <div class="container">
            <?php if (!empty($new)) { ?>
                <div class="container margin-bottom-35">
                    <?php foreach ($new as $key => $data) { ?>

                        <h1><?= $data->name ?></h1>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="service_image">
                                    <img src="<?php echo cwebc::getImage($data->image, 'service', 'big'); ?>" alt="<?php echo $data->name; ?>"/>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="col-sm-10">
                                    <div class="service_verndor"><strong>Category:</strong> 
                                        <?php
                                        $QueryObj = new categoryService();
                                        $rec = $QueryObj->getServiceCatNames($data->id);
                                        if (!empty($rec)):
                                            echo implode(', ', $rec);
                                        endif;
                                        ?>
                                    </div>
                                    <div class="service_verndor"><strong>Vendor:</strong> <?= $data->firstname . " " . $data->lastname ?></div>
                                    <div class="service_verndor"><strong>Phone:</strong> <?= $data->phone ?></div>
                                    <div class="service_price"><strong>Price:</strong> <?= CURRENCY_SYMBOL . number_format($data->price, 2) ?></div>
                                    <div class="service_description"><?= limit_text(html_entity_decode(strip_tags($data->description)), 400) ?>...</div>
                                    <div class="read_more text-right"><a href="<?= make_url('service_detail', 'id=' . $data->id) ?>">Read More..</a></div>

                                </div>
                                <div class="col-sm-2 text-right">
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            <?php }else { ?>
                <div class="container margin-bottom-35">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h1>No service found</h1>
                        </div>
                    </div>
                </div>
            <?php } ?>	
        </div>

        <?php include_once(DIR_FS_SITE_TEMPLATE . 'footer_bar.php'); ?>	
	 <?php include_once(DIR_FS_SITE_TEMPLATE.'footer_bar_2.php');?>	
        <!-- Load javascripts at bottom, this will reduce page load time -->
        <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
        <?php
        js($array = array('jquery.min', 'jquery-migrate.min', 'bootstrap.min', 'back-to-top', 'jquery.fancybox.pack'
            , 'jquery.slimscroll.min', 'owl.carousel.min', 'slider/greensock', 'slider/layerslider.transitions', 'slider/layerslider.kreaturamedia.jquery', 'slider/layerslider-init', 'layout'));
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                Layout.init();
                LayersliderInit.initLayerSlider();
            });
        </script>
        <!-- END PAGE LEVEL JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>