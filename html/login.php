<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="cache-control" content="no-cache" />
        <?php echo head(isset($content) ? $content : ''); ?>
        <!-- Fonts START -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
            <!-- Fonts END -->
            <?php css($array = array('css/style', 'css/bootstrap', 'font-awesome/css/font-awesome.min', 'bootstrap/css/bootstrap.min', 'fancybox/source/jquery.fancybox', 'validationEngine.jquery')); ?> 			
    </head>
    <!-- Head END -->

    <!-- Body BEGIN -->
    <body class="corporate">
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'pre_header.php'); ?>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'top_nav.php'); ?>
        <!-- Header END -->



        <!-- BEGIN Content -->
        <div class="container">
            <?php display_form_error(); ?>
            <?php validation_form_error(); ?>
            <div class="col-md-12 banner-bottom-grid-right">
                <?php include_once(DIR_FS_SITE_TEMPLATE . 'login_box.php'); ?>
                 <?php include_once(DIR_FS_SITE_TEMPLATE . 'signup_box.php'); ?>
                <?php //include_once(DIR_FS_SITE_TEMPLATE . 'signup_box.php'); ?> 	

            </div>
        </div>
        <!-- End Content -->	


        <?php include_once(DIR_FS_SITE_TEMPLATE . 'footer_bar.php'); ?>	
	<?php include_once(DIR_FS_SITE_TEMPLATE . 'footer_bar_2.php'); ?>	




        <!-- Load javascripts at bottom, this will reduce page load time -->

        <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->


        <?php
        js($array = array('jquery.min', 'jquery-migrate.min', 'bootstrap.min', 'back-to-top', 'jquery.fancybox.pack'
            , 'jquery.slimscroll.min', 'owl.carousel.min', 'layout', 'validation/jquery.validationEngine', 'validation/languages/jquery.validationEngine-en', 'custom', 'state_country'));
        ?>
        <script>
            $(function () {
                $(".validation").validationEngine();
            });
        </script>

        <!-- END PAGE LEVEL JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>