<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="cache-control" content="no-cache" />
        <?php echo head(isset($content) ? $content : ''); ?>
        <!-- Fonts START -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
            <?php css($array = array('reset', 'components', 'style', 'css/style', 'css/bootstrap', 'font-awesome/css/font-awesome.min', 'bootstrap/css/bootstrap.min', 'fancybox/source/jquery.fancybox', 'style-layer-slider', 'slider-layer-slider/css/layerslider')); ?> 		
    </head>
    <body class="corporate">
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'pre_header.php'); ?>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'top_nav.php'); ?>
        <?php if (SLIDER_ACTIVE == '1'): ?>
            <?php include_once(DIR_FS_SITE_TEMPLATE . 'home_slider.php'); ?>
            <div class="container">
                <div class="col-md-12 banner-bottom-grid-right">
                    <?php if (is_object($homeContent)): ?>
                        <h3><?= $homeContent->name ?></h3>
                        <p> <?= html_entity_decode($homeContent->page) ?></p>
                    <?php endif; ?>	
                </div>
                <?php include_once(DIR_FS_SITE_TEMPLATE . 'product.php'); ?>	
            </div>
            <?php //include_once(DIR_FS_SITE_TEMPLATE.'map.php');?>	
        <?php endif; ?>
        <?php if (is_object($homeContent)): ?>
            <div class="container margin-bottom-35">
            </div>
        <?php endif; ?>	
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'map.php'); ?>	
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'footer_bar.php'); ?>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'footer_bar_2.php'); ?>	

        <?php
        js($array = array('jquery.min', 'jquery-migrate.min', 'bootstrap.min', 'back-to-top', 'jquery.fancybox.pack'
            , 'jquery.slimscroll.min', 'owl.carousel.min', 'slider/greensock', 'slider/layerslider.transitions', 'slider/layerslider.kreaturamedia.jquery', 'slider/layerslider-init', 'layout'));
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                Layout.init();
                LayersliderInit.initLayerSlider();
            });
        </script>
    </body>
</html>