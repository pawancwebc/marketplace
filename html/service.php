<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="cache-control" content="no-cache" />
        <?php echo head(isset($content) ? $content : ''); ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
            <?php css($array = array('css/style', 'css/bootstrap', 'reset', 'components', 'style', 'style-responsive', 'font-awesome/css/font-awesome.min', 'bootstrap/css/bootstrap.min', 'fancybox/source/jquery.fancybox', 'slider-layer-slider/css/layerslider', 'style-layer-slider')); ?> 			
    </head>
    <body class="corporate">
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'pre_header.php'); ?>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'top_nav.php'); ?>
        <?php display_message(1); ?>
        <div class="container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 style="text-align:center;">Please select category</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="[ col-xs-12 col-sm-offset-2 col-sm-8 ]" id="list-services-page">
                        <ul>
                            <?php
                            if (!empty($categories)) {
                                foreach ($categories as $key => $value) {
                                    ?>
                                    <li>
                                        <a class="btn grey" href="<?php echo make_url('category', 'id=' . $value->id . '&act=show'); ?><?php (in_array($value->id, $selected_cat)) ? 'selected' : '' ?>"><h2><?= $value->name ?></h2></a>
                                    </li><br>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'footer_bar.php'); ?>	
	 <?php include_once(DIR_FS_SITE_TEMPLATE.'footer_bar_2.php');?>	
        <!-- Load javascripts at bottom, this will reduce page load time -->
        <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
        <?php
        js($array = array('jquery.min', 'jquery-migrate.min', 'bootstrap.min', 'back-to-top', 'jquery.fancybox.pack'
            , 'jquery.slimscroll.min', 'owl.carousel.min', 'slider/greensock', 'slider/layerslider.transitions', 'slider/layerslider.kreaturamedia.jquery', 'slider/layerslider-init', 'layout'));
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                Layout.init();
                LayersliderInit.initLayerSlider();
            });
        </script>
        <!-- END PAGE LEVEL JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>