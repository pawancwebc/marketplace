<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta http-equiv="cache-control" content="no-cache" />
<?php echo head(isset($content)?$content:'');?>
<!-- Fonts START -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
<!-- Fonts END -->

<?php css($array=array('reset','components','style','style-responsive','font-awesome/css/font-awesome.min','bootstrap/css/bootstrap.min','fancybox/source/jquery.fancybox', ));?> 			
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">

    <!-- BEGIN TOP BAR -->
		<?php include_once(DIR_FS_SITE_TEMPLATE.'pre_header.php');?>
    <!-- END TOP BAR -->
	
	
    <!-- BEGIN HEADER -->
		<?php include_once(DIR_FS_SITE_TEMPLATE.'top_nav.php');?>
    <!-- Header END -->

    
	
	<!-- BEGIN Content -->
		<div class="container margin-bottom-35 text-center">
			<h1>404 Error</h1>
			<div class='error_404'>
				<img src="<?=DIR_WS_SITE_ASSET?>img/404.jpg"/>
			</div>		
		</div>
	<!-- End Content -->	
	
    
    <?php include_once(DIR_FS_SITE_TEMPLATE.'footer_bar.php');?>
 <?php include_once(DIR_FS_SITE_TEMPLATE.'footer_bar_2.php');?>		

   


   <!-- Load javascripts at bottom, this will reduce page load time -->
   
   <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    
	
<?php js($array=array('jquery.min','jquery-migrate.min','bootstrap.min','back-to-top','jquery.fancybox.pack'
,'jquery.slimscroll.min','owl.carousel.min','layout'));?>
	
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>