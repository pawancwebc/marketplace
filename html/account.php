<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="cache-control" content="no-cache" />
        <?php echo head(isset($content) ? $content : ''); ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
            <?php css($array = array('style', 'css/style', 'css/bootstrap', 'font-awesome/css/font-awesome.min', 'bootstrap/css/bootstrap.min', 'fancybox/source/jquery.fancybox')); ?> 			

    </head>
    <body class="corporate">
	<?php include_once(DIR_FS_SITE_TEMPLATE . 'pre_header.php'); ?>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'top_nav.php'); ?>
        <div class="container margin-bottom-35">
            <?php display_form_error(); ?>
            <?php validation_form_error(); ?>
            <?php include_once(DIR_FS_SITE_TEMPLATE . 'myaccount.php'); ?>
        </div>
        <?php include_once(DIR_FS_SITE_TEMPLATE . 'footer_bar.php'); ?>	
	 <?php include_once(DIR_FS_SITE_TEMPLATE.'footer_bar_2.php');?>	
        <?php
        js($array = array('jquery.min', 'jquery-migrate.min', 'bootstrap.min', 'back-to-top', 'jquery.fancybox.pack'
            , 'jquery.slimscroll.min', 'owl.carousel.min', 'layout'));
        ?>
        <script>
            $(document).ready(function () {
                $('#rootwizard').bootstrapWizard({'tabClass': 'nav nav-tabs'});
            });
        </script>
    </body>
</html>