<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta http-equiv="cache-control" content="no-cache" />
<style>
    
    
</style>
<?php echo head(isset($content)?$content:'');?>
<!-- Fonts START -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
<!-- Fonts END -->
            <?php css($array = array('css/style', 'css/bootstrap', 'font-awesome/css/font-awesome.min', 'bootstrap/css/bootstrap.min', 'fancybox/source/jquery.fancybox')); ?> 			

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">

    <!-- BEGIN TOP BAR -->
		
    <!-- END TOP BAR -->
	
    <!-- BEGIN HEADER -->
    <?php include_once(DIR_FS_SITE_TEMPLATE . 'pre_header.php'); ?>
		<?php include_once(DIR_FS_SITE_TEMPLATE.'top_nav.php');?>
    <!-- Header END -->
	
	<!-- BEGIN Content -->
		<div class="container margin-bottom-35">
			<?php display_form_error();?>
			<?php validation_form_error();?>
              
			<?php include_once(DIR_FS_SITE_TEMPLATE.'editlist.php');?>
		    
					
		</div>
	<!-- End Content -->	
    
    <?php include_once(DIR_FS_SITE_TEMPLATE.'footer_bar.php');?>
 <?php include_once(DIR_FS_SITE_TEMPLATE.'footer_bar_2.php');?>		

   <!-- Load javascripts at bottom, this will reduce page load time -->
   
   <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    
	
<?php js($array=array('jquery.min','jquery-migrate.min','bootstrap.min','back-to-top','jquery.fancybox.pack'
,'jquery.slimscroll.min','owl.carousel.min','layout'));?>
   
    <!-- END PAGE LEVEL JAVASCRIPTS -->
    <script>
        $(document).ready(function() {
  	$('#rootwizard').bootstrapWizard({'tabClass': 'nav nav-tabs'});
});
        </script>
</body>
<!-- END BODY -->
</html>