<?php

/* WEBSITE CONSTANTS */
$constants = new query('setting');
$constants->DisplayAll();
while ($constant = $constants->GetObjectFromRecord()):
    define("$constant->key", html_entity_decode($constant->value), true);
endwhile;

// @todo we add these constants in settings for user control 
define("SOFT_DELETE", true);

define('SHOW_GOOGLE_TRACKING_CODE_ON_ORDER', 1, true);
define('GOOGLE_CONVERSION_ID', 966767965, true);

define("EMAIL_NEW_RESIGTRATION_USER_CONFIRMATION", '1');
define("EMAIL_NEW_RESIGTRATION_ADMIN", '2');
define("EMAIL_VERIFY_ACCOUNT_USER", '3');
define("EMAIL_CONTACT_US_ADMIN", '4');
define("EMAIL_ORDER_SUCCESS_USER", '5');
define("EMAIL_ORDER_STATUS_ADMIN", '6');
define("EMAIL_FORGOT_PASSWORD", '7');
define("EMAIL_ACCOUNT_CREATED_FROM_ADMIN", '8');
define("EMAIL_LOW_STOCK_NOTIFICATION_ADMIN", '9');
define("EMAIL_PASSWORD_CHANGE_USER", '10');
define("EMAIL_NEWSLETTER_SUBSCRIBE_USER", '11');
define("EMAIL_NEWSLETTER_UNSUBSCRIBE_USER", '12');
define("EMAIL_ORDER_SHIPPING_USER", '13');
define("EMAIL_PASSWORD_UPDATE_ADMIN", '14');
define("EMAIL_ORDER_STATUS_UPDATE_SHIPPED", '15');
define("EMAIL_SHARE_WISHLIST", '25');
define("COMMON_HEADER", '26');
define("COMMON_FOOTER", '27');
define("EMAIL_NEW_RESIGTRATION_USER", '29');
define("EMAIL_RESEND_EMAIL_CONFIRMATION", '30');
define("EMAIL_USERNAME_UPDATE", '31');



$AllowedImageTypes = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$AllowedFileTypes = array('application/vnd.ms-excel');

/* new allowed photo mime type array. */
$conf_allowed_file_mime_type = array('text/plain', 'application/msword', 'application/rtf', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'applications/vnd.pdf', 'application/pdf');
$conf_allowed_photo_mime_type = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$conf_order_status = array('Received', 'Processing', 'Shipped', 'Delivered');

define("DOC_TYPE", '<!DOCTYPE html>', true);
define("DOC_LANGUAGE", "en", true);
define("DOC_CHAR_SET", 'utf-8', true);

$not_to_open_page = array();

$imageThumbConfig = array(    
    'supplier' => array(
        'thumb' => array('width' => '50', 'height' => '50'),
        'small' => array('width' => '190', 'height' => '140'),
        'medium' => array('width' => '458', 'height' => '458'),
        'big' => array('width' => '800', 'height' => '600')
    ),
    'banner' => array(
        'thumb' => array('width' => '50', 'height' => '50'),
        'small' => array('width' => '190', 'height' => '140'),
        'medium' => array('width' => '300', 'height' => '195'),
        'big' => array('width' => '800', 'height' => '600')
    ),
	'service' => array(
        'thumb' => array('width' => '50', 'height' => '50'),
        'small' => array('width' => '190', 'height' => '140'),
        'medium' => array('width' => '458', 'height' => '458'),
        'big' => array('width' => '800', 'height' => '600')
    ),
    'category' => array(
        'thumb' => array('width' => '50', 'height' => '50'),
        'small' => array('width' => '190', 'height' => '140'),
        'medium' => array('width' => '300', 'height' => '195'),
        'big' => array('width' => '800', 'height' => '600')
    ),
);


/** defining tables for status on - off in admin as per module name* */
$moduleTableForStatus = array(
    'brand' => 'brand',
    'templates' => 'formula_template',
	'category' => 'category',
    'user' => 'user',
    'manufacturer' => 'manufacturer',
    'banner' => 'banner',
    'blog' => 'blog',
    'blog_comment' => 'blog_comment',
    'payment_method' => 'payment_method',
    'option' => 'option',
    'navigation' => 'navigation',
    'navigations' => 'navigations'
);

/*
 * positions for banner on one page
 * also define height width for each position below
 */
$banner_locations_on_each_page1 = array(
    '1' => 'Top',
    '2' => 'Bottom',
    '3' => 'Left',
    '4' => 'Right',
    '5' => 'Landing Page'
);
/*
 * define height width for each position
 */
$banner_locations_on_each_page = array(
    'Top' => array(
        'width' => '100%',
        'height' => 'auto'
    ),
    'Bottom' => array(
        'width' => '100%',
        'height' => 'auto'
    ),
    'Left' => array(
        'width' => '100%',
        'height' => 'auto'
    ),
    'Right' => array(
        'width' => '100%',
        'height' => 'auto'
    ),
    'Landing_Page' => array(
        'width' => '100%',
        'height' => 'auto'
    ),
);

/*
 * sitemap priorities
 */
$xml_sitemap_priorities = array(
    '1' => 'Highest Priority',
    '0.9' => '',
    '0.8' => '',
    '0.7' => '',
    '0.6' => '',
    '0.5' => 'Medium Priority',
    '0.4' => '',
    '0.3' => '',
    '0.2' => '',
    '0.1' => 'Lowest Priority',
);

/*
 * modules for url rewrite
 */
$modules_for_url_rewrite = array(
    'product' => 'product',
    'category' => 'category',
    'content' => 'content',
    'brand' => 'brand',
    'blog' => 'blog'
);

/*
 * colors array
 */
$all_true_Colors_array = array(
    'ANTIQUE WHITE' => '#FAEBD7',
    'AQUA' => '#00FFFF',
    'AQUAMARINE' => '#7FFFD4',
    'AZURE' => '#F0FFFF',
    'BEIGE' => '#F5F5DC',
    'BISQUE' => '#FFE4C4',
    'BLACK' => '#000000',
    'BLANCHED ALMOND' => '#FFEBCD',
    'BLUE' => '#0000FF',
    'BLUE VIOLET' => '#8A2BE2',
    'BROWN' => '#A52A2A',
    'BURLY WOOD' => '#DEB887',
    'CADET BLUE' => '#5F9EA0',
    'CHARTREUSE' => '#7FFF00',
    'CHOCOLATE' => '#D2691E',
    'CORAL' => '#FF7F50',
    'CORN FLOWER BLUE' => '#6495ED',
    'CORN SILK' => '#FFF8DC',
    'CRIMSON' => '#DC143C',
    'CYAN' => '#00FFFF',
    'DARK BLUE' => '#00008B',
    'DARK CYAN' => '#008B8B',
    'DARK GOLDENROD' => '#B8860B',
    'DARK GRAY' => '#A9A9A9',
    'DARK GREEN' => '#006400',
    'DARK KHAKI' => '#BDB76B',
    'DARK MAGENTA' => '#8B008B',
    'DARK OLIVE GREEN' => '#556B2F',
    'DARK ORANGE' => '#FF8C00',
    'DARK ORCHID' => '#9932CC',
    'DARK RED' => '#8B0000',
    'DARK SALMON' => '#E9967A',
    'DARK SEA GREEN' => '#8FBC8F',
    'DARK SLATE BLUE' => '#483D8B',
    'DARK SLATE GRAY' => '#2F4F4F',
    'DARK TURQUOISE' => '#00CED1',
    'DARK VIOLET' => '#9400D4',
    'DEEP PINK' => '#FF1493',
    'DEEP SKY BLUE' => '#00BFFF',
    'DULL GRAY' => '#696969',
    'DODGER BLUE' => '#1E90FF',
    'FIRE BRICK' => '#B22222',
    'FLORAL WHITE' => '#FFFAF0',
    'FOREST GREEN' => '#228B22',
    'FUCHSIA' => '#FF00FF',
    'GAINSBORO' => '#DCDCDC',
    'GHOST WHITE' => '#F8F8FF',
    'GOLD' => '#FFD700',
    'GOLDENROD' => '#DAA520',
    'GRAY' => '#808080',
    'GREEN' => '#008000',
    'GREEN YELLOW' => '#ADFF2F',
    'HONEYDEW' => '#F0FFF0',
    'HOT PINK' => '#FF69B4',
    'INDIAN RED' => '#CD5C5C',
    'INDIGO' => '#4B0082',
    'IVORY' => '#FFFFF0',
    'KHAKI' => '#F0E68C',
    'LAVENDER' => '#E6E6FA',
    'LAVENDER BLUSH' => '#FFF0F5',
    'LEAF' => '    #6B8E23',
    'LEMON CHIFFON' => '#FFFACD',
    'LIGHT BLUE' => '#ADD8E6',
    'LIGHT CORAL' => '#F08080',
    'LIGHT CYAN' => '#E0FFFF',
    'LIGHT GOLDENROD YELLOW' => '#FAFAD2',
    'LIGHT GREEN' => '#90EE90',
    'LIGHT GRAY' => '#D3D3D3',
    'LIGHT PINK' => '#FF86C1',
    'LIGHT SALMON' => '#FFA07A',
    'LIGHT SEA GREEN' => '#20B2AA',
    'LIGHT SKY BLUE' => '#87CEFA',
    'LIGHT STEEL BLUE' => '#778899',
    'LIGHT YELLOW' => '#FFFFE0',
    'LIME' => '#00FF00',
    'LIME GREEN' => '#32CD32',
    'LINEN' => '#FAF0E6',
    'MAGENTA' => '#FF00FF',
    'MAROON' => '#800000',
    'MEDIUM AQUA MARINE' => '#66CDAA',
    'MEDIUM BLUE' => '#0000CD',
    'MEDIUM ORCHID' => '#BA55D3',
    'MEDIUM PURPLE' => '#9370DB',
    'MEDIUM SEA GREEN' => '#3CB371',
    'MEDIUM SLATE BLUE' => '#7B68EE',
    'MEDIUM SPRING BLUE' => '#00FA9A',
    'MEDIUM TURQUOISE' => '#48D1CC',
    'MEDIUM VIOLET RED' => '#C71585',
    'MIDNIGHT BLUE' => '#191970',
    'MINT CREAM' => '#F5FFFA',
    'MISTY ROSE' => '#FFE4E1',
    'NAVAJO WHITE' => '#FFDEAD',
    'NAVY' => '#000080',
    'OLD LACE' => '#FDF5E6',
    'OLIVE' => '#808000',
    'ORANGE' => '#FFA500',
    'ORANGE RED' => '#FF4500',
    'ORCHID' => '#DA70D6',
    'PALE GOLDENROD' => '#EEE8AA',
    'PALE GREEN' => '#98FB98',
    'PALE TURQUOISE' => '#AFEEEE',
    'PALE VIOLET RED' => '#DB7093',
    'PAPAYAWHIP' => '#FFEFD5',
    'PEACH PUFF' => '#FFDAB9',
    'PERU' => '#CD853F',
    'PINK' => '#FFC0CB',
    'PLUM' => '#DDA0DD',
    'POWDER BLUE' => '#B0E0E6',
    'PURPLE' => '#800080',
    'RED' => '#FF0000',
    'ROSY BROWN' => '#BC8F8F',
    'ROYAL BLUE' => '#4169E1',
    'SADDLE BROWN' => '#8B4513',
    'SALMON' => '#FA8072',
    'SANDY BROWN' => '#F4A460',
    'SEA GREEN' => '#2E8B57',
    'SEASHELL' => '#FFF5EE',
    'SIENNA' => '#A0522D',
    'SILVER' => '#C0C0C0',
    'SKY BLUE' => '#87CEEB',
    'SLATE BLUE' => '#6A5ACD',
    'SLATE GRAY' => '#708090',
    'SNOW' => '#FFFAFA',
    'SPRING GREEN' => '#00FF7F',
    'STEEL BLUE' => '#4682B4',
    'TAN' => '#D2B48C',
    'TEAL' => '#008080',
    'THISTLE' => '#D88FD8',
    'TOMATO' => '#FF6347',
    'TURQUOISE' => '#40E0D0',
    'VIOLET' => '#EE82EE',
    'WHEAT' => '#F5DEB3',
    'WHITE' => '#FFFFFF',
    'WHITE SMOKE' => '#F5F5F5',
    'YELLOW' => '#FFFF00',
    'YELLOW GREEN' => '#9ACD32'
);

/*
 * pages to be open with ssl
 */

$pages_to_be_open_with_ssl = array('checkout', 'account', 'login', 'register');

$navigationPages = array(
	'content' => array('Content Pages', TRUE),
    'service' => array('Service Page', TRUE),
	'contact' => array('Contact Us', TRUE),
    
);
?>