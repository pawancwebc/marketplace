<?php
/* Login messages.*/
define("MSG_LOGIN_INVALID_USERNAME_PASSWORD","Invalid <b>Username</b> or <b>Password</b>.",true);
define("MSG_LOGIN_WELCOME_MSG","Welcome to my account section.",true);
define("MSG_LOGIN_EMAIL_ADDRESS_ALREADY_EXIST","This email address already exists. Please try again.",true);
define("MSG_LOGIN_USERNAME_ALREADY_EXIST","This Username already exists. Please try again.",true);
define("MSG_LOGIN_WRONG_PASSWORD","Password does not match.",true);
define("MSG_LOGIN_EMPTY_PASSWORD","Password cannot be left empty.",true);
define("MSG_LOGIN_ALREADY_LOGGED_IN", "Sorry! you are already logged in.");
define("MSG_LOGIN_EMAIL_FORMATE", "Please use correct Email format <b>like: username@username.com</b>");
define("MSG_LOGIN_NOT_BLANK", "You cannot leave <b>email field</b> blank.<br/>You cannot leave <b>password field</b> blank.");
define("MSG_LOGIN_ELSE", "You cannot leave <b>email field</b> blank.<br/> Please use correct Email format <b>like: username@username.com</b>.<br/> You cannot leave <b>password field</b> blank.");
define("MSG_SEC_NOT_VALID", "Security code could not be validated. Please try again.");
define("FILL_REQUIRED_FIELDS","Please fill the required fields.");
define("ACCOUT_NOT_APPROVED","Account not approved yet.");

/* my account messages.*/
define("MSG_REGISTER_PASSWORDS_NOT_SAME","Password and confirm password should be same.",true);
define("MSG_ACCOUNT_PASSWORD_CHANGE_SUCCESS","Your password has been successfully changed.",true);
define("MSG_ACCOUNT_UPDATE_SUCCESS","Your account information has been updated successfully.",true);
define("MSG_ACCOUNT_RESEND","An confirmation email has been resend successfully.",true);
define("MSG_ACCOUNT_UPDATE_FAILED","Your account was not updated. Please try again.",true);
define("MSG_ACCOUNT_DEACTIVATED","Your account is not activated. Please contact site administrator.");
define("MSG_ACCOUNT_ALREADY_ACTIVE", "Your account is already active. Please <a href='".DIR_WS_SITE."?page=login'>click here</a> to login.");
define("MSG_ACCOUNT_MADE_ACTIVE", "Your account has been activated. An email containing your login details has been sent to your email address.");
define("MSG_ACCOUNT_MADE_ACTIVE_NO_MESSAGE", "Welcome! You successfully activated your account. You can now login with your email address and password and get started.");
define("MSG_ACCOUNT_NOT_MADE_ACTIVE", "Alas! some technical malfunction has taken place. Please try again.");
define("MSG_ACCOUNT_WRONG_USER_INFO", "You have used a wrong user information. If you are not a registred user yet. Please <a href='".DIR_WS_SITE."?page=register'>click here</a> to register.");
define("MSG_ACCOUNT_WRONG_URL", "You have used a wrong URL. If you are not a registred user yet. Please <a href='".DIR_WS_SITE."?page=login'>click here</a> to register.");
define("MSG_ACCOUNT_CONFIRM_EMAIL_RESEND_SUCCESS", "Confirmation email has been successfully resent to your registered email address.");
define("MSG_ACCOUNT_CONFIRM_EMAIL_NEW_RESEND_SUCCESS", "Confirmation email has been successfully sent to your new email address.");
define("MSG_ACCOUNT_EMAIL_ALREADY_EXISTS", "Sorry User with same username already exists , Please try with different username");
define("MSG_ERROR_OCCURRED_WHILE_ADDING_USER","Some error occurred while creating new account.");
define("MSG_EMAIL_CONFIRMATION_RESEND_EMAIL_SUCCESS", "Account verification email has been sent successfully.");
define("MSG_EMAIL_CONFIRMATION_RESEND_EMAIL_FAIL", "Error sending email, Please try again later.");
define("MSG_CURRNT_UPDATE_CAN_NOT_SAME","Please use a different email address.");
define("MSG_EMAIL_UPDATE_SUCCESS","Email has been updated successfully.");
define("MSG_EMAIL_NOT_VERIFIED","Your email not verified, please check your email.");

/*logout messages.*/
define("MSG_LOGOUT_SUCCESS","You have successfully <b>logged out</b>.",true);
/*registrations messages.*/
define("MSG_EMAIL_CONFIRMATION_SUCCESS","Email verfied successfully.");
define("MSG_REGISTR_FAILED", "Sorry! Registration process failed. Please try again.");
define("MSG_REGISTER_PASSWORDS_NOT_SAME", "Password and confirm password must be same.");
define("MSG_REGISTER_EMAIL_ALREADY_EXIST", "Account with the same email has been already registered.");
define("MSG_REGISTER_SUCCESS", "You details have been accepted.");
define("MSG_REGISTERSUCCESS_CONFIRMATION", "An email has been sent to your inbox to activate your account.");
define("MSG_REGISTERSUCCESS", "Account has been registered successfully.");
define("MSG_REGISTER_SUCCESS_PASSWORD_MAIL_SENT", "Registered successfully. Passowrd has been sent on your email id.");
/* forgot password messages.*/
define("MSG_FORGOT_PASSWORD_SUCCESS", "A link has been sent to your email address. Please click that link to a new password.");
define("MSG_FORGOT_PASSWORD_FAIL", "Your email address does not exist in our database.<br />If you are a new user. Please <a href='".DIR_WS_SITE."?page=register'>click here</a> to register.<br /> You can <a href='".DIR_WS_SITE."?page=forgotpassword'>click here</a> to try again.");
/* change password messages.*/
define("MSG_CHANGE_PASSWORD_SUCCESS", "Your password has been changed successfully.<br>Your new login details have been sent to your email address.");
define("MSG_CHANGE_PASSWORD_FAILED", "You have entered a wrong password.");
define("MSG_PASSWORD_RESET", "Your <b>account password</b> has been reset successfully. continue login with new password");
define("MSG_NOT_AUTHORIZED", "You are not <b>authorized</b> to do this.");
/*others.*/
define("MSG_ONLINE_ENQUIRY_SUCCESS", "Your online enquiry has successfully been submitted.");
define("MSG_FREE_SAMPLE_SENT_SUCCESS", "Thank you, Your free sample request has been sent. Our support staff will contact you soon.");
define("MSG_PAYMENT_SUCCESS","Your order has successfully been placed. You can track your order states by logging into your online account.<br/> Your login details have been emailed to you (in case you are a new customer). Please <a href='".DIR_WS_SITE."?page=login'><strong><u>click here</u></strong></a> to login.");
define("MSG_CUSTOM_SUCCESS","Thank you for shopping at ".SITE_NAME.".Your order will be processed shortly.");
define("MSG_CONTACTUS","Your request have been submitted successfully.");
define("MSG_CONTACTUS_COMPLETE_INFO","Please enter compalete information");
define("OPERATION_PERFORM_SUCCESS","Operation has been performed successfully.");
/*cart messages.*/
define("MSG_CART_OUT_OF_STOCK", 'Sorry! This item is currently out of stock.');
define("MSG_CART_ALREADY_IN_CART", 'This item is already in the cart.');

/*shopping messages.*/
define("MSG_SHOP_CATEGORY_NO_PRODUCT_FOUND","Sorry! There is no product in this category.",true);
define("MSG_SHOP_ORDER_COMPLETE_SUCCESS","You have successfully completed your order.",true);


define("MSG_ERROR_PLACING_ORDER","An error occurred while placing order. Please try again.");
define("MSG_PAYMENT_METHOD_NOT_APPLICABLE","Payment Method not applicable.");
define("MSG_SELECT_PAYMENT_METHOD_FIRST","Select Payment Method First.");

define("MSG_UPDATE_PAYMENT_METHOD_SUCCESS","Payment method updated successfully.");
define("MSG_ERROR_UPDATING_PAYMENT_MOETHOD","Error updating payment method. Please try again.");


/* website control panel messages.*/
define("MSG_ADMIN_UPDATE_SUCCESS","Record updated successfully",true);
define("MSG_ADMIN_DELETE_SUCCESS","Record deleted successfully",true);
define("MSG_ADMIN_ADDITION_SUCCESS","Record added successfully",true);
define("MSG_ADMIN_PERMISSION_DENIED","You donot have permission to access the page.",true);
define("MSG_REVIEW_SUCCESS_FOR_USER", "Your review has been submitted and will be added as soon as it's been approved. Thanks");
define("MSG_BLOG_COMMENT_SUCCESS_FOR_USER", "Your blog comment has been submitted and will be added as soon as it's been approved. Thanks");
define("MSG_REVIEW_FAIL_FOR_USER", "Please login before submitting your review");
define("MSG_BLOG_FOR_USER", "You have successfully submit your Responce");
define("MSG_PRODUCT_QNT_UPD", "Product Quantity has been updated");
define("MSG_CART_PICKUP_TIME", "you can pick up your order on friday between 2PM-6PM");
define("MSG_CART_NO_SHIPCODE", "Sorry! shipping  code could not be applied");
define("MSG_CART_YES_SHIPCODE", "The shipping cost has been applied.");
define("MSG_CART_NO_VOUCHER_CODE", "Sorry! Voucher code could not be applied");
define("MSG_CART_YES_VOUCHER_CODE", "Voucher code has been applied");
define("MSG_CART_DEL_VOUCHER_CODE", "Voucher code sucessfully deleated");
define("MSG_CART_ADD_WISHLIST_PRODUCT_FAIL", "You Need to be login to add product in wishlist");
define("MSG_CART_ADD_WISHLIST_PRODUCT_SUCESS", "all Product's' added to wishlist");
define("MSG_VALID_CAPTCHA_CODE_FAIL", "The entered code was not correct, Please try again");
define("MSG_CHECKOUT_INFO_REQUIRE", 'Please select the "postal code" to calculate shipping costs');
define("MSG_MYACCOUNT_NEWSLATTER_SUBSCRIBE", "Thank you for Newsletter subscription");
define("MSG_MYACCOUNT_NEWSLATTER_UNSUBSCRIBE", "Your Newletter Unsubscription request accepted");
define("MSG_MYACCOUNT_EMAIL_MATCH_FALSE", "Your Email & Confirm Email does not match");
define("MSG_WISHLIST_ADD_PRODUCT_SUCESS", "Product added to wish list, continue shopping");
define("MSG_WISHLIST_ADD_MODAL_SUCESS", "Model has added to wish list, continue shopping");
define("MSG_CART_ADD_PRODUCT_SUCESS", "Products added to Cart from order, if they are available, continue shopping");
define("MSG_CART_ADD_PRODUCT_FAIL", "Sorry, Products could not be added to cart from order by some resion");
define("MSG_WISHLIST_PRODUCT_ADD_ALREADY", "Sorry! Product already added to your Wishlist");
define("MSG_WISHLIST_MODAL_ADD_ALREADY", "Sorry! This Model already added to your Wishlist");
define("MSG_WISHLIST_PRODUCT_UPDATE", "Wish List has been updated");
define("MSG_WISHLIST_ERROR_OCCURED", "Some error occurred while updating your wishlist.");
define("MSG_MYACCOUNT_USER_LOGIN_REQUIRE", "Sorry! You Need to Login");
define("MSG_NEWSLETTER_USER_REQUIRE", "Sorry! please select one of the subscription request");
define("MSG_NEWSLETTER_USER_SUCCESS", "Thanks, your request is accepted ");
define("MSG_CODE_USED", "You cannot continue with this coupon code");
define("MSG_ADDRESS_UPDATE_SUCCESS", "Address successfully update in your address book");
define("MSG_ADDRESS_NEW_SUCCESS", "Address successfully added to your address book");
define("MSG_ADDRESS_DELETE_SUCCESS", "Address successfully deleted in your address book");
define("MSG_SOMETHING_WENT_WRONG","Something went wrong. Please try again.");
define("MSG_INCORRECT_KEY","Incorrect key");
define("MSG_ADD_ATLEAST_ONE","Sorry, but you have to enter at least 1 of this product to add it too your cart");
define("MSG_PRODUCT_NOT_EXISTS","Product doesn't exists.");
define("MSG_INVALID_QUANTITY_VALUE","Invalid quantity value.");
define("MSG_ERROR_OCCURRED_UPDATE_CART","Error occurred while updating cart.");
define("MSG_ITEM_DELETED_FROM_CART_SUCCESS","Product removed from cart");
define("MSG_ITEM_ADDED_IN_CART_SUCCESS","Product has been added in cart successfully.");
define("MSG_CART_UPDATED_SUCCESS","Cart has been successfully updated.");
define("MSG_PRODUCT_ALREADY_IN_CART","Product already exists in cart.");

/*forgot password*/
define("MSG_PASSWORD_RESET_MAIL_SUCCUSS","Password reset mail has been sent to your email id. Please check your email.");
define("MSG_ACCOUNT_WITH_EMAIL_NOT_EXIST","Account not exists with the given email id.");
define("MSG_WRONG_EMAIL_FORMAT","Wrong email format.");

/* */
define("MSG_ADDRESS_ADD_SUCCESS","New address has been added successfully.");
define("MSG_ADDRESS_ADD_UNSUCCESS","Error occurred while adding new account. Please try again.");
define("MSG_WRONG_ADDRESS_SELECTED","Invalid Address Details.");
define("MSG_ADDRESSBOOK_UPDATED","Your address book has been updated successfully.");

define("MSG_COMMENT_ADD_FAIL","Error occurred while adding new comment, please try again.");
define("MSG_COMMENT_ADD_SUCCESS","Comment pending for admin approval.");
define("MSG_NEWSLETTER_ALREADY_SUBSCRIBED","Already subscribed for newsletter.");

define("MSG_ERROR_STOCK_REQUEST","Error occurred while submitting stock request.");

/*coupon messages*/
define("MSG_ENTER_COUPON_CODE","Please enter coupon code.");
define("MSG_INVALID_COUPON_CODE","Sorry, This coupon is not valid or has been expired.");
define("MSG_USER_LOGIN_COUPON","Please login to apply this coupon.");
define("MSG_USER_NOT_COUPON","Sorry, this coupon is not for you.");
define("MSG_ALREADY_USED_COUPON","Sorry, You have already used coupon.");
define("MSG_LESS_AMOUNT_COUPON","Sorry, Coupon is not applicable on this amount.");
define("MSG_LESS_QUANTITY_COUPON","Sorry, Coupon is not applicable on this quantity.");
define("MSG_NOT_APPLICABLE_COUPON","Sorry, Coupon is not applicable on cart items.");
define("MSG_APPLICABLE_COUPON","Coupon has been applied successfully.");
define("MSG_REMOVE_COUPON","Coupon has been removed successfully.");
define("MSG_COUNRY_INVALID_COUPON","Sorry, Coupon is not valid for this country.");
define("MSG_ERROR_FREE_GIFT_COUPON","Sorry, error in adding free gift.");

/* 
 * ADMIN MESSAGES
 */
define("MSG_SELECT_ATLEAST_ONE_FOR_SEARCH","Sorry, Please input atleast one parameter for search.");
define("MSG_SELECT_ATLEAST_ONE_FOR_OPERATION","Sorry, Please select atleast one item for operation.");
define("MSG_PRODUCT_REMOVE_SUCCESS"," product(s) has been removed successfully");
define("MSG_PRODUCT_ADD_SUCCESS"," product(s) has been added successfully");
define("MSG_ERROR_OCCURRED_ADDING_NEW_PRODUCT_IN_CATEGORY","Error occurred while adding product on category.");
define("MSG_ERROR_OCCURRED_REMOVING_PRODUCT_FROM_CATEGORY","Error occurred while removing product from category.");
?>