<?php
@session_start();
if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
    ob_start('ob_gzhandler');
else
    ob_start();


ob_start(); 
ob_flush();

error_reporting(E_ALL ^ E_DEPRECATED);


#error_reporting(E_ALL ^ E_DEPRECATED);

#session reassigning using cookie here.
if(isset($_COOKIE['user_unique_key']) && $_COOKIE['user_unique_key']!=''):
    session_id($_COOKIE['user_unique_key']);
endif;
###############################################################################################
############################     Website developed 
###############################################################################################

/* Password update days */
if(!defined("PASSWORD_CHANGE_REQUIRED_AFTER_DAYS")):
    define("PASSWORD_CHANGE_REQUIRED_AFTER_DAYS", '30',true);
endif;

/* Set Website Filesystem */
if(!defined("DIR_FS")):
    define("DIR_FS", $_SERVER['DOCUMENT_ROOT'].'/',true);
endif;
if(!defined("DIR_FS_SITE")):
    define("DIR_FS_SITE", dirname(dirname(dirname(__FILE__))).'/',true);
endif;

define('CURRENT_SITE_FOLDER_NAME',basename(DIR_FS_SITE));

define("DIR_FS_SITE_MAIN",DIR_FS_SITE);
//include_once(DIR_FS_SITE_MAIN.'/include/config/server_config.php');

/*include External File which include the function to check the site expire or block*/
require_once(DIR_FS_SITE_MAIN."include/config/connection.php");

/*get current full url */
$current_url=HTTP_SERVER.$_SERVER['REQUEST_URI'];
define("CURRENT_URL", $current_url);

///*************** Table Prefix **********************/
if(!defined("TABLE_PREFIX")):
    define("TABLE_PREFIX", "cwebc_");
endif;
/****************** Admin Folder Name ****************/
define("ADMIN_FOLDER",'admin');
define("MINIMUM_LENGTH_ADMIN_PASSWORD",'6');
 

/********** theme settings ************************/
define('CURRENT_THEME', 'default');
define('THEME_PATH',DIR_WS_SITE.'theme/'.CURRENT_THEME,true);

$hostName = str_replace("www.","", $_SERVER['HTTP_HOST']); 
define("EMAIL_ID_FROM_EMAIL",'help@'.$hostName);


//free shipping after
define("FREE_SHIPPING_ORDER_MINIMUM_LIMIT",'69');

/* 
 * *******************************************************
 *        DO NOT MAKE ANY CHANGE BELOW THIS LINE 
 * *******************************************************
 */
date_default_timezone_set('America/Los_Angeles');

/* include sub-configuration files here */
require_once(DIR_FS_SITE."include/config/url.php");

/* include the database class files */
require_once(DIR_FS_SITE_INCLUDE_CLASS."mysql.php");
require_once(DIR_FS_SITE_INCLUDE_CLASS."query.php");

/* include the utitlity files here */
require_once(DIR_FS_SITE_INCLUDE_CLASS."phpmailer.php");
require_once(DIR_FS_SITE_INCLUDE_CONFIG."constant.php");


/* custom files */
include_once(DIR_FS_SITE_INCLUDE_CLASS.'login_session.php');
include_once(DIR_FS_SITE_INCLUDE_CLASS.'admin_session.php');

/* include functions here */
include_once(DIR_FS_SITE_INCLUDE_FUNCTION.'date.php');

/* shopping cart */
//include_once(DIR_FS_SITE_INCLUDE_CLASS.'currency.php');
//include_once(DIR_FS_SITE_INCLUDE_CLASS.'language.php');
//include_once(DIR_FS_SITE_INCLUDE_CLASS.'cart_new.php');
//include_once(DIR_FS_SITE_INCLUDE_CLASS."wish-list.php");

/* include function files here */
include_once(DIR_FS_SITE.'include/function/basic.php');
require_once(DIR_FS_SITE_INCLUDE_CONFIG."message.php");
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/urlRewriteClass.php');
include_once(DIR_FS_SITE.'include/functionClass/imageManipulationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/fileClass.php');
include_once(DIR_FS_SITE.'include/functionClass/dateClass.php');
include_once(DIR_FS_SITE.'include/functionClass/emailClass.php');
include_once(DIR_FS_SITE_INCLUDE_CLASS.'errorClass.php');
include_once(DIR_FS_SITE_INCLUDE_CLASS."validation_u.php");
include_once(DIR_FS_SITE_INCLUDE_CLASS."validation_p.php");

include_once(DIR_FS_SITE_INCLUDE.'dompdf/dompdf_config.inc.php');      
include_once(DIR_FS_SITE_INCLUDE.'functionClass/searchIndexClass.php');
include_once(DIR_FS_SITE_INCLUDE.'functionClass/dictionaryClass.php');

include_once(DIR_FS_SITE_INCLUDE.'functionClass/userClass.php');


//google api key
$api_key = 'AIzaSyAuoAi2AHHjHgE4i50p7e0kzXNU8yvddJU';
?>