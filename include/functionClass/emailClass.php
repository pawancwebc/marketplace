<?php

/*
 * Email Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class email {

    function SendEmail($Subject, $ToEmail, $FromEmail, $FromName, $Message, $Bcc = "", $Format = "html", $FileAttachment = false, $AttachmentFileName = "", $IS_SMTP = true) {
	$this->send_email($Subject, $ToEmail, $FromEmail, $FromName, $Message, $Bcc = "", $Format = "html", $FileAttachment = false, $AttachmentFileName = "", $IS_SMTP = true);
    }

    function send_email($Subject, $ToEmail, $FromEmail, $FromName, $Message, $Bcc = "", $Format = "html", $FileAttachment = false, $AttachmentFileName = "", $IS_SMTP = true) {

	$mail = new PHPMailer();

	if (IS_SMTP == 'SMTP') {
	    $mail->IsSMTP();	    // send via SMTP
	    $mail->SMTPDebug = 2;		     // enables SMTP debug information (for testing)
	    $mail->SMTPSecure = (SMTP_SECURITY != '') ? SMTP_SECURITY : '';  // SSL - sets the prefix to the servier
	    $mail->Host = SMTP_HOST;      // "smtp.gmail.com" sets GMAIL as the SMTP server
	    $mail->Port = SMTP_PORT;      // 465 - set the SMTP port for the GMAIL server
	    $mail->Username = trim(SMTP_USERNAME);  // "rocky.developer001@gmail.com" GMAIL username
	    $mail->Password = trim(SMTP_PASSWORD); //"deve#001";    
	    $mail->SMTPAuth = (SMTP_AUTH) ? true : false;     // turn on SMTP authentication	
	}

	$mail->From = $FromEmail;
	$mail->FromName = $FromName;
	$mail->AddAddress(trim($ToEmail), trim($ToEmail));

	if (!is_array($Bcc) && $Bcc != $ToEmail):
	    $MyBccArray = explode(",", $Bcc);
	    foreach ($MyBccArray as $Key => $Value) {
		if (trim($Value) != "")
		    $mail->AddBCC("$Value");
	    }
	endif;

	if ($Format == "html")
	    $mail->IsHTML(true);			       // send as HTML
	else
	    $mail->IsHTML(false);			       // send as Plain

	$mail->Subject = $Subject;
	$mail->Body = $Message;
	//$mail->AltBody  =  $Message;
	if ($FileAttachment)
	    $mail->AddAttachment($AttachmentFileName, basename($AttachmentFileName));
	if (!$mail->Send()) {
	    //echo "Message was not sent <p>";
	    //echo "Mailer Error: " . $mail->ErrorInfo;
	    //exit;
	}
	return true;
    }

    function send_db_email($email_id, $to_email = ADMIN_EMAIL, $array = array()) {
	$object = get_object('email', $email_id);
	$content = $object->email_text;
	$subject = $object->email_subject;
	if (is_object($object)):
	    if (count($array)):
		foreach ($array as $k => $v):
		    $literal = '{' . trim(strtoupper($k)) . '}';
		    $content = html_entity_decode(str_replace($literal, $v, $content));
		    $subject = str_replace($literal, $v, $subject);
		endforeach;
	    endif;
	    //print $content;exit;
	    if ($this->SendEmail($subject, $to_email, EMAIL_ID_FROM_EMAIL, SITE_NAME, $content, ADMIN_EMAIL, 'html', false, '', true)):
		return true;
	    else:
		return false;
	    endif;
	else:
	    return false;
	endif;
    }

}

?>