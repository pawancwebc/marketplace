<?php

/*
 * Navigation Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class navigation extends cwebc {

    function __construct() {
        parent::__construct('navigations');
        $this->requiredVars = array('id', 'name', 'is_active', 'is_deleted');
    }

    /*
     * Get List of all navigations in array
     */

    function ListNavigations() {
        $this->Where = "where is_deleted='0'";
        $this->DisplayAll();
    }

    function CheckNavigationStatus($id) {
        $object = $this->getObject($id);
        if ($object->is_active == '1'):
            return true;
        else:
            return false;
        endif;
    }

}

class navigationItem extends cwebc {

    function __construct() {
        parent::__construct('navigation');
        $this->requiredVars = array('id', 'parent_id', 'nav_id', 'nav_class', 'is_mega_menu', 'navigation_title', 'navigation_link', 'position', 'is_active', 'navigation_query', 'is_external', 'open_in_new', 'is_deleted', 'date_add');
    }

    function setParentId($id) {
        $this->parent_id = $id;
    }

    function setNavId($id) {
        $this->nav_id = $id;
    }

    function getAllItems() {
        $this->Where = "where parent_id='$this->parent_id' AND nav_id='" . mysql_real_escape_string($this->nav_id) . "' AND is_deleted='0' order by $this->orderby $this->order";
        $this->DisplayAll();
    }

    /* Functions For Front */

    function getNavigation($nav_id, $parent_id) {
        $navigation = array();
        $this->Where = "where parent_id='$parent_id' AND nav_id='" . mysql_real_escape_string($nav_id) . "' AND is_active='1' AND is_deleted='0'  order by $this->orderby $this->order";
        $navigation = $this->ListOfAllRecords('object');
        return $navigation;
    }

    function getFullNavigation($nav_id, $parent_id, $get_all = FALSE) {
        $navigation = array();
        $object = new navigationItem();
        $navigation = $object->getNavigation($nav_id, $parent_id);

        if (count($navigation)):
            foreach ($navigation as $kk => $vv):

                $query_sub_nav = new navigationItem();
                $sub_nav = $get_all === TRUE ? $query_sub_nav->getFullNavigation($nav_id, $vv->id, TRUE) : $query_sub_nav->getNavigation($nav_id, $vv->id);

                if (count($sub_nav)):
                    $vv->sub_nav = $sub_nav;
                endif;
            endforeach;
        endif;
        return $navigation;
    }

    function getTopNavigation() {
        $nav = array();
        $check_nav = new navigation();
        if ($check_nav->CheckNavigationStatus('1')):
            $query_nav = new navigationItem();
            $nav = $query_nav->getFullNavigation('1', '0'); /* --Navigation id=2 and Parent id=0-- */
        endif;
        return $nav;
    }

    function getTopNavigation2() {
        $nav = array();
        $check_nav = new navigation();
        if ($check_nav->CheckNavigationStatus('16')):
            $query_nav = new navigationItem();
            $nav = $query_nav->getFullNavigation('16', '0'); /* --Navigation id=2 and Parent id=0-- */
        endif;
        return $nav;
    }

    function getTopNavigation3() {
        $nav = array();
        $check_nav = new navigation();
        if ($check_nav->CheckNavigationStatus('17')):
            $query_nav = new navigationItem();
            $nav = $query_nav->getFullNavigation('17', '0'); /* --Navigation id=2 and Parent id=0-- */
        endif;
        return $nav;
    }

    function getTopNavigation4() {
        $nav = array();
        $check_nav = new navigation();
        if ($check_nav->CheckNavigationStatus('20')):
            $query_nav = new navigationItem();
            $nav = $query_nav->getFullNavigation('20', '0'); /* --Navigation id=2 and Parent id=0-- */
        endif;
        return $nav;
    }

    function getTopNavigation5() {
        $nav = array();
        $check_nav = new navigation();
        if ($check_nav->CheckNavigationStatus('21')):
            $query_nav = new navigationItem();
            $nav = $query_nav->getFullNavigation('21', '0'); /* --Navigation id=2 and Parent id=0-- */
        endif;
        return $nav;
    }

    function getMegaMenus() {
        $this->Where = "where parent_id='0' AND is_active='1' AND is_deleted='0' AND is_mega_menu='1' order by $this->orderby $this->order";
        $data = $this->ListOfAllRecords('object');
        if (!empty($data)) {
            $nav_obj = new navigation;
            foreach ($data as $key => $navigation) {
                if ($navigation->nav_id) {
                    $data[$key]->nav_id = $nav_obj->getObject($navigation->nav_id);
                }
            }
        }
        return $data;
    }

}

?>