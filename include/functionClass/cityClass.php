<?php
/*
 * city Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class city extends cwebc {
    
    protected $orderby;
    protected $order;
    protected $requiredVars;
    
    /*
     * 
     */
    
    function __construct($order='desc', $orderby='name'){
        parent::__construct('city');
            $this->orderby=$orderby;
            $this->order=$order;
            $this->requiredVars=array('id', 'country_id','state_id', 'name', 'is_active','is_deleted','date_add');
    }

    /*
     * Create new city or update existing city
     */
    function saveCity($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active']=isset($POST['is_active'])?"1":"0";
        if(isset($this->Data['id']) && $this->Data['id']!=''){
       
            if($this->Update())
              return $Data['id'];
        }
        else{
          
            $this->Insert();
            return $this->GetMaxId();
        }
    }
        
    /*
     * Get city by id
     */
    function getCity($id){
        $this->Where=" where `id` = $id";
        return $this->DisplayOne();
    }
    
    
    /*
     * Get List of all city
     */
    function listCity(){
            $query = new query('city as cty');
            $query->Field=" ct.short_name as country,st.name as state,cty.* ";
            $query->Where=" left join ".TABLE_PREFIX."country as ct on cty.country_id=ct.id";
            $query->Where.=" left join ".TABLE_PREFIX."state as st on cty.state_id=st.id";
            $query->Where.=" where cty.is_deleted='0' order by cty.name asc";
            return $query->ListOfAllRecords();        
    }
    /*
     * get all state name by country id for ajax
     */
    function getCityNameByState($state_id){
        $this->Field = "id,name";
        $this->Where = " where state_id='$state_id' and is_deleted='0' and is_active='1' order by name asc";
        return $this->ListOfAllRecords();
    }
    /*
     * get all city id by name
     */
    function getCityIdByName($city,$country_id=''){
        $this->Field = "id";
        $this->Where = " where lower(name)='".strtolower($city)."'";
        if($country_id!=''):
            $this->Where .= " and country_id='$country_id'";
        endif;
        $object =  $this->DisplayOne();
        if(is_object($object)):
            return $object->id;
        else:
            return false;
        endif;
    }
}
?>