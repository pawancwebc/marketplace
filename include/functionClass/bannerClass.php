<?php
/*
 * Banner Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class banner extends cwebc {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($table = 'banner', $order = 'desc', $orderby = 'date_add') {
        parent::__construct($table);
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'code', 'description', 'url', 'image','banner_title', 'text', 'is_active',
            'is_deleted', 'date_add', 'date_upd', 'open_in_new_tab', 'image_alt_text');
    }

    /*
     * Create new page or update existing page
     */

    function saveBanner($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['code'] = $this->_sanitize($this->Data['name']);

        if ($this->Data['image_alt_text'] == '') {
            $this->Data['image_alt_text'] = $this->_sanitize($this->Data['name']);
        }

        $this->Data['is_active'] = isset($POST['is_active']) ? $POST['is_active'] : "0";
        $this->Data['open_in_new_tab'] = isset($POST['open_in_new_tab']) ? "1" : "0";

        $rand = rand(0, 99999999);
        $image_obj = new imageManipulation();

        if ($image_obj->upload_photo('banner', $_FILES['image'], $rand)):

            $this->Data['image'] = $image_obj->makeFileName($_FILES['image']['name'], $rand);

        endif;

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update()):
                return $id;
            endif;
        }
        else {
            $this->Data['date_add'] = date("Y-m-d H:i:s");
            if ($this->Insert()):
                return $this->GetMaxId();
            endif;
        }
        return false;
    }

    /*
     * Get Banner by id
     */

    function getBanner($id) {
        $this->Where = " where id = '$id'";
        return $this->DisplayOne();
    }


    function listBanner($active = false, $type = 'row', $main_fields = false) {

        if ($main_fields):
            $this->Field = "id,name,is_active,is_deleted";
        endif;

        if ($active):
            $this->Where = "where is_active='1' and is_deleted='0' order by $this->orderby $this->order";
        else:
            $this->Where = "where is_deleted='0' order by $this->orderby $this->order";
        endif;
		
        if ($type == 'row'):
            return $this->DisplayAll();
		elseif($type == 'object'):
            return $this->ListOfAllRecords('object');	
        else:
            return $this->ListOfAllRecords();
        endif;
    }

    /*
     * delete a page by id
     */

    function deleteBanner($id) {
        $this->id = $id;
        return $this->SoftDelete();
    }

    /* empty image value */

    function removeImage($id, $remove_mobile = FALSE) {
        if ($remove_mobile === TRUE) {
            $this->Data['mobile_image'] = '';
        } else {
            $this->Data['image'] = '';
        }
        $this->Data['id'] = $id;
        return $this->Update();
    }

    /*
     * get banners of particular page
     */



}

?>