<?php
/*
 * url rewrite Module Class - 
 * You are not adviced to make edits into this class.
 *   
 */

class url_rewrite extends cwebc {
    
    protected $orderby;
    protected $order;
    protected $requiredVars;
    
    /*
     * 
     */
    
    function __construct($order='desc', $orderby='id'){
        parent::__construct('url_rewrite');
            $this->orderby=$orderby;
            $this->order=$order;
            $this->requiredVars=array('id', 'page','module', 'module_id','urlname','date_add','date_upd');
    }

    
    /* sanitize funtion*/
    public static function urlSanitize($value){
            $prohibited_chars=array(" - $","--"," - "," / ","- ","  "," ", "/", "$", "&", "'", '%', '"', "@", "");
            foreach($prohibited_chars  as $k=>$v):
                    $value=str_replace($v, '-', $value);
            endforeach;
            return strtolower($value);
    }

    /*
     * list of module's all url rewrites
     */
    function listAllUrlRewrites($module){
        $query = new query('url_rewrite as cur');
        $query -> Field = "cur.*,module.name as name";
        $query -> Where .= "left join ".TABLE_PREFIX.$module." as module on cur.module='".$module."' and cur.module_id=module.id";
        $query -> Where .=" where module = '$module'"; 
        $query -> Where .=" order by date_upd desc";
        return $query->ListOfAllRecords('object');
        
    }
    
    /*
     * get url rewrite
     */
    function getUrl($id){
        $this->Where=" where id='$id'";
        return $this->DisplayOne();
    }
    
    /*check urlname in new insert*/
    public static function checkUrlnameExists($url_id,$urlname,$module='',$module_id='')
    {
       if($module!='' && $module_id!=''):
          $url_id = url_rewrite::getUrlId($module, $module_id);
       endif; 
        
       $query = new url_rewrite();
       $query->Field="id,urlname";  
       $query->Where="where urlname='$urlname'";
       
       if($url_id && $url_id!='' && $url_id!='0'):
           $query->Where.="and id!='$url_id'";
       endif;
       
       $query->DisplayAll();
        if($query->GetNumRows()):
            return true;
        else:
            return false;
        endif;
    }
        
    /*
     * Create new urlname or update existing urlname
     */
    public static function updateUrlRewrite($module,$module_id,$urlname='',$name='',$page=''){

        $query = new url_rewrite();
        $added_urlname = $query -> checkUrlNameAdded($module,$module_id);
        
        if($urlname==''):
            $urlname=url_rewrite::urlSanitize($name);
        endif;
        
        $urlname_update_required = false;
        if(is_object($added_urlname) && url_rewrite::checkUrlnameExists($added_urlname->id,$urlname)):
           $urlname_update_required = true;
        elseif(url_rewrite::checkUrlnameExists('',$urlname)):
           $urlname_update_required = true;
        endif;
        
        $newurlname = $urlname;
        if($urlname_update_required):
            if(is_object($added_urlname)):
                $newurlname = $urlname.'-'.$added_urlname->module_id; 
            else:
                $newurlname = $module.'-'.$urlname; 
            endif;
            if(url_rewrite::checkUrlnameExists('',$newurlname)):
                if($module_id!=''):
                    $newurlname = $newurlname.'-'.$module_id;
                else:
                    $newurlname = $newurlname.'-'.rand(0,1000);
                endif;
            endif;
        endif;

        if(is_object($added_urlname) && $added_urlname->urlname!=$urlname):

            $queryObj = new url_rewrite();
            return $queryObj ->updateUrl($module,$module_id,$newurlname,$page);
        else:

            $queryObj = new url_rewrite();
            return $queryObj ->insertUrl($module,$module_id,$newurlname,$page);
        endif;
        return;
    }
        
    /*
     * check url name added for module
     */
    function checkUrlNameAdded($module,$module_id){
        $this->Where=" where module = '$module' and module_id='$module_id'";
        $object = $this->DisplayOne();
        if(is_object($object)):
            return $object;
        endif;
        return false;
    }
    /*
     * update url
     */
    function updateUrl($module,$module_id,$urlname,$page){
        $this->Data['urlname'] = $urlname;
        $this->Data['page'] = $page;
        $this->Where=" where module='$module' and module_id='$module_id'";
        return $this->UpdateCustom();
    }
    /*
     * insert url
     */
    function insertUrl($module,$module_id,$urlname,$page){
        $this->Data['module'] = $module;
        $this->Data['module_id'] = $module_id;
        $this->Data['page'] = $page;
        $this->Data['urlname'] = $urlname;
        $this->Data['date_add'] = date('Y-m-d H:i:s');
        return $this->Insert();
    }
    /*
     * get url name
     */
    public static function getUrlname($module,$module_id){
        $query = new url_rewrite();
        $query->Where=" where module='$module' and module_id='$module_id'";
        $object = $query->DisplayOne();
        if(is_object($object)):
            return $object->urlname;
        endif;
        return false;
    }
    /*
     * get url name
     */
    public static function getUrlId($module,$module_id){
        $query = new url_rewrite();
        $query->Where=" where module='$module' and module_id='$module_id'";
        $object = $query->DisplayOne();
        if(is_object($object)):
            return $object->id;
        endif;
        return false;
    }
    
    /*
     * get url by urlname
     */
    public static function getUrlByUrlName($urlname){
        $query = new url_rewrite();
        $query -> Where =" where lower(urlname)='".strtolower(urldecode($urlname))."' ";
        return $query -> DisplayOne();
    }
    
    /*
     * delete url rewrite entry
     */
    public static function deleteUrlRewriteEntry($module,$module_id){
        $query = new url_rewrite();
        $query -> Where=" where module='$module' and module_id='$module_id'";
        return $query ->Delete_where();
    }
}
?>