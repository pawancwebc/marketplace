<?php

/*
 * Content Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class content extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('content');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'urlname', 'page', 'short_description', 'page_name', 'meta_keyword', 'meta_description', 'position', 'is_deleted', 'publish_date', 'parent_id', 'image', 'meta_robots_index',
            'meta_robots_follow', 'include_xml_sitemap', 'sitemap_priority', 'canonical');
    }

    /*
     * Create new page or update existing page
     */

    function savePage($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        /*
          if($this->Data['page_name']==''){
          $this->Data['page_name']=create_meta_title($this->Data['name'],'page','content');
          }

          if($this->Data['meta_keyword']==''){
          $this->Data['meta_keyword']=$this->Data['name'];
          }

          if($this->Data['meta_description']==''){
          $this->Data['meta_description']=$this->Data['name'];
          }
         */
        if ($this->Data['urlname'] == '') {
            $this->Data['urlname'] = $this->_sanitize($this->Data['name']);
        }

        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                return $Data['id'];
        }
        else {

            if ($this->Insert()) {
                $new_id = $this->GetMaxId();
                cwebc::concatenateIdWithSlug('content', $new_id);
                return $new_id;
            } else {
                return false;
            }
        }
    }

    /*
     * Get page by id
     */

    function getPage($id) {
        $this->Where = " where `id` = $id";
        return $this->DisplayOne();
    }

    /*
     * Get List of all pages in array
     */

    function listPages($get_all = TRUE) {
        if ($get_all === FALSE) {
            $this->Field = 'id,name';
        }
        $this->Where = "where is_deleted='0' order by $this->orderby $this->order";
        $this->DisplayAll();
    }

    /*
     * delete a page by id
     */

    function deletePage($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    /*
     * Update page position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

    function getPageContent($id) {
        return html_entity_decode($this->getPage($id)->page);
    }

    function getPageTitle($id) {
        return $this->getPage($id)->page_name;
    }

    function getPageSlug($id) {
        return $this->getPage($id)->urlname;
    }

}

?>