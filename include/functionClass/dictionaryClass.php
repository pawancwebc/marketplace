<?php
class dictionary_synonym extends cwebc
{
    protected $orderby;
    protected $order;
    protected $requiredVars;
    
    function __construct($table='synonym',$order='desc', $orderby='id'){
        parent::__construct($table);
            $this->orderby=$orderby;
            $this->order=$order;
            $this->requiredVars=array('id','keywords','is_deleted','date_add','date_upd');
    }
    function listSynonym(){
        $this->Where=" where is_deleted='0' order by id desc";
        return $this->DisplayAll();
    }
    
    function saveSynonym($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        //$this->print=1;
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            $data_id = $this->Data['id'];
            if($this->Update())
              return $data_id;
        }
        else{
            $this->Data['date_add']=date("Y-m-d H:i:s");
            $this->Insert();
            return $this->GetMaxId();
        }
        return false;
    }
    
    function getSynonym($id){
        $this->Where=" where `id` = $id";
        return $this->DisplayOne();
    }
    
    /*
     * delete a entry by id
     */
    function deleteSynonym($id){
        $this->id=$id;
        return $this->SoftDelete();
    }
    /* search for keywords */
    function searchSynonym($keyword){
        $this -> Where=" where is_deleted='0' and LOWER(keywords) like '%$keyword%'";
        return $this -> DisplayOne();
    }

    function getSynonyms($keyword_array){
        $synonyms = array();
        foreach($keyword_array as $kk=>$vv):
            $QuerySynom = new dictionary_synonym();
            $synonym = $QuerySynom -> searchSynonym($vv);
            if($synonym):
                unset($keyword_array[$kk]);
                $synonym_array = explode(',',$synonym->keywords);
                foreach($synonym_array as $kk=>$vv):
                    $keyword_array[] = $vv;
                endforeach;
            endif;
        endforeach;
        return $keyword_array;
    }
}

class dictionary_hypernym extends cwebc
{
    protected $orderby;
    protected $order;
    protected $requiredVars;
    
    function __construct($table='hypernym',$order='desc', $orderby='id'){
        parent::__construct($table);
            $this->orderby=$orderby;
            $this->order=$order;
            $this->requiredVars=array('id','name','keywords','is_deleted','date_add','date_upd');
    }
    
    function listHypernym(){
        $this->Where=" where is_deleted='0'";
        return $this->DisplayAll();
    }
    
    function saveHypernym($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            $data_id = $this->Data['id'];
            if($this->Update())
              return $data_id;
        }
        else{
            $this->Data['date_add']=date("Y-m-d H:i:s");
            $this->Insert();
            return $this->GetMaxId();
        }
        return false;
    }
    
    function getHypernym($id){
        $this->Where=" where `id` = $id";
        return $this->DisplayOne();
    }
    
    /*
     * delete a entry by id
     */
    function deletehypernym($id){
        $this->id=$id;
        return $this->SoftDelete();
    }
    
    /* search for keywords */
    function searchHyperNym($keyword){
        $this -> Where=" where is_deleted='0' and LOWER(name) like '%$keyword%'";
        return $this -> DisplayOne();
    }
    
    function getHyperNyms($keyword_array){
        $hypernyms = array();
        foreach($keyword_array as $kk=>$vv):
            $QueryHyper = new dictionary_hypernym();
            $hypernym = $QueryHyper -> searchHyperNym($vv);
            if($hypernym):
                $hypernym_array = explode(',',$hypernym->keywords);
                foreach($hypernym_array as $kk=>$vv):
                    $keyword_array[] = $vv;
                endforeach;
            endif;
        endforeach;
        return $keyword_array;
    }
}
?>