<?php

/*
 * FileHandling Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class file {

    private $FileMimeTypes;
    private $FileImportMimeTypes;
    private $AudioMimeTypes;

    function __construct() {
        global $conf_allowed_import_file_mime_type;
        global $conf_allowed_file_mime_type;
        global $conf_allowed_audio_mime_type;
        $this->FileMimeTypes = $conf_allowed_file_mime_type;
        $this->FileImportMimeTypes = $conf_allowed_import_file_mime_type;
        $this->AudioMimeTypes = $conf_allowed_audio_mime_type;
        $this->errorMsg = new errorManipulation();
    }

    function uploadDocument($to_upload_path, $file, $random='') {

        $upload_path = DIR_FS_SITE_UPLOAD . 'file/';
        $upload_path.=$to_upload_path . '/';

        // echo $upload_path;exit;

        $allowed_mime_types = $this->FileMimeTypes;
        if ($file['error'] == 0):
            if (in_array($file['type'], $allowed_mime_types)):

                $filename = $this->makeFileName($file['name'], $random);

                if (move_uploaded_file($file['tmp_name'], $upload_path . $filename)):
                    return 1;
                else:
                    $this->errorMsg->errorAdd("unable to upload!!");
                    return false;
                endif;
            else:

                $this->errorMsg->errorAdd("Wrong file format.");

                return false;
            endif;
        else:
            $error = $this->file_upload_error_codes($file['error']);
            //$this->errorMsg->errorAdd($error);
            return false;
        endif;
        return 1;
    }

    function uploadImportDocument($to_upload_path, $file, $random='') {

        $upload_path = DIR_FS_SITE_UPLOAD . 'file/';
        $upload_path.=$to_upload_path . '/';

        // echo $upload_path;exit;

        $allowed_mime_types = $this->FileImportMimeTypes;
        if ($file['error'] == 0):
            if (in_array($file['type'], $allowed_mime_types)):

                $filename = $this->makeFileName($file['name'], $random);
                if (move_uploaded_file($file['tmp_name'], $upload_path . $filename)):
                    return 1;
                else:
                    $this->errorMsg->errorAdd("unable to upload!!");
                    return false;
                endif;
            else:

                $this->errorMsg->errorAdd("Wrong file format.");

                return false;
            endif;
        else:
            $error = $this->file_upload_error_codes($file['error']);
            //$this->errorMsg->errorAdd($error);
            return false;
        endif;
        return 1;
    }

    function uploadAudio($to_upload_path, $file, $random='') {

        $upload_path = DIR_FS_SITE_UPLOAD . 'audio/';
        $upload_path.=$to_upload_path . '/';

        $allowed_mime_types = $this->AudioMimeTypes;

        if ($file['error'] == 0):
            if (in_array($file['type'], $allowed_mime_types)):
                $filename = $this->makeFileName($file['name'], $random);
                if (move_uploaded_file($file['tmp_name'], $upload_path . $filename)):
                    return 1;
                else:
                    $this->errorMsg->errorAdd("unable to upload!!");
                    return false;
                endif;
            else:
                $this->errorMsg->errorAdd("Wrong file format.");
                return false;
            endif;
        else:
            $error = $this->file_upload_error_codes($file['error']);
            //$this->errorMsg->errorAdd($error);
            return false;
        endif;
        return 1;
    }

    function file_upload_error_codes($code) {
        switch ($code):
            case '1':#UPLOAD_ERR_INI_SIZE
                return 'File size limit exceeds. Max file size: 2MB.';
                break;
            case '2': #UPLOAD_ERR_FORM_SIZE
                return 'Max file size limit set in page has crossed.';
                break;
            case '3': #UPLOAD_ERR_PARTIAL 
                return 'File was only partially uploaded';
                break;
            case '4': #UPLOAD_ERR_NO_FILE
                return 'No file was uploaded';
                break;
            default: return 'No Message Found!';
        endswitch;
    }

    function download_file($file) {
        if (!is_file($file)) {
            die("<b>404 File not found!</b>");
        }


        //Gather relevent info about file
        $len = filesize($file);
        $filename = basename($file);
        $file_extension = strtolower(substr(strrchr($filename, "."), 1));

        //This will set the Content-Type to the appropriate setting for the file
        switch ($file_extension) {
            case "pdf": $ctype = "application/pdf";
                break;
            case "exe": $ctype = "application/octet-stream";
                break;
            case "zip": $ctype = "application/zip";
                break;
            case "doc": $ctype = "application/msword";
                break;
            case "xls": $ctype = "application/vnd.ms-excel";
                break;
            case "csv": $ctype = "application/vnd.ms-excel";
                break;
            case "ppt": $ctype = "application/vnd.ms-powerpoint";
                break;
            case "gif": $ctype = "image/gif";
                break;
            case "png": $ctype = "image/png";
                break;
            case "jpeg":
            case "jpg": $ctype = "image/jpg";
                break;
            case "mp3": $ctype = "audio/mpeg";
                break;
            case "wav": $ctype = "audio/x-wav";
                break;
            case "mpeg":
            case "mpg":
            case "mpe": $ctype = "video/mpeg";
                break;
            case "mov": $ctype = "video/quicktime";
                break;
            case "avi": $ctype = "video/x-msvideo";
                break;

            //The following are for extensions that shouldn't be downloaded (sensitive stuff, like php files)
            case "php":
            case "htm":
            case "html":
            case "txt":die("<b>Cannot be used for " . $file_extension . " files!</b>");
                break;

            default: $ctype = "application/force-download";
        }
        ob_clean();
        //Begin writing headers
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Description: File Transfer");

        //Use the switch-generated Content-Type
        header("Content-Type: $ctype");

        //Force the download
        $header = "Content-Disposition: attachment; filename=" . $filename . ";";
        header($header);
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . $len);
        @readfile($file);
        exit;
    }

    function backupFile($backup_file, $MemberArray) {

        $fp = fopen($backup_file, "w");
        foreach ($MemberArray as $Value)
            fputs($fp, $Value . "\n");

        fclose($fp);
    }

    function makeFileName($name, $id) {
        $file_name_parts = explode('.', $name);
        $file_name_parts['0'].=$id;
        $file = $file_name_parts['0'] . '.' . $file_name_parts['1'];
        return $file;
    }

    function human_filesize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }

    function get_file_size($filename, $size_in = 'MB') {
        $size_in_bytes = filesize($filename);

        // Precision: decimals at the end for each type of size

        if ($size_in == 'B') {
            $size = $size_in_bytes;
            $precision = 0;
        } elseif ($size_in == 'KB') {
            $size = (($size_in_bytes / 1024));
            $precision = 2;
        } elseif ($size_in == 'MB') {
            $size = (($size_in_bytes / 1024) / 1024);
            $precision = 2;
        } elseif ($size_in == 'GB') {
            $size = (($size_in_bytes / 1024) / 1024) / 1024;
            $precision = 2;
        }

        $size = round($size, $precision);

        return $size . ' ' . $size_in;
    }

    /* function to delete a directory, if directory containing files */
    function delete_directory($dirname) {
        if (is_dir($dirname))
            $dir_handle = opendir($dirname);
        if (!$dir_handle)
            return false;
        while ($file = readdir($dir_handle)) {
            if ($file != "." && $file != "..") {
                if (!is_dir($dirname . "/" . $file))
                    unlink($dirname . "/" . $file);
                else
                    $this->delete_directory($dirname . '/' . $file);
            }
        }
        closedir($dir_handle);
        rmdir($dirname);
        return true;
    }

    function clear_cache_of_module($module_name) {

        global $module_cache_folder;

        $module_cache_folders = array();
        if (isset($module_cache_folder[$module_name])):

            $module_cache_folders = $module_cache_folder[$module_name];

            foreach ($module_cache_folders as $k => $module):

                /* delete from cache folder */
                $module_cache_path = DIR_FS_SITE_SMARTY_CACHE . CURRENT_THEME . '/' . $module;
                $this->delete_directory($module_cache_path);

                /* delete from compiled folder */
                $module_compiled_path = DIR_FS_SITE_SMARTY_COMPILED . CURRENT_THEME . '/' . $module;
                $this->delete_directory($module_compiled_path);
            endforeach;

        endif;
    }

    /* generate sitemap */
    public static function write_xml($url, $priority = 0.5, $changefreq='daily') {
        date_default_timezone_set('Asia/Calcutta');
        global $handle, $counter, $file_number, $filename;

        if ($counter == LINKS_SITEMAP_PER_PAGE) {
            $file_number++;
            $counter = 0;
            // Attach end of file, and close it here.
            fwrite($handle, "</urlset>\n");
            fclose($handle);
        }

        if ($counter == 0) {
            // Open next file here.
            if ($file_number == 0) {
                $filename = "../sitemap.xml";
            } else {
                $filename = "../sitemap" . $file_number . ".xml";
            }

            $handle = fopen($filename, "w+");
            fwrite($handle, '<?xml version="1.0" encoding="UTF-8"?>' . "\n");
            fwrite($handle, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n");
        }
        if ($url != "" && $url != "END") {
            fwrite($handle, " <url>\n");
            fwrite($handle, "  <loc>" . str_replace('&', '&amp;', $url) . "</loc>\n");
            fwrite($handle, "  <priority>$priority</priority>\n");
            fwrite($handle, "  <changefreq>$changefreq</changefreq>\n");
            fwrite($handle, " </url>\n");
        }
        if ($url == "END") {
            fwrite($handle, "</urlset>\n");
            fclose($handle);

            /* update fields */
            $QueryObj = new query('setting');
            $QueryObj->Where = ("where `key`='SITEMAP_PATH'");
            $QueryObj->Data['value'] = DIR_WS_SITE . 'sitemap.xml';
            $QueryObj->UpdateCustom();

            $QueryObj = new query('setting');
            $QueryObj->Where = ("where `key`='LAST_SITEMAP_DATE'");
            $QueryObj->Data['value'] = date("Y-m-d H:i:s");
            $QueryObj->UpdateCustom();

            if ($file_number >> 0) {
                $filename_index = "../sitemap_index.xml";
                $handle_index = fopen($filename_index, "w+");
                fwrite($handle_index, '<?xml version="1.0" encoding="UTF-8"?>' . "\n");
                fwrite($handle_index, '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n");
                while ($file_number >= 0) {
                    fwrite($handle_index, '<sitemap>' . "\n");
                    fwrite($handle_index, '<loc>' . "\n");
                    if ($file_number == 0) {
                        $file_name = DIR_WS_SITE . 'sitemap.xml';
                        $file_fs_name = DIR_FS_SITE . 'sitemap.xml';
                    } else {
                        $file_name = DIR_WS_SITE . 'sitemap' . $file_number . '.xml';
                        $file_fs_name = DIR_FS_SITE . 'sitemap' . $file_number . '.xml';
                    }
                    fwrite($handle_index, $file_name . "\n");
                    fwrite($handle_index, '</loc>' . "\n");
                    if (file_exists($file_fs_name)) {
                        $file_modify_time = date("Y-m-d", filemtime($file_fs_name));
                        fwrite($handle_index, '<lastmod>' . "\n");
                        fwrite($handle_index, $file_modify_time . "\n");
                        fwrite($handle_index, '</lastmod>' . "\n");
                    }
                    fwrite($handle_index, '</sitemap>' . "\n");
                    $file_number--;
                }
                fwrite($handle_index, '</sitemapindex>' . "\n");
                fclose($handle_index);
                /* update fields */
                $QueryObj = new query('setting');
                $QueryObj->Where = ("where `key`='SITEMAP_PATH'");
                $QueryObj->Data['value'] = DIR_WS_SITE . 'sitemap_index.xml';
                $QueryObj->UpdateCustom();
            }
        }
        // Increment counter for every URL.
        $counter++;
    }

    public static function listFolderFiles($dir, $module, $return = false) {

        $dir = rtrim($dir, '/');

        $all_files = scandir($dir);

        $output = '';
        $output .= '<ul>';

        $folders = $files = '';
        foreach ($all_files as $file_name) {
            if ($file_name != '.' && $file_name != '..') {

                $file_type = self::getFileType($file_name);
                $file_icon = self::getFileIcon($file_type);

                if (is_dir($dir . '/' . $file_name)) {
                    $folders .= '<li class="directory" dir_path="'.$dir.'/'.$file_name.'" dir_name="'.$file_name.'">';
                    $folders .= $file_name;
                    $folders .= self::listFolderFiles($dir . '/' . $file_name, $module, true);
                    $folders .= '</li>';
                } else {
                    $files .= '<li class="files" file_path="' . $dir . '"';
                    $files .= ' file_name="' . $file_name . '" ';
                    $files .= ' module="' . $module . '" ';
                    $files .= ' data-jstree=' . "'" . '{ "icon" : "' . $file_icon . '" }' . "'" . '>';
                    $files .= $file_name;
                    $files .= '</li>';
                }
            }
        }

        $output .= $folders;
        $output .= $files;

        $output .= '</ul>';

        if ($return):
            return $output;
        else:
            echo $output;
        endif;
    }

    public static function getFileType($file) {

        return substr($file, strrpos($file, '.') + 1);
    }

    public static function getFileIcon($file_type) {
        $icon = "icon-exclamation-sign";

        if ($file_type == 'tpl' || $file_type == 'txt') {
            $icon = "icon-file-text";
        } elseif ($file_type == 'css') {
            $icon = "icon-css3";
        } elseif ($file_type == 'js') {
            $icon = "icon-link";
        } elseif ($file_type == 'html') {
            $icon = "icon-html5";
        } elseif ($file_type == 'jpeg' || $file_type == 'jpg' || $file_type == 'png' || $file_type == 'gif') {
            $icon = "icon-picture";
        }

        return $icon;
    }

    public static function fileWrite($file_name, $data) {
        
        if($data==''):
            $data = ' ';
        endif;
        
        $write = false;

        $fp = fopen($file_name, "w");

        $write = fwrite($fp, $data);

        fclose($fp);

        return $write;
    }

    public static function createFile($dir_path,$file_name){

        $myfile = fopen($dir_path.'/'.$file_name, "w");
        fwrite($myfile, '');
        fclose($myfile);
        
        return true;
    }
}

?>