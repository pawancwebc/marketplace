<?php
/*
 * state Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class state extends cwebc {
    
    protected $orderby;
    protected $order;
    protected $requiredVars;
    
    /*
     * 
     */
    
    function __construct($order='asc', $orderby='name'){
        parent::__construct('state');
            $this->orderby=$orderby;
            $this->order=$order;
            $this->requiredVars=array('id', 'country_id', 'name', 'iso_code', 'is_active','is_deleted','date_add');
    }

    /*
     * Create new state or update existing state
     */
    function saveState($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active']=isset($POST['is_active'])?"1":"0";
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            $id = $this->Data['id'];
            if($this->Update())
              return $id;
        }
        else{
            if($this->Insert())
              return $this->GetMaxId();
        }
        return false;
    }
        
    /*
     * Get state by id
     */
    function getState($id){
        $this->Where=" where `id` = $id";
        return $this->DisplayOne();
    }
    
    function getStatebyName($Name){
        $this->Where=" where `name` = '$Name'";
        return $this->DisplayOne();
    }
    
    
    /*
     * Get List of all state in array
     */
    function listState(){
            $query = new query('state as st');
            $query->Field=" ct.short_name as country,st.* ";
            $query->Where=" left join ".TABLE_PREFIX."country as ct on st.country_id=ct.id";
            $query->Where.=" where is_deleted='0' order by name asc";
            return $query->ListOfAllRecords();        
    }
    /*
     * get all state name by country id for ajax
     */
    function getStateNameByCountry($country_id){
        $this->Field = "id,name";
        $this->Where = " where country_id='$country_id' and is_deleted='0' and is_active='1' order by name asc";
        return $this->ListOfAllRecords();
    }
    /*
     * get all state id by name
     */
    function getStateIdByName($state,$country_id=''){
        $this->Field = "id";
        $this->Where = " where lower(name)='".strtolower($state)."'";
        if($country_id!=''):
            $this->Where .= " and country_id='$country_id'";
        endif;
        $object =  $this->DisplayOne();
        if(is_object($object)):
            return $object->id;
        else:
            return false;
        endif;
    }
}
?>