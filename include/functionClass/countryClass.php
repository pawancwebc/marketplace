<?php
/*
 * country Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class country extends cwebc {
    
    protected $orderby;
    protected $order;
    protected $requiredVars;
    
    /*
     * 
     */
    
    function __construct($order='asc', $orderby='short_name'){
        parent::__construct('country');
            $this->orderby=$orderby;
            $this->order=$order;
            $this->requiredVars=array('id', 'iso2', 'short_name', 'long_name', 'iso3','numcode', 'calling_code', 'flag','contains_states', 'need_zip_code', 'is_active','date_add');
    }

    /*
     * Create new country or update existing country
     */
    function saveCountry($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active']=isset($POST['is_active'])?"1":"0";
        if(isset($this->Data['id']) && $this->Data['id']!=''){
       
            if($this->Update())
              return $Data['id'];
        }
        else{
          
            $this->Insert();
            return $this->GetMaxId();
        }
    }
        
    /*
     * Get country by id
     */
    function getCountry($id){
        $this->Where=" where `id` = $id";
        return $this->DisplayOne();
    }
    
    
    /*
     * Get List of all country in array
     */
    function listCountry(){
            $this->Where="order by $this->orderby $this->order";
            return $this->DisplayAll();        
    }
    
    
    
    /*get country from iso2*/
    
    public static function getCountryFromISO($iso){
        $query =new country();
        $query->Where=" where `iso2` = '$iso'";

        return $query->DisplayOne();
    }
    
     /*
     * get country id by name
     */
    function getCountryIdByName($country){
        $this->Field = "id";
        $this->Where = " where lower(short_name)='".strtolower($country)."'";
        $object =  $this->DisplayOne();
        if(is_object($object)):
            return $object->id;
        else:
            return false;
        endif;
    }
    
     /*
     * get country id by name
     */
    function getCountryISO2ByName($country){
        $this->Field = "iso2";
        $this->Where = " where lower(short_name)='".strtolower($country)."'";
        $object =  $this->DisplayOne();
        if(is_object($object)):
            return $object->iso2;
        else:
            return false;
        endif;
    }
    
    
    
}
?>