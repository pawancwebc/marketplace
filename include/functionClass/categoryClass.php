<?php

/*
 * Category Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class category extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('service_cat');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'image', 'description', 'position', 'is_active', 'is_deleted', 'date_add', 'last_update_dt');
    }

    /*
     * Create new Category or update existing Category
     */
    
    function saveCategorys($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($POST['is_active']) ? $POST['is_active'] : "0";
        $rand = rand(0, 99999999);
        $image_obj = new imageManipulation();
        if ($image_obj->upload_photo('category', $_FILES['image'], $rand)) {
            $this->Data['image'] = $image_obj->makeFileName($_FILES['image']['name'], $rand);
        }
        
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Update();
        } else {
            if ($this->Insert()) {
                return $this->GetMaxId();
            }
        }
        return false;
    }


    function saveCategory($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        $this->Data['is_active'] = isset($POST['is_active']) ? $POST['is_active'] : "0";
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $data_id = $this->Data['id'];
            if ($this->Update()):
                return $data_id;
            endif;
        }
        else {
            $this->Data['date_add'] = date("Y-m-d H:i:s");
            if ($this->Insert()) {
                $new_id = $this->GetMaxId();
                return $new_id;
            }
        }
        return false;
    }

    /*
     * Get Category by id
     */

    function getCategory($id) {
        $this->Where = " where `id` = $id";
        return $this->DisplayOne();
    }

    /*
     * Get List of all Category in array with parent id
     */

    function listCategory($show_active = false) {
        if ($show_active):
            $this->Where = "where is_active='1' and is_deleted='0' order by $this->orderby $this->order, id desc";
        else:
            $this->Where = "where is_deleted='0' order by $this->orderby $this->order, id desc";
        endif;

        return $this->ListOfAllRecords('object');
    }

    /*
     * delete a Category by id
     */

    function deleteCategory($id) {
        $this->id = $id;
        return $this->SoftDelete();
    }

}

?>
