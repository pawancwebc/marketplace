<?php

/*
 * Service Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class service extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('service');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'description', 'vendor_id', 'service_area', 'price', 'position', 'is_approve', 'is_active', 'is_deleted', 'date_add', 'last_update_dt');
    }

    /*
     * Create new Service or update existing Service
     */

    function saveService($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        $this->Data['is_active'] = isset($POST['is_active']) ? $POST['is_active'] : "0";
        $this->Data['is_on'] = isset($POST['is_on']) ? $POST['is_on'] : "0";
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $data_id = $this->Data['id'];
            if ($this->Update()):
                return $data_id;
            endif;
        }
        else {
            $this->Data['date_add'] = date("Y-m-d H:i:s");
            if ($this->Insert()) {
                $new_id = $this->GetMaxId();
                return $new_id;
            }
        }
        return false;
    }

    function update_status($POST, $user_id) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update()) {
                return $id;
            }
        } else {
            if ($this->Insert()) {
                return $this->GetMaxId();
            }
        }
        return false;
    }

    function update_field($id, $field, $value) {
        $this->Data['id'] = $id;
        $this->Data[$field] = $value;
        $this->Update();
    }

    function update_fields($id, $field, $value) {
        $this->Data['id'] = $id;
        $this->Data[$field] = $value;
        $this->Update();
    }

    /*
     * Get Service by id
     */

    function getService($id) {
        $this->Where = " where `id` = $id";
        return $this->DisplayOne();
    }

    /*
     * Get List of all Service in array with parent id
     */

    function listService($show_active = false) {
        if ($show_active):
            $this->Where = "where is_active='1' and is_deleted='0' order by $this->orderby $this->order, id desc";
        else:
            $this->Where = "where is_deleted='0' order by $this->orderby $this->order, id desc";
        endif;

        return $this->ListOfAllRecords('object');
    }

    function listService_homepage($show_active = false) {
        if ($show_active):
            $this->Where = "where is_active='1' and is_deleted='0' order by $this->orderby $this->order, id desc";
        else:
            $this->Where = "where is_deleted='0' order by $this->orderby $this->order, id desc";
        endif;

        return $this->ListOfAllRecords('object');
    }

    function listService_homepag($show_active = false) {
        $this->Where = "where is_active='1' and is_on='1' order by $this->orderby $this->order, id desc";
        return $this->ListOfAllRecords('object');
    }

    function listmyservice($user_id) {
        $this->Where = "WHERE `vendor_id` = '$user_id' ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function listmyservices($user_id, $value) {
        $this->Where = "WHERE `vendor_id` = '$user_id' and `is_deleted` = '$value' ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function listActiveServiceWithDetails() {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where.=" where s.is_active='1' and s.is_on='1'";
        return $Query->ListOfAllRecords('object');
    }

    function listActiveServiceWithDetails2($p, $d) {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where.=" where s.is_active='1' and s.is_on='1' LIMIT $p,$d";
        return $Query->ListOfAllRecords('object');
    }

    function getServiceWithDetails($id) {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1'";
        $Query->Where.=" where s.id='" . $id . "'";
        return $Query->DisplayOne();
    }

    function getServicedetail($id) {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1'";
        $Query->Where.=" where s.id='" . $id . "'";
        return $Query->DisplayOne();
    }

    function listUserService($user_id, $show_active = false) {
        if ($show_active):
            $this->Where = "where is_active='1' and vendor_id='" . $user_id . "' and is_deleted='0' order by $this->orderby $this->order, id desc";
        else:
            $this->Where = "where is_deleted='0' and vendor_id='" . $user_id . "' order by $this->orderby $this->order, id desc";
        endif;

        return $this->ListOfAllRecords('object');
    }

    /*
     * delete a Service by id
     */

    function deleteService($id) {
        $this->id = $id;
        return $this->SoftDelete();
    }

    public static function is_location_exists($id, $service_area) {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where.=" WHERE s.service_area LIKE '%$service_area%'";
        return $Query->ListOfAllRecords('');
    }

    public static function is_location_exist($id, $service_area, $category_id) {
        $Query = new query('service as s');

        $Query->Field = " s.*,i.image,rel.category_id,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_cat_rel as rel on rel.service_id=s.id";
        $Query->Where .=" WHERE s.is_on='1' and s.is_active='1' and s.service_area LIKE '%$service_area%' and rel.category_id LIKE '%$category_id'";
        return $Query->ListOfAllRecords('');
    }

    public static function is_category($id) {
        $Query = new query('service as s');
        $Query->Field = " s.*,i.image,rel.category_id,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_cat_rel as rel on rel.service_id=s.id";
        $Query->Where .=" WHERE s.is_on='1' and s.is_active='1' and rel.category_id LIKE '%$id'";
        return $Query->ListOfAllRecords('');
    }

}

/* Service images class */

class simage extends query {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('service_image');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'service_id', 'image', 'position', 'is_main', 'date_add', 'date_upd');
    }

    public static function validServiceImage($img_id, $service_id) {
        $obj = new simage();
        $obj->Field = "id";
        $obj->Where = "WHERE id=$img_id AND service_id=$service_id";
        $data = $obj->DisplayOne();
        return is_object($data) ? TRUE : FALSE;
    }

    public static function updateCoverImage($img_id, $service_id) {
        if (simage::validServiceImage($img_id, $service_id)) {
            //reset previous main image
            $obj = new simage();
            $obj->Where = " WHERE service_id=$service_id AND is_main='1'";
            $obj->Data['is_main'] = 0;
            $obj->UpdateCustom();
            //set new image as main image
            $obj = new simage();
            $obj->Data['id'] = $img_id;
            $obj->Data['is_main'] = 1;
            $obj->Update();
            return "success";
        } else {
            return "error";
        }
    }

    function getcoverimage($service_id) {
        $this->Where = "WHERE service_id=$service_id AND is_main='1'";
        return $this->DisplayOne();
    }

    function insertServiceImage($sid, $image) {
        if ($sid != '' && $sid != '0'):
            $Query = new simage();
            $Query->Field = "id,is_main,service_id";
            $Query->Where = " where is_main='1' and service_id='$sid'";
            $object = $Query->DisplayOne();

            $Query1 = new simage();
            $Query1->Data['service_id'] = $sid;
            $Query1->Data['image'] = $image;
            $Query1->Data['is_main'] = (is_object($object)) ? "0" : "1";

            $Query1->Data['date_add'] = date("Y-m-d H:i:s");
            if ($Query1->Insert()):
                return true;
            else:
                return false;
            endif;
        endif;
        return false;
    }

    /* get image detail */

    function getImageDetail($id) {
        $this->Where = " where id='$id'";
        return $this->DisplayOne();
    }

    /* save Service images *** */

    function uploadServiceImages($sid, $FILES, $Fieldname = 'image') {
        if ($sid != '' && $sid != '0' && !empty($FILES)):
            foreach ($FILES as $k => $v):
                $rand = rand(0000, 9999);

                $image_obj = new imageManipulation();
                if ($image_obj->upload_photo('service', $v, $rand)):
                    $image_name = $image_obj->makeFileName($v['name'], $rand);

                    $queryObj = new simage();
                    $queryObj->insertServiceImage($sid, $image_name);
                endif;

            endforeach;
            return true;
        endif;
        return false;
    }

    /* get images of one product */

    function getServiceImages($id) {
        if ($id != '' && $id != '0'):
            $this->Where = " where `service_id` = $id order by is_main asc, $this->orderby $this->order";
            //$this->print = 1;
            return $this->ListOfAllRecords();
        endif;
        return false;
    }

    function update_field($id, $field, $value) {
        $this->Data['id'] = $id;
        $this->Data[$field] = $value;
        $this->Update();
    }

    /* delet image from table and folder */

    function deleteServiceImage($id) {

        $QueryObj = new simage();
        $object = $QueryObj->getImageDetail($id);

        $QueryObj1 = new simage();
        $deleted = $QueryObj1->deleteImage($object->id);

        #delete images from all folders
        $image_obj = new imageManipulation();
        $image_obj->DeleteImagesFromAllFolders('service', $object->image);

        if ($object->is_main == '1'):
            $QueryObj2 = new simage();
            $QueryObj2->Data['is_main'] = '1';
            $QueryObj2->Where = " where service_id = $object->service_id order by id desc limit 1";
            $QueryObj2->UpdateCustom();
        endif;

        if ($deleted):
            return true;
        else:
            return false;
        endif;
    }

    /* delete image value */

    function deleteImage($id) {
        $this->id = $id;
        return $this->Delete();
    }

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

    /* check image by name */

    public static function checkImageByName($service_id, $image) {
        $query = new query('service_image');
        $query->Field = " id,image";
        $query->Where = " where image='$image' AND service_id='$service_id'";
        $object = $query->Displayone();
        if ($object):
            return $object->id;
        endif;
        return false;
    }

}

/* product vairant class */





/* Category Service Rel */

class categoryService extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('service_cat_rel');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = array('id', 'service_id', 'category_id', 'last_update_dt');
    }

    /*
     * Create new Category Service or update existing Service
     */

    function saveCatService($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $data_id = $this->Data['id'];
            if ($this->Update()):
                return $data_id;
            endif;
        }
        else {
            if ($this->Insert()) {
                $new_id = $this->GetMaxId();
                return $new_id;
            }
        }
        return false;
    }

    /*
     * Get List of all Service in array with parent id
     */

    function listCatService($cat_id) {

        $this->Where = "where category_id='" . $cat_id . "' order by id asc";
        return $this->ListOfAllRecords('object');
    }

    function saveServiceCat($service_id, $post) {
        if (!empty($post)):
            foreach ($post as $key => $value):
                $array = array('service_id' => $service_id, 'category_id' => $value);
                $QueryObj = new categoryService();
                $QueryObj->saveCatService($array);
            endforeach;
        endif;
        return true;
    }

    function deleteCatService($cat_id) {
        $this->Where = " where category_id='" . $cat_id . "'";
        $this->Delete_where();
        return true;
    }

    function deleteServiceCat($service_id) {

        $this->Where = " where service_id='" . $service_id . "'";
        $this->Delete_where();
        return true;
    }

    function getServiceCat($service_id) {
        $records = array();
        $this->Where = " where service_id='" . $service_id . "'";
        $rec = $this->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $records[] = $value->category_id;
            endforeach;
        endif;
        return $records;
    }

    function getServiceCatNames($service_id) {
        $records = array();
        $Query = new query('service_cat_rel as rel');
        $Query->Field = " cat.name";
        $Query->Where = " INNER JOIN " . TABLE_PREFIX . "service_cat as cat on cat.id=rel.category_id";
        $Query->Where.=" where rel.service_id='" . $service_id . "'";

        $rec = $Query->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $records[] = $value->name;
            endforeach;
        endif;
        return $records;
    }

}

class trans extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('trans');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function saveServiceorder($value) {
//	$this->Data['txn_id'] = $txn_id;
//	$this->Data['user_id'] = $uid;
//	$this->Data['service_id'] = $sid;
//	$this->Data['name'] = $name;
//	$this->Data['date_time'] = $date_time;
//	$this->Data['u_name'] = $u_name;
//	$this->Data['address'] = $address;
//	$this->Data['lat'] = $lat;
//	$this->Data['longi'] = $longi;
//	$this->Data['phone'] = $phone;
//	$this->Data['payment_gross'] = $payment_gross;
//	$this->Data['currency_code'] = $currency_code;
//	$this->Data['payment_status'] = $payment_status;
        $this->Data = $this->_makeData($value, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Insert();
        } else {
            if ($this->Insert()) {
                return $this->GetMaxId();
            }
        }
        return false;
    }

    function listmyorder($u_id) {
        $this->Where = "WHERE `u_id` = '$u_id' ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function listorder($service_id) {
        $this->Where = "WHERE `service_id` = '$service_id' ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function update_field($id, $field, $value) {
        $this->Data['id'] = $id;
        $this->Data[$field] = $value;
        $this->Update();
    }

    function update_fields($id, $field, $value) {
        $this->Data['id'] = $id;
        $this->Data[$field] = $value;
        $this->Update();
    }

    /*
     * Get Service by id
     */

    function getService($id) {
        $this->Where = " where `id` = $id";
        return $this->DisplayOne();
    }

    /*
     * Get List of all Service in array with parent id
     */

    function listService($show_active = false) {
        if ($show_active):
            $this->Where = "where is_active='1' and is_deleted='0' order by $this->orderby $this->order, id desc";
        else:
            $this->Where = "where is_deleted='0' order by $this->orderby $this->order, id desc";
        endif;

        return $this->ListOfAllRecords('object');
    }

    function listmyservice($user_id) {
        $this->Where = "WHERE `vendor_id` = '$user_id' ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function listmyservices($user_id, $value) {
        $this->Where = "WHERE `vendor_id` = '$user_id' and `is_deleted` = '$value' ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function listActiveServiceWithDetails() {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where.=" where s.is_active='1' and s.is_on='1'";
        return $Query->ListOfAllRecords('object');
    }

    function listActiveServiceWithDetails2($p, $d) {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where.=" where s.is_active='1' and s.is_on='1' LIMIT $p,$d";
        return $Query->ListOfAllRecords('object');
    }

    function getServiceWithDetails($id) {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1'";
        $Query->Where.=" where s.id='" . $id . "'";
        return $Query->DisplayOne();
    }

    function listUserService($user_id, $show_active = false) {
        if ($show_active):
            $this->Where = "where is_active='1' and vendor_id='" . $user_id . "' and is_deleted='0' order by $this->orderby $this->order, id desc";
        else:
            $this->Where = "where is_deleted='0' and vendor_id='" . $user_id . "' order by $this->orderby $this->order, id desc";
        endif;

        return $this->ListOfAllRecords('object');
    }

    /*
     * delete a Service by id
     */

    function deleteService($id) {
        $this->id = $id;
        return $this->SoftDelete();
    }

    public static function is_location_exists($id, $service_area) {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where.=" WHERE s.service_area LIKE '%$service_area%'";
        return $Query->ListOfAllRecords('');
    }

    public static function is_location_exist($id, $service_area, $category_id) {
        $Query = new query('service as s');
        $Query->Field = " s.*,i.image,rel.category_id,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_cat_rel as rel on rel.service_id=s.id";
        $Query->Where .=" WHERE s.is_on='1' and s.is_active='1' and s.service_area LIKE '%$service_area%' and rel.category_id LIKE '%$category_id'";
        return $Query->ListOfAllRecords('');
    }

    public static function is_category($id) {
        $Query = new query('service as s');

        $Query->Field = " s.*,i.image,rel.category_id,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_cat_rel as rel on rel.service_id=s.id";
        $Query->Where .=" WHERE s.is_on='1' and s.is_active='1' and rel.category_id LIKE '%$id'";
        return $Query->ListOfAllRecords('');
    }

}

class status extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('status');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function saveServiceorder($value) {
        $this->Data = $this->_makeData($value, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Insert();
        } else {
            if ($this->Insert()) {
                return $this->GetMaxId();
            }
        }
        return false;
    }

    function update_status($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update()) {
                return $id;
            }
        } else {
            if ($this->Insert()) {
                return $this->GetMaxId();
            }
        }
        return false;
    }

    function list_status($orderid, $us_id) {
        $this->Where = "WHERE `orderid` = '$orderid' AND `us_id` = '$us_id' ";
        return $this->ListOfAllRecords();
    }

    function update_field($id, $field, $value) {
        $this->Data['id'] = $id;
        $this->Data[$field] = $value;
        $this->Update();
    }

    function update_field_custom($POST, $order_id, $us_id) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Where = "WHERE `orderid` = '$order_id' AND `us_id` = '$us_id'";
        $this->UpdateCustom();
    }

    function getorderdetail($orderid) {
        $this->Where = " where `orderid` = $orderid";
        return $this->DisplayOne();
    }

    function listService($show_active = false) {
        if ($show_active):
            $this->Where = "where is_active='1' and is_deleted='0' order by $this->orderby $this->order, id desc";
        else:
            $this->Where = "where is_deleted='0' order by $this->orderby $this->order, id desc";
        endif;

        return $this->ListOfAllRecords('object');
    }

    function listmyservice($user_id) {
        $this->Where = "WHERE `vendor_id` = '$user_id' ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function listmyservices($user_id, $value) {
        $this->Where = "WHERE `vendor_id` = '$user_id' and `is_deleted` = '$value' ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function listActiveServiceWithDetails() {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where.=" where s.is_active='1' and s.is_on='1'";
        return $Query->ListOfAllRecords('object');
    }

    function listActiveServiceWithDetails2($p, $d) {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where.=" where s.is_active='1' and s.is_on='1' LIMIT $p,$d";
        return $Query->ListOfAllRecords('object');
    }

    function getServiceWithDetails($id) {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1'";
        $Query->Where.=" where s.id='" . $id . "'";
        return $Query->DisplayOne();
    }

    function listUserService($user_id, $show_active = false) {
        if ($show_active):
            $this->Where = "where is_active='1' and vendor_id='" . $user_id . "' and is_deleted='0' order by $this->orderby $this->order, id desc";
        else:
            $this->Where = "where is_deleted='0' and vendor_id='" . $user_id . "' order by $this->orderby $this->order, id desc";
        endif;

        return $this->ListOfAllRecords('object');
    }

    /*
     * delete a Service by id
     */

    function deleteService($id) {
        $this->id = $id;
        return $this->SoftDelete();
    }

    public static function is_location_exists($id, $service_area) {
        $Query = new query('service as s');
        $Query->Field = "s.*,i.image,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where.=" WHERE s.service_area LIKE '%$service_area%'";
        return $Query->ListOfAllRecords('');
    }

    public static function is_location_exist($id, $service_area, $category_id) {
        $Query = new query('service as s');
        $Query->Field = " s.*,i.image,rel.category_id,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_cat_rel as rel on rel.service_id=s.id";
        $Query->Where .=" WHERE s.is_on='1' and s.is_active='1' and s.service_area LIKE '%$service_area%' and rel.category_id LIKE '%$category_id'";
        return $Query->ListOfAllRecords('');
    }

    public static function is_category($id) {
        $Query = new query('service as s');

        $Query->Field = " s.*,i.image,rel.category_id,u.firstname,u.lastname,u.firstname,u.username as email,u.phone,u.address,u.address1";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as u on u.id=s.vendor_id";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_image as i on i.service_id=s.id AND i.is_main='1' ";
        $Query->Where .=" LEFT JOIN " . TABLE_PREFIX . "service_cat_rel as rel on rel.service_id=s.id";
        $Query->Where .=" WHERE s.is_on='1' and s.is_active='1' and rel.category_id LIKE '%$id'";
        return $Query->ListOfAllRecords('');
    }

    public static function is_order_user_exit($orderid, $us_id) {
        $query = new status();
        $query->Where = "WHERE `orderid` = '$orderid' AND `us_id` = '$us_id'";
        return $query->DisplayOne();
    }

}

class orderstatus extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('orderstatus');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function saveServiceorder($value) {
        $this->Data = $this->_makeData($value, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Insert();
        } else {
            if ($this->Insert()) {
                return $this->GetMaxId();
            }
        }
        return false;
    }

    function listmyservice() {
        return $this->ListOfAllRecords();
    }

    function list_info($status_id) {
        $this->Where = "WHERE `id` = '$status_id'";
        return $this->DisplayOne();
    }

}

class service_detail extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('service_detail');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function saveService_save($id, $user_id) {
        $this->Data['user_id'] = $user_id;
        $this->Data['service_id'] = $id;
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $data_id = $this->Data['id'];
            if ($this->Update()):
                return $data_id;
            endif;
        }
        else {
            $this->Data['date_add'] = date("Y-m-d H:i:s");
            if ($this->Insert()) {
                $new_id = $this->GetMaxId();
                return $new_id;
            }
        }
        return false;
    }

    function saveServiceorder($value) {
        $this->Data = $this->_makeData($value, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Insert();
        } else {
            if ($this->Insert()) {
                return $this->GetMaxId();
            }
        }
        return false;
    }

    function listmyservice() {
        return $this->ListOfAllRecords();
    }

    function list_info($user_id) {
        $this->Where = "WHERE `user_id` = '$user_id' GROUP BY `service_id` ORDER BY `id` DESC LIMIT 3";
        return $this->ListOfAllRecords();
    }

}

?>