<?php

/*
 * Image Manupulation Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */



class imageManipulation {

    private $mimeTypes;
    private $errorMsg;

    function __construct() {
        global $conf_allowed_photo_mime_type;
        $this->mimeTypes = $conf_allowed_photo_mime_type;
        $this->isCrop = false;
        $this->errorMsg = new errorManipulation();
    }

   

    function makeSourcePath($type, $image, $folderName='large') {

        if ($type == '') {
            return false;
        }
        if ($image == '') {
            return false;
        }
        if ($folderName == '') {
            $folderName = 'large';
        }

        return DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $folderName . '/' . $image;
    }

    function makeDestinationPath($type, $image, $folderName='thumb') {

        if ($type == '') {
            $type = 'default';
        }
        if ($image == '') {
            $image = date("ymdhis") . '.jpg';
        }
        if ($folderName == '') {
            $folderName = 'thumb';
        }

        if (!is_dir(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $folderName))
            @mkdir(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $folderName);
        return DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $folderName . '/' . $image;
    }

   

    function makeImageName($name, $id) {
        $file_name_parts = explode('.', $name);
        $file_name_parts['0'].=$id;
        return $file_name_parts['0'] . '.' . $file_name_parts['1'];
    }

    function makeFileName($name, $id) {
        $file_name_parts = explode('.', $name);
        $file_name_parts['0'].=$id;
        $file = $file_name_parts['0'] . '.' . $file_name_parts['1'];
        return $file;
    }
    
    function create_resized_for_module($module, $large_image){
	global $imageThumbConfig;
  
	if(isset($imageThumbConfig[$module]) && is_array($imageThumbConfig[$module])){
              
		foreach($imageThumbConfig[$module] as $k=>$v){
		        $crop=0; 
                        (isset($v['crop']) &&  ($v['crop']==='1' || $v['crop']===true) )?$crop='1':"";
                        $this->create_resized_crop($module, $large_image, $k, $v['width'], $v['height'],$crop);
		}
	}
    }
    
    
   function create_resized_for_module_feed($module, $large_image){
	global $imageThumbConfig;
       
	if(isset($imageThumbConfig[$module]) && is_array($imageThumbConfig[$module])){
              
		foreach($imageThumbConfig[$module] as $k=>$v){
		        $crop=0; 
                        (isset($v['crop']) &&  ($v['crop']==='1' || $v['crop']===true) )?$crop='1':"";
                        $this->create_resized_crop_feed($module, $large_image, $k, $v['width'], $v['height'],$crop);
		}
	}
    }
    

 /*
     * type = module
     * image = image name
     * folderName = thumbnail folder name (for identification and access)
     * width = final image width (max)
     * height = final image height  (max)
     */

function create_resized_crop_old($type, $image, $folderName, $Rwidth, $Rheight,$crop=0){
        /* Get the original geometry and calculate scales */
	$source_path=DIR_FS_SITE_UPLOAD.'photo/'.$type.'/large/'.$image;
	$destination_path=DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folderName.'/'.$image;
	
        if(!is_dir(DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folderName)){
		@mkdir(DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folderName, 644);   //maximum 777
	}
        
	list($width, $height, $img_type, $attr) = getimagesize($source_path);
       
	/* should we resize the image? only if height and width both are higher than the base values */
	if($width>$Rwidth && $height>$Rheight){
                $xscale=$width/$Rwidth;
                $yscale=$height/$Rheight;
		
                /* Yes - we should resize it */
		/* Recalculate new size with default ratio */
		if ($yscale>$xscale){   
                    /* image is portrait */
                    $new_width = round($width * (1/$xscale));
                    $new_height = round($height * (1/$xscale));
		}
		else {					
                    /* image is landscape */
                    $new_width  = round($width  * (1/$yscale));
                    $new_height = round($height * (1/$yscale));
		}
		/* Resize the original image & output */
		$imageResized = imagecreatetruecolor($new_width, $new_height);
                
                 /*Turn on image alpha blending for png images*/
                if(strtolower(substr($source_path, -3))=='png'):
                    imagealphablending($imageResized, false);
                    imagesavealpha($imageResized,true);
                    $transparent = imagecolorallocatealpha($imageResized, 255, 255, 255, 127);
                    imagefilledrectangle($imageResized, 0, 0, $new_width, $new_height, $transparent);

                endif;
		
		/* check image format and create image */
	
                $img_mime_type = getimagesize($source_path);
                switch ($img_mime_type['mime']) {
                    case 'image/jpeg':
                    case 'image/jpg':
                    case 'image/pjpeg':
                    case 'image/pjpg':
                        ini_set("gd.jpeg_ignore_warning", 1);
                        $imageTmp = imagecreatefromjpeg($source_path);
                        break;
                    case 'image/gif':
                        $imageTmp = imagecreatefromgif($source_path);
                       
                        break;
                    case 'image/png':
                        $imageTmp = imagecreatefrompng($source_path);
                      
                        break;
                    default:
                        return false;
                        break;
                }
                
		imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		$this->output_img($imageResized, image_type_to_mime_type($img_type), $destination_path);
                if($crop==1):
                   $this->crop_image($type, $image, $folderName, $Rwidth, $Rheight);
                endif;
        }
	else{
                /* We should crop the image */
                if($crop==1):
                    /* firstly, we should copy the image to right folder */
                    if(copy($source_path, $destination_path)){
                       $this->crop_image($type, $image, $folderName, $Rwidth, $Rheight);
                    }
                    else{
                       echo 'sorry! could not copy the file to destination folder. Plese check folder permissions';
                    }
                else:
                    if(!copy($source_path, $destination_path)){
                      echo 'sorry! could not copy the file to destination folder. Plese check folder permissions';
                    }
                 
                endif; 
        }
}



/*
 * type = module
 * image = image name
 * folderName = thumbnail folder name (for identification and access)
 * width = final image width (max)
 * height = final image height  (max)
 */

function create_resized_crop_feed($type, $image, $folderName, $Rwidth, $Rheight,$crop=0){
        
        global $USER_WEBISTE_PATH;
        
       
       /* Get the original geometry and calculate scales */

	$source_path=$USER_WEBISTE_PATH.'upload/photo/'.$type.'/large/'.$image;
	$destination_path=$USER_WEBISTE_PATH.'upload/photo/'.$type.'/'.$folderName.'/'.$image;
	
        if(!is_dir($USER_WEBISTE_PATH.'upload/photo/'.$type.'/'.$folderName)){
		@mkdir($USER_WEBISTE_PATH.'upload/photo/'.$type.'/'.$folderName, 644);   //maximum 777
	}
        
	list($width, $height, $img_type, $attr) = getimagesize($source_path);
       
	/* should we resize the image? only if height and width both are higher than the base values */
	if($width>$Rwidth || $height>$Rheight){
                $xscale=$width/$Rwidth;
                $yscale=$height/$Rheight;
		
                /* Yes - we should resize it */
		/* Recalculate new size with default ratio */
		if ($yscale>$xscale){   
                    /* image is portrait */
                    $new_width = round($width * (1/$yscale));
                    $new_height = round($height * (1/$yscale));
		}
		else {					
                    /* image is landscape */
                    $new_width  = round($width  * (1/$xscale));
                    $new_height = round($height * (1/$xscale));
		}
		/* Resize the original image & output */
		$imageResized = imagecreatetruecolor($new_width, $new_height);
                
                 /*Turn on image alpha blending for png images*/
                if(strtolower(substr($source_path, -3))=='png'):
                    imagealphablending($imageResized, false);
                    imagesavealpha($imageResized,true);
                    $transparent = imagecolorallocatealpha($imageResized, 255, 255, 255, 127);
                    imagefilledrectangle($imageResized, 0, 0, $new_width, $new_height, $transparent);

                endif;
		
		/* check image format and create image */
	
                $img_mime_type = getimagesize($source_path);
                switch ($img_mime_type['mime']) {
                    case 'image/jpeg':
                    case 'image/jpg':
                    case 'image/pjpeg':
                    case 'image/pjpg':
                        ini_set("gd.jpeg_ignore_warning", 1);
                        $imageTmp = imagecreatefromjpeg($source_path);
                        break;
                    case 'image/gif':
                        $imageTmp = imagecreatefromgif($source_path);
                       
                        break;
                    case 'image/png':
                        $imageTmp = imagecreatefrompng($source_path);
                      
                        break;
                    default:
                        return false;
                        break;
                }
                
		imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		$this->output_img($imageResized, image_type_to_mime_type($img_type), $destination_path);
                if($crop==1):
                   $this->crop_image($type, $image, $folderName, $Rwidth, $Rheight);
                endif;
        }
        else{
        /* copy direct images without resizing and cropping */
        copy($source_path, $destination_path);
        }
        
}      
  function create_resized_crop($type, $image, $folderName, $Rwidth, $Rheight,$crop=0){
        /* Get the original geometry and calculate scales */
	$source_path=DIR_FS_SITE_UPLOAD.'photo/'.$type.'/large/'.$image;
	$destination_path=DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folderName.'/'.$image;
	
        if(!is_dir(DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folderName)){
		@mkdir(DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folderName, 644);   //maximum 777
	}
        
	list($width, $height, $img_type, $attr) = getimagesize($source_path);
       
	/* should we resize the image? only if height and width both are higher than the base values */
	if($width>$Rwidth || $height>$Rheight){
                $xscale=$width/$Rwidth;
                $yscale=$height/$Rheight;
		
                /* Yes - we should resize it */
		/* Recalculate new size with default ratio */
		if ($yscale>$xscale){   
                    /* image is portrait */
                    $new_width = round($width * (1/$yscale));
                    $new_height = round($height * (1/$yscale));
		}
		else {					
                    /* image is landscape */
                    $new_width  = round($width  * (1/$xscale));
                    $new_height = round($height * (1/$xscale));
		}
		/* Resize the original image & output */
		$imageResized = imagecreatetruecolor($new_width, $new_height);
                
                 /*Turn on image alpha blending for png images*/
                if(strtolower(substr($source_path, -3))=='png'):
                    imagealphablending($imageResized, false);
                    imagesavealpha($imageResized,true);
                    $transparent = imagecolorallocatealpha($imageResized, 255, 255, 255, 127);
                    imagefilledrectangle($imageResized, 0, 0, $new_width, $new_height, $transparent);

                endif;
		
		/* check image format and create image */
	
                $img_mime_type = getimagesize($source_path);
                switch ($img_mime_type['mime']) {
                    case 'image/jpeg':
                    case 'image/jpg':
                    case 'image/pjpeg':
                    case 'image/pjpg':
                        ini_set("gd.jpeg_ignore_warning", 1);
                        $imageTmp = imagecreatefromjpeg($source_path);
                        break;
                    case 'image/gif':
                        $imageTmp = imagecreatefromgif($source_path);
                       
                        break;
                    case 'image/png':
                        $imageTmp = imagecreatefrompng($source_path);
                      
                        break;
                    default:
                        return false;
                        break;
                }
                
		imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		$this->output_img($imageResized, image_type_to_mime_type($img_type), $destination_path);
                if($crop==1):
                   $this->crop_image($type, $image, $folderName, $Rwidth, $Rheight);
                endif;
        }
        else{
        /* copy direct images without resizing and cropping */
        copy($source_path, $destination_path);
        }

	
}





function crop_image($type, $image, $folder, $twidth, $theight){	       
        $source_path=DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folder.'/'.$image;
	$destination_path=DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folder.'/'.$image;
	list($width, $height) = getimagesize($source_path);
        
        $new_width=$twidth;
        $new_height=$theight;
        
        if($width<=$twidth){ $new_width=$width; }
	if($height<=$theight){ $new_height=$height; }	
        
	$cropping=$this->cropFromCenter($twidth, $theight, $width, $height);
	if(count($cropping))
	{ 								
            $cropX=$cropping['cropX'];
            $cropY=$cropping['cropY'];									

            $imageResized = imagecreatetruecolor($new_width, $new_height);	

            /*Turn on image alpha blending for png images*/
            if(strtolower(substr($source_path, -3))=='png'):
                imagealphablending($imageResized, false);
                imagesavealpha($imageResized,true);
                $transparent = imagecolorallocatealpha($imageResized, 255, 255, 255, 127);
                imagefilledrectangle($imageResized, 0, 0, $new_width, $new_height, $transparent);

            endif;
            
            
             #check image format and create image
            $img_mime_type = getimagesize($source_path);
                switch ($img_mime_type['mime']) {
                    case 'image/jpeg':
                    case 'image/jpg':
                    case 'image/pjpeg':
                    case 'image/pjpg':
                        ini_set("gd.jpeg_ignore_warning", 1);
                        $imageTmp = imagecreatefromjpeg($source_path);
                        break;
                    case 'image/gif':
                        $imageTmp = imagecreatefromgif($source_path);
                       
                        break;
                    case 'image/png':
                        $imageTmp = imagecreatefrompng($source_path);
                      
                        break;
                    default:
                        return false;
                        break;
                }
			
            //imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            imagecopyresampled($imageResized, $imageTmp, 0, 0, $cropX, $cropY, $new_width, $new_height, $new_width, $new_height);
            $this->output_img($imageResized, image_type_to_mime_type(getimagesize($source_path)), $destination_path);
	}	
}

function cropFromCenter($new_w, $new_h, $curr_w, $curr_h) {

        if($new_w > $curr_w)$new_w=$curr_w;
        if($new_h > $curr_h)$new_h=$curr_h;

        $cropping=array();
    $cropX = intval(($curr_w - $new_w) / 2);
    $cropY = intval(($curr_h - $new_h) / 2);		

        $cropping['cropX']=$cropX;
        $cropping['cropY']=$cropY;

        return $cropping;
}

    function upload_photo($type, $file_name, $rand='') {
        $admin_user = new admin_session();
        global $conf_allowed_photo_mime_type;
        if ($file_name['error']):
            // $this->errorMsg->errorAdd("unable to upload!!");   
            return false;
        endif;
        $image_name = $this->makeImageName($file_name['name'], $rand);
        if (in_array($file_name['type'], $conf_allowed_photo_mime_type)):
            if (move_uploaded_file($file_name['tmp_name'], DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/large/' . $image_name)):
                if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/thumb/' . $image_name)):
                    $admin_user->set_pass_msg('Image already exists with same name.Please select another image .');
                    return false;
                else:
                    $this->create_resized_for_module($type, $image_name);

                    //$this->createThumbs($type, $image_name);
                    return true;
                endif;
            else:
                return false;
            endif;
        endif;
        return false;
    }

    function upload_photo_from_url($type, $url,$image_name, $rand='') {
        global $conf_allowed_photo_mime_type;
        
        if($image_name!=''):
            $image_arr=explode('/',$url);
            $image_name=end($image_arr);
            $image_name = $this->makeImageName($image_name, $rand);
        endif;
        
        $img  =  DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/large/' . $image_name;

        //$file_mime_type = mime_content_type($image_name);
        
        //if(in_array($file_mime_type, $conf_allowed_photo_mime_type)):
            if(file_put_contents($img, file_get_contents($url))):
                if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/thumb/' . $image_name)):
                    $admin_user->set_pass_msg('Image already exists with same name.Please select another image .');
                    return false;
                else:
                    $this->create_resized_for_module($type, $image_name);
                    return true;
                endif;
            else:
                return false;
            endif;
        //endif;
        return false;
    }
    
    /*
     * type = module
     * image = image name
     * size= thumb,medium,large etc
     * rtype = return image path or not
     */

    function get_image($type, $size, $image, $rtype=false) {
        if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image) && $image):
            if ($rtype):
                return DIR_WS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image;
            else:
                echo DIR_WS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image;
            endif;
        else:
            if ($rtype):
                return DIR_WS_SITE_GRAPHIC . 'noimage.jpg';
            else:
                echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg';
            endif;
        endif;
    }

    function get_image_tag($type, $size, $image, $rtype=false) {
        if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image) && $image):
            if ($rtype)
                return '<img src="' . $this->get_image($type, $size, $image, true) . '" border="0" alt="' . $image . '">';
            else
                echo '<img src="' . $this->get_image($type, $size, $image) . '" border="0" alt="' . $image . '">';
        else:
            if ($rtype):
                return '<img src="' . DIR_WS_SITE_GRAPHIC . 'noimage.jpg' . '" border="0" alt="' . $image . '">';
            else:
                echo '<img src="' . DIR_WS_SITE_GRAPHIC . 'noimage.jpg' . '" border="0" alt="' . $image . '">';
            endif;
        endif;
    }

    function delete_if_image_exists($type, $size, $image) {
        if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image)):
            unlink(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image);
        endif;
    }

    function DeleteImagesFromAllFolders($type, $image) {
        global $imageThumbConfig;
        /* delete large image if exists */
        if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/large/' . $image)):
            @unlink(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/large/' . $image);
        endif;
        /* delete resized image if exists */
        if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/resized/' . $image)):
            @unlink(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/resized/' . $image);
        endif;
        /* delete from all other folders */
        foreach ($imageThumbConfig[$type] as $key => $value):
            if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $key . '/' . $image)):
                @unlink(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $key . '/' . $image);
            endif;
        endforeach;
    }

    /* Crop Image */

    function crop_photo($type, $size, $image, $crop_width, $crop_height, $crop_x, $crop_y, $new_width, $new_height) {

        if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image)):

            $this->create_resized($type, $size, $image, $new_width, $new_height);

            /* Get the original geometry and calculate scales */

            $source_path = DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/resized/' . $image;
            $destination_path = DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image;

            list($width, $height, $itype) = getimagesize($source_path);
            /* Resize the original image & output */

            #check image format and create image
            $img_mime_type = getimagesize($source_path);
                switch ($img_mime_type['mime']) {
                    case 'image/jpeg':
                    case 'image/jpg':
                    case 'image/pjpeg':
                    case 'image/pjpg':
                        ini_set("gd.jpeg_ignore_warning", 1);
                        $imageTmp = imagecreatefromjpeg($source_path);
                        break;
                    case 'image/gif':
                        $imageTmp = imagecreatefromgif($source_path);
                       
                        break;
                    case 'image/png':
                        $imageTmp = imagecreatefrompng($source_path);
                      
                        break;
                    default:
                        return false;
                        break;
                }
                
            $imageResized = imagecreatetruecolor($crop_width, $crop_height);
            imagecopyresized($imageResized, $imageTmp, 0, 0, $crop_x, $crop_y, $crop_width, $crop_height, $crop_width, $crop_height);
            $this->output_img($imageResized, image_type_to_mime_type($itype), $destination_path);

            return true;
        else:

            return false;
        endif;
    }

    function create_resized($type, $size, $image, $w, $h) {

        // Get the original geometry and calculate scales

        $source_path = DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/large/' . $image;
        $destination_path = DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/resized/' . $image;

        list($width, $height, $itype, $attr) = getimagesize($source_path);
        $new_width = $w;
        $new_height = $h;

         #check image format and create image
        $img_mime_type = getimagesize($source_path);
            switch ($img_mime_type['mime']) {
                case 'image/jpeg':
                case 'image/jpg':
                case 'image/pjpeg':
                case 'image/pjpg':
                    ini_set("gd.jpeg_ignore_warning", 1);
                    $imageTmp = imagecreatefromjpeg($source_path);
                    break;
                case 'image/gif':
                    $imageTmp = imagecreatefromgif($source_path);

                    break;
                case 'image/png':
                    $imageTmp = imagecreatefrompng($source_path);

                    break;
                default:
                    return false;
                    break;
            }

        // Resize the original image & output
        $imageResized = imagecreatetruecolor($new_width, $new_height);
        # $imageTmp     = imagecreatefromjpeg ($source_path);
        imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        $this->output_img($imageResized, image_type_to_mime_type($itype), $destination_path);
    }

    function output_img($rs, $mime, $path) {
        switch ($mime) {
            case 'image/jpeg':
            case 'image/jpg':
            case 'image/pjpeg':
            case 'image/pjpg':
                imagejpeg($rs, $path, 100);
                break;
            case 'image/gif':
                imagegif($rs, $path, 100);
                break;
            case 'image/png':
                imagepng($rs, $path, 9);
                break;
            default:
                imagejpeg($rs, $path, 100);
        }
    }




/*
 * Please use this function very carefully. It shall resize all the images for your project. 
 */
    
public static function resize_all_images(){
    set_time_limit(0);
	
    global $imageThumbConfig;
    foreach($imageThumbConfig as $kk=>$vv){
        if(isset($imageThumbConfig[$kk]) && is_array($imageThumbConfig[$kk])){
            foreach($imageThumbConfig[$kk] as $k=>$v){
                $source_dir=DIR_FS_SITE.'upload/photo/'.$kk.'/large/';
                $images_to_resize = scandir($source_dir);
                if(count($images_to_resize)){
                    foreach($images_to_resize as $kt=>$vt){
                        if(strtolower(substr($vt, -3))=='jpg' or strtolower(substr($vt, -3))=='png' or strtolower(substr($vt, -3))=='gif'){
                            $this->create_resized_crop($kk, $vt, $k, $v['width'], $v['height']);
                        }
                    }
                }
            }
        }
    }
}


public static function resize_product_images(){
    set_time_limit(0);
	
    global $imageThumbConfig;
   
        if(isset($imageThumbConfig['product']) && is_array($imageThumbConfig['product'])){
            foreach($imageThumbConfig['product'] as $k=>$v){
                $source_dir=DIR_FS_SITE.'upload/photo/product/large/';
                $images_to_resize = scandir($source_dir);
                if(count($images_to_resize)){
                    foreach($images_to_resize as $kt=>$vt){
                            $image_obj=new imageManipulation();
                            $image_obj->create_resized_crop('product', $vt, $k, $v['width'], $v['height']);
                        
                    }
                }
            }
        }
    
}

}
?>