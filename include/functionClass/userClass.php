<?php

/*
 * User Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

//include_once(DIR_FS_SITE.'include/functionClass/serviceClass.php');
//include_once(DIR_FS_SITE.'include/functionClass/productClass.php');
//include_once(DIR_FS_SITE.'include/functionClass/moduleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userMetaClass.php');

class user extends cwebc {

    function __construct($order = 'desc', $orderby = 'firstname') {
        parent::__construct('user');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'username', 'password', 'firm_name', 'ip_address', 'address1', 'last_visit', 'total_visit', 'is_active', 'is_email_verified', 'firstname', 'lastname', 'gender', 'education', 'birthdate', 'phone', 'address', 'user_type', 'city_id', 'zip_code', 'state_id', 'country_id', 'is_newsletter', 'newsletter_add_date', 'forgot_password_token', 'email_confirmation_token', 'file', 'reg_info', 'practitioner_type', 'is_deleted', 'date_add', 'date_upd');
    }

    /*
     * check user exists
     */
    
    function email_exist($username) {
        $this->Where = "WHERE `username` = '$username'";
        return $this->DisplayOne();
    }
    function usertype($id) {
        $this->Where = "WHERE `id` = '$id'";
        return $this->DisplayOne();
    }


    function checkUserExists($username, $password) {
        $this->Where = "where is_deleted='0' AND username='" . mysql_real_escape_string($username) . "' AND password='" . mysql_real_escape_string($password) . "'";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object;
        endif;
        return false;
    }

    
    /*
     * get user from email token
     */

    function getUserByEmailToken($token) {
        $this->Where = " where is_deleted='0' and email_confirmation_token='$token'";
        $user = $this->DisplayOne();
        if (is_object($user)) {
            return $user;
        } else {
            return false;
        }
    }

    function update_user($POST, $user_id) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update()) {
                return $id;
            }
        } else {
            if ($this->Insert()) {
                return $this->GetMaxId();
            }
        }
        return false;
    }

    
    public static function vendorinfo($id, $u_id) {
        $Query = new query('user');
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "trans on cwebc_user.id=cwebc_trans.u_id";
        $Query->Where .=" WHERE cwebc_trans.id='$id'  and cwebc_trans.u_id='$u_id'  ";
        return $Query->ListOfAllRecords('');
    }
    /*
     * add user from admin
     */

    function updateUserbyAdmin($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if ($this->Data['password'] != ''):
            $this->Data['password'] = md5($this->Data['password']);
        endif;

        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';
        $this->Data['is_newsletter'] = isset($this->Data['is_newsletter']) ? '1' : '0';
        $this->Data['newsletter_add_date'] = isset($this->Data['is_newsletter']) ? date("Y-m-d H:i:s") : '';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            unset($this->Data['username']);
            $id = $this->Data['id'];
            if ($this->Update()):
                return $id;
            endif;
        }
        else {
            $this->Data['is_email_verified'] = '1';
            $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $this->Data['date_add'] = date("Y-m-d H:i:s");
            if ($this->Insert()):
                return $this->GetMaxId();
            endif;
        }
        return false;
    }

    /*
     * login user
     */

    public static function loginUser($user_id, $user_key = false) {

        $QueryObj1 = new user;
        $user = $QueryObj1->getUser($user_id);

        # set logged in and redirect to needed page.
        $login_session = new user_session();

        $login_session->user_id = $user->id;
        $login_session->verified = $user->is_email_verified;
        $login_session->set_user_id();
        $login_session->username = $user->username;
        $login_session->user_type = $user->user_type;

        $login_session->set_username();
        $login_session->set_usertype($user->user_type);

        #Update total visits
        $QueryObj1 = new user;
        $QueryObj1->updateTotalVisits($user->id, $user->total_visit);

        #Update last visit
        $QueryObj1 = new user;
        $QueryObj1->updateUserLastVisit($user->id);

        return true;
    }

    /*
     * check user 
     */

    function checkUser($id) {
        $this->Where = " where id='$id' and is_deleted='0' and is_active='1'";
        if ($this->DisplayOne()):
            return true;
        else:
            return false;
        endif;
    }
    function update_reset_password($password, $token) {
	$this->Data['password'] = md5($password);
	$this->Where = "WHERE `forgot_password_token` = '$token'";
	return $this->UpdateCustom();
    }

    /*
     * check user exists with email ---using---
     */

    function checkUserEmailExists($username) {
        $this->Where = "where is_deleted='0' AND username='" . mysql_real_escape_string($username) . "'";
        //$this->print=1;
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object;
        endif;
        return false;
    }

    
  
    
    /* update forgot password verify code of user */

    function updateForgotPasswordVerifyCode($id, $code) {

        $this->Data['id'] = $id;
        $this->Data['forgot_password_token'] = $code;
        if ($this->Update()):
            return true;
        else:
            return false;
        endif;
    }

    /* update  verify code of user */

    function updateConfirmationVerifyCode($id, $code) {

        $this->Data['id'] = $id;
        $this->Data['email_confirmation_token'] = $code;
        if ($this->Update()):
            return true;
        else:
            return false;
        endif;
    }

    /* check user exists or not by code */

    function getUserFromForgotPasswordCode($code) {
        $this->Where = " where `forgot_password_token` = '$code'";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object;
        else:
            return false;
        endif;
    }

    /*
     * Get List of all users in array
     */

    function listUsers($vendor = false, $rtype = '', $limited_info = false) {
        if ($limited_info):
            $this->Field = "id,username,companyname,firstname,lastname,gender,address1,address2,phone";
        endif;

        if ($vendor):
            $this->Where.=" where is_deleted='0' AND user_type='vendor' order by $this->orderby $this->order";
        else:
            $this->Where.=" where is_deleted='0' AND user_type='user' order by $this->orderby $this->order";
        endif;

        if ($rtype == 'array'):
            return $this->ListOfAllRecords();
        else:
            return $this->DisplayAll();
        endif;
    }

    function getUsers() {
        $this->Where = "where is_deleted='0' AND user_type='user'";
        return $this->ListOfAllRecords('object');
    }

    /*
     * Get List of all admin users in array
     */

    function listAdminUsers() {
        $this->Where = "where is_deleted='0'";
        $this->DisplayAll();
    }

    function getLimitedInfo($vendor = false) {
        $this->Field = "id,username as email,firstname,lastname,address,zip_code";
        if ($vendor):
            $this->Where.=" where is_deleted='0' AND user_type='vendor' order by $this->orderby $this->order";
        else:
            $this->Where.=" where is_deleted='0' AND user_type='user' order by $this->orderby $this->order";
        endif;

        return $this->ListOfAllRecords('object');
    }

    function getAdminUsers() {
        $this->Field = "id,business_email,first_name,last_name,user_type,department";
        $this->Where = "where is_deleted='0' AND user_type='admin'";

        return $this->ListOfAllRecords('object');
    }

    /*
     * Create new user ---using---
     */

    function createUser($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        /* $rand=rand(0, 99999999);
          $image_obj=new imageManipulation();

          if($image_obj->upload_photo('logo', $_FILES['business_logo'], $rand)):
          $this->Data['business_logo']=$image_obj->makeFileName($_FILES['business_logo']['name'], $rand);
          endif; */

        $this->Data['password'] = md5($this->Data['password']);
        $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->Data['date_add'] = date("Y-m-d H:i:s");
        $this->Insert();
        return $this->GetMaxId();
    }

    /*
     * update user info
     */

    function updateUserDetail($POST) {

        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $POST['id'];
        }
        return false;
    }

    /*
     * get latest users 
     */

    function listLatestUsers($limit = 0) {
        $this->Where = " where is_deleted= '0' order by date_add desc";
        if ($limit != '0'):
            $this->Where.=" limit 0,$limit";
        endif;
        return $this->ListOfAllRecords();
    }

    /*
     * get user email from id
     */

    public static function getUserEmail($id) {
        $query = new user();
        $query->Where = "where id='$id'";
        $object = $query->DisplayOne();
        if (is_object($object)):
            return $object->username;
        endif;
        return false;
    }

    /*
     * change user password
     */
function update_field($field, $value, $id) {
       $this->Data[$field] = $value;
       $this->Data['id'] = $id;
       return $this->Update();
   }
    function changePassword($id, $password) {
        $this->Data['id'] = $id;
        $this->Data['password'] = md5($password);
        if ($this->Update()):
            return true;
        else:
            return false;
        endif;
    }

    public static function is_email_exist($username) {
        $query = new user;
        $query->Where = "WHERE `username` = '$username'";
        return $query->DisplayOne();
    }

    /*
     * Create new admin
     */

    function saveAdminUser($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['user_type'] = 'admin';
        $this->Data['password'] = md5($this->Data['password']);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                return $Data['id'];
        }
        else {

            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * get user new accounts
     */

    function getCountofNewAccounts($from_date, $to_date) {
        $this->Field = " count(*) as count";
        $this->Where = " where cast(date_add as DATE) >= '$from_date' AND CAST(date_add as DATE) <= '$to_date'";
        $object = $this->DisplayOne();
        return $object->count;
    }

    /* save user data */

    function saveUserDetail($POST) {

        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($POST['password'])):
            $this->Data['password'] = md5($this->Data['password']);
        endif;

        $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $POST['id'];
        }
        else {
            $this->Data['reg_date'] = date('Y-m-d');
            $this->Data['is_active'] = '1';
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*     * ***** my custom query ******* */

    function getUser($id) {
        $Query = new query('user as cu');
        $Query->Where.=" where cu.id='" . $id . "'";
        return $Query->DisplayOne();
    }

    /*     * ***** my custom query end ******* */

    function updateTotalVisits($id, $total_visit = 0) {
        $this->Data['id'] = $id;
        $this->Data['total_visit'] = $total_visit + 1;
        $this->Update();
    }

    /*
     * update user last visit when login
     */

    function updateUserLastVisit($id) {
        $this->Data['id'] = $id;
        $this->Data['last_visit'] = date("Y-m-d H:i:s");
        $this->Data['last_visit_ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->Update();
    }

    function getUserLimitedInfo($id) {
        $this->Field = "id,username,companyname,firstname,lastname,age,gender,address1,address2,phone";
        $this->Where = "where id='" . $id . "'";
        $user = $this->DisplayOne();
        return $user;
    }

    /*
     * Get admin by id
     */

    function getAdminUser($id) {
        return $this->_getObject('user', $id);
    }

    function check_user_name_exists($username, $id = 0) {
        $username = mysql_real_escape_string($username);
        $id = mysql_real_escape_string($id);

        if ($username == ""):
            return false;
        endif;

        $this->Field = "id";
        if ($id && $id != 0 && $id != ""):
            $this->Where = "where username='" . $username . "' AND id!='" . $id . "'";
        else:
            $this->Where = "where username='" . $username . "'";
        endif;


        $user = $this->DisplayOne();
        if ($user && is_object($user)):

            return $user;
        else:
            return false;
        endif;
    }

    function verify_url_string($string) {

        if ($string != '')
            if (substr($string, -1) == '/')
                return substr($string, 0, strlen($string) - 1);

        return $string;
    }

    function getUsernameFromURL() {


        $string_parts = array();
        $prefix = str_replace(HTTP_SERVER, '', DIR_WS_SITE);
        $URL = $_SERVER['REQUEST_URI'];
        $string = substr($URL, -(strlen($URL) - strlen($prefix)));
        $string = $this->verify_url_string($string);
        $string_parts = explode('/', $string);

        $user_name = isset($string_parts['1']) ? $string_parts['1'] : "";

        return $user_name;
    }

    function getUserFromUsername($username) {
        $username = mysql_real_escape_string($username);
        if ($username == ""):
            return false;
        endif;

        $this->Where = "where username='" . $username . "'";
        $buss = $this->DisplayOne();
        if ($buss && is_object($buss)):
            return $buss;
        else:
            return false;
        endif;
    }

    /* set user email verified */

    function setEmailVerified($id) {

        $this->Data['id'] = $id;
        $this->Data['is_email_verified'] = '1';
        $this->Data['email_confirmation_token'] = '';
        $this->Update();
        return true;
    }

    /* set user email verified */

    function setEmailVerifiedAndSignUpComplete($id) {

        $this->Data['id'] = $id;
        $this->Data['is_email_verified'] = '1';
        $this->Data['is_signup_complete'] = '1';
        $this->Data['is_on_demo'] = '0';
        $this->Update();
    }

    /*
     * check admin user with name
     */

    function checkUsers($username, $email) {
        $this->Where = "where is_deleted='0' AND (username='" . mysql_real_escape_string($username) . "' OR email='" . mysql_real_escape_string($email) . "')";
        $this->DisplayAll();
    }

    /*
     * check admin user with name in update case
     */

    function checkUsersWithID($username, $email, $id) {
        $this->Where = "where is_deleted='0' AND id!='$id' AND (username='" . mysql_real_escape_string($username) . "' OR business_email='" . mysql_real_escape_string($email) . "')";
        $this->DisplayAll();
    }

    /*
     * delete a page by id
     */

    function deleteAdminUser($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    /* update business logo */

    function updateLogo($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        $rand = rand(0, 99999999);
        $image_obj = new imageManipulation();

        if ($image_obj->upload_photo('logo', $_FILES['business_logo'], $rand)):
            $this->Data['business_logo'] = $image_obj->makeFileName($_FILES['business_logo']['name'], $rand);
        endif;

        if (isset($this->Data['id']) && $this->Data['id'] != '') {


            if ($this->Update())
                return $this->Data['id'];
        }
        else {

            return false;
        }
    }

    /* update business favicon */

    function updateFavicon($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        $rand = rand(0, 99999999);
        $image_obj = new imageManipulation();

        if ($image_obj->upload_photo('favicon', $_FILES['business_favicon'], $rand)):
            $this->Data['business_favicon'] = $image_obj->makeFileName($_FILES['business_favicon']['name'], $rand);
        endif;

        if (isset($this->Data['id']) && $this->Data['id'] != '') {


            if ($this->Update())
                return $this->Data['id'];
        }
        else {

            return false;
        }
    }

    /* update theme id of user */

    function updateThemeId($id, $theme_id) {

        $this->Data['id'] = $id;
        $this->Data['theme_id'] = $theme_id;
        $this->Update();
    }

    /* check if demo period of user is not expired */

    function ValidateDemoPeriod($reg_date, $is_on_demo = 0) {

        if ($is_on_demo && $is_on_demo != 0):

            /* user is on demo period, check date */
            $new_obj = new date();
            $new_date = $new_obj->add_days_to_date(COUNT_DEMO_DAYS, $reg_date);
            $current_date = date('Y-m-d');

            if (strtotime($current_date) > strtotime($new_date)):
                return false;
            else:
                return true;
            endif;


        else:
            return true;
        endif;
    }

    /* check if demo period of user is not expired */

    function GetTotalCompleteSteps($com_steps) {
        $total_complete = 0;

        if (count($com_steps) && is_array($com_steps)):
            foreach ($com_steps as $k => $v):
                if ($v == 1):
                    $total_complete++;
                endif;

            endforeach;
        endif;

        return $total_complete;
    }

    /* get array of user navigation according to selected modules
     * $id is user id
     * $sub_navigation, send it true for getting sub navigation
     * $sub_navigation_limit, set it 0 if no limit
     */

    function GetUserNavigation($id, $sub_navigation = 0, $sub_navigation_limit = 0) {
        global $user_meta;
        $navigation = array();
        $selected_modules = array();
        $this->Field = "id,module";
        $this->Where = "where user_type='user' AND id='" . $id . "'";

        $user = $this->DisplayOne();
        if ($user && is_object($user)):
            $modules = array();

            /* get all selected theme modules */
            $modules = array();
            $modules = unserialize(html_entity_decode($user_meta->user_module_config));

            $modules_arr = html_entity_decode($user->module);
            $selected_modules = unserialize($modules_arr);

            $sr = 0;
            foreach ($modules as $k => $v):

                if (in_array($v['page_name'], $selected_modules)):
                    $navigation[$v['page_name']]['page_name'] = $v['title'];
                    $navigation[$v['page_name']]['page'] = $v['page_name'];

                    if ($sub_navigation && $sub_navigation == 1):/* get subnavigation according to page */

                        if ($k == 'service'): /* get user services */
                            /* Get limited services */
                            $services = array();
                            $service_obj = new service();
                            $service_obj->setUserId($id);
                            $services = $service_obj->listLimitedActiveServices('array', $sub_navigation_limit, 1);
                            $navigation[$v['page_name']]['sub_navigation'] = $services;

                        elseif ($k == 'product'):
                            /* Get limited products */
                            $products = array();
                            $product_obj = new product();
                            $product_obj->setUserId($id);
                            $products = $product_obj->listLimitedActiveProducts('array', $sub_navigation_limit, 1);
                            $navigation[$v['page_name']]['sub_navigation'] = $products;
                        endif;



                    endif;
                endif;


            endforeach;


        endif;

        return $navigation;
    }

    /* get user not to open pages */

    function GetUserNotOpenPages($id) {
        global $user_meta;
        $not_open_pages = array();
        $selected_modules = array();
        $this->Field = "id,module";
        $this->Where = "where user_type='user' AND id='" . $id . "'";

        $user = $this->DisplayOne();
        if ($user && is_object($user)):

            /* get all selected theme modules */
            $modules = array();
            $modules = unserialize(html_entity_decode($user_meta->user_module_config));

            $modules_arr = html_entity_decode($user->module);
            $selected_modules = unserialize($modules_arr);
            $sr = 0;
            foreach ($modules as $k => $v):

                if (!in_array($v['page_name'], $selected_modules)):
                    $not_open_pages[] = $v['page_name'];
                endif;

            endforeach;

        endif;

        return $not_open_pages;
    }

    /*
     * Get count of all users
     */

    function countUsers($user_type = 'user', $show_active = 0) {
        $total_count = 0;
        $this->Field = "id";
        if ($show_active):
            $this->Where = "where user_type='$user_type' AND is_deleted='0' AND is_active='1' ";
        else:
            $this->Where = "where user_type='$user_type' AND is_deleted='0' ";

        endif;

        $object = $this->ListOfAllRecords('object');

        if (count($object)):
            $total_count = count($object);
        endif;

        return $total_count;
    }

    /* check user password is correct */

    function checkUserPassword($id, $password) {

        $password = md5($password);
        $this->Field = "id";
        $this->Where = "where id='$id' AND password='$password'";

        $object = $this->DisplayOne();

        if (is_object($object) && count($object)):
            return true;
        endif;

        return false;
    }

    /* update  verify code of user */

    function updatedVerifyCode($id, $code) {

        $this->Data['id'] = $id;
        $this->Data['verify_code'] = $code;
        $this->Update();
    }

    /* set wizard complete */

    function setWizardComplete($id) {

        $this->Data['id'] = $id;
        $this->Data['is_wizard_complete'] = '1';
        $this->Update();
    }

    /* copy logo to favicon */

    function copyFavicon($image, $id) {

        $this->Data['id'] = $id;
        $this->Data['business_favicon'] = $image;
        $this->Update();

        /* copy logo image to favicon folder */
        $source_path = DIR_FS_SITE_UPLOAD . 'photo/logo/large/' . $image;
        $destination_path = DIR_FS_SITE_UPLOAD . 'photo/favicon/large/' . $image;

        copy($source_path, $destination_path);

        $image_fav = new imageManipulation();
        $image_fav->create_resized_for_module('favicon', $image);
    }

    /* function to hide a module for user */

    function HideModuleForUser($id, $module) {
        $module_find = 0;
        $selected_modules = array();
        $this->Field = "id,module";
        $this->Where = "where user_type='user' AND id='" . $id . "'";
        $user = $this->DisplayOne();
        if ($user && is_object($user)):

            $modules_arr = html_entity_decode($user->module);
            $selected_modules = unserialize($modules_arr);

            foreach ($selected_modules as $k => $v):

                if ($v == $module):
                    unset($selected_modules[$k]);
                    $module_find = 1;
                endif;

            endforeach;

            if ($module_find == 1): /* unset module and save array again */
                $query = new user();
                $query->Data['id'] = $id;
                $query->Data['module'] = serialize($selected_modules);
                $query->Update();
                return true;
            else:
                return false; /* failed to hide */
            endif;

        else:
            return false; /* failed to hide */

        endif;
    }

    /*
     * count total new users of today
     */

    function getCountTodayUsers() {
        $this->Field = " count(*) as count";
        $this->Where = " where DATE(date_add) = CURDATE()";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object->count;
        else:
            return '0';
        endif;
    }

}

/* user address class */

class user_address extends cwebc {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'desc', $orderby = 'id', $parent_id = 0) {
        parent::__construct('user_address');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'cart_id', 'address_name', 'address_type', 'is_default', 'firstname', 'lastname', 'address1',
            'address2', 'city_id', 'city', 'state_id', 'country_id', 'phone', 'mobile', 'zip_code', 'fax', 'email', 'is_active',
            'is_deleted', 'date_add', 'date_upd');
    }

    /*
     * add user account
     */

    function saveUserAddress($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['city_id']) && !is_numeric($this->Data['city_id'])):
            unset($this->Data['city_id']);
        endif;
        if (!is_numeric($this->Data['state_id'])):
            unset($this->Data['state_id']);
        endif;
        if (!is_numeric($this->Data['country_id'])):
            unset($this->Data['country_id']);
        endif;

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update()) {
                return $id;
            } else {
                return false;
            }
        } else {
            $this->Data['date_add'] = date("Y-m-d H:i:s");
            if ($this->Insert()) {
                return $this->GetMaxId();
            } else {
                return false;
            }
        }
    }

    /*
     * get all addresses of a user
     */

    function getUserAddresses($user_id) {
        $this->Where = " where is_active='1' and is_deleted='0' and user_id='$user_id'";
        return $this->ListOfAllRecords();
    }

    /*
     * list user addresses
     */

    function listUserAddresses($user_id) {
        $this->Where = " where is_deleted='0' and user_id='$user_id'";
        return $this->DisplayAll();
    }

    /*
     * get address
     */

    function getAddress($id, $rtype = 'object') {
        $this->Where = " where id='$id'";
        return $this->DisplayOne($rtype);
    }

    /*
     * get user default addresss
     */

    function getUserDefaultAddress($user_id) {
        $query = new user_address();
        $query->Where = " where user_id='$user_id' and is_default='1' and is_active='1' and is_deleted='0'";
        $default = $query->DisplayOne();
        if (is_object($default)) {
            return $default;
        } else {
            $query = new user_address();
            $query->Where = " where user_id='$user_id' and is_active='1' and is_deleted='0' order by id asc limit 0,1";
            $address_array = $query->DisplayOne();
            if (is_object($address_array)) {
                $Query_default_set = new user_address();
                $Query_default_set->Data['id'] = $address_array->id;
                $Query_default_set->Data['is_default'] = '1';
                if ($Query_default_set->Update()):
                    return $address_array;
                endif;
            }
        }
        return false;
    }

    /*
     * check if address belongs to the user or not
     */

    function addressBelongsToUser($user_id, $add_id) {
        $this->Where = " where id='$add_id'";
        $object = $this->DisplayOne();
        if (is_object($object)):
            if ($object->user_id == $user_id):
                return true;
            endif;
        endif;
        return false;
    }

    /*
     * set default
     */

    function setDefaultAddress($user_id, $add_id) {
        $Query = new user_address();
        $Query->Where = " where user_id='$user_id'";
        $Query->Data['is_default'] = '0';
        $Query->UpdateCustom();

        $Query2 = new user_address();
        $Query2->Data['id'] = $add_id;
        $Query2->Data['is_default'] = '1';
        if ($Query2->Update()):
            return true;
        else:
            return false;
        endif;
    }

    /*
     * soft delete address from front end
     */

    function deleteAddress($id) {
        $this->id = $id;
        return $this->SoftDelete();
    }

    /*
     * get all user address
     */

    function ListAddresses($from_date, $to_date) {
        $Query = new query('user_address as cua');
        $Query->Field = " cua.id, CONCAT(cu.firstname,' ',cu.lastname) as username, cua.address_name, cua.firstname, cua.lastname, cua.address1, cua.address2, cua.city,";
        $Query->Field.=" cs.name as state, ccntry.short_name as country, cua.zip_code, cua.email, cua.phone";
        $Query->Where = " LEFT JOIN " . TABLE_PREFIX . "user as cu on cua.user_id=cu.id";
        $Query->Where.=" LEFT JOIN " . TABLE_PREFIX . "state as cs on cua.state_id=cs.id";
        $Query->Where.=" LEFT JOIN " . TABLE_PREFIX . "country as ccntry on cua.country_id=ccntry.id";
        $Query->Where.=" where cua.is_deleted='0'";

        if ($from_date != '')
            $Query->Where.=" and cast(cua.date_add as DATE) >= '$from_date'";

        if ($to_date != '')
            $Query->Where.=" AND CAST(cua.date_add as DATE) <= '$to_date'";

        return $Query->ListOfAllRecords('object');
    }

}

class user_wishlist extends cwebc {

    function __construct($order = 'desc', $orderby = 'id') {
        parent::__construct('user_wishlist');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'session_id', 'product_id', 'product_variant_id', 'quantity', 'ip_address', 'date_add', 'date_upd');
    }

    /*
     * add item to wishlist
     */

    function addWishlistItem($POST) {
        if ($POST['user_id'] != '0'):
            $this->Data['user_id'] = $POST['user_id'];
        endif;
        $this->Data['session_id'] = $POST['session_id'];
        $this->Data['product_id'] = $POST['product_id'];
        $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->Data['date_add'] = date("Y-m-d H:i:s");
        return $this->Insert();
    }

    /*
     * check product exist in wishlist
     */

    public static function checkProductExistsWishlist($product_id, $session, $user_id = '') {
        $query = new user_wishlist();
        $query->Where = " where product_id='$product_id'";
        if ($user_id != ''):
            $query->Where.=" and user_id='$user_id'";
        else:
            $query->Where.=" and session_id='$session'";
        endif;
        $object = $query->DisplayOne();
        if ($object && !empty($object)):
            return true;
        endif;
        return false;
    }

    /*
     * remove product from wishlist
     */

    function RemoveFromWishlist($product_id, $session, $user_id = '') {
        $this->Where = " where product_id='$product_id'";
        if ($user_id != ''):
            $this->Where.=" and user_id='$user_id'";
        else:
            $this->Where.=" and session_id='$session'";
        endif;
        return $this->Delete_where();
    }

    /* update user recent views */

    public static function UpdateUserWishlist($uid, $user_key = '') {
        if ($uid && $uid != 0 && $user_key != ''):
            /* update enteries */
            $Query = new user_wishlist();
            $Query->Data['user_id'] = $uid;
            $Query->Where = " where session_id='$user_key'";
            $Query->UpdateCustom();
        endif;
    }

    /*
     * get user wishlist
     */

    function getUserWishlist($uid, $user_key = '', $limit = '-1') {


        $query = new query('user_wishlist as cuw,product as p');

        $query->Field = "p.id,p.brand_id,p.is_active,p.is_deleted,p.sku,p.name,p.urlname,p.wholesale_price,p.quantity,p.unit_price,p.sale_price,p.discount,p.discount_type,p.product_type,p.date_add, SUM(pv.quantity) as variant_quantity,
                           pv.impact_on_price,pv.impact_type,impact_price,pi.image,pi.from_feed,pi.feed_name,p.sale_from,p.sale_to,p.latest_from,p.latest_to,cc.name as default_category_name,p.default_category";

        $query->Where = " left join " . TABLE_PREFIX . "product_variant as pv ON pv.product_id=p.id";

        /* image */
        $query->Where .= " left join " . TABLE_PREFIX . "product_image AS pi ON pi.product_id=p.id AND pi.is_main='1'";

        //default category name
        $query->Where .= " left join " . TABLE_PREFIX . "category AS cc ON cc.id=p.default_category";

        $query->Where.=" where cuw.product_id=p.id ";

        if ($uid && $uid != 0):
            $query->Where.=" and cuw.user_id='$uid' ";
        else:
            $query->Where.=" and cuw.session_id='$user_key' ";

        endif;

        $query->Where.=" and p.is_active='1' and p.is_deleted='0'";

        $query->Where .= " group by p.id";

        if (!SHOW_OUTOFSTOCK_PRODUCTS):
            //$query -> Where .= " HAVING (product_type='downloadable' or (variant_quantity > 0) OR (variant_quantity IS NULL AND quantity > 0))";  
            $query->Where .= " HAVING (product_type='downloadable' or (variant_quantity >= " . MINIMUM_QUANTITY_REQUIRED_FOR_IN_STOCK . " and variant_quantity != '0' ) OR (variant_quantity IS NULL AND quantity >= " . MINIMUM_QUANTITY_REQUIRED_FOR_IN_STOCK . "  AND quantity != '0'))";
        endif;

        $query->Where.=" order by cuw.date_add desc";
        if ($limit != '-1'):
            $query->Where.=" limit 0,$limit";
        endif;
        //$query->print=1;
        $relations = $query->ListOfAllRecords();

        return $relations;
    }

    /*
     * get user shipping address

     */

    function getUserAddress($id) {
        echo $id;
        $Query = new query('user as cu');
        $Query->Field = "cu.*,cc.name as city_name,cco.short_name as country_name,cs.name as state_name";
        $Query->Where = " left join " . TABLE_PREFIX . "city as cc on cc.id=cu.city_id";
        $Query->Where.=" left join " . TABLE_PREFIX . "country as cco on cco.id=cu.country_id";
        $Query->Where.=" left join " . TABLE_PREFIX . "state as cs on cs.id=cu.state_id";
        $Query->Where.=" where cu.id='" . $id . "'";
        return $Query->DisplayOne();
    }

}
