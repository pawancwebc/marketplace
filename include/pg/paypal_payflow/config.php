<?php

/** DoDirectPayment NVP example; 
 *
 *  Process a credit card payment. 
 *
 * Send HTTP POST Request
 *
 * @param	string	The API method name
 * @param	string	The POST Message fields in &name=value pair format
 * @return	array	Parsed HTTP Response body
 */
function PPHttpPost($methodName_='DoDirectPayment',$nvpStr_,$payment_method) {
        
        if(IS_FEED):
            if(SUPPLIER_PAYPAL_SANDBOX):
                $environment = 'sandbox';
            else:
                $environment = 'live';
            endif;
            $API_UserName = urlencode(SUPPLIER_PAYPAL_PAYFLOW_USERNAME); // set your spi username
            $API_Password = urlencode(SUPPLIER_PAYPAL_PAYFLOW_PASSSWORD); // set your spi password
            $API_Partner = urlencode(SUPPLIER_PAYPAL_PAYFLOW_PARTNER); // set your spi partner
            $API_Merchant = urlencode(SUPPLIER_PAYPAL_PAYFLOW_MERCHANT); // set your spi merchant
            
        else:
            if($payment_method->is_sandbox):
                $environment = 'sandbox';
            else:
                $environment = 'live';
            endif;
            
            $API_Partner = urlencode($payment_method->paypal_business); // set your partner
            $API_Merchant = urlencode($payment_method->api_username); // set your merchant
            $API_UserName = urlencode($payment_method->api_password); // set your username
            $API_Password = urlencode($payment_method->api_signature); // set your password

        endif;
	
	
	$API_Endpoint = "https://payflowpro.paypal.com";
	if("sandbox" === $environment || "beta-sandbox" === $environment) {
            $API_Endpoint = "https://pilot-payflowpro.paypal.com";
	}

        // Set the curl parameters.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
 
	// Turn off the server and peer verification (TrustManager Concept).
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
 
	// Set the API operation, version, and API signature in the request.
	$nvpreq = "USER=$API_UserName&VENDOR=$API_Merchant&PWD=$API_Password&PARTNER=$API_Partner$nvpStr_";
        
        // Set the request as a POST FIELD for curl.
	curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
 
	// Get response from the server.
	$httpResponse = curl_exec($ch);
	
	// Extract the response details.
	$httpResponseAr = explode("&", $httpResponse);
 
	$httpParsedResponseAr = array();
	foreach ($httpResponseAr as $i => $value) {
		$tmpAr = explode("=", $value);
		if(sizeof($tmpAr) > 1) {
			$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
		}
	}

	return $httpParsedResponseAr;
}

$countryObj = new country();
$country  = $countryObj -> getCountryISO2ByName($order->billing_country);

$form=array();
$form['TRXTYPE']='S';
$form['TENDER']='C';
$form['AMT']=number_format($order->grand_total, 2);
$form['CREDITCARDTYPE']=urlencode($order->card_type);
$form['ACCT']=urlencode($order->card_number);
$form['EXPDATE']=urlencode(str_pad($order->expiry_month, 2, '0', STR_PAD_LEFT)).urlencode(substr($order->expiry_year, '-2'));
$form['CVV2']=urlencode($order->cvv);
$form['FIRSTNAME']=urlencode($order->billing_firstname);
$form['LASTNAME']=urlencode($order->billing_lastname);
$form['STREET']= urlencode($order->billing_address1);
$form['CITY']= urlencode($order->billing_city);
$form['STATE']= urlencode($order->billing_state);
$form['ZIP']= urlencode($order->billing_zip);
$form['BILLTOCOUNTRY']= urlencode($country);
$form['CURRENCYCODE']= urlencode('USD');
$form['COMMENT1']= SITE_NAME." Order Payment";
$form['CUSTIP']= urlencode($order->ip_address);
?>