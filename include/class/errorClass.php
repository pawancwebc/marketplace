<?php 
/*
 * Error Manupulation Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 */

 class errorManipulation{


	private $msg;
	private $msgQueue;
	private $clearOnShow;
	private $msgRetrunType;
	
	function __construct(){
			//$_SESSION['msgQueue']=array();
			$this->clearOnShow=true;
			$this->msgReturnType=false;    /* false = print , true = return */
	}	

	
	function __destruct(){
	
	}

	
	function errorMsg(){
		$_SESSION['msgQueue'][]=$this->msg;
	}
	
        /**
         * Function to add erro messages in session
         *
         * @param string $msg error message.
         */
	 function errorAdd($msg){
		$this->msg=$msg;
		$this->errorMsg();
                
	 }
         
         /**
         * Function to add error messages in session
         *
         * @param array $msg error message.
         */
	 function errorAddArray($msg){
              if(is_array($msg)): 
                foreach($msg as $k=>$v):
				   $this->msg=$v;
				   $this->errorMsg();
                endforeach;  
			  else:
				   $this->msg=$msg;
				   $this->errorMsg();
			  endif;
	 }
        
	/**
         * Function to show all error messages in session
         *
         */
	function errorShow(){
                
		if(isset($_SESSION['msgQueue']) && is_array($_SESSION['msgQueue']) && count($_SESSION['msgQueue'])){
			if($this->msgReturnType){
				return $_SESSION['msgQueue'];
			}
			else{
                           
                                         
					foreach($_SESSION['msgQueue'] as $k=>$v){
						
                                                echo '<div class="alert  alert-warning  uppercase  fade  in">';
                                                
                                                echo '<i class="icon-warning-sign"></i>&nbsp;';
                                                
                                                echo str_replace('_', ' ', $v);
                                                
                                                echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                                                
                                                echo '</div>';
					}
			}
		}
		
		if($this->clearOnShow){
			$_SESSION['msgQueue']=array();
		}
	}
	
        /**
         * Function to set clear on show
         *
         */
        
	function setClearOnShow(){
		$this->clearOnShow=true;
	}
	/**
         * Function to unset clear on show
         *
         */
	function unsetClearOnShow(){
		$this->clearOnShow=false;
	}

}

$error_obj= new errorManipulation();?>