<?
class wishlist
{
	var $product_id;
	var $user_id;
	var $name;
	
	function add($product_id)
	{
		$this->is_logged_in($product_id);
		$user= new user_session();
		$userid=$user->get_user_id();
		$query= new query('wish_list');
		$query->Where="where product_id='$product_id' and user_id='$userid'";
		//$query->print=1;
		if(!$query->DisplayOne()):
			$query= new query('wish_list');
			$query->Data['product_id']=$product_id;
			$query->Data['user_id']=$userid;
			//$query->print=1;
			$query->Insert();
//			$user->pass_msg[]='Product has successfully been added to your the wish list.';
//			$user->set_pass_msg();
			return true;
		else:
//			$user->pass_msg[]='This product is already there in your wish list.';
//			$user->set_pass_msg();
			return true;
		endif;
	}
	
	function delete($product_id)
	{
		
		$this->is_logged_in($product_id);
		$user= new user_session();
		$userid=$user->get_user_id();
		$query= new query('wish_list');
		$query->Where="where product_id='$product_id' and user_id='$userid'";
		//$query->print=1;
		if($query->DisplayOne()):
			$query= new query('wish_list');
			$query->Where="where product_id='$product_id' and user_id='$userid'";
			$query->Delete_where();
			return true;
		else:
			$user->pass_msg[]='There is no such product in your wish list.';
			$user->set_pass_msg();
			return false;
		endif;
	}
		
	function is_logged_in($product_id=0)
	{
		$user= new user_session();
		if(!$user->is_logged_in()):
			$user->pass_msg[]='You are not logged in. Please login to add product into wishlist.';
			$user->set_pass_msg();
			$product_id?$user->redirect_url=make_url('wish_list', 'id='.$product_id.'&add=1'):$user->redirect_url=make_url('wish_list');
			$user->set_redirect_url();
			Redirect(make_url('login'));	
		endif;
	}
	
	function get_all()
	{
		$this->is_logged_in();
		$user= new user_session();
		$userid=$user->get_user_id();
		$query= new query('wish_list');
		$query->Where="where user_id='$userid'";
		$query->DisplayAll();
		$products=array();
		if($query->GetNumRows()):
			while($row= $query->GetObjectFromRecord()):
				$products[]=$row->product_id;
			endwhile;
			return $products;
		else:
			return false;
		endif;
	}
};
$wish_list =new wishlist();
?>