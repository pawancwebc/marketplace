<?
class currency {
	var $base_currency;
	var $target_currency;
	var $sign=array('GBP'=>'&pound;', 'USD'=>'$');
	
	function currency()
	{
		if(!isset($_SESSION['currency_format'])):
			$_SESSION['currency_format']=array('base_currency'=>'GBP', 'target_currency'=>'GBP', 'ex_rate'=>1);	
		endif;
	}	
	function get_exchange_rate()
	{
		return $_SESSION['currency_format']['ex_rate'];
	}	
	function set_base_currency()
	{
		$_SESSION['currency_format']['base_currency']=BASE_CURRENCY;
	}	
	function get_base_currency()
	{
		return $_SESSION['currency_format']['base_currency'];
	}	
	function get_target_currency_sign()
	{
		return $this->sign[$this->GetTargetCurrency()];
	}	
	function get_target_currency()
	{
		return $_SESSION['currency_format']['target_currency'];
	}	
	function set_target_currency($target)
	{
		$query= new query('currency');
		$query->Where="where base='".$this->GetBaseCurrency()."' and target='".$target."'";
		$object=$query->DisplayOne();
		$_SESSION['currency_format']['ex_rate']=$object->ex_rate;
		$_SESSION['currency_format']['target_currency']=$target;
	}	
	
	function get_target_currency_logo_url()
	{
		return DIR_WS_SITE_GRAPHIC_PHOTO_CURRENCY.$this->target_currency.'.jpg';
	}	
	function apply_currency_to_amount($amt)
	{
		$exRate=$this->GetExRate();
		//print $exRate; exit;
		return $amt*$exRate;
		//print $amt*$exRate;exit;
	}
};
$currency_obj= new currency();

/*
----------------------------------------------
database_table:currency
id, target, base, ex_rate
1.	USD				  GBP			2.0
----------------------------------------------
*/

?>