<?php
/**
 * <p>This is main abstract controller which must be extended by user defined
 * controllers for TPL output performing.</p>
 */
 class smarty_controller
{

	/**
	 *
	 * @var smarty
	 */
	protected $_tpl;

	/**
	 * @var string
	 */
	protected $layout = 'index';


	
	public function __construct($page)
	{
		/*check template directory, if not exists then create cache directory*/
                if (!is_dir(DIR_FS_SITE_SMARTY_COMPILED)) {
                    @mkdir(DIR_FS_SITE_SMARTY_COMPILED,0755);
                    chmod(DIR_FS_SITE_SMARTY_COMPILED,0755);  // octal; correct value of mode
                }
                if (!is_dir(DIR_FS_SITE_SMARTY_CACHE)) {
                    @mkdir(DIR_FS_SITE_SMARTY_CACHE,0755);
                    chmod(DIR_FS_SITE_SMARTY_CACHE,0755);  // octal; correct value of mode
                }
               
                if (!is_dir(DIR_FS_SITE_SMARTY_COMPILED.CURRENT_THEME)) {
                    @mkdir(DIR_FS_SITE_SMARTY_COMPILED.CURRENT_THEME,0755);
                    chmod(DIR_FS_SITE_SMARTY_COMPILED.CURRENT_THEME,0755);  // octal; correct value of mode
                }
                if (!is_dir(DIR_FS_SITE_SMARTY_CACHE.CURRENT_THEME)) {
                    @mkdir(DIR_FS_SITE_SMARTY_CACHE.CURRENT_THEME,0755);
                    chmod(DIR_FS_SITE_SMARTY_CACHE.CURRENT_THEME,0755);  // octal; correct value of mode
                }
                
                
                if (!is_dir(DIR_FS_SITE_SMARTY_COMPILED.CURRENT_THEME.'/'.$page)) {
                    @mkdir(DIR_FS_SITE_SMARTY_COMPILED.CURRENT_THEME.'/'.$page,0755);
                    chmod(DIR_FS_SITE_SMARTY_COMPILED.CURRENT_THEME.'/'.$page,0755);  // octal; correct value of mode
                }
                if (!is_dir(DIR_FS_SITE_SMARTY_CACHE.CURRENT_THEME.'/'.$page)) {
                    @mkdir(DIR_FS_SITE_SMARTY_CACHE.CURRENT_THEME.'/'.$page,0755);
                    chmod(DIR_FS_SITE_SMARTY_CACHE.CURRENT_THEME.'/'.$page,0755);  // octal; correct value of mode
                }
                /*directory check ends here*/
                
            
		/*
		 * Configure template engine settings
		 */
		$this->_tpl = new Smarty();
		// Set template engine working directories
		$this->_tpl->template_dir = DIR_FS_SITE_TEMPLATE.$page.'/';
		$this->_tpl->compile_dir = DIR_FS_SITE_SMARTY_COMPILED.CURRENT_THEME.'/'.$page.'/';
		$this->_tpl->config_dir = DIR_FS_SITE_INCLUDE_CONFIG;
		$this->_tpl->cache_dir = DIR_FS_SITE_SMARTY_CACHE.CURRENT_THEME.'/'.$page.'/';
                $this->_tpl->caching = SITE_CACHE_STATUS;
                
		// Set template files variable encapsulate delimiters
		$this->_tpl->left_delimiter = '{';
		$this->_tpl->right_delimiter = '}';
              
                /*plugins directory*/
                $this->_tpl->plugins_dir = DIR_FS_SITE_SMARTY_FUNCTION;
                
                /*debugging*/
                $this->_tpl->debugging = SITE_DEBUG_STATUS;
                
                /* error reporting */
                //$this->_tpl->error_reporting = 0;

		
	}
        /*function to assign values to variable*/
	public function assign_notnull($name, $var,$noCache=false)
	{
		if (!is_null($var)) {
			$this->_tpl->assign($name, $var,$noCache);
		}
	}
        /*function to display layout*/
	public function renderLayout($view='')
	{
		
                    if($view==''):
			$this->_tpl->display($this->layout . '.tpl');
                    else:
                        $this->_tpl->display($view.'.tpl');
                    endif;
		
	}
        /*function to set template directory*/
        public function setTemplateDirectory($view){
           
		$this->_tpl->template_dir = DIR_FS_SITE_TEMPLATE. '/'.$view.'/';
                
              
             
        }
        /*function to set cache status*/
        public function setCacheStatus($status='1'){
            
                $this->_tpl->caching = $status;
                
        }
        /*function to set delimiters*/
        public function setDelimiters($left_delimiter='<!--[',$right_delimiter=']-->'){
            
                
                 // Set template files variable encapsulate delimiters
		$this->_tpl->left_delimiter = $left_delimiter;
		$this->_tpl->right_delimiter = $right_delimiter;
                
        }
        
         /*function to check if smarty variable exists*/
        public function issetVariable($variable){
                  
               if ($this->_tpl->getTemplateVars($variable) === null) 
               {
             
                  return false;
               }
               else
               {
                   return true;
               }
                
        }


}


