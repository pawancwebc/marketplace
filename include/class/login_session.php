<?php
class user_session
{
	var $user_id;
	var $logged_in=false;
	var $error='';
	var $pass_msg=array();
	var $pass_msg_flag=false;
	var $redirect_url;
	var $redirect_url_flag=false;
	var $msg_type=false;
	var $username;
    var $country_id;
	var $verified=false;
	var $user_type='';
	var $pop_up=array();	
        var $pop_up_shown=array();	
        
	function user_session()
	{
		if(!isset($_SESSION['user_session'])): 
			$_SESSION['user_session']= array('user_id'=>'',
                                                        'logged_in'=>false,
                                                        'error'=>'',
                                                        'pass_msg'=>array(),
                                                        'pass_msg_flag'=>false,
                                                        'verified'=>false,
														'user_type'=>'',
                                                        'redirect_url'=>'',
                                                        'redirect_url_flag'=>false,
                                                        'username'=>'',
                                                        'country_id' => '',
                                                        'msg_type'=>false,
                                                        'pop_up' => array(),
                                                        'pop_up_shown' => array()
                                                   );
		endif;
	}

	function is_logged_in()
	{
		if(!isset($_SESSION['user_session'])):
			return false;
		endif;
		
		if($_SESSION['user_session']['logged_in']):
			return true;
		else:
			$this->error='Sorry! you are not logged in.';
			return false;
		endif;
	}
	
	function logout_user()
	{
		if(!isset($_SESSION['user_session'])):
			return true;
		endif;
		if($_SESSION['user_session']['logged_in']):
                        session_destroy();
                        /*
			$_SESSION['user_session']['logged_in']=false;
			$_SESSION['user_session']['user_id']='';
			$_SESSION['user_session']['username']='';
			$_SESSION['user_session']['pass_msg']='';
			$_SESSION['user_session']['pass_msg_flag']=false;
			$_SESSION['user_session']['verified']=false;
			$_SESSION['user_session']['error']='';
                         * 
                         */
			return true;
		else:
			$this->error='Sorry! you are not logged in.';
			return false;
		endif;
	}
	
	function get_user_id()
	{
		if(!isset($_SESSION['user_session'])):
			return false;
		endif;
		
		if($_SESSION['user_session']['logged_in']):
			return $_SESSION['user_session']['user_id'];
		else:
			$this->error='Sorry! you are not logged in.';
			return false;
		endif;
	}
	
	function set_user_id()
	{
		if(!isset($_SESSION['user_session'])):
			return false;
		endif;
		$_SESSION['user_session']['user_id']=$this->user_id;
		$_SESSION['user_session']['logged_in']=true;
		$_SESSION['user_session']['verified']=$this->verified;
		$_SESSION['user_session']['user_type']=$this->user_type;
		$this->logged_in='true';
		return true;
	}
	
        function get_country_id()
	{
		if(!isset($_SESSION['user_session'])):
			return false;
		endif;
		
                return $_SESSION['user_session']['country_id'];
	}
        
        function set_country_id()
	{
		if(!isset($_SESSION['user_session'])):
			return false;
		endif;
                
		$_SESSION['user_session']['country_id']=$this->country_id;
                
		return true;
	}
        
	function set_username()
	{
		$_SESSION['user_session']['username']=$this->username;
		return true;
	}
	function set_usertype($usertype)
	{
		$_SESSION['user_session']['user_type']=$usertype;
		return true;
	}	
	function get_username()
	{
		return $_SESSION['user_session']['username'];
	}
	
	function set_pass_msg()
	{
		if(!isset($_SESSION['user_session'])):
			return false;
		endif;
		$_SESSION['user_session']['pass_msg']=$this->pass_msg;
		$_SESSION['user_session']['pass_msg_flag']=1;
		return true;
	}
	
        
	function unset_pass_msg()
	{
		if(!isset($_SESSION['user_session'])):
			return false;
		endif;
		$_SESSION['user_session']['pass_msg']=array();
		$_SESSION['user_session']['pass_msg_flag']=false;
		return true;
	}
	
	function isset_pass_msg()
	{
		if(!isset($_SESSION['user_session'])):
			return false;
		endif;
		
		if($_SESSION['user_session']['pass_msg_flag']):
			return true;
		else:
			return false;
		endif;
	}
	
	function get_pass_msg()
	{
		if(!isset($_SESSION['user_session'])):
			return false;
		endif;
		if($_SESSION['user_session']['pass_msg_flag']):
			return $_SESSION['user_session']['pass_msg'];
		else:
			return false;
		endif;
		
	}
	
	function set_redirect_url()
	{	
		if(!isset($_SESSION['user_session'])):
			return false;
		endif;
		$_SESSION['user_session']['redirect_url']=$this->redirect_url;
		$_SESSION['user_session']['redirect_url_flag']=true;
		return true;
	}
	
	function isset_redirect_url()
	{
		if(!isset($_SESSION['user_session'])):
			return false;
		endif;
		
		if($_SESSION['user_session']['redirect_url_flag']==true):
			return true;
		else:
			return false;
		endif;
	}
	
	function get_redirect_url()
	{
		if(!isset($_SESSION['user_session'])):
			return false;
		endif;
		if($_SESSION['user_session']['redirect_url_flag']):
			return $_SESSION['user_session']['redirect_url'];
		else:
			return false;
		endif;
	}
	
	function set_success()
	{
		$_SESSION['user_session']['msg_type']=true;
		return true;
	}
	
	function set_error()
	{
		$_SESSION['user_session']['msg_type']=false;
		return true;
	}
	
	function get_msg_type()
	{
		return $_SESSION['user_session']['msg_type'];
	}
	
	function is_verified()
	{
		return $_SESSION['user_session']['verified'];
	}
        
        function add_popup($key,$msg,$class='success',$show_once=false,$page=false,$extra_info=false){
                if(!isset($_SESSION['user_session'])):
			return false;
		endif;
                $_SESSION['user_session']['pop_up'][$key] = array(
                                                                'msg'=>$msg,
                                                                'class' => $class,
                                                                'show_once'=>$show_once,
                                                                'page' => $page,
                                                                'extra_info' => $extra_info
                                                              );
		return true;
        }
        
        function show_popup(){
                global $page;
                // if session not set - return
                if(!isset($_SESSION['user_session'])):
			return false;
		endif;
                //print_r($_SESSION['user_session']);
                // check values in session
                if(isset($_SESSION['user_session']['pop_up']) && count($_SESSION['user_session']['pop_up'])):
                    echo '<script type="text/javascript">';
                    foreach($_SESSION['user_session']['pop_up'] as $kk=>$vv):
                        
                        $show = true;
                    
                        // check if msg to be shown once add in pop up shown array for checking next time
                        // if 'show once' added and alert will come again then pop up will not shown
                        if($vv['show_once'] && !isset($_SESSION['user_session']['pop_up_shown'][$kk])):
                                // check on which page alert should be.
                                if($vv['page'] && $page!=$vv['page']):
                                    $show = false;
                                else:
                                    $_SESSION['user_session']['pop_up_shown'][$kk] = '';
                                    $show = true;
                                endif;
                                
                        elseif($vv['show_once']):
                                // if show once and already added in shown array - then not to show pop up again.
                                $show = false;
                        endif;
                        
                        
                        // if show then add new entry for alert - popup
                        if($show):
                            if($vv['class']=='success'):
                                $pop_up_msg = '<span class="glyphicon glyphicon-ok"></span>&nbsp;';
                            else:
                                $pop_up_msg = '<span class="glyphicon glyphicon-remove"></span>&nbsp;';
                            endif;
                            $pop_up_msg .= $vv['msg'];
                            echo "generate_pop_up('".$pop_up_msg."','".$vv['class']."',null,'".$vv['extra_info']."');";
                        endif;
                        
                    endforeach;
                    echo "</script>";
                    
                    $_SESSION['user_session']['pop_up']=array();
                endif;
                return;
        }
        
        function remove_shown_popup($key=false){
                // if session not set - return
                if(!isset($_SESSION['user_session'])):
			return true;
		endif;
                if($key):
                    unset($_SESSION['user_session']['pop_up_shown'][$key]);
                else:
                    $_SESSION['user_session']['pop_up_shown']=array();
                endif;	
		return true;
        }
        
        public static function check_free_gift_msg_manually(){
            
                // if session not set - return
                if(!isset($_SESSION['user_session'])):
			return false;
		endif;
                
                // check values in session
                if(isset($_SESSION['user_session']['pop_up']['FREE_GIFT_ADD']) && 
                        count($_SESSION['user_session']['pop_up']['FREE_GIFT_ADD']) && 
                             !isset($_SESSION['user_session']['pop_up_shown']['FREE_GIFT_ADD'])
                    ):
                    // set in session that message has shown
                    $_SESSION['user_session']['pop_up_shown']['FREE_GIFT_ADD'] = '';
                    unset($_SESSION['user_session']['pop_up']['FREE_GIFT_ADD']);
                    return true;
                endif;
                return false;
        }
        
};
$login_session= new user_session();
//$_SESSION['user_session']['pop_up_shown']=array();
?>