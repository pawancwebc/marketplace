<?php 
$conf_rewrite_url=array(
	'404'=>'404',
        'home'=>'',
        'account' => 'account',
        'product' => 'product',
        'shop'   => 'shop',
        'logout'=>'logout',
        'login'=>'login',
//        'blog'=>'blog',
        'brand' => 'brand',
        'forgot_password'=> 'forgot_password',
        'category'=> 'category',
        'order'=>'order',
        'register'=>'register',
        'verify_account'=>'verify_account',
        'checkout' => 'checkout',
        'cart' => 'cart',
        'sitemap' => 'sitemap',
        'wishlist' => 'wishlist',
        'thank_you' => 'thank-you'
 );

function make_url($page, $query=NULL){
		global $conf_rewrite_url;
                global $pages_to_be_open_with_ssl;
                
		parse_str($query, $string);
		if(isset($conf_rewrite_url[strtolower($page)]) && URL_REWRITE):
			return _makeurl($page, $string);
		else:
            $dir_ws_site = DIR_WS_SITE;
            return $dir_ws_site.'?page='.$page.'&'.$query;
        endif;
}
function verify_string($string){
    if($string!='')
        if(substr($string, -1)=='/')
            return substr($string, 0, strlen($string)-1);
        
    return $string;
}
function load_url(){
    global $conf_rewrite_url;
    $prefix=str_replace(HTTP_SERVER, '', DIR_WS_SITE);
    $URL = $_SERVER['REQUEST_URI'];
    $parse_url = parse_url($URL);
//    if(strpos($URL, '?')===false):
    if(rtrim($parse_url['path'], '/') && !array_key_exists('page', $_REQUEST)):
        $string=substr($URL, -(strlen($URL)-strlen($prefix)));
        $string=verify_string($string);
        $string_parts=explode('/', $string);
        
        $url_array=array_flip($conf_rewrite_url);
        
        if(isset($url_array[$string_parts['0']])):
            _load($url_array[$string_parts['0']], $string_parts);
        else:
            if(count($string_parts)>0){
                   $URLREWRITE =  url_rewrite::getUrlByUrlName($string_parts['0']);
                   if(is_object($URLREWRITE)){
                       $string_parts['0'] = $URLREWRITE->module_id;
                       _load($URLREWRITE->page, $string_parts);
                   }
                   elseif(substr($string_parts[0], 0, 1)!='?' AND substr($string_parts[0], 0, 5)!='index'){
                     $_REQUEST['page']=$string_parts['0'];
                   }
                   else{
                      $_REQUEST['page']='404';
                   }
            }
            elseif(count($string_parts)==1){
                if(substr($string_parts[0], 0, 1)!='?' AND substr($string_parts[0], 0, 5)!='index'){
                    $_REQUEST['page']=$string_parts['0'];
                }
                else{
                   $_REQUEST['page']='404';
                }
            }
            else{
                $_REQUEST['page']='404';
            }
        endif;
    endif;
//    pr($_REQUEST);die;
}

function _makeurl($page, $string){
    
	global $pages_to_be_open_with_ssl;
        
	switch($page){
                case 'home':
                        return DIR_WS_SITE;
                        break;
                  
                case 'thank_you':
                        return DIR_WS_SITE.'thank-you';
                        break;
                    
                case '404':
                        return DIR_WS_SITE.'404/';
                        break;
                    
                case 'wishlist':
                        if(isset($string['remove'])):
                            return DIR_WS_SITE.'wishlist/'.$string['remove'];
                        else:
                            return DIR_WS_SITE.'wishlist/';
			endif;
			break;
                    
                case 'sitemap':
                        return DIR_WS_SITE.'sitemap/';
                        break;
                  
                case 'category':
                        return DIR_WS_SITE.'category/';
                        break;
                    
		case 'product':
			if(isset($string['id'])):
                            $object = url_rewrite::getUrlname('product', $string['id']);
                            if($object):
                                    return DIR_WS_SITE.$object.'/';
                            endif;
                        endif;
                        return DIR_WS_SITE.'404/';
			break;
                        
                case 'shop':
                     /*
                        $shop_defailt_id = (isset($string['id']))?$string['id']:"0";
                        $shop_defailt_p = (isset($string['p']))?$string['p']:"1";
			if(isset($string['id'])):
				$object = cwebc::getUrlname('category', $string['id']);
                                if($object):
                                    $shop_defailt_id = $object; 
                                else:
                                    $shop_defailt_id = '0';
                                endif;
			endif;
                        return DIR_WS_SITE.'shop/'.$shop_defailt_id."/".$shop_defailt_p."/";
                     
                        if(isset($string['id']) && $string['id']!='0'): 
                            $object=cwebc::getUrlname('category', $string['id']);
                            $querry = '';
                            if($object):
                                unset($string['id']);
                                if(!empty($string)):
                                    foreach($string as $kk=>$vv):
                                        $querry .= '&'.$kk.'='.$vv;
                                    endforeach;
                                endif;
                                return DIR_WS_SITE.'shop/'.$object.'/'.$querry;
                            endif;
                        else:
                            $querry ='';
                            foreach($string as $kk=>$vv):
                                $querry .= '&'.$kk.'='.$vv;
                            endforeach;
                            return DIR_WS_SITE.'shop/'.$querry;
                        endif;
                        */
                        if(isset($string['id'])):
                            $object=url_rewrite::getUrlname('category', $string['id']);
                            if($object):
                                return DIR_WS_SITE.$object.'/';
                            endif;
			endif;
                        return DIR_WS_SITE.'shop/';
			break;
                        
                case 'login':
                        
                        $dir_ws_site = DIR_WS_SITE;
                        if(in_array('login', $pages_to_be_open_with_ssl) && ENABLE_SSL_FRONT):
                            $dir_ws_site = DIR_WS_SITE_SSL;
                        endif;
                        
                        if(isset($string['redirect'])):
                            return $dir_ws_site.'login/'.$string['redirect'].'/';
                        endif;
                        
                        return $dir_ws_site.'login/';
                        
                        break;
                        
               case 'logout':
                        return DIR_WS_SITE.'logout/';
                        break; 
                    
               case 'content':
                        if(isset($string['id'])):
                            $object = url_rewrite::getUrlname('content', $string['id']);
                            if($object):
                                    return DIR_WS_SITE.$object.'/';
                            endif;
                        endif;
                        return DIR_WS_SITE.'404/';
                        break;  
                        
               case 'blog':
                        if(isset($string['id'])): 
                            $object = url_rewrite::getUrlname('blog', $string['id']);
                            if($object):
                                return DIR_WS_SITE.$object.'/';
                            endif;
                        elseif(isset($string['p'])):
                            return DIR_WS_SITE.'blog/page/'.$string['p'].'/';
                        else:
                            return DIR_WS_SITE.'blog/';
                        endif;
                        break;     
                        
                case 'brand':
                        if(isset($string['id'])):
                            $object=url_rewrite::getUrlname('brand', $string['id']);
                            if($object):
                                return DIR_WS_SITE.$object.'/';
                            endif;
			endif;
                        return DIR_WS_SITE.'brand/';
			break;   
                        
                 case 'contact':
                        if(URL_SLUG_CONTACT_US_PAGE!=''):
                            return DIR_WS_SITE.urlencode(URL_SLUG_CONTACT_US_PAGE).'/';
                        else:
                            return DIR_WS_SITE.'contact/';
                        endif;
                        break; 
                    
                case 'forgot_password':
                    
                        if(isset($string['req'])):
                            return DIR_WS_SITE.'forgot_password/'.$string['req'].'/';
                        endif;
                        return DIR_WS_SITE.'forgot_password/';
                        break;
                        
                case 'order':
                        if(isset($string['success'])):
                            //return DIR_WS_SITE.'order/account/id/'.$string['id'].'/success/';
                            return DIR_WS_SITE.'order/account/id/success/thanks/';
			elseif(isset($string['id'])):
                            return DIR_WS_SITE.'order/'.$string['id'].'/';
                        elseif(isset($string['order_id'])):
                            return DIR_WS_SITE.'order/order_id/'.$string['order_id'].'/';
                        elseif(isset($string['account'])):
                            return DIR_WS_SITE.'order/account/id/'.$string['account'].'/';
			else:
                            return DIR_WS_SITE.'order/';
			endif;
			break;
                         
                case 'register':
                        $dir_ws_site = DIR_WS_SITE;
                        if(in_array('register', $pages_to_be_open_with_ssl) && ENABLE_SSL_FRONT):
                            $dir_ws_site = DIR_WS_SITE_SSL;
                        endif;
                        
                        return DIR_WS_SITE.'register/';
                        break;
                    
                case 'verify_account':
                        if(isset($string['key'])):
                            return DIR_WS_SITE.'verify_account/'.$string['key'].'/';
			else:
                            return DIR_WS_SITE.'404/';
			endif;
                        break;
                        
               case 'account':
                        $dir_ws_site = DIR_WS_SITE;
                        if(in_array('account', $pages_to_be_open_with_ssl) && ENABLE_SSL_FRONT):
                            $dir_ws_site = DIR_WS_SITE_SSL;
                        endif;
                        
                        $default_id = (isset($string['id']))?$string['id']:"0";
                        $default_section = (isset($string['section']))?$string['section']:"dashboard";
                        $default_redirect = (isset($string['redirect']))?$string['redirect']:"account";
                        
                        if(isset($string['resend'])):
                            return $dir_ws_site.'account/'.$default_section.'/'.$default_id.'/1/'.$default_redirect.'/'.$string['resend'].'/';
                        elseif(isset($string['redirect'])):
                            return $dir_ws_site.'account/'.$default_section.'/'.$default_id.'/1/'.$string['redirect'].'/';
                        elseif(isset($string['p'])):
                            return $dir_ws_site.'account/'.$default_section.'/'.$default_id.'/'.$string['p'].'/';
                        elseif(isset($string['id'])):
                            return $dir_ws_site.'account/'.$default_section.'/'.$string['id'].'/';
                        elseif(isset($string['section'])):
                            return $dir_ws_site.'account/'.$string['section'].'/';
                        endif;
                        return $dir_ws_site.'account/';
                        break;
                        
                case 'checkout':
                        $dir_ws_site = DIR_WS_SITE;
                        if(in_array('checkout', $pages_to_be_open_with_ssl) && ENABLE_SSL_FRONT):
                            $dir_ws_site = DIR_WS_SITE_SSL;
                        endif;
                        
                        if(isset($string['view'])):
                            return $dir_ws_site.'checkout/'.$string['view'].'/';
                        elseif(isset($string['action'])):
                            return $dir_ws_site.'checkout/action/'.$string['action'].'/';
                        endif;
                        
                        return $dir_ws_site.'checkout/';
                        
                        break; 

                case 'new_products':
                        $querry ='';
                        foreach($string as $kk=>$vv):
                            $querry .= '&'.$kk.'='.$vv;
                        endforeach;
                        if(trim($querry)!=''):
                            $querry = $querry.'/';
                        endif;
                        if(URL_SLUG_NEW_PRODUCTS_PAGE!=''):
                            return DIR_WS_SITE.urlencode(URL_SLUG_NEW_PRODUCTS_PAGE).'/'.$querry;
                        else:
                            return DIR_WS_SITE.'new_products/'.$querry;
                        endif;
                        
                        break;   
                        
                case 'top_products':
                        $querry ='';
                        foreach($string as $kk=>$vv):
                            $querry .= '&'.$kk.'='.$vv;
                        endforeach;
                        if(trim($querry)!=''):
                            $querry = $querry.'/';
                        endif;
                        if(URL_SLUG_TOP_PRODUCTS_PAGE!=''):
                            return DIR_WS_SITE.urlencode(URL_SLUG_TOP_PRODUCTS_PAGE).'/'.$querry;
                        else:
                            return DIR_WS_SITE.'top_products/'.$querry;
                        endif;
                        break;  
                        
                case 'on_sale':
                        $querry ='';
                        foreach($string as $kk=>$vv):
                            $querry .= '&'.$kk.'='.$vv;
                        endforeach;
                        if(trim($querry)!=''):
                            $querry = $querry.'/';
                        endif;
                        if(URL_SLUG_ON_SALE_PAGE!=''):
                            return DIR_WS_SITE.urlencode(URL_SLUG_ON_SALE_PAGE).'/'.$querry;
                        else:
                            return DIR_WS_SITE.'on_sale/'.$querry;
                        endif;
                        break;   
                        
               case 'cart':
                        if(isset($string['update'])):
                            
                            return DIR_WS_SITE.'cart/'.$string['update']."/".$string['row'].'/';
                        
                        elseif(isset($string['remove_coupon'])):
                            
                            return DIR_WS_SITE.'cart/remove/';
                        
			else:
                            return DIR_WS_SITE.'cart/';
			endif;
                        break;
		default: break;
	}
}

function _load($page, $string_parts){
    
	global $conf_rewrite_url;
        
	switch($page){
                case 'home':
                        return DIR_WS_SITE; 
                        break;
                    
                case 'thank_you':
                        $_REQUEST['page'] = 'thank_you';
                        break;
                    
                case '404':
                        $_REQUEST['page']='404';
                        break;
                
                case 'wishlist':
                        /* only product id is passed with the URL */
			if(count($string_parts)==2):
                            $_GET['remove']=$string_parts['1'];
			endif;
                        $_REQUEST['page']='wishlist';
                        break;
                  
                case 'sitemap':
                        $_REQUEST['page']='sitemap';
                        break;
                    
                case 'category':
                        $_REQUEST['page']='category';
                        break;
                    
		case 'product':
                   
                        if(count($string_parts)){
                            foreach($string_parts as $kk=>$vv):
                                if($kk==0):
                                    $_GET['id']=$vv;
                                else:
                                    $queries = explode('&',$vv);
                                    foreach($queries as $kkk=>$vvv):
                                         $query = explode('=',$vvv);
                                         if(isset($query[0]) && $query[0]!=''):
                                            $_GET[trim($query[0],'&')]=$query[1];
                                         endif;
                                    endforeach;
                                endif;
                            endforeach;
                            $_REQUEST['page']='product';
                        }
                        else{
                          $_REQUEST['page']='404';  
                        }
			break;
                        
              case 'shop':
                        if(count($string_parts)){
                            foreach($string_parts as $kk=>$vv):
                                if($kk==0 && $vv!='shop'):
                                    $_GET['id']=$vv;
                                else:
                                    $queries = explode('&',$vv);
                                    foreach($queries as $kkk=>$vvv):
                                         $query = explode('=',$vvv);
                                         if(isset($query[0]) && $query[0]!='' && isset($query[1])):
                                            $_GET[trim($query[0],'&')]=$query[1];
                                         endif;
                                    endforeach;
                                endif;
                            endforeach;
                        }
                        $_REQUEST['page']='shop';
                        break;  

              case 'login':
                          if(count($string_parts)==2):
                             $_GET['redirect']=$string_parts['1'];
                          endif;
                          $_REQUEST['page']='login';
                          break;  
                        
              case 'logout':
			    $_REQUEST['page']='logout';
                            break;  
              case 'content':
                        if(count($string_parts)){
                            foreach($string_parts as $kk=>$vv):
                                if($kk==0):
                                    $_GET['id']=$vv;
                                else:
                                    $queries = explode('&',$vv);
                                    foreach($queries as $kkk=>$vvv):
                                         $query = explode('=',$vvv);
                                         if(isset($query[0]) && $query[0]!=''):
                                            $_GET[trim($query[0],'&')]=$query[1];
                                         endif;
                                    endforeach;
                                endif;
                            endforeach;
                            $_REQUEST['page']='content';
                        }
                        else{
                          $_REQUEST['page']='404';  
                        }
			break;
              case 'blog':
                        if(count($string_parts)==3):
                            $_GET['p']=$string_parts['2'];
                        endif;
                        if(is_numeric($string_parts)){
                            $_GET['id']=$string_parts;
                        }
                        $_REQUEST['page']='blog';
			break;  
                        
              case 'brand':
                        if(count($string_parts)){
                            foreach($string_parts as $kk=>$vv):
                                if($kk==0):
                                    $_GET['id']=$vv;
                                else:
                                    $queries = explode('&',$vv);
                                    foreach($queries as $kkk=>$vvv):
                                         $query = explode('=',$vvv);
                                         if(isset($query[0]) && $query[0]!=''):
                                            $_GET[trim($query[0],'&')]=$query[1];
                                         endif;
                                    endforeach;
                                endif;
                            endforeach;
                        }
                        $_REQUEST['page']='brand';
                        break;  
                        
                case 'contact':
                        $_REQUEST['page']='contact';
                        break;   
                    
                case 'forgot_password':
                          if(count($string_parts)==2):
                             $_GET['req']=$string_parts['1'];
                          endif;
                          $_REQUEST['page']='forgot_password';
                          break;  
                          
                case 'order':
			if(count($string_parts)==2):
                            $_GET['id']=$string_parts['1'];
                        elseif(count($string_parts)==3):
                            $_GET['order_id']=$string_parts['2'];
                        elseif(count($string_parts)==4):
                            $_GET['account']=$string_parts['3'];
                        elseif(count($string_parts)==5):
                            if('success'!=$string_parts['3']):
                                $_GET['id']=$string_parts['3'];
                            else:
                                if(isset($_SESSION['user_session']['order_id'])):
                                    $_GET['id'] = $_SESSION['user_session']['order_id'];
                                    unset($_SESSION['user_session']['order_id']);
                                endif;
                            endif;
                            $_GET['success']=1;
			endif;
                        $_REQUEST['page']='order';
			break; 
                        
                case 'register':
                        $_REQUEST['page']='register';
                        break; 
                    
                case 'verify_account':
                          if(count($string_parts)==2):
                             $_GET['key']=$string_parts['1'];
                             $_REQUEST['page']='verify_account';
                          else:
                              $_REQUEST['page']='404';
                          endif;
                          break; 
                          
               case 'account':
                          if(count($string_parts)==6):
                            $_GET['resend']=$string_parts['5'];
                            $_GET['redirect']=$string_parts['4'];
                            $_GET['p']=$string_parts['3'];
                            $_GET['id']=$string_parts['2'];
                            $_GET['section']=$string_parts['1'];
                          elseif(count($string_parts)==5):
                            $_GET['redirect']=$string_parts['4'];
                            $_GET['p']=$string_parts['3'];
                            $_GET['id']=$string_parts['2'];
                            $_GET['section']=$string_parts['1'];
                          elseif(count($string_parts)==4):
                            $_GET['p']=$string_parts['3'];
                            $_GET['id']=$string_parts['2'];
                            $_GET['section']=$string_parts['1'];  
                          elseif(count($string_parts)==3):
                            $_GET['id']=$string_parts['2'];
                            $_GET['section']=$string_parts['1'];
                          elseif(count($string_parts)==2): 
                            $_GET['section']=$string_parts['1'];
                          endif;
                            $_REQUEST['page']='account';
                          break;
                   
                 case 'checkout':
                        if(count($string_parts)==3): 
                            $_GET['action']=$string_parts['2'];
                        elseif(count($string_parts)==2): 
                            $_GET['view']=$string_parts['1'];
                        endif;
                        $_REQUEST['page']='checkout';
                        break;
                    
                 case 'new_products':
                         if(count($string_parts)>1):
                             $last_parts=explode('&',$string_parts['1']);
                             foreach($last_parts as $key=>$part):
                                 if($part!=''):
                                    $get_variables=explode('=',$part);
                                    $_GET[$get_variables[0]]=$get_variables[1];
                                endif;
                             endforeach;
                          endif;
                          $_REQUEST['page']='new_products';
                          break;  
                       
                case 'top_products':

                          if(count($string_parts)>1):
                             $last_parts=explode('&',$string_parts['1']);
                             foreach($last_parts as $key=>$part):
                                 if($part!=''):
                                    $get_variables=explode('=',$part);
                                    $_GET[$get_variables[0]]=$get_variables[1];
                                endif;
                             endforeach;
                          endif;
                          $_REQUEST['page']='top_products';
                         
                          break; 
                      
                case 'on_sale':

                          if(count($string_parts)>1):
                             $last_parts=explode('&',$string_parts['1']);
                             foreach($last_parts as $key=>$part):
                                 if($part!=''):
                                    $get_variables=explode('=',$part);
                                    $_GET[$get_variables[0]]=$get_variables[1];
                                endif;
                             endforeach;
                          endif;
                          $_REQUEST['page']='on_sale';
                         
                          break;  
                          
               case 'cart':
                          if(count($string_parts)==3):
                              
                             $_GET['update']=$string_parts['1'];
                             $_GET['row']=$string_parts['2'];
                             
                          elseif(count($string_parts)==2):
                              
                             $_GET['remove_coupon'] = '1';
                          
                          endif;
                          
                          $_REQUEST['page']='cart';
                          
                          break; 
                          
               default:
	}
}

?>