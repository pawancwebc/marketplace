<?
function PageControl($page,$totalPages,$totalRecords,$url,$querystring='',$type=1,$Class='pad',$tdclass='',$Title='Records',$LClass='cat')
	{
			# $Pp-previous page
			# $Np- next page
			($page>=$totalPages)?$Np=$totalPages:$Np=$page+1;
			($page<=1)?$Pp=1:$Pp=$page-1;
			if($totalPages>3):
				if(($page+3) <=$totalPages):
					$end=$page+3;
					$begin=$page;
				else:
					$begin=$totalPages-3;
					$end=$totalPages;
				endif;
			else:
				$begin=1;
				$end=$totalPages;
			endif;
			?>
					<div class="pagination">
                                            <a href="<?=$url?>?page=<?=$Pp?>&<?=$querystring?>" title="Previous Page">&laquo;</a>
						<?php for($i=$begin;$i<=$totalPages && $i<=$end;$i++):?>
								<?php if($i==$page):?>
								<a href="<?=$url?>?page=<?=$i?>&<?=$querystring?>" class="active" title="Page No: <?=$i?>"><?=$i?></a>
								<?php else:?>
								<a href="<?=$url?>?page=<?=$i?>&<?=$querystring?>"  title="Page No: <?=$i?>"><?=$i?></a>
								<?php endif;?>

						<?php endfor;?>
                                                                <a href="<?=$url?>?page=<?=$Np?>&<?=$querystring?>" title="Next Page">&raquo;</a>
					</div>
			<?php


	}

	function PageControl_front($page,$totalPages,$totalRecords,$url,$querystring='',$type=1,$Class='pad',$tdclass='',$Title='Records',$LClass='cat')
{
	if($type==1):
		?>
		<table width="100%" cellspacing="1" cellpadding="2" align="center" class="<?=$Class?>">
			<tr>
				<td class="<?=$tdclass?>" width="30%" align="left">Total&nbsp;<?=$Title?>:&nbsp;&nbsp;<?=$totalRecords?></td>
				<td align="right" >Pages:&nbsp;&nbsp;
				<?php for($i=1;$i<=$totalPages;$i++):?>
					<?php if($i==$page):?>
						<?=display_url($i, $url, 'p='.$i.'&'.$querystring,'blockselected');?>
					<?php else: ?>
						<?=display_url($i, $url, 'p='.$i.'&'.$querystring,$LClass);?>
					<?php endif;?>
				<?php endfor;?>
				</td>
			</tr>
		</table>
		<?
	elseif($type==2):
		# $Pp-previous page
		# $Np- next page
		($page>=$totalPages)?$Np=$totalPages:$Np=$page+1;
		($page<=1)?$Pp=1:$Pp=$page-1;
		if($totalPages>3):
			if(($page+3) <=$totalPages):
				$end=$page+3;
				$begin=$page;
			else:
				$begin=$totalPages-3;
				$end=$totalPages;
			endif;
		else:
			$begin=1;
			$end=$totalPages;
		endif;
		?>
		<table width="100%" cellspacing="0" cellpadding="0" align="center"  >
            <tr >
				<td  align="right" class="page_height" >
                <div class="page_outer">
				<a href="<?=make_url($url)?>p=<?=$Pp?>&<?=$querystring?>" class="pnp" title="Previous Page"><?php echo"<<"?></a>&nbsp;
				<?
				for($i=$begin;$i<=$totalPages && $i<=$end;$i++):
					if($i==$page):
						echo display_url($i, $url, 'p='.$i.'&'.$querystring,'blockselected'); echo'&nbsp;&nbsp;';
					else: 
						echo display_url($i, $url, 'p='.$i.'&'.$querystring,$LClass);echo'&nbsp;&nbsp;';
					endif;					
				endfor;
				?>
				<a href="<?=make_url($url)?>p=<?=$Np?>&<?=$querystring?>" class="pnp" title="Next Page"  ><?php echo">>"?></a>
				</div>
                </td>
			</tr>
                    </thead>
		</table>
		<?
	endif;
}	



function PageControl_front_tabs($page,$totalPages,$totalRecords,$url,$querystring='',$type=1,$Class='pad',$tdclass='',$Title='Records',$LClass='cat', $pag='')
{
	if($type==1):
		?>
		<table width="100%" cellspacing="1" cellpadding="2" align="center" class="<?=$Class?>">
			<tr>
				<td class="<?=$tdclass?>" width="30%" align="left">Total&nbsp;<?=$Title?>:&nbsp;&nbsp;<?=$totalRecords?></td>
				<td align="right" >Pages:&nbsp;&nbsp;
				<?php for($i=1;$i<=$totalPages;$i++):?>
					<?php if($i==$page):?>
						<?=display_url($i, $url, $pag.'='.$i.'&'.$querystring,'blockselected');?>
					<?php else: ?>
						<?=display_url($i, $url, $pag.'='.$i.'&'.$querystring,$LClass);?>
					<?php endif;?>
				<?php endfor;?>
				</td>
			</tr>
		</table>
		<?
	elseif($type==2):
		# $Pp-previous page
		# $Np- next page
		($page>=$totalPages)?$Np=$totalPages:$Np=$page+1;
		($page<=1)?$Pp=1:$Pp=$page-1;
		if($totalPages>3):
			if(($page+3) <=$totalPages):
				$end=$page+3;
				$begin=$page;
			else:
				$begin=$totalPages-3;
				$end=$totalPages;
			endif;
		else:
			$begin=1;
			$end=$totalPages;
		endif;
		?>
		<table width="100%" cellspacing="0" cellpadding="0" align="center"  >
            <tr >
				<td  align="right" class="page_height" >
                <div class="page_outer">
				<a href="<?=make_url($url)?><?=$pag?>=<?=$Pp?>&<?=$querystring?>" class="pnp" title="Previous Page"><?php echo"<<"?></a>&nbsp;
				<?
				for($i=$begin;$i<=$totalPages && $i<=$end;$i++):
					if($i==$page):
						echo display_url($i, $url, $pag.'='.$i.'&'.$querystring,'blockselected'); echo'&nbsp;&nbsp;';
					else: 
						echo display_url($i, $url, $pag.'='.$i.'&'.$querystring,$LClass);echo'&nbsp;&nbsp;';
					endif;					
				endfor;
				?>
				<a href="<?=make_url($url)?><?=$pag?>=<?=$Np?>&<?=$querystring?>"  class="pnp" title="Next Page"  ><?php echo">>"?></a>
				</div>
                </td>
			</tr>
                    </thead>
		</table>
		<?
	endif;
}	



























?>