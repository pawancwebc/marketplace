<?php 
$conf_rewrite_url=array(
	'404'=>'404',
        'home'=>'',
        'account' => 'account',
        'product' => 'product',
        'shop'   => 'shop',
        'logout'=>'logout',
        'login'=>'login',
        'blog'=>'blog',
        'brand' => 'brand',
        'forgot_password'=> 'forgot_password',
        'order'=>'order',
        'register'=>'register',
        'verify_account'=>'verify_account',
        'checkout' => 'checkout',
        'cart' => 'cart',
        'sitemap' => 'sitemap',
        'wishlist' => 'wishlist',
    
        'contact'=>(URL_SLUG_CONTACT_US_PAGE!='')?urlencode(URL_SLUG_CONTACT_US_PAGE):"contact",
        'content'=>(URL_SLUG_CONTENT_PAGE!='')?urlencode(URL_SLUG_CONTENT_PAGE):"page",
        'new_products' => (URL_SLUG_NEW_PRODUCTS_PAGE!='')?urlencode(URL_SLUG_NEW_PRODUCTS_PAGE):'new_products',
        'top_products' => (URL_SLUG_TOP_PRODUCTS_PAGE!='')?urlencode(URL_SLUG_TOP_PRODUCTS_PAGE):'top_products',
        'on_sale' => (URL_SLUG_ON_SALE_PAGE!='')?urlencode(URL_SLUG_ON_SALE_PAGE):'on_sale'
);

function make_url($page, $query=NULL){
		global $conf_rewrite_url;
		parse_str($query, $string);
		if(isset($conf_rewrite_url[strtolower($page)]) && URL_REWRITE)
			return _makeurl($page, $string);
		else
			return DIR_WS_SITE.'?page='.$page.'&'.$query;
}
function verify_string($string){
    if($string!='')
        if(substr($string, -1)=='/')
            return substr($string, 0, strlen($string)-1);
        
    return $string;
}
function load_url(){
    global $conf_rewrite_url;
    $prefix=str_replace(HTTP_SERVER, '', DIR_WS_SITE);
    $URL = $_SERVER['REQUEST_URI'];
    if(strpos($URL, '?')===false):
        $string=substr($URL, -(strlen($URL)-strlen($prefix)));
        $string=verify_string($string);
        $string_parts=explode('/', $string);
        $url_array=array_flip($conf_rewrite_url);
        if(isset($url_array[$string_parts['0']])):
            _load($url_array[$string_parts['0']], $string_parts);
        else:
            if(count($string_parts)==1){
                if(substr($string_parts['0'], -5)=='.html'){
                    $product_url_name = substr($string_parts['0'], 0,-5);
                    _load('product', $product_url_name);
                }
                else{   
                    $category_id = $object=cwebc::getIdByUrlname('category', urldecode($string_parts['0']));
                    if($category_id){
                        _load('shop', $category_id);
                    }
                    elseif(substr($string_parts[0], 0, 1)!='?' AND substr($string_parts[0], 0, 5)!='index'){
                        $_REQUEST['page']=$string_parts['0'];
                    }
                }
            }
        endif;
    endif;
}

function _makeurl($page, $string){
	
	switch($page){
                case 'home':
                        return DIR_WS_SITE;
                        break;
                    
                case '404':
                        return DIR_WS_SITE.'404';
                        break;
                    
                case 'wishlist':
                        if(isset($string['remove'])):
                            return DIR_WS_SITE.'wishlist/'.$string['remove'];
                        else:
                            return DIR_WS_SITE.'wishlist';
			endif;
			break;
                    
                case 'sitemap':
                        return DIR_WS_SITE.'sitemap';
                        break;
                    
		case 'product':
			if(isset($string['id'])):
				$object = cwebc::getUrlname('product', $string['id']);
				if($object):
                                        //return DIR_WS_SITE.'product/'.$object; 
                                        return DIR_WS_SITE.$object.'.html';
				else:
					return DIR_WS_SITE.'404';
				endif;
			else:
                            return DIR_WS_SITE.'404';
			endif;
			break;
                        
                case 'shop':
                     /*
                        $shop_defailt_id = (isset($string['id']))?$string['id']:"0";
                        $shop_defailt_p = (isset($string['p']))?$string['p']:"1";
			if(isset($string['id'])):
				$object = cwebc::getUrlname('category', $string['id']);
                                if($object):
                                    $shop_defailt_id = $object; 
                                else:
                                    $shop_defailt_id = '0';
                                endif;
			endif;
                        return DIR_WS_SITE.'shop/'.$shop_defailt_id."/".$shop_defailt_p."/";
                     
                        if(isset($string['id']) && $string['id']!='0'): 
                            $object=cwebc::getUrlname('category', $string['id']);
                            $querry = '';
                            if($object):
                                unset($string['id']);
                                if(!empty($string)):
                                    foreach($string as $kk=>$vv):
                                        $querry .= '&'.$kk.'='.$vv;
                                    endforeach;
                                endif;
                                return DIR_WS_SITE.'shop/'.$object.'/'.$querry;
                            endif;
                        else:
                            $querry ='';
                            foreach($string as $kk=>$vv):
                                $querry .= '&'.$kk.'='.$vv;
                            endforeach;
                            return DIR_WS_SITE.'shop/'.$querry;
                        endif;
                        */
                        if(isset($string['id'])):
                            $object=cwebc::getUrlname('category', $string['id']);
                            if($object):
                                return DIR_WS_SITE.$object;
                            endif;
			endif;
                        return DIR_WS_SITE.'shop';
			break;
                        
                case 'login':
                        if(isset($string['redirect'])):
                            return DIR_WS_SITE.'login/'.$string['redirect'];
                        endif;
                        return DIR_WS_SITE.'login';
                        break;
                        
               case 'logout':
                        return DIR_WS_SITE.'logout';
                        break; 
                    
               case 'content':
                        if(isset($string['id'])):
                            $object=cwebc::getUrlname('content', $string['id']);
                            if($object):
                                if(URL_SLUG_CONTENT_PAGE!=''):
                                    return DIR_WS_SITE.urlencode(URL_SLUG_CONTENT_PAGE).'/'.$object;
                                else:
                                    return DIR_WS_SITE.'page/'.$object;
                                endif;
                            endif;
                        else:
                            return DIR_WS_SITE.'404';
                        endif;
                        break;  
                        
               case 'blog':
                        if(isset($string['id'])): 
                            $object=cwebc::getUrlname('blog', $string['id']);
                            if($object):
                                return DIR_WS_SITE.'blog/'.$object;
                            endif;
                        elseif(isset($string['p'])):
                            return DIR_WS_SITE.'blog/page/'.$string['p'];
                        else:
                            return DIR_WS_SITE.'blog';
                        endif;
                        break;     
                        
                case 'brand':
                        /*
                        $brand_defailt_id = (isset($string['id']))?$string['id']:"0";
                        $brand_defailt_p = (isset($string['p']))?$string['p']:"1";
			if(isset($string['id'])):
				$object = cwebc::getUrlname('brand', $string['id']);
                                if($object):
                                    $brand_defailt_id = $object; 
                                else:
                                    $brand_defailt_id = '0';
                                endif;
			endif;
                        return DIR_WS_SITE.'brand/'.$brand_defailt_id."/".$brand_defailt_p."/";
                        */
                        if(isset($string['id'])):
                            $object=cwebc::getUrlname('brand', $string['id']);
                            if($object):
                                return DIR_WS_SITE.'brand/'.$object;
                            endif;
			endif;
                        return DIR_WS_SITE.'brand';
			break;   
                        
                 case 'contact':
                        if(URL_SLUG_CONTACT_US_PAGE!=''):
                            return DIR_WS_SITE.urlencode(URL_SLUG_CONTACT_US_PAGE);
                        else:
                            return DIR_WS_SITE.'contact';
                        endif;
                        break; 
                    
                case 'forgot_password':
                        if(isset($string['req'])):
                            return DIR_WS_SITE.'forgot_password/'.$string['req'];
                        endif;
                        return DIR_WS_SITE.'forgot_password';
                        break;
                        
                case 'order':
			if(isset($string['id'])):
                            return DIR_WS_SITE.'order/'.$string['id'];
			else:
                            return DIR_WS_SITE.'404';
			endif;
			break;
                        
                case 'register':
                        return DIR_WS_SITE.'register';
                        break;
                    
                case 'verify_account':
                        if(isset($string['key'])):
                            return DIR_WS_SITE.'verify_account/'.$string['key'];
			else:
                            return DIR_WS_SITE.'404';
			endif;
                        break;
                        
               case 'account':
                        $default_id = (isset($string['id']))?$string['id']:"0";
                        $default_section = (isset($string['section']))?$string['section']:"dashboard";
                        if(isset($string['redirect'])):
                            return DIR_WS_SITE.'account/'.$default_section.'/'.$default_id.'/'.$string['redirect'];
                        elseif(isset($string['id'])):
                            return DIR_WS_SITE.'account/'.$default_section.'/'.$string['id'];
                        elseif(isset($string['section'])):
                            return DIR_WS_SITE.'account/'.$string['section'];
                        endif;
                        return DIR_WS_SITE.'account';
                        break;
                        
                case 'checkout':
                        return DIR_WS_SITE.'checkout';
                        break; 

                case 'new_products':
                        /*
                        if(isset($string['p'])): 
                            return DIR_WS_SITE.'new_products/'.$string['p'].'/';
                        else:
                            return DIR_WS_SITE.'new_products/1/';
                        endif;
                        */
                        $querry ='';
                        foreach($string as $kk=>$vv):
                            $querry .= '&'.$kk.'='.$vv;
                        endforeach;
                        if(URL_SLUG_NEW_PRODUCTS_PAGE!=''):
                            return DIR_WS_SITE.urlencode(URL_SLUG_NEW_PRODUCTS_PAGE).'/'.$querry;
                        else:
                            return DIR_WS_SITE.'new_products/'.$querry;
                        endif;
                        
                        break;   
                        
                case 'top_products':
                        $querry ='';
                        foreach($string as $kk=>$vv):
                            $querry .= '&'.$kk.'='.$vv;
                        endforeach;
                        if(URL_SLUG_TOP_PRODUCTS_PAGE!=''):
                            return DIR_WS_SITE.urlencode(URL_SLUG_TOP_PRODUCTS_PAGE).'/'.$querry;
                        else:
                            return DIR_WS_SITE.'top_products/'.$querry;
                        endif;
                        break;  
                        
                case 'on_sale':
                        $querry ='';
                        foreach($string as $kk=>$vv):
                            $querry .= '&'.$kk.'='.$vv;
                        endforeach;
                        if(URL_SLUG_ON_SALE_PAGE!=''):
                            return DIR_WS_SITE.urlencode(URL_SLUG_ON_SALE_PAGE).'/'.$querry;
                        else:
                            return DIR_WS_SITE.'on_sale/'.$querry;
                        endif;
                        break;   
                        
               case 'cart':
                        if(isset($string['update'])):
                            return DIR_WS_SITE.'cart/'.$string['update']."/".$string['row'];
			else:
                            return DIR_WS_SITE.'cart';
			endif;
                        break;
		default: break;
	}
}

function _load($page, $string_parts){
	global $conf_rewrite_url;
	switch($page){
                case 'home':
                        return DIR_WS_SITE; 
                        break;
                    
                case '404':
                        $_REQUEST['page']='404';
                        break;
                
                case 'wishlist':
                        /* only product id is passed with the URL */
			if(count($string_parts)==2):
                            $_GET['remove']=$string_parts['1'];
			endif;
                        $_REQUEST['page']='wishlist';
                        break;
                  
                case 'sitemap':
                        $_REQUEST['page']='sitemap';
                        break;
                    
		case 'product':
                        if(is_string($string_parts)){
                            $id = cwebc::getIdByUrlname('product',urldecode($string_parts));
                            $_GET['id']=$id;
                            $_REQUEST['page']='product';
                        }
                        else{
                          $_REQUEST['page']='404';  
                        }
                        
			break;
                        
              case 'shop':
                        /*
                          $total_string_parts = count($string_parts);
                          $last_parts=explode('&',$string_parts[$total_string_parts-1]);
                          if(!empty($last_parts)):
                             foreach($last_parts as $key=>$part):
                                 if($part!=''):
                                    $get_variables=explode('=',$part);
                                    $_GET[$get_variables[0]]=$get_variables[1];
                                endif;
                             endforeach;
                          endif;
                          if(count($string_parts)==2):
                          $object=cwebc::getIdByUrlname('category', urldecode($string_parts['1']));
                          endif;
                          $_GET['id']=$object;
                          $_GET['p']=$string_parts['2'];
                          $_REQUEST['page']='shop';
                        */
                            if(is_numeric($string_parts)){
                                $_GET['id']=$string_parts;
                            }
                            $_REQUEST['page']='shop';
                          
                          break;  

              case 'login':
                          if(count($string_parts)==2):
                             $_GET['redirect']=$string_parts['1'];
                          endif;
                          $_REQUEST['page']='login';
                          break;  
                        
              case 'logout':
			    $_REQUEST['page']='logout';
                            break;  
              case 'content':
                        if(count($string_parts)==2):
                             $object=cwebc::getIdByUrlname('content', urldecode($string_parts['1']));
                             $_REQUEST['page']='content';
                             $_GET['id']=$object;
                        else:
                            $_REQUEST['page']='404';
                        endif;
			break;   
              case 'blog':
                        if(count($string_parts)==3):
                             $_GET['p']=$string_parts['2'];
                        elseif(count($string_parts)==2):
                             $object=cwebc::getIdByUrlname('blog', urldecode($string_parts['1']));
                             $_GET['id']=$object;
                        endif;
                        $_REQUEST['page']='blog';
			break;  
                        
              case 'brand':
                    /*
			if(count($string_parts)==4):
                             $last_parts=explode('&',$string_parts['3']);
                             foreach($last_parts as $key=>$part):
                                 if($part!=''):
                                    $get_variables=explode('=',$part);
                                    $_GET[$get_variables[0]]=$get_variables[1];
                                endif;
                             endforeach;
                          endif;
                          $object=cwebc::getIdByUrlname('brand', urldecode($string_parts['1']));
                          $_GET['id']=$object;
                          $_GET['p']=$string_parts['2'];
                          $_REQUEST['page']='brand';
                          */
                        if(count($string_parts)==2):
                             $object=cwebc::getIdByUrlname('brand', urldecode($string_parts['1']));
                             $_GET['id']=$object;
                        else:
                        endif;
                        $_REQUEST['page']='brand';
                        break;  
                        
                case 'contact':
                        $_REQUEST['page']='contact';
                        break;   
                    
                case 'forgot_password':
                          if(count($string_parts)==2):
                             $_GET['req']=$string_parts['1'];
                          endif;
                          $_REQUEST['page']='forgot_password';
                          break;  
                          
                case 'order':
			if(count($string_parts)==2):
                            $_GET['id']=$string_parts['1'];
                            $_REQUEST['page']='order';
                        else:
                            $_REQUEST['page']='404';  
			endif;
			break; 
                        
                case 'register':
                        $_REQUEST['page']='register';
                        break; 
                    
                case 'verify_account':
                          if(count($string_parts)==2):
                             $_GET['key']=$string_parts['1'];
                             $_REQUEST['page']='verify_account';
                          else:
                              $_REQUEST['page']='404';
                          endif;
                          break; 
                          
               case 'account':
                          if(count($string_parts)==4):
                            $_GET['redirect']=$string_parts['3'];
                            $_GET['id']=$string_parts['2'];
                            $_GET['section']=$string_parts['1'];
                          elseif(count($string_parts)==3):
                            $_GET['id']=$string_parts['2'];
                            $_GET['section']=$string_parts['1'];
                          elseif(count($string_parts)==2): 
                            $_GET['section']=$string_parts['1'];
                          endif;
                            $_REQUEST['page']='account';
                          break;
                   
                 case 'checkout':
                        $_REQUEST['page']='checkout';
                        break;
                    
                 case 'new_products':
                     /*
                          if(count($string_parts)==3):
                             $_GET['p']=$string_parts['1'];
                             $last_parts=explode('&',$string_parts['2']);
                             foreach($last_parts as $key=>$part):
                                 if($part!=''):
                                    $get_variables=explode('=',$part);
                                    $_GET[$get_variables[0]]=$get_variables[1];
                                endif;
                             endforeach;
                          endif;
                          if(count($string_parts)==2):
                             $_GET['p']=$string_parts['1'];
                          endif;
                          $_REQUEST['page']='new_products';
                       */  
                         if(count($string_parts)>1):
                             $last_parts=explode('&',$string_parts['1']);
                             foreach($last_parts as $key=>$part):
                                 if($part!=''):
                                    $get_variables=explode('=',$part);
                                    $_GET[$get_variables[0]]=$get_variables[1];
                                endif;
                             endforeach;
                          endif;
                          $_REQUEST['page']='new_products';
                          break;  
                       
                case 'top_products':

                          if(count($string_parts)>1):
                             $last_parts=explode('&',$string_parts['1']);
                             foreach($last_parts as $key=>$part):
                                 if($part!=''):
                                    $get_variables=explode('=',$part);
                                    $_GET[$get_variables[0]]=$get_variables[1];
                                endif;
                             endforeach;
                          endif;
                          $_REQUEST['page']='top_products';
                         
                          break; 
                      
                case 'on_sale':

                          if(count($string_parts)>1):
                             $last_parts=explode('&',$string_parts['1']);
                             foreach($last_parts as $key=>$part):
                                 if($part!=''):
                                    $get_variables=explode('=',$part);
                                    $_GET[$get_variables[0]]=$get_variables[1];
                                endif;
                             endforeach;
                          endif;
                          $_REQUEST['page']='on_sale';
                         
                          break;  
                          
               case 'cart':
                          if(count($string_parts)==3):
                             $_GET['update']=$string_parts['1'];
                             $_GET['row']=$string_parts['2'];
                          endif;
                          $_REQUEST['page']='cart';
                          break; 
                          
               default:
	}
}

?>