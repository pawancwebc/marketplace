<?php
include('url_rewrite.php');

/* sanitize funtion */

function sanitize($value) {
    $prohibited_chars = array(" - $", "--", " - ", " / ", "- ", "  ", " ", "/", "$", "&", "'", '%', '"', "@", "");
    foreach ($prohibited_chars as $k => $v):
        $value = str_replace($v, '-', $value);
    endforeach;
    return strtolower($value);
}

function include_functions($functions) {
    foreach ($functions as $value):
        if (file_exists(DIR_FS_SITE . 'include/function/' . $value . '.php')):
            include_once(DIR_FS_SITE . 'include/function/' . $value . '.php');
        endif;
    endforeach;
}

function display_message($unset = 0) {
    $admin_user = new admin_session();
    if ($admin_user->isset_pass_msg()):
        if (isset($_SESSION['admin_session_secure']['msg_type']) && $_SESSION['admin_session_secure']['msg_type'] == 0):
            $error_type = 'danger';
            $icon_class = 'icon-warning-sign';
        else:
            $error_type = 'success';
            $icon_class = 'icon-ok';
        endif;
        foreach ($admin_user->get_pass_msg() as $value):
            echo '<div class="alert alert-' . $error_type . '">';
            echo '<i class="' . $icon_class . '"></i>&nbsp;&nbsp;';
            echo '<button class="close" data-dismiss="alert"></button><strong>';
            echo $value;
            echo '</strong></div>';
        endforeach;
    endif;
    ($unset) ? $admin_user->unset_pass_msg() : '';
}

function get_var_if_set($array, $var, $preset = '') {
    return isset($array[$var]) ? $array[$var] : $preset;
}

function get_var_set($array, $var, $var1) {
    if (isset($array[$var])):
        return $array[$var];
    else:
        return $var1;
    endif;
}

function get_template($template_name, $array, $selected = '') {
    include_once(DIR_FS_SITE . 'template/' . TEMPLATE . '/' . $template_name . '.php');
}

function get_meta($page) {
    $page = trim($page);
    if ($page != ''):
        $query = new query('keywords');
        $query->Where = "where page_name='$page'";
        if ($content = $query->DisplayOne()):
            return $content;
        else:
            return null;
        endif;
    endif;
    return null;
}

function head($content = '') {
    global $page;
    global $content;

    if (defined('TRACKING_CODE_HEADER'))
        echo TRACKING_CODE_HEADER . "\n";
    echo '<meta charset="utf-8"/>' . "\n";
    if (is_object($content)):
        ?>
        <title><?php echo isset($content->name) && $content->name ? $content->name : ''; ?></title>	
        <meta name="keywords" content="<?php echo isset($content->meta_keyword) ? $content->meta_keyword : ''; ?>" />
        <meta name="description" content="<?php echo isset($content->meta_description) ? $content->meta_description : ''; ?>" />
        <?php
    else:
        $content = get_meta($page);
        ?>
        <title><?php echo isset($content->page_title) && $content->page_title ? $content->page_title : ''; ?></title>	
        <meta name="keywords" content="<?php echo isset($content->keyword) ? $content->keyword : ''; ?>" />
        <meta name="description" content="<?php echo isset($content->description) ? $content->description : ''; ?>" />
    <?php
    endif;
    $index = isset($content->index) ? $content->index : 'index';
    $follow = isset($content->follow) ? $content->follow : 'follow';
    $canonical = isset($content->canonical) ? $content->canonical : '';
    echo '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>' . "\n";
    echo '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">' . "\n";
    echo '<meta name="robots" content="' . $index . ', ' . $follow . '" />' . "\n";
    echo '<link rel="canonical" href="' . $canonical . '" />' . "\n";
    echo '<meta name="software" content="Custom eCommerce Package" />' . "\n";
    echo '<link rel="shortcut icon" href="" />' . "\n";
    echo '<link rel="icon" href="" type="image/png"/>' . "\n";
    //echo '<link rel="shortcut icon" href="favicon.ico" />'."\n";
    //echo '<link rel="icon" href="'.DIR_WS_SITE_GRAPHIC.'favicon.png" type="image/png"/>'."\n";

  
        if (isset($content->next)):
            echo '<link rel="next" href="' . $content->next . '">' . "\n";
        endif;
        if (isset($content->prev)):
            echo '<link rel="prev" href="' . $content->prev . '">' . "\n";
        endif;
   

        include_once(DIR_FS_SITE . 'include/template/stats/google_analytics.php');
  
    ?>
    <?php
}

/* SEO Information......... */

function add_metatags($title = "", $keyword = "", $description = "") {
    /* description rule */
    $description_length = MAXIMUM_META_DESC_LENGHT; /* characters */
    //if (strlen($description) > $description_length)
    //  $description = substr($description, 0, $description_length);
    global $page;

    if ($page != 'content' && $page != 'search'):
        $title = create_meta_title($title);
    endif;
    /* title rule */
    $title_length = MAXIMUM_META_TITLE_LENGHT;
    //$title = substr($title, 0, $title_length);

    $content = new stdClass();
    $content->name = ucwords($title);
    $content->meta_keyword = $keyword;
    $content->meta_description = ucfirst(trim(strip_tags($description)));

    return $content;
}


  function css($array=array('reset','master')){
	  echo "\n".'<link href="'.DIR_WS_SITE_CSS.'print.css" rel="stylesheet" type="text/css" media="print" >'."\n";
	  foreach ($array as $k=>$v):
	  if($v=='style' && isset($_SESSION['use_stylesheet'])):
			echo '<link href="'.DIR_WS_SITE_CSS.''.$_SESSION['use_stylesheet'].'.css" rel="stylesheet" type="text/css" media="screen, projection" >'."\n";
	  else:
			echo '<link href="'.DIR_WS_SITE_CSS.''.$v.'.css" rel="stylesheet" type="text/css" media="screen, projection" >'."\n";
	  endif;
	  endforeach;
  }

  function js($array=array()){
	  ?>
	  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	  <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	  <![endif]-->
	  <?php
	  foreach ($array as $k=>$v):
		echo '<script src="'.DIR_WS_SITE_JS.''.$v.'.js" type="text/javascript"></script> '."\n";
	  endforeach;
  }

  function body()
  {
  /*include all the body related things like... tracking code here. *//*

  }

  function footer()
  {
  /* if you need to add something to the website footer... please add here. */
  }


function array_map_recursive($callback, $array) {
    $b = Array();
    foreach ($array as $key => $value) {
        $b[$key] = is_array($value) ? array_map_recursive($callback, $value) : call_user_func($callback, $value);
    }
    return $b;
}

function if_set_in_post_then_display($var) {
    if (isset($_POST[$var])):
        echo $_POST[$var];
    endif;
    echo '';
}

function validate_captcha() {
    global $privatekey;

    if ($_POST["recaptcha_response_field"]) {
        $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
        if ($resp->is_valid) {
            return true;
        } else {
            /* set the error code so that we can display it */
            return false;
        }
    }
}

function is_set($array = array(), $item, $default = 1) {
    if (isset($array[$item]) && $array[$item] != 0) {
        return $array[$item];
    } else {
        return $default;
    }
}

function limit_text($text, $limit = 100) {
    if (strlen($text) > $limit):
        return substr($text, 0, strpos($text, ' ', $limit));
    else:
        return $text;
    endif;
}

function get_object($tablename, $id, $type = 'object') {
    $query = new query($tablename);
    $query->Where = "where id='$id'";
    return $query->DisplayOne($type);
}

function get_object_by_col($tablename, $col, $col_value, $type = 'object') {
    $query = new query($tablename);
    $query->Where = "where $col='$col_value'";
    return $query->DisplayOne($type);
}

function get_object_var($tablename, $id, $var) {
    $q = new query($tablename);
    $q->Field = "$var";
    $q->Where = "where id='" . $id . "'";
    if ($obj = $q->DisplayOne()):
        return $obj->$var;
    else:
        return false;
    endif;
}

function echo_y_or_n($status) {
    echo ($status) ? 'Yes' : 'No';
}

function target_dropdown($name, $selected = '', $tabindex = 1) {
    $values = array('new window' => '_blank', 'same window' => '_parent');
    echo '<select name="' . $name . '" size="1" tabindex="' . $tabindex . '">';
    foreach ($values as $k => $v):
        if ($v == $selected):
            echo '<option value="' . $v . '" selected>' . ucfirst($k) . '</option>';
        else:
            echo '<option value="' . $v . '">' . ucfirst($k) . '</option>';
        endif;
    endforeach;
    echo '</select>';
}

function make_csv_from_array($array) {
    $sr = 1;
    $heading = '';
    $file = '';
    foreach ($array as $k => $v):
        foreach ($v as $key => $value):
            if ($sr == 1):$heading.=strtoupper(str_replace("_", " ", $key)) . ', ';
            endif;
            $file.=str_replace("\r\n", "<<>>", str_replace(",", ".", html_entity_decode($value))) . ', ';
        endforeach;
        $file = substr($file, 0, strlen($file) - 2);
        $file.="\n";
        $sr++;
    endforeach;
    return $file = $heading . "\n" . $file;
}

function get_y_n_drop_down($name, $selected) {
    echo '<select class="form-control span4"  name="' . $name . '" size="1">';
    if ($selected):
        echo '<option value="1" selected>Yes</option>';
        echo '<option value="0">No</option>';
    else:
        echo '<option value="0" selected>No</option>';
        echo '<option value="1">Yes</option>';
    endif;
    echo '</select>';
}

function get_setting_name($name) {
    $return = $name;
    preg_match('~%(.*?)%~', $name, $output);
    if (count($output)):
        $constant = trim($output['0'], '%');
        $constant_value = (defined($constant)) ? constant($constant) : '';
        if ($constant_value != ''):
            $return = str_replace('%' . $constant . '%', $constant_value, $name);
        endif;
    endif;

    return ucfirst($return);
}

function get_setting_control($key, $type, $value, $options = '') {
    switch ($type) {
        case 'text':
            echo '<input class="form-control" type="text" name="key[' . $key . ']" value="' . $value . '" size="30">';
            break;
        case 'textarea':
            echo '<textarea class="form-control" name="key[' . $key . ']">' . $value . '</textarea>';
            break;
        case 'radio':
            $options_array = array();
            $options_array = explode(",", $options);
            echo "<div class='radio-list'>";
            foreach ($options_array as $option):
                echo '<label class="radio-inline"><input type="radio" class="form-control" value="' . $option . '" name="key[' . $key . ']"';
                echo ($option == $value) ? "checked" : "";
                echo '/>' . ucfirst($option) . '</label>';
            endforeach;
            echo "</div>";
            break;
        case 'select':
            if ($options == ''):
                echo get_y_n_drop_down('key[' . $key . ']', $value);
            else:
                $options_array = array();
                $options_array = explode(",", $options);
                echo '<select class="span6  form-control" name="key[' . $key . ']">';
                foreach ($options_array as $option):
                    echo '<option value="' . $option . '" ';
                    echo (html_entity_decode($option) == $value) ? "selected" : "";
                    echo '>' . str_replace('_', ' ', $option) . '</option>';
                endforeach;
                echo "</select>";
            endif;
            break;
        default:
            echo get_y_n_drop_down('key[' . $key . ']', $value);
            break;
    }
}

function css_active($page, $value, $class) {
    if ($page == $value)
        echo 'class=' . $class;
}

function parse_into_array($string) {
    return explode(',', $string);
}

function MakeDataArray($post, $not) {
    $data = array();
    foreach ($post as $key => $value):
        if (!in_array($key, $not)):
            $data[$key] = $value;
        endif;
    endforeach;
    return $data;
}

function is_var_set_in_post($var, $check_value = false) {
    if (isset($_POST[$var])):
        if ($check_value):
            if ($_POST[$var] === $check_value):
                return true;
            else:
                return false;
            endif;
        endif;
        return true;
    else:
        return false;
    endif;
}

function display_form_error() {
    $login_session = new user_session();
	
    if ($login_session->isset_pass_msg()):

		$array = $login_session->get_pass_msg();
        ?>
        <div class="alert  alert-danger in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php
            foreach ($array as $value): 
                echo html_entity_decode($value) . '<br/>';
            endforeach;
            ?>
        </div>
    <?php elseif (isset($_GET['msg']) && $_GET['msg'] != ''):
        ?>
        <div class="alert  alert-warning  uppercase  fade  in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo html_entity_decode($_GET['msg']) . '<br/>'; ?>
        </div>
        <?php
    endif;
    $login_session->isset_pass_msg() ? $login_session->unset_pass_msg() : '';
}




function validation_form_error() {  
   if (isset($_SESSION['msgQueue']) && !empty($_SESSION['msgQueue'])):
       
        ?>
        <div class="alert  alert-warning  fade  in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php
            foreach ($_SESSION['msgQueue'] as  $value):
                echo html_entity_decode($value) . '<br/>';
            endforeach;
            ?>
        </div>
    
        <?php
    endif;
    unset($_SESSION['msgQueue']);
}

function Redirect($URL) {
    header("location:$URL");
    exit;
}

function Redirect1($filename) {
    if (!headers_sent())
        header('Location: ' . $filename);
    else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . $filename . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . $filename . '" />';
        echo '</noscript>';
    }
}

function re_direct($URL) {
    header("location:$URL");
    exit;
}

function display_url($title, $page, $query = '', $class = '', $rel = '') {
    return '<a href="' . make_url($page, $query) . '" class="' . $class . '" rel="' . $rel . '">' . $title . '</a>';
}

function make_admin_url($page, $action = 'list', $section = 'list', $query = '') {
    //return DIR_WS_SITE_CONTROL.'control.php?Page='.$page.'&action='.$action.'&section='.$section.'&'.$query;
    return 'control.php?Page=' . $page . '&action=' . $action . '&section=' . $section . '&' . $query;
}

function make_admin_url_window($page, $action = 'list', $section = 'list', $query = '') {
    return DIR_WS_SITE_ADMIN . 'control_window.php?Page=' . $page . '&action=' . $action . '&section=' . $section . '&' . $query;
    // return 'control_window.php?Page='.$page.'&action='.$action.'&section='.$section.'&'.$query;
}

function make_admin_url2($page, $action = 'list', $section = 'list', $query = '') {
    if ($page == 'home'):
        return DIR_WS_SITE . 'index.php';
    else:
        return DIR_WS_SITE_CONTROL . 'control.php?Page=' . $page . '&action2=' . $action . '&section2=' . $section . '&' . $query;
    endif;
}

function make_admin_link($url, $text, $title = '', $class = '', $alt = '') {
    return '<a href="' . $url . '" class="' . $class . '" title="' . $title . '" alt="' . $alt . '" >' . $text . '</a>';
}

function download_newsletterlist() {
    $users = new query('newsletterlist');
    $users->Field = "id,user_id,name,email,newsletter,promotion,ads";
    //$users->print=1;
    $users->DisplayAll();
    $users_arr = array();
    if ($users->GetNumRows()):
        while ($user = $users->GetArrayFromRecord()):

            array_push($users_arr, $user);
        endwhile;
    endif;
    $file = make_csv_from_array($users_arr);
    $filename = "newsletterlist" . '.csv';
    $fh = @fopen('download/' . $filename, "w");
    fwrite($fh, $file);
    fclose($fh);
    download_file('download/' . $filename);
}

function quit($message = 'processing stopped here') {
    echo $message;
    exit;
}

function download_orders($payment_status, $order_status) {
    $orders = new query('orders');
    $orders->Field = "id,user_id,sub_total,vat,shipping,voucher_code,voucher_amount,shipping_firstname,shipping_lastname,shipping_address1,shipping_address2,shipping_city,shipping_state,shipping_zip,shipping_country,shipping_phone,shipping_fax,billing_firstname,billing_lastname,billing_email,billing_address1,billing_address2,billing_city,billing_state,billing_zip,billing_country,billing_phone,billing_fax,grand_total,order_type,order_status,order_date,ip_address,order_comment,shipping_comment,cart_id";
    if ($order_status == 'paid'):
        $orders->Where = "where payment_status=" . $payment_status . " and order_status!='delivered'";
    elseif ($order_status == 'attempted'):
        $orders->Where = "where payment_status=" . $payment_status . " and order_status='received'";
    else:
        $orders->Where = "where payment_status=" . $payment_status . " and order_status='delivered'";
    endif;
    $orders->DisplayAll();
    #print_r($orders);exit();
    $orders_arr = array();
    if ($orders->GetNumRows()):
        while ($order = $orders->GetArrayFromRecord()):
            $order['Username'] = get_username_by_orders($order['user_id']);
            array_push($orders_arr, $order);
        endwhile;
    endif;
    $file = make_csv_from_array($orders_arr);
    $filename = "orders" . '.csv';
    $fh = @fopen('download/' . $filename, "w");
    fwrite($fh, $file);
    fclose($fh);
    download_file('download/' . $filename);
}

function get_username_by_orders($id) {
    if ($id == 0):
        return 'Guest';
    elseif ($id):
        $q = new query('user');
        $q->Field = "firstname,lastname";
        $q->Where = "where id='" . $id . "'";
        $o = $q->DisplayOne();
        return $o->firstname;
    endif;
}

function get_zones_box($selected = 0) {
    $q = new query('zone');
    $q->DisplayAll();
    echo '<select name="zone" size="1">';
    while ($obj = $q->GetObjectFromRecord()):
        if ($selected = $obj->id):
            echo '<option value="' . $obj->id . '" selected>' . $obj->name . '</option>';
        else:
            echo '<option value="' . $obj->id . '">' . $obj->name . '</option>';
        endif;
    endwhile;
    echo '</select>';
}

function zone_drop_down($zone_id, $id, $s, $z) {
    $query = new query('zone_country');
    $query->Where = "where zone_id=$zone_id";
    $query->DisplayAll();
    $country_list = array();
    $country_name = '';
    while ($object = $query->GetObjectFromRecord()):
        $country_name = get_country_name_by_id($object->country_id);
        $idd = $object->country_id;
        //$country_list('id'=>$object->id, 'name'=>$country_name));
        array_push($country_list, array('name' => $country_name, 'id' => $object->id));
    //$country_list[$object->id]=$country_name;
    endwhile;
    $total_list = array();
    foreach ($country_list as $k => $v):
        $total_list[] = $v['name'];
    endforeach;
    array_multisort($total_list, SORT_ASC, $country_list);
    /* print_r($country_list);exit; */
    echo '<select name="' . $id . '" size="10" style="width:200px;" multiple>';
    foreach ($country_list as $k => $v):
        if (($z == $zone_id) && $s):
            echo '<option value="' . $v['id'] . '" selected="selected">' . ucfirst($v['name']) . '</option>';
        else:
            echo '<option value="' . $v['id'] . '">' . ucfirst($v['name']) . '</option>';
        endif;
    endforeach;
    echo'</select>';
}

function get_supplier_dd($supplier) {
    $query = new query('supplier');
    $query->Where = "where is_active=1";
    $query->DisplayAll();
    $options = '<option value="">None</option>';
    while ($sup = $query->GetObjectFromRecord()) {
        if ($supplier && $supplier == $sup->name)
            $options.='<option value="' . $sup->name . '" selected>' . ucwords($sup->name) . '</option>';
        else
            $options.='<option value="' . $sup->name . '">' . ucwords($sup->name) . '</option>';
    }
    return $options;
}

function get_page_head_description($page = 'home') {
    $defoult_text_for_home = "Whether you live in the province, have visited Newfoundland, or are a homesick Newfie looking for some much-needed reminders of The Rock, Island Treasures is your one source for everything Newfoundland and Labrador.";
    $defoult_text = "Mauris hendrerit ligula ut sapien varius in adipiscing nisl cursus. Nullam varius lacinia hendrerit. Ut eleifend porttitor porta. In et justo justo, eget dignissim lorem. Etiam eleifend enim eu purus fermentum a vulputate diam dapibus.";
    switch ($page) {
        case 'product':
            global $category;
            if (isset($_GET['id'])):
                echo html_entity_decode(limit_text($category->description, 303));
            else:
                echo html_entity_decode($defoult_text);
            endif;
            break;
        case 'content':
            global $object;
            echo html_entity_decode(limit_text($object->top_content, 303));
            break;
        case 'home':
            global $object;
            echo html_entity_decode($defoult_text_for_home);
            break;
        case 'pdetail':
            global $object;
            echo html_entity_decode(limit_text($object->top_description, 303));
            break;
        default:
            echo html_entity_decode($defoult_text);
    }
}

/* get table name from module name */

function getModuleTable($module) {
    global $moduleTableForStatus;
    if (array_key_exists($module, $moduleTableForStatus)):
        return $moduleTableForStatus[$module];
    endif;
    return false;
}

;
/* ----------------------- new functions ----------- */
/* serialize upload files[] array */

function serializeUploadArray($FILES, $Field_name) {
    $count = count($FILES[$Field_name]['name']);
    $file = array();
    $files = array();
    if ($count):
        for ($i = 0; $i < $count; $i++):
            $file['name'] = $FILES[$Field_name]['name'][$i];
            $file['type'] = $FILES[$Field_name]['type'][$i];
            $file['tmp_name'] = $FILES[$Field_name]['tmp_name'][$i];
            $file['error'] = $FILES[$Field_name]['error'][$i];
            $file['size'] = $FILES[$Field_name]['size'][$i];
            array_push($files, $file);
        endfor;
    endif;
    return $files;
}

function category_chain($id, $tablename = 'category', $front = false) {

    $QueryObj1 = new query('category');
    $QueryObj1->Where = "where id='" . $id . "'";
    $category = $QueryObj1->DisplayOne();
    $cat_chain = '';

    $chain = array();
    $sr = 1;
    while ($id != 0):
        $QueryObj = new query($tablename);
        $QueryObj->Where = "where id='" . $id . "'";
        $cat = $QueryObj->DisplayOne();

        if ($front):
            $chain[] = '<li>' . '<a href="' . make_url('shop', 'id=' . $cat->id) . '">' . ucfirst(html_entity_decode($cat->name)) . '</a>' . '</li>';
        else:
            //if($sr>1):
            $chain[] = '<a href="' . make_admin_url('category', 'list', 'list', 'pid=' . $cat->id) . '">' . ucfirst(html_entity_decode($cat->name)) . '</a>';
        //endif;
        endif;

        $id = $cat->parent_id;
        $sr++;
    endwhile;

    for ($i = count($chain) - 1; $i >= 0; $i--):
        if ($front):
            $cat_chain.=$chain[$i];
        else:
            $cat_chain.=$chain[$i] . ' <i class="icon-angle-right"></i> ';
        endif;
    endfor;
    if (!$front):
    //$cat_chain .= ucfirst($category->name);
    endif;

    return $cat_chain;
}

/* get product main image */

function getProductMainImage($id, $size = 'medium', $rtype = 'echo') {
    $image = '';
    $QueryObj = new query('product_image');
    $QueryObj->Where = " where `product_id` = $id and is_main = '1'";
    $pro_image = $QueryObj->DisplayOne();
    if (is_object($pro_image)):
        $image = $pro_image->image;
    else:
        $QueryObj = new query('product_image');
        $QueryObj->Where = " where `product_id` = $id order by position,id asc limit 0,1";
        $postition_image = $QueryObj->DisplayOne();
        if (is_object($postition_image)):
            $image = $postition_image->image;
        endif;
    endif;
    $return = getProductImage($image, $size, $rtype);
    return $return;
}

/* get product image */

function getProductImage($image, $size = 'medium', $rtype = 'echo') {
    $return_image = '';
    if (file_exists(DIR_FS_SITE . 'upload/photo/product/' . $size . '/' . $image) && $image):
        $return_image = DIR_WS_SITE . 'upload/photo/product/' . $size . '/' . $image;
    else:
        $return_image = DIR_WS_SITE_GRAPHIC . 'noimage_' . $size . '.jpg';
    endif;
    if ($rtype == 'echo'):
        echo $return_image;
        return;
    else:
        return $return_image;
    endif;
}

/* echo in selected language - from language file */

function show($var, $echo = true) {
    if ($echo):
        echo trim($var);
    else:
        return trim($var);
    endif;
}

/* show_date */

function show_date($show_date, $echo = true) {


    if (DATE_FORMAT_SETTINGS != ''):
        $return = date(DATE_FORMAT_SETTINGS, strtotime($show_date));
    else:
        $return = $show_date;
    endif;

    if ($echo):
        echo $return;
    else:
        return $return;
    endif;
}

/* show_time */

function show_time($show_time, $echo = true) {


    if (TIME_FORMAT_SETTINGS != ''):
        $return = date(TIME_FORMAT_SETTINGS, strtotime($show_time));
    else:
        $return = $show_time;
    endif;

    if ($echo):
        echo $return;
    else:
        return $return;
    endif;
}

/* get user first name from session id */

function getUserFirstName() {
    $login_session = new user_session();
    $user_id = $login_session->get_user_id();

    $query = new query('user');
    $query->Field = "id,firstname";
    $query->Where = " where `id` = '$user_id'";
    
	$object = $query->DisplayOne();

    return $object->firstname;
}

/* get all active countries */

function getCountries($active = true) {
    $QueryObj = new query('country');
    if ($active):
        $QueryObj->Where = " where is_active='1'";
    else:
        $QueryObj->Where = " where 1=1";
    endif;
    $QueryObj->Where.=" order by short_name asc";
    return $QueryObj->ListOfAllRecords();
}

/* show price */

function show_price($price, $return = false) {
    $result = '';
    $price = number_format($price, 2);
   

    $result.=$price;

    

    if ($return):
        return $result;
    else:
        echo $result;
    endif;
}

/* get country name by id */

function country_name($id) {
    $Query = new query('country');
    $Query->Where = " where id='$id'";
    $object = $Query->DisplayOne();
    if (is_object($object)) {
        return $object->short_name;
    } else {
        return false;
    }
}

/* get city name by id */

function city_name($id) {
    $Query = new query('city');
    $Query->Where = " where id='$id'";
    $object = $Query->DisplayOne();
    if (is_object($object)) {
        return $object->name;
    } else {
        return false;
    }
}

/* get state name by id */

function state_name($id) {
    $Query = new query('state');
    $Query->Where = " where id='$id'";
    $object = $Query->DisplayOne();
    if (is_object($object)) {
        return $object->name;
    } else {
        return false;
    }
}


/* get visitor country */

function visitor_country() {
    $ip = $_SERVER["REMOTE_ADDR"];
    $ip = '74.6.136.244';
    if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    $result = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip))
            ->geoplugin_countryName;
    return $result <> NULL ? $result : "0";
}

/*
 * create file from array
 * then download it
 */

function download_csv_file($data_array, $name) {
    $file = make_csv_from_array($data_array);
    $filename = $name . "_" . date("d-m-Y H-i-s") . '.csv';

    if (!file_exists('download')) {
        mkdir('download', 0755, true);
    }

    $fh = @fopen('download/' . $filename, "w");

    fwrite($fh, $file);
    fclose($fh);
    $fileObj = new file();
    $fileObj->download_file('download/' . $filename);
}

/*
 * download pdf file
 */

function download_pdf_file($name, $url = '', $html = '', $papersize = 'A4', $print_layout = 'portrait') {
    set_time_limit(5000);
    if ($url == '' && $html == ''):
        return false;
    endif;
    if ($url != ''):
        $html = file_get_contents($url);
    endif;

    $dompdf = new DOMPDF();
    $dompdf->load_html(html_entity_decode($html));
    $dompdf->set_paper($papersize, $print_layout);
    $dompdf->render();
    $dompdf->stream($name . ".pdf");
    return true;
}

function addSortDetails($url, $orderby, $orderway) {

    $new_url = explode('&', $url);

    if (!empty($new_url)):
        foreach ($new_url as $key => $val):


            if ((strpos($val, 'sort_by=') !== false) || (strpos($val, 'sort_order=') !== false) || (strpos($val, 'p=') !== false)):

                unset($new_url[$key]);

            endif;

        endforeach;
    endif;

    $url = implode('&', $new_url);


    return $url . (!strstr($url, '?') ? '?' : substr($url, -1) == '&' ? '' : '&') . 'sort_by=' . urlencode($orderby) . '&sort_order=' . urlencode($orderway);
}

function create_meta_title($meta_title, $name = '', $section = 'DEFAULT') {

    $section = strtoupper($section);

    $useful_meta = $meta_title;

    if ($useful_meta == ''):
        $useful_meta = $name;
    endif;

    $useful_meta = ucwords($useful_meta);

    if ($section == 'PRODUCT'):
        $defined_pattern = PRODUCT_PAGE_META_TITLE;
    elseif ($section == 'BRAND'):
        $defined_pattern = BRAND_PAGE_META_TITLE;
    elseif ($section == 'CONTENT'):
        $defined_pattern = CONTENT_PAGE_META_TITLE;
    elseif ($section == 'CATEGORY'):
        $defined_pattern = CATEGORY_PAGE_META_TITLE;
    elseif ($section == 'SEARCH'):
        $defined_pattern = SEARCH_PAGE_META_TITLE;
    else:
        $defined_pattern = DEFAULT_PAGE_META_TITLE;
    endif;

    $defined_values = array('%%SITENAME%%', '%%PRODUCT_NAME%%', '%%PAGE_NAME%%', '%%BRAND_NAME%%', '%%CATEGORY_NAME%%', '%%SEARCH_KEYWORD%%');
    $new_values = array(SITE_NAME, $useful_meta, $useful_meta, $useful_meta, $useful_meta, $useful_meta);
    $new_meta_title = str_replace($defined_values, $new_values, $defined_pattern);

    return $new_meta_title;
}

function create_meta_desc($meta_desc, $desc = '', $section = 'DEFAULT') {

    $useful_meta = $meta_desc;

    if ($useful_meta == '' && FILL_META_DESC_AUTOMATICALLY):

        $useful_meta = trim(strip_tags($desc));

    endif;

    $descrptn = html_entity_decode($useful_meta);
    //$descrptn = substr($descrptn, 0, MAXIMUM_META_DESC_LENGHT);
    return $descrptn;
}

function removeUnwantedKeywords($keywords) {
    $unwanted = explode(',', REMOVE_WORDS_FROM_SEARCH);
    $searched = explode(' ', $keywords);
    foreach ($searched as $kk => $vv):
        if (in_array($vv, $unwanted)):
            unset($searched[$kk]);
        endif;
    endforeach;
    $keywords = implode(' ', $searched);
    return $keywords;
}

function createSeoNavigationLinks($content, $p, $total_pages, $query = '') {
    global $page;
    if ($p == '1' && $total_pages > '1'):

        $content->next = make_url($page, 'p=2&' . $query);

    elseif ($p != '1' && $p != '0' && $p != $total_pages):

        $content->next = make_url($page, 'p=' . ($p + 1) . '&' . $query);
        $content->prev = make_url($page, 'p=' . ($p - 1) . '&' . $query);

    elseif ($p == $total_pages && $total_pages != '1'):

        $content->prev = make_url($page, 'p=' . ($p - 1) . '&' . $query);

    elseif ($total_pages == '1' && (!isset($content->canonical) || (isset($content->canonical) && $content->canonical == ''))):

        $content->canonical = make_url($page, $query);

    endif;
    return $content;
}

if (!function_exists('mime_content_type')) {

    function mime_content_type($filename) {

        $mime_types = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.', $filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        } elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        } else {
            return 'application/octet-stream';
        }
    }

}

function sendOrderXml($order_file, $id) {
    // connect and login to FTP server

    $ftp_server = trim(ORDER_XML_SFTP_SERVER);
    $ftp_username = trim(ORDER_XML_SFTP_USERNAME);
    $ftp_userpass = trim(ORDER_XML_SFTP_PASSWORD);

    set_include_path(DIR_FS_SITE_INCLUDE . 'sftpLib');

    include('Net/SFTP.php');

    /* Change the following directory path to your specification */
    $file_name = $order_file . ".xml";
    $local_directory = DIR_FS_SITE_UPLOAD . 'order_xml' . '/' . $file_name;
    $remote_directory = $file_name;

    /* Add the correct FTP credentials below */
    $sftp = new Net_SFTP($ftp_server);
    if ($sftp->login($ftp_username, $ftp_userpass)) {
        /* Upload the local file to the remote server 
          put('remote file', 'local file');
         */
        $success = $sftp->put($remote_directory, $local_directory, NET_SFTP_LOCAL_FILE);
    }
    if ($success) {
        $queryObj = new order();
        $queryObj->Data['id'] = $id;
        $queryObj->Data['is_ftp'] = '1';
        $queryObj->Update();
    }
    restore_include_path();
    return;
}

function checkAdminSSLMethod() {
    if (ENABLE_SSL_ADMIN):
        if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == ""):
            $redirect = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            Redirect($redirect);
        endif;
    else:
        if (isset($_SERVER['HTTPS'])):
            $redirect = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            Redirect($redirect);
        endif;
    endif;
    return true;
}

function checkSSLMethod($view = 'front') {
    if ($view == 'admin'):
        $condition = ENABLE_SSL_ADMIN;
    else:
        $condition = ENABLE_SSL_FRONT;
    endif;
    if ($condition):
        if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == ""):
            $redirect = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            Redirect($redirect);
        endif;
    else:
        if (isset($_SERVER['HTTPS'])):
            $redirect = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            Redirect($redirect);
        endif;
    endif;
    return true;
}

function checkMobile() {
    if (preg_match("/Mobile|Android|BlackBerry|iPhone|Windows Phone/", $_SERVER['HTTP_USER_AGENT'])) {
        return $mobile = 1;
    } else {
        return $mobile = 0;
    }
}

function getExpectedDeliveryDate($start, $end) {

    if ($start & $end):
        $return = '';

        $start_year = date('Y', strtotime($start));
        $end_year = date('Y', strtotime($end));

        $start_month = date('M', strtotime($start));
        $end_month = date('M', strtotime($end));

        if ($start_month == $end_month):
            $return .= $start_month . ' ';
        endif;

        if ($start == $end):
            $return .= date('d', strtotime($start)) . ', ';
        else:
            $return .= date('d', strtotime($start)) . ' ';
            if ($start_month != $end_month):
                $return .= $start_month;
            endif;
            if ($start_year != $end_year):
                $return .= ', ' . $start_year;
            endif;
            $return .= '- ';
            $return .= date('d', strtotime($end));
            if ($start_month != $end_month):
                $return .= ' ' . $end_month;
            endif;
            $return .= ', ';
            if ($start_year != $end_year):
                $return .= $end_year;
            endif;
        endif;

        if ($start_year == $end_year):
            $return .= $start_year;
        endif;
        echo $return;
    elseif ($start):
        return date("M d, Y", strtotime($start));
    endif;
    return false;
}

function validateUrl($url) {

    if (!filter_var($url, FILTER_VALIDATE_URL)) {
        return false;
    }

    $array = array(">", "<", "(", ")", '"', "'", '!', '`', '~', '$', '^', '*');

    foreach ($array as $k => $vv):

        if (strpos($url, "$vv")):

            return false;

        endif;

    endforeach;

    return true;
}

function pr($e) {
    echo '<pre>';
    print_r($e);
    echo '</pre>';
    return;
}

function make_admin_menus($menus, $Page, $action = '') {
    if (is_array($menus) && !empty($menus)) {
        foreach ($menus as $menu) {
            $submenus = $menu['submenus'];
            //$keep_active_nav_array = array_keys($submenus);
			$keep_active_nav_array = $menu['pages'];
            //pr($keep_active_nav_array);
            echo '<li class="' . (in_array($Page, $keep_active_nav_array) ? 'active' : '') . '">'
            . '<a href="' . $menu['link'] . '">'
            . '<i class="' . $menu['icon'] . '"></i> '
            . '<span class="title">' . $menu['name'] . '</span>'
            . ((in_array($Page, $keep_active_nav_array)) ? '<span class="selected"></span>' : '')
            . '<span class="arrow' . ((in_array($Page, $keep_active_nav_array)) ? ' open' : '') . '"></span></a>'
            . '<ul class="sub-menu">';
            foreach ($submenus as $submenu_id => $submenu) {
                $class = '';
                if (isset($submenu['action'])) {
                    if ($submenu['action'] == $action && $Page == $submenu_id) {
                        $class = 'active';
                    }
                } elseif($Page == $submenu_id) {
                    $class = 'active';
                } elseif(isset($_GET['sname']) && ($_GET['sname'] == $submenu_id)) {
                    $class = 'active';					
                }
                echo '<li class="' . $class . '"><a href="' . $submenu['link'] . '">' . $submenu['name'] . '</a></li>';
//                echo '<li class="' . ((($Page == $submenu_id)) ? 'active' : '') . '"><a href="' . $submenu['link'] . '">' . $submenu['name'] . '</a></li>';
            }
            echo '</ul></li>';
        }
    }
}

function show_sub_nav($navigation) {
    ?>
    <li>
        <span><?php echo $navigation->navigation_title ?></span>
        <ul>
            <?php foreach ($navigation->sub_nav as $sub_nav) { ?>
                <?php if (isset($sub_nav->sub_nav)) { ?>
                    <?php show_sub_nav($sub_nav) ?>
                <?php } else { ?>
                    <li><a rel="canonical" href="<?php echo ($sub_nav->is_external == 1) ? $sub_nav->navigation_link : make_url($sub_nav->navigation_link, $sub_nav->navigation_query) ?>" title="<?php echo $sub_nav->navigation_title ?>"<?php $sub_nav->open_in_new ? ' target="_blank" ' : '' ?>><?php echo $sub_nav->navigation_title ?></a></li>
                <?php } ?>
            <?php } ?>
        </ul>
    </li>
    <?php
}

function randomString($length = 6) {
	$str = "";
	$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;
}

?>