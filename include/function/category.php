<?php
/*
 * Fetch prodcut main image from database
 * @id - product id
 * @return Image Name or false
 */
function product_get_main_image($id){
    $image= new query('pimage');
    $image->Where="where product_id='$id' and main_image='1'";
    $di=$image->DisplayOne();
    if(is_object($di))
           return $di->image;
    else
 				$imag1= new query('pimage');
				$imag1->Where="where product_id='$id' and main_image!='1'";				
				$di1=$imag1->DisplayOne();	
				if(is_object($di1)){
					return $di1->image;
				}         
		  return false;
}

/*
 * Fetch prodcut default image from database
 * @id - product id
 * @return Image Name or false
 */
function product_get_default_image($id){
    $image= new query('pimage');
    $image->Where="where product_id='$id' and default_image='1'";
    $di=$image->DisplayOne();
    if(is_object($di))
           return $di->image;
    else
           return false;
}


/*
 * Retrive All Sub-Categories from Database of a parent $id
 * @return list of categories
 */

function get_all_categories($id=0, $status_check=1){
    $query = new query('category');
	if($status_check):
		$query->Where="where is_active='1' and parent_id='$id' order by position";
	else:
		$query->Where="where parent_id='$id' and is_active='1' order by position";
	endif;	
	$query->DisplayAll();
    $categories=array();
    if($query->GetNumRows()){
        while($object=$query->GetObjectFromRecord()){
            $categories[]=$object;
        }
    }

    return $categories;
}

function get_all_sub_cat($id=0){
    $query = new query('category');
		$query->Where="where parent_id='$id' and is_active='1' order by position";
	$query->DisplayAll();
    $categories=array();
    if($query->GetNumRows()){
        while($object=$query->GetObjectFromRecord()){
            $categories[]=$object;
        }
    }	
    return $categories;
}


  function get_all_brands(){
            $query_partners= new query('supplier');
            $query_partners->Where="where is_active='1' order by position";
            $query_partners->DisplayAll();
            $cat3=array();
            if($query_partners->GetNumRows()):
            while($object_partners = $query_partners->GetObjectFromRecord()):
            $cat3[]=$object_partners;
            endwhile;
            endif;
            return($cat3);
}

function get_all_sub_cat_arr($id=0){
    $query = new query('category');
		$query->Where="where parent_id='$id' and is_active='1' order by position";
	$query->DisplayAll();
    $categories=array();
    if($query->GetNumRows()){
        while($object=$query->GetObjectFromRecord()){
            $categories[]=$object->id;
        }
    }	
    return $categories;
}


function get_all_category_dd_options($selected=array()){
    $query = new query('category');
    $query->Where="where is_active='1' order by name";
    $query->DisplayAll();
    $categories=array();
    if($query->GetNumRows()){
        while($object=$query->GetObjectFromRecord()){
            if(!$object->parent_id)
                $categories[$object->id]=array('name'=>$object->name, '');
            else{
                $ccobj=get_object('category', $object->parent_id);
                $categories[$object->id]=$ccobj->name.'&nbsp;>&nbsp;'.$object->name;
            }
        }
    }
    return $categories;
}

function products_all_free_of_category($cat_id=0){
    
    #if category id is supplied - exclude already added products;
    $product_id='';
    if($cat_id){
        $query = new query('category_product');
        $query->Where="where category_id='$cat_id'";
        $query->DisplayAll();
        if($query->GetNumRows()){
            while($object=$query->GetObjectFromRecord()){
                $product_id.=$object->product_id.', ';
            }
        }
       
       if($product_id!=''){
           $product_id = substr($product_id, 0, strlen($product_id)-2);
       }
    }
    
    #get all products minus already added products $product_id
    $query= new query('product');
    $query->Where="where is_active='1'";
    
    #change the statement if product_id is set
    if($product_id!='')
        $query->Where="where is_active='1' and id NOT IN($product_id)";
    #$query->print=1;
    $query->DisplayAll();
    
    $products=array();
    if($query->GetNumRows()){
        while($object=$query->GetObjectFromRecord()){
            $products[]=array('id'=>$object->id, 'name'=>$object->name);
        }
    }
    return $products;
}


function products_all_for_category($cat_id=0){
    
    #if category id is supplied - exclude already added products;
    $product_id=array();
    if($cat_id){
        $query = new query('category_product');
        $query->Where="where category_id='$cat_id'";
        $query->DisplayAll();
        if($query->GetNumRows()){
            while($object=$query->GetObjectFromRecord()){
                $product_id[]=$object->product_id;
            }
        }
    }
    
    #get all products minus already added products $product_id
    $query= new query('product');
    $query->Where="where is_active='1'";
    $query->DisplayAll();
    
    $products=array();
    if($query->GetNumRows()){
        while($object=$query->GetObjectFromRecord()){
            if(in_array($object->id, $product_id))
                $products[]=array('id'=>$object->id, 'name'=>$object->name, 'selected'=>'1');
            else
                $products[]=array('id'=>$object->id, 'name'=>$object->name, 'selected'=>'0');
            
        }
    }
    return $products;
}




function products_all_selected_in_category($cat_id=0){
   $products=array();
   if($cat_id){
        $query = new query('category_product');
        $query->Where="where category_id='$cat_id'";
        $query->DisplayAll();
        
        if($query->GetNumRows()){
            while($object=$query->GetObjectFromRecord()){
                $obj= get_object('product', $object->id);
                $products[]=array('id'=>$object->product_id, 'name'=>$obj->name);
            }
        }
    }
    return $products;
}


function get_product_status_link($id, $status)
{
	echo '<select name="is_active['.$id.']" size="1">';
	if($status):
		echo '<option value="1" selected>Active</option>';
		echo '<option value="0">Not-Active</option>';
	else:
		echo '<option value="1" selected>Active</option>';
		echo '<option value="0" selected>Not-Active</option>';
	endif;
	echo '</select>';
}
function get_country_status_link($id, $status)
{
	echo '<select name="is_active['.$id.']" size="1">';
	if($status):
		echo '<option value="1" selected>Active</option>';
		echo '<option value="0">Not-Active</option>';
	else:
		echo '<option value="1" selected>Active</option>';
		echo '<option value="0" selected>Not-Active</option>';
	endif;
	echo '</select>';
}
function get_category_id($id)
{    
$query = new query('category_product');   
  $query->Where="where product_id='$id'";  
  $object=$query->DisplayOne();  
  {   
  return $object->category_id;   
  } 
  }

?>