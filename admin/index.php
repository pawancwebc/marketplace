<?php
        require_once("../include/config/config.php");

        $redirect_back = (isset($_GET['redirect']))?true:false;

        $include_fucntions=array('http', 'image_manipulation','users');
		include_functions($include_fucntions);

        // check if already logged in 
        if($admin_user->is_logged_in()){
             // Check User type
            if(isset($_SESSION['admin_session_secure']['login_type']) && $_SESSION['admin_session_secure']['login_type']!='admin'){ 
                    
					$admin_user->set_error();
                    $admin_user->set_pass_msg('Sorry, You are already logged in as '.ucfirst($_SESSION['admin_session_secure']['login_type']).'.'); 
                    Redirect(DIR_WS_SITE.'account/index.php'); 
            }
            else{
                 redirect(make_admin_url('home', 'list', 'list')); 
            }  
          
        }
    
        // check cookie
        if(isset($_COOKIE['admin'])){ 
            $object= get_object('admin_user', $_COOKIE['admin']);
            if(is_object($object)){
                if($user=validate_admin('admin_user', array('username'=>$object->username, 'password'=>$object->password),0)){
                    $admin_user->set_admin_user_from_object($object);
                    update_last_access($user->id, 1);
                   
                    // Set the login type 
                    $_SESSION['admin_session_secure']['login_type']='admin';
                    
					redirect(make_admin_url('home', 'list', 'list')); 
                }
            }
        }

         /* login user */
        if(is_var_set_in_post('login')):

                if($user=validate_admin('admin_user', $_POST)):
                   
                       if($user && $user->is_active=='1'):
                            if(isset($_POST['remember'])){                       
                                setcookie('admin',$user->id, time()+60*60*24*30);
                            }
                            $admin_user->set_admin_user_from_object($user);
                            update_last_access_admin($user->id, 1);
 
                            /* Set the login type */
                            $_SESSION['admin_session_secure']['login_type']='admin';

                            $redirect_url = '';

                            if($redirect_url==''):
                                $redirect_url = DIR_WS_SITE_ADMIN."control.php";
                            endif;
                            Redirect1($redirect_url);
                       else:
		        $admin_user->set_error();
				$admin_user->set_pass_msg(MSG_LOGIN_INVALID_USERNAME_PASSWORD);
			//Redirect1(DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/index.php');
                        
		       endif;
		else:
                
            $admin_user->set_error();
			$admin_user->set_pass_msg(MSG_LOGIN_INVALID_USERNAME_PASSWORD);
                        
			//Redirect1(DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/index.php');
		endif;
	endif;
        
      
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title><?php echo SITE_NAME?></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="assets/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
        
        <!-- BEGIN PAGE LEVEL STYLES for validation -->
	
        <link rel="stylesheet" type="text/css" href="assets/plugins/validation/validationEngine.jquery.css" />
        
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
	<!-- BEGIN LOGO -->
	<div class="logo" style="color:white; font-weight:bold;font-size:20px">
		<?php echo SITE_NAME?>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
                <form class="login-form" method="post">
		
			<h3 class="form-title">Login to your account</h3>
			 <?php display_message(1); ?>
			<div class="control-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Username</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-user"></i>
                                                 <input class="m-wrap placeholder-no-fix validate[required]" type="text" id="username" name="username" placeholder="User Name" />
					
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
                                                <input class="m-wrap placeholder-no-fix validate[required]" type="password" autocomplete="off" id="password" name="password" placeholder="Password"/>
						
					</div>
				</div>
			</div>
			<div class="form-actions">
                                <?php if($redirect_back): ?>
                                    <input type="hidden" name="redirect_back" value="1"/>
                                <?php endif; ?>
				<button type="submit"  name="login"  value="submit" class="btn blue pull-right" id="login_button">
                                    Login <i class="m-icon-swapright m-icon-white"></i>
				</button>            
			</div>
                        <!--
			<div class="forget-password">
				<h4>Forgot your password ?</h4>
				<p>
					no worries, click <a href="javascript:void(0)" class="" id="forget-password">here</a>
					to reset your password.
				</p>
			</div>-->
			
		</form>
		<!-- END LOGIN FORM -->        
		<!-- BEGIN FORGOT PASSWORD FORM -->
		<form class="form-vertical forget-form" action="index.html">
			<h3 class="">Forget Password ?</h3>
			<p>Enter your e-mail address below to reset your password.</p>
			<div class="control-group">
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-envelope"></i>
						<input class="m-wrap placeholder-no-fix" type="text" placeholder="Email" name="email" />
					</div>
				</div>
			</div>
			<div class="form-actions">
				<button type="button" id="back-btn" class="btn">
				<i class="m-icon-swapleft"></i> Back
				</button>
				<button type="submit" class="btn blue pull-right">
				Submit <i class="m-icon-swapright m-icon-white"></i>
				</button>            
			</div>
		</form>
		<!-- END FORGOT PASSWORD FORM -->
		
	</div>
	<!-- END LOGIN -->
	<!-- BEGIN COPYRIGHT -->
	<div class="copyright">
		
	</div>
	<!-- END COPYRIGHT -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
        
        <!-- Validation engine -->
        <script src="assets/plugins/validation/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
        <script src="assets/plugins/validation/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/login-soft.js" type="text/javascript"></script> 
         <script src="assets/scripts/form-validation.js"></script> 
	<!-- END PAGE LEVEL SCRIPTS --> 
	<script>
		jQuery(document).ready(function() {     
		  App.init();
		  Login.init();
                  FormValidation.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
