$('#same_date_time').click(function () {
    if ($("#same_date_time").is(":checked")) {
        $("#location_from_date").attr("disabled", true)
        $("#location_to_date").attr("disabled", true)
    }
    else {
        $("#location_from_date").attr("disabled", false)
        $("#location_to_date").attr("disabled", false)

    }
});
$("#module").change(function () {
    var module = $(this).val();
    var position_mega_menu = '<option value="Left">Left</option><option value="Right">Right</option>';
    var position_default = '<option value="Top">Top</option><option value="Bottom">Bottom</option><option value="Left">Left</option><option value="Right">Right</option><option value="Landing_Page">Landing Page</option>';
    $("#select_categoires_div").hide(600);
    $('#selected_categoires_div').hide(600);
    $("#select_mega_menus_div").hide(600);
    $('#selected_mega_menus_div').hide(600);
    $('#position').html(position_default);
    if (module == 'shop') {
        $("#select_categoires_div").show(600);
        $('#selected_categoires_div').show(600);
    }
    if (module == 'mega_menus') {
        $("#select_mega_menus_div").show(600);
        $('#selected_mega_menus_div').show(600);
        $('#position').html(position_mega_menu);
    }
});
$(document).on('click', '.selected_parent_category_id', function (event) {
    var select_parent_category_id = $(this).attr('rel');
    var url = "script/ajax_banner_sub_cats.php";
    var data = 'select_parent_category_id=' + select_parent_category_id;

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (data)
        {

            $('#sub_cat_td_' + select_parent_category_id).html(data);
            $('#sub_cat_row_' + select_parent_category_id).toggle();
            App.init();
            FormComponents.init();
            FormSamples.init();
            TableManaged.init();
            UIJQueryUI.init();
            ComponentsFormTools.init();
            ComponentsPickers.init();
            ComponentsDropdowns.init();
        }
    });
});

$('#submit_categories').click(function () {
    var categories = [];
    var names = [];
    $(".categoryCheckBox").each(function () {
        if ($(this).is(':checked')) {
            var id = $(this).attr('rel');
            var name = $(this).attr('name');
            categories.push(id);
            names.push(name + "<br/>");
        }
    });
    if (names.length > 0) {
        $('#selected_categoires_div').html(names);
        $('#selected_categoires_div').show();
    }
    $('#new_categories_selected').remove();
    $('#banner_location').append('<input type="hidden" name="categories_id" id="new_categories_selected" value="' + categories + '" />');

});

$('#submit_mega_menus').click(function () {
    var mega_menus = [];
    var names = [];
    $(".mega_menusCheckBox").each(function () {
        if ($(this).is(':checked')) {
            var id = $(this).attr('rel');
            var name = $(this).attr('name');
            mega_menus.push(id);
            names.push(name + "<br/>");
        }
    });
    if (names.length > 0) {
        $('#selected_mega_menus_div').html(names);
        $('#selected_mega_menus_div').show();
    }
    $('#new_mega_menus_selected').remove();
    $('#banner_location').append('<input type="hidden" name="mega_menus_id" id="new_mega_menus_selected" value="' + mega_menus + '" />');

});