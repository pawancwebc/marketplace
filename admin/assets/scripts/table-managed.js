var TableManaged = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }
            
            var records_per_page_admin = $("#records_per_page_admin").val();
            
            // begin first table
            $('#sample_1').dataTable({
                "aoColumns": [
                  { "bSortable": false },
                  null,
                  { "bSortable": false },
                  null,
                  { "bSortable": false },
                  { "bSortable": false }
                ],
                "aLengthMenu": [
                    [15, 30, 50, -1],
                    [15, 30, 50, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": records_per_page_admin,
                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#sample_1 .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#sample_1_wrapper .dataTables_filter input').addClass("m-wrap medium"); // modify table search input
            jQuery('#sample_1_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
            //jQuery('#sample_1_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown

            // begin second table
            $('#sample_2').dataTable({
                "aLengthMenu": [
                    [50, 100, 200, -1],
                    [50, 100, 200, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": records_per_page_admin,
                "sDom": "<'row-fluid'<'span6'f><'span6'p>r>t<'row-fluid'<'span6'l<'clearfix'>i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#sample_2 .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#sample_2_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
            jQuery('#sample_2_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
            jQuery('#sample_2_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown

            
            // table for filter products for relations 
            $('#filter_products_relations').dataTable({
                "aLengthMenu": [
                    [20, 100, 200, -1],
                    [20, 100, 200, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 10,
                "sPaginationType": "bootstrap",
                "bLengthChange": false,
                "bFilter": false,
                "bSort" : false,
                "oLanguage": {
                    "sLengthMenu": "_MENU_ per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#filter_products_relations .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#filter_products_relations_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
            jQuery('#filter_products_relations_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
            jQuery('#filter_products_relations_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown

            
            // data table withour sorting
            $('#datatable_without_sorting').dataTable({
                "aLengthMenu": [
                    [50, 100, 200, -1],
                    [50, 100, 200, "All"] // change per page values here
                ],
                "bSort" : false,
                // set the initial value
                "iDisplayLength": records_per_page_admin,
                //"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sDom": "<'row-fluid'<'span6'f><'span6'p>r>t<'row-fluid'<'span6'l<'clearfix'>i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#datatable_without_sorting .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#datatable_without_sorting_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
            jQuery('#datatable_without_sorting_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
            jQuery('#datatable_without_sorting_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown

            
            // begin: third table
            $('table#sample_3').dataTable({
                "aLengthMenu": [
                    [50, 100, 200, -1],
                    [50, 100, 200, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": records_per_page_admin,
                //"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sDom": "<'row-fluid'<'span6'f><'span6'p>r>t<'row-fluid'<'span6'l<'clearfix'>i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#sample_3 .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#sample_3_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
            jQuery('#sample_3_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
            jQuery('#sample_3_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown
            
            
            $('#dataTableWithoutPagination').dataTable({
                "aLengthMenu": [
                    [15, 30, 50, -1],
                    [15, 30, 50, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": records_per_page_admin,
                //"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sDom": "<'row-fluid'<'span6'f><'span6'p>r>t<'row-fluid'<'span6'l<'clearfix'>i><'span6'p>>",
                "bPaginate": false,
                'bInfo':false,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#dataTableWithoutPagination .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#dataTableWithoutPagination_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
            jQuery('#dataTableWithoutPagination_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
            jQuery('#dataTableWithoutPagination_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown
        
            
            }
    };

}();
$(document).ready(function() {
    var category_id = $("#category_id_for_product_listing").val();
    var products_per_page_admin = $("#products_per_page_admin").val();
    
// product ajx data table
            var table  = $('#product_ajax_table').dataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "script/ajax_product_listing.php?cat_id="+category_id,
                "aLengthMenu": [
                    [50, 100, 200, -1],
                    [50, 100, 200, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": products_per_page_admin,
                //"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sDom": "<'row-fluid'<'span6'f><'span6'p>r>t<'row-fluid'<'span6'l<'clearfix'>i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "order": [[ 6, "desc" ]],
                "oLanguage": {
                    "sLengthMenu": "_MENU_ per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{ "bSortable": false, "aTargets": [0,1,7]}]
            });
            
            jQuery('#product_ajax_table .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#product_ajax_table_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
            jQuery('#product_ajax_table_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
            jQuery('#product_ajax_table_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown
});