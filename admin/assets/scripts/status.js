jQuery(document).ready(function() {

    /* on click */
    $(".status_on").live("click",function(){

        var id=$(this).attr('on');
        var parent_id="on_off_button"+id;
        var module = $(this).attr('module');

        $("#" + parent_id + " .status_on").addClass("active");
        $("#" + parent_id + " .status_off").removeClass("active");

        var dataString = 'module='+module+'&id='+id;
        
        $.ajax({
            type: "GET",
            url: "script/status.php",
            data: dataString,
            success: function(data, textStatus) {}
            //error:function(xhr, ajaxOptions, thrownError){alert(thrownError);}
       });
     });

    $(".status_off").live("click",function(){
        var id=$(this).attr('off');
        var parent_id="on_off_button"+id;
        var module = $(this).attr('module');
        
        $("#" + parent_id + " .status_on").removeClass("active");
        $("#" + parent_id + " .status_off").addClass("active");

         var dataString = 'module='+module+'&id='+id;
        $.ajax({
            type: "GET",
            url: "script/status.php",
            data: dataString,
            success: function(data, textStatus) {}
            //error:function(xhr, ajaxOptions, thrownError){alert(thrownError);}
        });
    });
    $(document).on('change', '.status_change', function(event){  
    //$(".status_change").change(function(){
        var new_status = $(this).val();
        var id=$(this).attr('row_id');
        var module = $(this).attr('module');

        var dataString = 'module='+module+'&id='+id+'&new_status='+new_status;

        $.ajax({
            type: "GET",
            url: "script/ajax_status_update.php",
            data: dataString,
            success: function(data, textStatus) {
                if(data=="success"){
                    toastr.success('Status has been successfully updated.');
                }
                else{
                    toastr.error('An error occurred while updating status.');
                }
                
            },
            error:function(xhr, ajaxOptions, thrownError){
                toastr.error('An error occurred while updating status.');
            }
        });
     });
     
     $(document).on('change', '.position_update', function(event){  
        var new_position = $(this).val();
        var id = $(this).attr('row_id');
        var module = $(this).attr('module');

        var dataString = 'module='+module+'&id='+id+'&new_position='+new_position;
        
        $.ajax({
            type: "GET",
            url: "script/ajax_position_update.php",
            data: dataString,
            success: function(data, textStatus) {
                if(data=="success"){
                    toastr.success('Position has been successfully updated.');
                }
                else{
                    toastr.error('An error occurred while updating position.');
                }
                
            },
            error:function(xhr, ajaxOptions, thrownError){
                toastr.error('An error occurred while updating position.');
            }
        });
     });
});