var TableEditableWeight = function () {

    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow,range_id) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                if(aData[1]=='-- All --'){
                    aData[1] = '';
                }
                jqTds[0].innerHTML = '<input type="text" id="min" name="min" class="m-wrap small" value="' + aData[0] + '" readonly="readonly">';
                jqTds[1].innerHTML = '<input type="text" id="max" name="max" class="m-wrap small" value="' + aData[1] + '"'+((range_id!='0')?' readonly="readonly ':'')+'">';
                jqTds[2].innerHTML = '<input type="text" id="price" name="price" class="m-wrap small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<a class="edit" rel="'+range_id+'" href="">Save</a>';
                jqTds[4].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            function saveRow(oTable, nRow,range_id) {
                var jqInputs = $('input', nRow);
                var last_max_value = '';
                last_max_value = $('#sample_editable_1_new').attr('rel');
                var weight_rule_id = $('#weight_rule_id').val();
                var min = parseFloat(jqInputs[0].value);
                var max = parseFloat(jqInputs[1].value);
                var price = parseFloat(jqInputs[2].value);
                if(isNaN(max)){
                    max = '-1';
                }
                var url = "script/ajax_shipping_weight_range.php";
                var data = 'weight_rule_id='+weight_rule_id+'&min='+min+'&max='+max+'&price='+price+'&last_max_value='+last_max_value;

                if(range_id!='0'){
                    data+='&id='+range_id;
                }
               $.ajax({
                   type: "POST",
                   url: url,
                   data: data,   
                   success: function(data)
                   {
                        data = $.parseJSON(data);
                        if(data=='1'){
                            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 3, false);
                            oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 4, false);
                            oTable.fnDraw();
                            location.reload();
                        }
                        else{
                            alert('Please select the proper range.');
                        }
                   }
               });
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 3, false);
                oTable.fnDraw();
            }

            var oTable = $('#sample_editable_1').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 15,
                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "bPaginate": false,
                "bFilter" : false,
                'bInfo':false,
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#sample_editable_1_wrapper .dataTables_filter input').addClass("m-wrap medium"); // modify table search input
            jQuery('#sample_editable_1_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
            jQuery('#sample_editable_1_wrapper .dataTables_length select').select2({
                showSearchInput : false //hide search box with special css class
            }); // initialzie select2 dropdown

            var nEditing = null;

            $('#sample_editable_1_new').click(function (e) {
                e.preventDefault();
                var range_id = 0;
                var last_max_value = '';
                last_max_value = $(this).attr('rel');
                if(last_max_value=='-1'){
                    alert('Maximum range already selected.');
                    return false;
                }
                var aiNew = oTable.fnAddData([last_max_value, '', '',
                        '<a class="edit" rel="new" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow,range_id);
                $('#min').attr("readonly",true);
                nEditing = nRow;
            });

            $('#sample_editable_1 a.delete').live('click', function (e) {
                e.preventDefault();

                if (confirm("Are you sure to delete this row ?") == false) {
                    return;
                }
                var range_id = $(this).attr('rel');
                var nRow = $(this).parents('tr')[0];

                var url = "script/ajax_shipping_weight_range.php";
                var data = 'del=1&id='+range_id;

                   $.ajax({
                       type: "POST",
                       url: url,
                       data: data,   
                       success: function(data)
                       {
                            data = $.parseJSON(data);
                            if(data=='1'){
                                oTable.fnDeleteRow(nRow);
                                location.reload();
                            }
                            else{
                                alert('Error occurred while deleting range.');
                            }
                       }
                   });
                //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
            });

            $('#sample_editable_1 a.cancel').live('click', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            $('#sample_editable_1 a.edit').live('click', function (e) {
                e.preventDefault();
                var range_id = '';
                var min = '';
                var max = '';
                var price = '';
                
                var range_id = 0;
                range_id = $(this).attr('rel');
                
                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];
                
                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oTable, nEditing);
                    editRow(oTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    
                    /* Editing this row and want to save it */
                    var jqInputs = $('input', nRow);
                    min = jqInputs[0].value;
                    max = jqInputs[1].value;
                    price = jqInputs[2].value;
                    
                    if(min==''){
                       $('#min').css('border','1px solid red');
                       return false;
                    }
                    if(price==''){
                       $('#price').css('border','1px solid red');
                       return false;
                    }
                    if(parseFloat(max) < parseFloat(min)){
                        $('#min').css('border','1px solid red');
                        $('#max').css('border','1px solid red');
                        return false;
                    }

                    saveRow(oTable, nEditing,range_id);
                    nEditing = null;
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow,range_id);
                    nEditing = nRow;
                }
            });
        }

    };

}();