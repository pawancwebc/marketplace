$(document).ready(function(){
       var attribute;
       var value;
       var data;
       
       $("#attribute").change(function(){
          
           attribute=$(this).val();
           
           var url = "script/product_attribute.php";
           var data = $(this).serialize();

           $.ajax({
               type: "POST",
               url: url,
               data: data,   
               success: function(data)
               {
                     var options="";
                    data = $.parseJSON(data);
                   /*fill address fields with json*/
                    //$("#vsalsues").html(data);
                    $.each(data, function(i, item) {
                       options+='<option value="'+item['id']+'">'+item['name']+'</option>'; 
                    });
                    $("#values").find('option').remove().end().append(options);
                    //alert(options);
               }
           });
       }); 
       
       
       var selected_attr = '';
       var selected_val = '';
       var selected_attr_name = '';
       var selected_val_name = '';
       
       $("#add_attr").click(function(){
           
              var new_option = ''; 
              selected_attr = $("#attribute").val();
              selected_val =  $("#values").val();
              selected_attr_name = $("#attribute option:selected").text();
              selected_val_name =  $("#values option:selected").text();
              
           var existing_value = $("#combination option[value='"+selected_attr+"']").val();
           //alert(existing_value);return false;
           if(existing_value){
               alert('You can only add one combination per attribute type.');
           }
           else{
              if(selected_attr!='' && selected_val!=''){
                  new_option = '<option value="'+selected_attr+'" attribte="'+selected_val+'">'+selected_attr_name+' : '+selected_val_name+'</option>';
                  $("#combination").append(new_option);
              }
              else{
                  alert("Please select the attribute and value first.");
                  return false;
              }
           }
       });
       
       $("#delete_attr").click(function(){
          $("#combination option:selected").remove();
       });
  });
  
  function selectAll() 
    { 
        selectBox = document.getElementById("combination");
        for (var i = 0; i < selectBox.options.length; i++) 
        { 
             selectBox.options[i].selected = true; 
        } 
        
        $('#combination option').each(function(i) {
           var option_attribte = $(this).attr('attribte');
           $(this).val(option_attribte);
        });
        
    }
    
$(document).ready(function(){
   $('#toggle_add_group_product').click(function(){
       $('#add_group_product').toggle(1000);
   });
});

$(document).ready(function(){
/*
$("#select_attribuute_set").change(function(){
   var attribute_set_id = $(this).val();
   var product_id_variant = $('#product_attribute_super_set').val();
   var product_attribute_super_set = $('#product_attribute_super_set').val();
   
   if(attribute_set_id!=''){
       var url = "script/ajax_product_attribute_set.php";
       var data = 'attribute_set_id='+attribute_set_id+'&product_id='+product_id_variant+'&attribute_super_set='+product_attribute_super_set;
       $.ajax({
           type: "POST",
           url: url,
           data: data,   
           success: function(data)
           {
               App.init();
               FormComponents.init();
               ComponentsFormTools.init();
               $('#get_ajax_response_variant_fields').html(data);
           }
       });
   }
});
*/
$("#select_attribuute_set").change(function(){
    var type = $(this).val();
    var base_ulr_link = $('#url_page_base_url').val();  
    window.location.assign(base_ulr_link+type+'#variant');
});

$('#impact_on_price').click(function(){
    if($("#impact_on_price").is(":checked")){
        $("#product_impact_type").attr("disabled", false) 
        $("#product_impact_price").attr("disabled", false) 
    }
    else{
        $("#product_impact_type").attr("disabled", true) 
        $("#product_impact_price").attr("disabled", true) 
    }
});

$('.notdefaultswitch').change(function(e){
    var rel = $(this).attr('rel');
    var select_value = $(this).val();
    var product_id = $('.product_id_for_autocomplete').val();
    
    if(select_value!=''){
        $('.'+rel+'-not-default').show('1000');
        $('.'+rel+'-selected-products').show('1000');
    }
    else{
        
        var x = $('.'+rel+'-selected-products');
        if(x.size()){
            var confirm_del =  confirm('Are you sure? By setting default settings, all the selected products will be removed.');
            if(confirm_del==true){
                
                   var url = "script/ajax_remove_product_relation.php";
                   var datasTRING = 'id='+product_id+'&rel='+rel;

                   $.ajax({
                       type: "POST",
                       url: url,
                       data: datasTRING,   
                       success: function(data)
                       {

                           if(data=='fail'){
                               toastr.error('An error occurred while removing selected products.');
                               return false;
                           }
                           if(data=='success'){
   
                               $('.form-body').removeClass(rel+'-selected-products');
                           }
                       }
                   });
                
                
            } else {
                $(this).val('choose');
                return false;
            }
        }
        
        $('.'+rel+'-not-default').hide('1000');
        $('.'+rel+'-selected-products').hide('1000');
    }
});

    
    
});

$(document).on('click', '#enable_sale', function(event){  
    $('#sale_price_div').toggle(1000);
});

$(document).on('click','.is_active',function(){
    var v = $(this).val();
    if(v=='3'){
        $('#scheduled_for_div').slideDown('medium');
    } else {
        $('#scheduled_for_div').slideUp('medium');
    }
});