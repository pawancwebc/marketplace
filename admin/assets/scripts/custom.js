
$(function () {
    $('#unlimited_time').click(function () {
        if ($(this).is(':checked')) {
            $("#show_date_display").slideUp();
            $("#show_unlimited_or").slideUp();
        }
        else
        {
            $("#show_date_display").slideDown();
            $("#show_unlimited_or").slideDown();
        }
    });
});

$(function () {
    $("#validation").validationEngine();	
});



$(function () {
    $('#unlimited_times').click(function () {
        if ($("#unlimited_times").is(':checked')) {
            $("#show_date_displays").slideUp();
            // $("#show_unlimited_or").slideUp();
        }
        else
        {
            $("#show_date_displays").slideDown();
            //$("#show_unlimited_or").slideDown();
        }
    });
});

$('#type_url_rewrite').change(function () {
    var type = $(this).val();
    var base_ulr_link = $('#url_page_base_url').val();
    window.location.assign(base_ulr_link + type);
});

$('#option_value_selected').change(function () {
    var type = $(this).val();
    var base_ulr_link = $('#url_page_base_url').val();
    window.location.assign(base_ulr_link + type);
});


$('.redirecting').click(function () {
    window.location = $(this).attr('href');
});

$('.notclickable').click(function (e) {
    return false;
});

$('.clickable').click(function () {
    var url = $(this).parent().find('#do_action').attr('href');
    //var url=$(this).find('#do_action').attr('href');
    if (url != '' && url != null) {
        window.location.href = url;
    }
    return false;
});
// seo information

$(document).ready(function () {
    /*
     $(".meta_info").each(function() {
     var max = $(this).attr('maxlength');
     var len = $(this).val().length;
     
     if (len >= max) {
     $(this).parent().find('#characterLeft').text(' you have reached the limit');
     } else {
     var ch = max - len;
     $(this).parent().find('#characterLeft').text(ch + ' characters left');
     }
     });
     */
    // for meta title
    var meta_title_len = $('.seo_result_title').text().length;
    var meta_title_total_len = $('.seo_result_title_outer').text().trim().length;
    var max_title_len = $('.seo_result_title_outer').attr('maxlength');

    if (meta_title_total_len >= max_title_len) {
        $('.characterLeftTitle').text(meta_title_total_len + ' characters. you have reached the limit (' + max_title_len + ')');
    } else {
        var ch = max_title_len - meta_title_total_len;
        $('.characterLeftTitle').text(meta_title_total_len + ' characters');
    }

    // for meta desc
    var meta_desc_total_len = $('.seo_result_desc').text().trim().length;
    var max_desc_len = $('.seo_result_desc').attr('maxlength');

    if (meta_desc_total_len >= max_desc_len) {
        $('.characterLeftDesc').text(meta_desc_total_len + ' characters. you have reached the limit (' + max_desc_len + ')');
    } else {
        var ch = max_desc_len - meta_desc_total_len;
        $('.characterLeftDesc').text(meta_desc_total_len + ' characters');
    }

});

$('.meta_info').keyup(function () {

    var len = $(this).val().length;
    var type = $(this).attr('rel');
    var value = $(this).val();

    if (type == 'title') {

        var max_title_len = $('.seo_result_title_outer').attr('maxlength');
        var meta_title_len = $('.seo_result_title').text().length;
        var meta_title_total_len = $('.seo_result_title_outer').text().trim().length;
        var main_consumed_len = meta_title_total_len - meta_title_len;

        var actual_len = main_consumed_len + len;

        if (actual_len >= max_title_len) {
            $('.characterLeftTitle').text(actual_len + ' characters. you have reached the limit (' + max_title_len + ')');
        } else {
            var ch = max_title_len - actual_len;
            $('.characterLeftTitle').text(actual_len + ' characters');
        }

        $('.seo_result_title').text(value);
    }
    else if (type == 'desc') {

        var max_desc_len = $('.seo_result_desc').attr('maxlength');

        if (len >= max_desc_len) {
            $('.characterLeftDesc').text(len + ' characters. you have reached the limit (' + max_desc_len + ')');
        } else {
            var ch = max_desc_len - len;
            $('.characterLeftDesc').text(len + ' characters');
        }

        $('.seo_result_desc').text(value);
    }
});
// seo information ends


////////////////////////////////// on - off switch //////////////////////////////////
// checkbox design change
/*
 $(document).ready(function() {
 $('.iPhoneStyle').iphoneStyle({
 checkedLabel: 'YES',
 uncheckedLabel: 'NO'
 });
 });
 
 $('.iPhoneStyle').click(function(){
 alert('asdasd');
 return true;
 });
 /*
 * smaal button design - on/off switch
 * 
 $(document).ready(function() {
 $('.form-group :checkbox').parent().css('padding-top','7px');
 $('.form-group :checkbox').addClass('make-switch');
 $('.form-group :checkbox').addClass('switch-mini');
 });
 */
////////////////////////////////// on - off switch //////////////////////////////////



$(document).ready(function () {
    $('#rating_div').raty({
        click: function (score, evt) {
            $('#review_score').val(score);
        }
    });

    $('.show_rating').each(function () {
        var show_score = $(this).attr('score');
        $(this).raty({readOnly: true, score: show_score});
    });

    $('#score_show_edit').raty({
        click: function (score, evt) {
            $('#review_score').val(score);
        },
        score: function () {
            return $(this).attr('data-score');
        }
    });

});

// synonyms
$(document).on('change', '.synonyms', function (event) {
    var tags_input = $(this).val();
    var synonym_id = $(this).attr("rel");
    var dataString = 'tags_input=' + tags_input + '&id=' + synonym_id;

    $.ajax({
        type: "GET",
        url: "script/ajax_synonyms.php",
        data: dataString,
        success: function (data, textStatus) {
            if (data == "success") {
                toastr.success('Synonyms has been successfully updated.');
            }
            else {
                toastr.error('An error occurred while updating synonyms.');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.error('An error occurred while updating synonyms.');
        }
    });
    return false;
});

$(document).on('click', '#add_synonyms', function (event) {
    var tags_input = $("#synonyms_field").val();
    var dataString = 'tags_input=' + tags_input;

    $.ajax({
        type: "GET",
        url: "script/ajax_synonyms.php",
        data: dataString,
        success: function (data, textStatus) {
            if (data == "success") {
                toastr.success('Synonyms has been successfully added.');
                setTimeout(function () {
                    location.reload()
                }, 2000);
            }
            else {
                toastr.error('An error occurred while adding synonyms.');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.error('An error occurred while adding synonyms.');
        }
    });
    return false;
});

$(document).on('click', '#add_hyponyms', function (event) {

    var tags_name = $("#hypernym_field").val();
    var tags_group = $("#hyponym_group_field").val();

    var dataString = 'tags_name=' + tags_name + '&tags_group=' + tags_group;

    $.ajax({
        type: "GET",
        url: "script/ajax_hypernyms.php",
        data: dataString,
        success: function (data, textStatus) {
            if (data == "success") {
                toastr.success('Hypernym has been successfully added.');
                setTimeout(function () {
                    location.reload()
                }, 2000);
            }
            else {
                toastr.error('An error occurred while adding hypernym.');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.error('An error occurred while adding hypernym.');
        }
    });
    return false;
});

$(document).on('change', '.hypernyms', function (event) {
    var tags_input = $(this).val();

    var tags_name = '';
    var tags_group = '';

    var hypernym_id = $(this).attr("rel");
    var hypernym_field = $(this).attr("field");

    if (hypernym_field == 'name') {
        tags_name = tags_input;
    }
    else {
        tags_group = tags_input;
    }

    var dataString = 'tags_name=' + tags_name + '&tags_group=' + tags_group + '&id=' + hypernym_id;

    $.ajax({
        type: "GET",
        url: "script/ajax_hypernyms.php",
        data: dataString,
        success: function (data, textStatus) {

            if (data == "success") {
                toastr.success('Hypernym has been successfully updated.');
            }
            else {
                toastr.error('An error occurred while updating hypernym.');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.error('An error occurred while updating hypernym.');
        }
    });
    return false;
});

$(document).find('.chosen-select').chosen();

$(document).on('change', '[data-trigger="module"]', function (event) {
    event.preventDefault();
    var module = $(this).val();
    $('#ajax_output').html('<div class="offset2" style="font-weight: bold">Loading Data Please Wait...</div>');
    $.get('script/ajax.php', {act: 'get_submenus', module: module}, function (data) {
        $('#ajax_output').html(data);
        $(document).find('.chosen-select').chosen();
    });
});

$(document).on('change', 'select#make_mega_menu', function () {
    var menu_id = $(this).attr('menu_id');
    var value = $(this).val();
    $.post('script/ajax.php', {act: 'make_mega_menu', menu_id: menu_id, value: value}, function () {
        toastr.success('Menu Type changed successfully.');
    });
});
