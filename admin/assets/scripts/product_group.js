$(function(){
/* on change */
$(document).on('change', '.group_message', function(event){  

    var id = $(this).attr('rel');
    var description=$(this).val();
    var dataString = 'description='+description+'&id='+id;
   
    $.ajax({
        type: "GET",
        url: "script/product_group.php",
        data: dataString,
        success: function(data, textStatus) {
           toastr.success('Group description has been successfully updated.');
 
        },
        error:function(xhr, ajaxOptions, thrownError){
           toastr.error('An error occurred while updating group description.');

        }
   });
 });
});
