$(document).ready(function(){
   $('#rating_div').raty({
       click: function(score, evt) {
        $('#review_score').val(score);
      }
   });
   
   $('.show_rating').each(function(){
       var show_score = $(this).attr('score');
       $(this).raty({ readOnly: true, score: show_score });
   });
});