var UITree = function () {
    var product_id;
    product_id = $('.product_id_for_autocomplete').val();
   
    var handleSample2 = function () {
        $('#tree_2').jstree({
            'core': {
                "themes" : {
                    "responsive": false
                },    
                'data': {
                    'url' : function (node) {
                      return 'script/ajax_category_tree.php?id='+product_id;
                    },
                    'data' : function (node) {
                      return { 'parent' : node.id };
                    }
                }
            },
            'checkbox':{
                 'visible' : true,
                 'three_state': false,
                 'whole_node' : true,
                 'keep_selected_style' : false
            },
            "types" : {
                "default" : {
                    "icon" : "icon-folder icon-warning icon-lg"
                },
                "file" : {
                    "icon" : "icon-file icon-warning icon-lg"
                }
            },
            'plugins': ["wholerow", "checkbox", "types"]
        })
        .on('changed.jstree', function (e, data)
        {
            var i, j;
            var checked_ids = [];
            for(i = 0, j = data.selected.length; i < j; i++)
            {
               checked_ids.push(data.instance.get_node(data.selected[i]).id);
            }

            document.getElementById('categories').value = checked_ids.join(",");
        })
    }
    
    var category_id;
    category_id = $('#category_id').val();
    
    var handleSample3 = function () {
        $('#tree_cats').jstree({
            'core': {
                "themes" : {
                    "responsive": false
                },    
                'data': {
                    'url' : function (node) {
                      return 'script/ajax_category_tree_data_cats.php?id='+category_id;
                    },
                    'data' : function (node) {
                      return { 'parent' : node.id };
                    }
                }
            },
            'checkbox':{
                 'visible' : true,
                 'three_state': false,
                 'whole_node' : true,
                 'keep_selected_style' : false
            },
            "types" : {
                "default" : {
                    "icon" : "icon-folder icon-warning icon-lg"
                },
                "file" : {
                    "icon" : "icon-file icon-warning icon-lg"
                }
            },
            'plugins': ["wholerow", "checkbox", "types"]
        })
        .on('changed.jstree', function (e, data)
        {
            var i, j;
            var checked_id = '';
            for(i = 0, j = data.selected.length; i < j; i++)
            {
               checked_id = data.instance.get_node(data.selected[i]).id;
            }

            document.getElementById('parent_id').value = checked_id;
        })
        
    }
    
    var handleSample1 = function () {

        $('.fileeditor').jstree({
                "core" : {
                    "themes" : {
                        "responsive": true
                    }            
                },
                "types" : {
                    "default" : {
                        "icon" : "icon-folder-close icon-warning icon-lg"
                    },
                    "file" : {
                        "icon" : "icon-file icon-warning icon-lg"
                    }
                },
                "plugins": ["types"]
            });

        }
        
    return {
        //main function to initiate the module
        init: function () {
            handleSample1();
            handleSample2();
            handleSample3();

        }

    };

}();