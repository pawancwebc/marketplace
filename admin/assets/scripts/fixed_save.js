jQuery(function($) {

    var id_fix = "form-actions";

    var device_width = parseInt($(window).width());
    var device_height = parseInt($(window).height());
    var page_height = parseInt($(document).height());
    var top_px = 0;
    var bottom_px = 170;

    if (device_height <= page_height - 170) {

        //$("."+id_fix).css('width',$('.span12').width());

        $("." + id_fix).affix({
            offset: {
                top: top_px, bottom: bottom_px
            }
        });

        if (!$("." + id_fix).hasClass("affix")) {
            $("." + id_fix).addClass('affix');
        }
        $("." + id_fix).parent().css('margin-bottom','110px');
        $("." + id_fix).removeClass('affix-bottom');
        $("." + id_fix).removeClass('affix-top');

        $(window).scroll(function() {
            var scroll = parseInt($(window).scrollTop());


            if ($(window).scrollTop() + $(window).height() > $(document).height() - bottom_px) {
                if (!$("." + id_fix).hasClass("affix-bottom")) {
                    $("." + id_fix).addClass('affix-bottom');
                }
                $("." + id_fix).parent().css('margin-bottom','0px');
                $("." + id_fix).removeClass('affix');
                $("." + id_fix).removeClass('affix-top');
            }
            else if ($(window).scrollTop() == 0) {

                $("." + id_fix).addClass('affix');
                $("." + id_fix).parent().css('margin-bottom','110px');
                $("." + id_fix).removeClass('affix-bottom');
                $("." + id_fix).removeClass('affix-top');
            }
            else {

                if (!$("." + id_fix).hasClass("affix")) {
                    $("." + id_fix).addClass('affix');
                }
                $("." + id_fix).removeClass('affix-bottom');
                $("." + id_fix).parent().css('margin-bottom','110px');
                $("." + id_fix).removeClass('affix-top');

            }

        }); //missing );  

    }

    else {
        $("." + id_fix).css('position', 'inherit');
        $("." + id_fix).removeClass('affix');
        $("." + id_fix).removeClass('affix-bottom');
        $("." + id_fix).removeClass('affix-top');
    }

});