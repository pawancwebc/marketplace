$(document).on('click', '.selected_parent_category_id', function(event){  
    var select_parent_category_id = $(this).attr('rel');
    
    var url = "script/ajax_category_sub_cats.php";
    var data = 'select_parent_category_id='+select_parent_category_id;

       $.ajax({
           type: "POST",
           url: url,
           data: data,   
           success: function(data)
           {

               $('#sub_cat_td_'+select_parent_category_id).html(data);
               $('#sub_cat_row_'+select_parent_category_id).toggle();
               App.init();
               FormComponents.init();
               FormSamples.init();
               TableManaged.init();
               UIJQueryUI.init();
               ComponentsFormTools.init();
               ComponentsPickers.init();
               ComponentsDropdowns.init();
           }
       });
});
/*
$(document).on('click', '.main_parent_div', function(event){  
    var main_parent_div_rel = $(this).attr('rel');
    $('.sub-cat-div-'+main_parent_div_rel).toggle();
});
*/