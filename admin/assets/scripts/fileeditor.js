// after clicking the file name show the content in right space
$(document).on('click', '.files', function(event){  
   
   if(!checkRefreshBound()){
       return false;
   }
    
   var file_path = $(this).attr('file_path');
   var file_name = $(this).attr('file_name');
   var module = $(this).attr('module');
   var li_id = $(this).attr('id');

   var show_file_path = file_path.substring(3);
   var show_file_name = ' > '+file_name;
   show_file_name = show_file_name + ' <a id="'+li_id+'" class="rename_file" title="Rename File"';
   show_file_name = show_file_name + ' onclick="renameFunction('+"'"+file_name+"'"+')" file_path="'+file_path+'" file_name="'+file_name+'"';
   show_file_name = show_file_name + '> <i class="icon-pencil"></i></a>'
   
   // hide add file button if file name selected instead of directory
   $('#create_file_outer').fadeOut('slow');
   $('#add_image_button_outer').fadeOut('slow');
   
   var dataString = 'file_path='+encodeURIComponent(file_path)+'&file_name='+encodeURIComponent(file_name)
                        +'&module='+encodeURIComponent(module);

    $.ajax({
        type: "POST",
        url: "ajax_file_editor.php",
        data: dataString,
        beforeSend: function() {
             $('.emptydiv').hide();
             $('.code_editor_div').hide();
             $('#loader').show();
        },
        success: function(data, textStatus) {
            
            $('.code_editor_div').html(data);
            
            $('#loader').hide();
            
            $('#title_file_name').html(show_file_name);
            
            $('.code_editor_div').fadeIn('slow');
            
            $('html, body').animate({
                scrollTop: $("#editor_top").offset().top-45
            }, 1000);
        },
        error: function(xhr, ajaxOptions, thrownError){
            toastr.error('An error occurred while reading file.');
        }
    });
  return false;
});

// save new file content
$(document).on('click', '#save_file', function(event){  
    
   var file_path = $('#file_path').val();
   var file_name = $('#file_name').val();
   var module = $('#module').val();
   var file_text = editor.getSession().getValue();
   
   if(typeof file_text === 'undefined'){
       toastr.error('Input text undefined.');
       return false;
   };
   var dataString = 'file_path='+encodeURIComponent(file_path)+'&file_name='+encodeURIComponent(file_name)
                        +'&module='+encodeURIComponent(module)+'&file_text='+encodeURIComponent(file_text);

    $.ajax({
        type: "POST",
        url: "ajax_file_editor.php",
        data: dataString,
        beforeSend: function() {
             $('.code_editor_div').fadeOut('fast');
             $('#loader').fadeIn('fast');
        },
        success: function(data, textStatus) {

            $('#loader').hide();
            
            $('.code_editor_div').fadeIn('slow');

            if(data=='success'){
                toastr.success('File has been updated successfully.');
                editor.session.getUndoManager().markClean();
                $('#save_file').addClass("hide");
                disableBeforeUnload();
            }
            else if(data=='fail2'){
                toastr.error('Wrong File path.');
            }
            else if(data=='fail3'){
                toastr.error('File not writable.');
            }
            else{
                toastr.error('An error occurred while updating file.');
            }
            
            $('html, body').animate({
                scrollTop: $("#editor_top").offset().top-45
            }, 1000);
            
        },
        error: function(xhr, ajaxOptions, thrownError){
            toastr.error('An error occurred while updating file.');
        }
    });
});

// after clicking the folder name create new file option
$(document).on('click', '.directory', function(event){
    
    if(!checkRefreshBound()){
       return false;
    }
   
    var dir_path = $(this).attr('dir_path');
    var dir_name = $(this).attr('dir_name');
    
    $('.code_editor_div').hide();

    var activeTabIdx = $('ul.nav-tabs').find('.active').attr('id');
    
    $('#image_dir_path').val(dir_path);
    $('#add_button').attr('dir_path',dir_path);
    
    $('.current_dir_name').html('Directory : '+dir_name);
    
    if(activeTabIdx=='Images'){
        $('#create_file_outer').hide();
        $('#add_image_button_outer').fadeIn('slow');
    }else{
        $('#add_image_button_outer').hide();
        $('#create_file_outer').fadeIn('slow');
    }
    
    $('.emptydiv').fadeIn('slow');
    $('#title_file_name').html('');
            
    return false;
});

// add new file if file name given
$(document).on('click','#add_new_file',function(event){
    
   if(!checkRefreshBound()){
      return false;
   }
    
   var create_file_name = $('#new_file_name').val();
   var base_dir_path = $('#base_dir_path').val();
   
   if(typeof create_file_name === 'undefined' || create_file_name == ''){
       $('#new_file_name').css('border', '1px solid red');
       return false;
   };
   
   var dataString = 'create_file_name='+encodeURIComponent(create_file_name)+'&base_dir_path='+encodeURIComponent(base_dir_path);

    $.ajax({
        type: "POST",
        url: "ajax_file_editor.php",
        data: dataString,
        beforeSend: function() {
             
        },
        success: function(data, textStatus) {

            $('#add_file_div').modal('hide');
            
            if(data=='success'){
                toastr.success('New file created successfully.');
                var activeTabIdx = $('ul.nav-tabs').find('.active').attr('id');
                setTimeout(function(){
                    var new_redirect_path = window.location.href.split('?')[0]+'?Page=file_editor&module='+activeTabIdx;
                    window.location.href = new_redirect_path
                },2000);
            }
            else if(data=='fail1'){
                toastr.error('Error occurred while creating new file.');
            }
            else if(data=='fail2'){
                toastr.error('Invalid Directory.');
            }
            else if(data=='fail3'){
                toastr.error('Empty file name.');
            }
            else if(data=='fail4'){
                toastr.error('Directory not writeable.');
            }
            else if(data=='fail5'){
                toastr.error('File with same name already exists.');
            }
            else{
                toastr.error('An error occurred while updating file.');
            }
            
        },
        error: function(xhr, ajaxOptions, thrownError){
            toastr.error('An error occurred while updating file.');
        }
    });

});


function renameFunction(current_name){

    if(!checkRefreshBound()){
      return false;
    }
   
    var rename = prompt("Please enter new file name", current_name);
    
    if (rename != null) {
        var file_id = $('.rename_file').attr('id');
        var file_path = $('.rename_file').attr('file_path');
        var file_name = $('.rename_file').attr('file_name');

       var dataString = 'file_id='+encodeURIComponent(file_id)+'&file_path='+encodeURIComponent(file_path)
                        +'&rename='+encodeURIComponent(rename)+'&file_name='+encodeURIComponent(file_name);

        $.ajax({
            type: "POST",
            url: "ajax_file_editor.php",
            data: dataString,
            success: function(data, textStatus) {
                if(data=='success'){
                    
                     toastr.success('File has been renamed successfully.');

                     $('#'+file_id+' .jstree-anchor ').contents().last()[0].textContent=rename;
                     $('#'+file_id).attr('file_name',rename);
                     var show_file_name = file_path +'/'+rename;
                     show_file_name = show_file_name + ' <a id="'+file_id+'" class="btn rename_file btn-sm blue-stripe" ';
                     show_file_name = show_file_name + ' onclick="renameFunction('+"'"+rename+"'"+')" file_path="'+file_path+'" file_name="'+file_name+'"';
                     show_file_name = show_file_name + '> <i class="icon-pencil"></i> Rename</a>';
                     
                     $('#title_file_name').html(show_file_name);
                    
                }
                else if(data=='fail3'){
                    toastr.error('Wrong file path.');
                }
                else if(data=='fail2'){
                    toastr.error('Empty file name.');
                }
                else if(data == 'fail4'){
                    toastr.error('File with same name already exists.');
                }
                else{
                    toastr.error('An error occurred while renaming file.');
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                toastr.error('An error occurred while renaming file.');
            }
        });

    }
}

// submit image form after upload new image
$(document).on('change','#image_upload',function(event){
    
    $('.emptydiv').hide();
    $('.code_editor_div').hide();
    $('#loader').show();
    
    $('#upload_image_form').submit();
    return false;
});

//when tab changed
$('#file_editor_tabs').on('click', 'a[data-toggle="tab"]', function (e) {   
   
   if(!checkRefreshBound()){
      return false;
   }
    
});


$(document).on('click','#file_editor_tabs .nav-tabs li',function(event){
   
   toggle_create_file_add_image();
   
});

//on page load
$(document).ready(function(){

    toggle_create_file_add_image();

});

function toggle_create_file_add_image(){
    var activeTabIdx = $('ul.nav-tabs').find('.active').attr('id');
    var selected_tab_path = $('ul.nav-tabs').find('.active').attr('path');
    
    $('.code_editor_div').hide();
    $('.emptydiv').fadeIn('slow');
    
    $('#module_name').html(activeTabIdx);
    $('#title_file_name').html('');
    
    $('#image_dir_path').val(selected_tab_path);
    $('#add_button').attr('dir_path',selected_tab_path);
    
    $('.current_dir_name').html('Directory : '+activeTabIdx);

    if(activeTabIdx=='Images'){
        $('#create_file_outer').hide();
        $('#add_image_button_outer').fadeIn('slow');
    }else{
        $('#add_image_button_outer').hide();
        $('#create_file_outer').fadeIn('slow');
    }
}

function enableBeforeUnload() {
    $('body').addClass('noredirect');
    window.onbeforeunload = function (e) {
        return "Discard changes?";
    };
}

function disableBeforeUnload() {
    $('body').removeClass('noredirect');
    window.onbeforeunload = null;
}

function checkRefreshBound(){
    if($('body').hasClass('noredirect')){
        var confirm_refresh = confirm('This page is asking you to confirm that you want to leave - data you have entered may not be saved.');
        if(confirm_refresh == true){
            disableBeforeUnload();
        }
        else{
            return false;
        }
    }
    return true;
}