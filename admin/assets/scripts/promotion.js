/* conditions */
$(document).ready(function(){
    if(!$("#is_zone_restricted").is(":checked")){ 
       var type;
       type = $("#is_zone_restricted").attr('rel');
       getSelectOptions(type);
    }
    if(!$("#is_country_restricted").is(":checked")){ 
       var type;
       type = $("#is_country_restricted").attr('rel');
       getSelectOptions(type);
    }
    if(!$("#is_product_restricted").is(":checked")){ 
       var type;
       type = $("#is_product_restricted").attr('rel');
       getSelectOptions(type);
    }
    if(!$("#is_user_restricted").is(":checked")){ 
       var type;
       type = $("#is_user_restricted").attr('rel');
       getSelectOptions(type);
    }
    if(!$("#is_category_restricted").is(":checked")){ 
       var type;
       type = $("#is_category_restricted").attr('rel');
       getSelectOptions(type);
    }
    if(!$("#is_brand_restricted").is(":checked")){ 
       var type;
       type = $("#is_brand_restricted").attr('rel');
       getSelectOptions(type);
    }
    if(!$("#is_supplier_restricted").is(":checked")){ 
       var type;
       type = $("#is_supplier_restricted").attr('rel');
       getSelectOptions(type);
    }
});

$('#is_zone_restricted').click(function(){
   if(!$("#is_zone_restricted").is(":checked")){ 
       var type;
       type = $(this).attr('rel');
       getSelectOptions(type);
   }
   else{
       $('#zones_selection').hide(600);
   }
});
$('#is_country_restricted').click(function(){
   if(!$("#is_country_restricted").is(":checked")){ 
       var type;
       type = $(this).attr('rel');
       getSelectOptions(type);
   }
   else{
       $('#countries_selection').hide(600);
   }
});
$('#is_product_restricted').click(function(){
   if(!$("#is_product_restricted").is(":checked")){ 
       var type;
       type = $(this).attr('rel');
       getSelectOptions(type);
   }
   else{
       $('#products_selection').hide(600);
   }
});
$('#is_user_restricted').click(function(){
   if(!$("#is_user_restricted").is(":checked")){ 
       var type;
       type = $(this).attr('rel');
       getSelectOptions(type);
   }
   else{
       $('#users_selection').hide(600);
   }
});
$('#is_category_restricted').click(function(){
   if(!$("#is_category_restricted").is(":checked")){ 
       var type;
       type = $(this).attr('rel');
       getSelectOptions(type);
   }
   else{
       $('#categories_selection').hide(600);
   }
});
$('#is_brand_restricted').click(function(){
   if(!$("#is_brand_restricted").is(":checked")){ 
       var type;
       type = $(this).attr('rel');
       getSelectOptions(type);
   }
   else{
       $('#brands_selection').hide(600);
   }
});
$('#is_supplier_restricted').click(function(){
   if(!$("#is_supplier_restricted").is(":checked")){ 
       var type;
       type = $(this).attr('rel');
       getSelectOptions(type);
   }
   else{
       $('#suppliers_selection').hide(600);
   }
});

function getSelectOptions(type){
    var url = "script/ajax_promotions.php";
       var id = $('#id_conditions').val();

       var dataString = 'type='+type+'&id='+id;
       $.ajax({
               type: "POST",
               url: url,
               data: dataString,   
               success: function(data)
               {
                $("#ajax_data_"+type).html(data);
                ComponentsDropdowns.init();
                $("#"+type+"_selection").show(600);
               },
               error: function(jqXHR, textStatus, errorThrown) {
                  alert(textStatus);
                }
       });
}

/******* actions ***********/
$('#apply_discount_on_order').click(function(){
    $('#div_reduction_type').show(600);
    $('#div_apply_discount_type').hide(600);
});
$('#apply_discount_on_product').click(function(){
    $('#div_reduction_type').show(600);
    $('#div_apply_discount_type').show(600);
});
$('#apply_discount_on_none').click(function(){
    $('#div_reduction_type').hide(600);
    $('#div_apply_discount_type').hide(600);
});
$('#is_send_gift').click(function(){
    if($(this).is(":checked")){ 
        $('#div_gift_product').show(600);
    }
    else{
        $('#div_gift_product').hide(600);
        $('#div_gift_product_variant').hide(600);
    }
    
});
$(document).ready(function(){
    if($("#apply_discount_on_order").is(":checked")){ 
         $('#div_reduction_type').show(600);
         $('#div_apply_discount_type').hide(600);
    }
    if($("#apply_discount_on_product").is(":checked")){ 
         $('#div_reduction_type').show(600);
         $('#div_apply_discount_type').show(600);
    }
    if($("#apply_discount_on_none").is(":checked")){ 
         $('#div_reduction_type').hide(600);
         $('#div_apply_discount_type').hide(600);
    }
    if($("#is_send_gift").is(":checked")){ 
        $('#div_gift_product').show(600);
    }
});

$("#product_auto_complete_promotion").on("typeahead:selected typeahead:autocompleted", function(e,datum) {
    var selected_product_id = datum.id;
    $('#selected_product_promotion').val(selected_product_id);
    if(selected_product_id!=''){

        var url = "script/ajax_product_variant.php";
        var data = 'product_id='+selected_product_id;

        $.ajax({
           type: "POST",
           url: url,
           data: data,   
           success: function(data)
           {
              var options="";
              data = $.parseJSON(data);

              if(data.length == ''){
                $('#div_gift_product_variant').hide(600);
                $('#gift_product_variant').val('0');
              }
              else{
               /*fill address fields with json*/
                $.each(data, function(i, item) {
                   options+='<option value="'+item['id']+'">'+item['attribute_name']+'</option>'; 
                });
                $("#gift_product_variant").find('option').remove().end().append(options);
                $('#div_gift_product_variant').show(600);
              }
           }
        });
    }
});

$('#is_auto').click(function(){
    if($("#is_auto").is(":checked")){ 
       $('#coupon_code_div').show(600);
   }
   else{
       $('#coupon_code_div').hide(600);
   }
});