<!-- Begin left panel -->

<a href="javascript:;" id="show_menu">&raquo;</a>

<div id="left_menu">

        <a href="javascript:;" id="hide_menu">&laquo;</a>

        <ul id="main_menu">

                <li>
                    <a href="<?php echo make_admin_url('home', 'list', 'list');?>">
                        <img src="images/icon_home.png" alt="Home"/>Home
                    </a>
                </li>
                <li>
                    <?php include_once(DIR_FS_SITE_CONTROL.'/left/order.php'); ?>
                </li>
                <li>
                    <?php include_once(DIR_FS_SITE_CONTROL.'/left/product.php');?>
                </li>
                <li>
                    <?php  include_once(DIR_FS_SITE_CONTROL.'/left/ship.php');?>
                </li>
                <li>
                    <?php include_once(DIR_FS_SITE_CONTROL.'/left/content.php');?>
                </li>
                <!--<li>
                    <?php include_once(DIR_FS_SITE_CONTROL.'/left/discount.php');?>
                </li>-->				
                <!--<li>
                    <?php include_once(DIR_FS_SITE_CONTROL.'/left/supplier.php');?>
                </li>
                <li>
                    <?php include_once(DIR_FS_SITE_CONTROL.'/left/newsletterlist.php');?>
                </li> -->
                <li>
                    <?php include_once(DIR_FS_SITE_CONTROL.'/left/banner.php');?>
                </li>				
                <li>
                    <?php include_once(DIR_FS_SITE_CONTROL.'/left/user.php');?>
                </li>				
                <li>
                    <?php include_once(DIR_FS_SITE_CONTROL.'/left/email.php');?>
                </li>
                <li>
                    <?php include_once(DIR_FS_SITE_CONTROL.'/left/admin.php');?>
                </li> 
                <li>
                    <?php include_once(DIR_FS_SITE_CONTROL.'/left/setting.php');?>
                </li>
        </ul>

        <br class="clear"/>

        <!-- Begin left panel calendar -->

        <div id="calendar"></div>

        <!-- End left panel calendar -->

        <br class="clear">

</div>

<!-- End left panel -->

