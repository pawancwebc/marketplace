                                </div>
                                </div>
                            </div>
                        </div>
                        <br class="clear"/>
                    <!-- Begin footer -->
                    <div class="footer container">
                        <?php 
						$records_per_page_admin='20';
						if($Page=='order'): 
                            $records_per_page_admin = ORDER_PAGE_SIZE_ADMIN;
                        endif;?>
                        <input type="hidden" id="records_per_page_admin" value="<?php echo $records_per_page_admin;?>"/>
                        <div class="footer-inner"><?php echo date("Y");?> &copy; <?php echo SITE_NAME;?></div>
                        <div class="footer-tools">
                            <span class="go-top"><i class="icon-angle-up"></i></span>
                        </div>
                    </div>
                    <!-- End footer -->
                </div>
            <!-- End content -->
        </div>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/fuelux/js/spinner.min.js"></script>
        <script type="text/javascript" src="assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
        <script src="assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
   <script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
        <script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="assets/plugins/jstree/dist/jstree.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>   
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>   
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script> 
	<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.rowReordering.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>
        <script src="assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript" ></script>
        <script src="assets/plugins/bootstrap-toastr/toastr.min.js"></script>
        <script src="assets/scripts/iphone-style-checkboxes.js"></script>
        <script src="assets/scripts/jquery.raty.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
        
         <!-- BEGIN PAGE LEVEL PLUGINS for validation-->
        
        <!-- Validation engine -->
        <script src="assets/plugins/validation/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
        <script src="assets/plugins/validation/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        
        <!--fancybox-->
        <script src="assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>   
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
        <!--fancybox ends-->
        
         <!-- Image Croping js -->
         <script type="text/javascript" src="assets/plugins/jrac/jquery.jrac.js"></script> 
         <!--jrac ends-->
        
          <!--charts-->
        <script src="assets/plugins/flot/jquery.flot.js"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js"></script>
	<script src="assets/plugins/flot/jquery.flot.pie.js"></script>
        <!--charts-->
       
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
        
	<script src="assets/scripts/app.js"></script>
	<script src="assets/scripts/form-components.js"></script>   
        <script src="assets/scripts/table-managed.js"></script>    
        <script src="assets/scripts/ui-jqueryui.js"></script>  
        <script src="assets/scripts/ui-toastr.js"></script>
        <script src="assets/scripts/category_tree.js"></script>
        <script src="assets/scripts/gallery.js"></script>  
        <script src="assets/scripts/form-samples.js"></script>  
        <script src="assets/scripts/form-wizard.js"></script>
        <script src="assets/scripts/components-pickers.js"></script>
        <script src="assets/scripts/components-dropdowns.js"></script>
        <!--<script src="assets/scripts/form-validation.js"></script> --->
        <script src="assets/scripts/fixed_save.js"></script>  
        <script src="assets/scripts/product_group.js"></script>  
        <script src="assets/scripts/custom.js"></script>  
        
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {       
		   // initiate layout and plugins
		   App.init();
		   FormComponents.init();
                   FormSamples.init();
                   FormWizard.init();
                   TableManaged.init();
                   UIJQueryUI.init();
                   UITree.init();
                   Gallery.init();
                   ComponentsPickers.init();
                   ComponentsDropdowns.init();
                   //FormValidation.init();
                   UIToastr.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>
