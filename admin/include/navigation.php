<!-- BEGIN HEADER -->
<div class="header navbar <?php echo (BACKEND_STICKY_NAV_BAR) ? 'navbar-fixed-top' : ''; ?> ">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="header-inner container">

        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler hidden-phone">
        </div>
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <a class="navbar-brand" href="<?php echo make_admin_url('home', 'list', 'list'); ?>" style="color:#fff;padding-left: 18px;">
            <?php echo SITE_NAME ?>
        </a>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <img src="assets/img/menu-toggler.png" alt=""/>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <div class="hor-menu hidden-sm hidden-xs">
            <ul class="nav navbar-nav">
 
            </ul>
        </div>
        <!-- BEGIN TOP NAVIGATION MENU -->
        <ul class="nav navbar-nav pull-right">
            <li class="dropdown user">
                <a style="line-height:26px;" class="dropdown-toggle" href="<?php echo make_admin_url('setting', 'list', 'list'); ?>">
                    <i class="icon-cogs"></i>
                    <span class="username">Settings</span>
                </a>
            </li>
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user">
                <a style="line-height:26px;" href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-user"></i>
                    <span class="username">Account</span>
                    <i class="icon-angle-down"></i>
                </a>
                <ul class="dropdown-menu">
                    
                    <li>
                        <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=password'); ?>">
                            <i class="icon-lock"></i> Change Password
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo make_admin_url('logout'); ?>">
                            <i class="icon-key"></i> Log Out
                        </a>
                    </li>
                </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="container">
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
                    <li class="start <?php echo ($Page == 'home') ? 'active' : '' ?>">
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">
                            <i class="icon-home"></i>
                            <span class="title">
                                Dashboard
                            </span>
                        </a>
                    </li>
                    <?php
                    $_Menus = array(
                        array(
                            'name' => 'Users Manager',
                            'link' => 'javascript:;',
                            'icon' => 'icon-user',
                            'has_submenus' => true,
							'pages' => array('user'),
                            'submenus' => array(
                                'user' => array(
                                    'name' => 'List Users',
                                    'link' => make_admin_url('user', 'list', 'list')
                                )
                            )
                        ),	
						
                        array(
                            'name' => 'Manage Vendors',
                            'link' => 'javascript:;',
                            'icon' => 'icon-bookmark',
                            'has_submenus' => TRUE,
							'pages' => array('vendor'),
                            'submenus' => array(
                                'vendor' => array(
                                    'name' => 'List Vendors',
                                    'link' => make_admin_url('vendor', 'list', 'list')
                                )
                            )
                        ),
						
						array(
                            'name' => 'Service Order',
                            'link' => 'javascript:;',
                            'icon' => 'icon-shopping-cart',
                            'has_submenus' => TRUE,
							'pages' => array('order'),
                            'submenus' => array(
                                'order' => array(
                                    'name' => 'Service Order List',
                                    'link' => make_admin_url('order', 'list', 'list')
                                )
                            )
                        ),
						
						array(
                            'name' => 'Manage Services',
                            'link' => 'javascript:;',
                            'icon' => 'icon-bullhorn',
                            'has_submenus' => TRUE,
							'pages' => array('service'),
                            'submenus' => array(
                                'service' => array(
                                    'name' => 'List Services',
                                    'link' => make_admin_url('service', 'list', 'list')
                                )
                            )
                        ),
						
						array(
                            'name' => 'Service Categories',
                            'link' => 'javascript:;',
                            'icon' => 'icon-sitemap',
                            'has_submenus' => TRUE,
							'pages' => array('category'),
                            'submenus' => array(
                                'category' => array(
                                    'name' => 'List Categories',
                                    'link' => make_admin_url('category', 'list', 'list')
                                )
                            )
                        ),
						 
						
                        array(
                            'name' => 'Content Pages',
                            'link' => 'javascript:;',
                            'icon' => 'icon-list-alt',
                            'has_submenus' => TRUE,
							'pages' => array('content'),
                            'submenus' => array(
                                'content' => array(
                                    'name' => 'List Pages',
                                    'link' => make_admin_url('content', 'list', 'list')
                                )
                            )
                        ),

                        array(
                            'name' => 'Manage Banner',
                            'link' => 'javascript:;',
                            'icon' => 'icon-globe',
                            'has_submenus' => TRUE,
							'pages' => array('banner'),
                            'submenus' => array(
                                'prize' => array(
                                    'name' => 'List Banners',
                                    'link' => make_admin_url('banner', 'list', 'list')
                                )
                            )
                        ),
						 
						array(
                            'name' => 'Manage Navigation',
                            'link' => 'javascript:;',
                            'icon' => 'icon-barcode',
                            'has_submenus' => TRUE,
							'pages' => array('navigation','content_navigation'),
                            'submenus' => array(
                                'prize' => array(
                                    'name' => 'Navigation',
                                    'link' => make_admin_url('navigation', 'list', 'list')
                                )
                            )
                        ),	
						array(
                            'name' => 'Manage Region',
                            'link' => 'javascript:;',
                            'icon' => 'icon-list',
                            'has_submenus' => TRUE,
							'pages' => array('country','state','city'),
                            'submenus' => array(
                                'country' => array(
                                    'name' => 'Manage Country',
                                    'link' => make_admin_url('country', 'list', 'list')
                                ),
								'state' => array(
                                    'name' => 'Manage States',
                                    'link' => make_admin_url('state', 'list', 'list')
                                ),
								'city' => array(
                                    'name' => 'Manage City',
                                    'link' => make_admin_url('city', 'list', 'list')
                                )
                            )
                        ),	
						array(
                            'name' => 'Manage Emails',
                            'link' => 'javascript:;',
                            'icon' => 'icon-envelope',
                            'has_submenus' => TRUE,
							'pages' => array('email'),							
                            'submenus' => array(
                                'setting' => array(
                                    'name' => 'Emails',
                                    'link' => make_admin_url('email', 'list', 'list')
                                )
                            )
                        ),
						
                        array(
                            'name' => 'Preferences',
                            'link' => 'javascript:;',
                            'icon' => 'icon-cogs',
                            'has_submenus' => TRUE,
							'pages' => array('setting'),							
                            'submenus' => array(
                                'setting' => array(
                                    'name' => 'Preferences',
                                    'link' => make_admin_url('setting', 'list', 'list', 'sname=general')
                                )
                            )
                        ),

                        array(
                            'name' => 'Manage Reports',
                            'link' => 'javascript:;',
                            'icon' => 'icon-file',
                            'has_submenus' => TRUE,
							'pages' => array('report'),
                            'submenus' => array(
                                'prize' => array(
                                    'name' => 'Reports',
                                    'link' => make_admin_url('report', 'list', 'list')
                                )
                            )
                        ),						
                    );
                    make_admin_menus($_Menus, $Page, $action);
                    ?>
                    
                </ul>
            </div>
        </div>
        <div class="page-content-wrapper">
            <div class="page-content">
