<?php
require_once("../include/config/config.php");

include DIR_FS_SITE . 'include/class/adminlog.php';
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/adminUserClass.php');

$include_fucntions = array('http', 'image_manipulation', 'calender', 'url_rewrite', 'email', 'video', 'users', 'paging');
include_functions($include_fucntions);

/* check if already logged in */
if ($admin_user->is_logged_in()) {
    $loggind_in_admin = $admin_user->get_user_id();
    $user = get_object('admin_user', $loggind_in_admin);
    $snamee = isset($_GET['sname']) ? $_GET['sname'] : '';

    /* Check User type */
    if (isset($_SESSION['admin_session_secure']['login_type']) && $_SESSION['admin_session_secure']['login_type'] != 'admin') {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Sorry, You are already logged in as ' . ucfirst($_SESSION['admin_session_secure']['login_type']) . '.');
        Redirect(DIR_WS_SITE_ADMIN . 'control.php');
    }
}

if (!$admin_user->is_logged_in()):
    Redirect(DIR_WS_SITE_ADMIN . 'index.php?' . $_SERVER['QUERY_STRING']);
endif;
if ($admin_user->get_permission() != '*'):
    $PageAccess = split("@@", $admin_user->get_permission());
else:
    $PageAccess = '*';
endif;
$Page = isset($_GET['Page']) ? $_GET['Page'] : "home";
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';

/* Udpate admin log  - only update operations are logged */
if (count($_POST) && $action == 'update') {

    $adlog = new adminlog();
    $adlog->setMsg('Action ' . $action . ' performed on ' . $id . ' in ' . $Page . ' module');
}
if ($Page != '' && file_exists(DIR_FS_SITE_ADMIN . '/script/' . $Page . '.php')):
    $script_page = true;
    if ($PageAccess == '*' || in_array($Page, $PageAccess)):
        include(DIR_FS_SITE_ADMIN . '/script/' . $Page . '.php');
    endif;
else:
    $script_page = false;
endif;

include_once(DIR_FS_SITE_ADMIN . '/include/common.php');
include_once(DIR_FS_SITE_ADMIN . '/include/header.php');
include_once(DIR_FS_SITE_ADMIN . '/include/navigation.php');
//require_once(DIR_FS_SITE_ADMIN.'/include/'."left.php");

if ($Page != "") {
    if (file_exists(DIR_FS_SITE_ADMIN . '/form/' . $Page . ".php")):
        if ($PageAccess == '*' || in_array($Page, $PageAccess)):
            require_once(DIR_FS_SITE_ADMIN . '/form/' . $Page . ".php");
        else:
            echo"<br><br><font size='3'><b>Welcome " . $admin_user->get_username() . "!</b></font><br><br>";
            echo "You do not have the permission to access this page.";
        endif;
    else:
        if ($PageAccess == '*' || in_array($Page, $PageAccess)):
            if ($script_page):
                require_once(DIR_FS_SITE_ADMIN . '/form/default.php');
            else:
                echo "<font size='3'><br><br><b>Invalid Page request....</b></font>";
            endif;
        else:
            echo "<font size='3'><br><br><b>Page is under construction....</b></font>";
        endif;
    endif;
}


require_once(DIR_FS_SITE_ADMIN . '/include/' . "footer.php");
?>