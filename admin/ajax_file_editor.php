<?php
include('../include/config/config.php');
isset($_POST['file_path']) ? $file_path = ($_POST['file_path']) : $file_path = '';
isset($_POST['file_name']) ? $file_name = ($_POST['file_name']) : $file_name = '';
isset($_POST['module']) ? $module = ($_POST['module']) : $module = '';
isset($_POST['file_text']) ? $file_text = $_POST['file_text'] : $file_text = '';
isset($_POST['create_file_name']) ? $create_file_name = ($_POST['create_file_name']) : $create_file_name = '';
isset($_POST['base_dir_path']) ? $base_dir_path = ($_POST['base_dir_path']) : $base_dir_path = '';
isset($_POST['rename']) ? $rename = ($_POST['rename']) : $rename = '';
isset($_POST['file_id']) ? $file_id = ($_POST['file_id']) : $file_id = '';

$file_full_path = $file_path.'/'.$file_name;

if(isset($_POST['file_text'])):

        if($file_path!='' && $file_name!=''):
           if(file_exists($file_full_path)):
               if(is_writable($file_full_path)):
                   if(file::fileWrite($file_full_path,$file_text)):
                       echo 'success';exit;
                   else:
                       echo 'fail1';exit;
                   endif;
               else:
                   echo 'fail3';exit;
               endif;
           endif;
        endif;
        echo 'fail2';exit;
        
elseif(isset($_POST['create_file_name'])):   
        if($base_dir_path!='' && $base_dir_path!=''):
           if(file_exists($base_dir_path)):
                if(is_writable($base_dir_path)) :
                   if($create_file_name!=''):
                       if(!file_exists($base_dir_path.'/'.$create_file_name)):
                           if(file::createFile($base_dir_path,$create_file_name)):
                               echo 'success';exit;
                           else:
                               echo 'fail1';exit;
                           endif;
                       else:
                           echo 'fail5';exit;
                       endif;
                    else:
                        echo 'fail3';exit;
                    endif;    
                else:
                    echo 'fail4';exit;
                endif;    
           endif;
        endif;
        echo 'fail2';exit;
        
elseif(isset($_POST['rename'])): 
    
        if($file_path!='' && $file_name!=''):
            if(file_exists($file_path.'/'.$file_name)):
                if($rename!=''):
                    if(!file_exists($file_path.'/'.$rename)):
                       if(rename($file_path.'/'.$file_name,$file_path.'/'.$rename)):
                           echo 'success';exit;
                       endif;
                    else:
                       echo 'fail4';exit;
                    endif;
                else:
                    echo 'fail2';exit;
                endif;
            endif;
        endif;
        echo 'fail3';exit;
        
else:?>
    <?php $extenstion =  file::getFileType($file_name);
          $extenstion = strtolower($extenstion);?>
    <?php if($extenstion=='jpeg' || $extenstion=='jpg' || $extenstion=='png' || $extenstion=='gif'):?>
            
            <h5><strong>Image Url : </strong><?php echo DIR_WS_SITE_GRAPHIC.$file_name;?></h5>
            <div class="clearfix"></div>
            <hr/>
            <div style="width:100%;text-align: center;">
                <img src="<?php echo $file_full_path;?>" style="max-width:100%;"/>
            </div>
            
    <?php elseif($extenstion=='css' || $extenstion=='js' || $extenstion=='tpl' || $extenstion=='php' 
                    || $extenstion=='txt' || $extenstion=='xml' || $extenstion=='json' || $extenstion=='html'):?>
            
            <?php
            $myfile = fopen($file_full_path, "r");
            if(!$myfile):
                echo "<h3>Unable to locate file!</h3>";exit;
            endif;
            $check_file_size = filesize($file_full_path);
            if($check_file_size):
                $file_text = fread($myfile,filesize($file_full_path));
            else:
                $file_text = '';
            endif;
            fclose($myfile);
            ?>
    
            <?php //$file_text = trim(file_get_contents($file_full_path)); ?>
            <div class="editor_outer">
                <?php echo '<pre id="code_editor" style="width:100%"><code>'.htmlspecialchars($file_text).'</code></pre>'; ?>
                <div class="clearfix margin-bottom-15"></div>
                <input type="hidden" id="module" name="module" value="<?php echo $module;?>"/>
                <input type="hidden" id="file_path" name="file_path" value="<?php echo $file_path;?>"/>
                <input type="hidden" id="file_name" name="file_name" value="<?php echo $file_name;?>"/>
                <script>
                    var editor = ace.edit("code_editor");
                    editor.setTheme("ace/theme/chrome");
                    editor.setFontSize("14px");
                    editor.setOptions({
                        maxLines: 71,
                        minLines: 20
                    });  
                    // using input event instead of change since it's called with some timeout
                    editor.on('input', function() {
                        if (editor.session.getUndoManager().isClean()){
                            $('#save_file').addClass("hide");
                            disableBeforeUnload();
                        } else{
                            $('#save_file').removeClass("hide");
                            enableBeforeUnload();
                        }
                    });
                </script>
            </div>
            <input class="btn green hide" type="submit" name="save_file" id="save_file" value="Save File"/>
    <?php else: ?>
            <div class="whiteBack">
                <span class="focus">File Preview</span><span class="focuslight"> not available.</span>
            </div>
    <?php endif;
endif;?>