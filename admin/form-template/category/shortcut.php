<?php if($action!='list' || $pid!='0'):?>
<a href="<?php echo make_admin_url('category','list','list','pid='.$parent_id);?>" class="btn default blue-stripe">
    <i class="icon-angle-left"></i>
    <span class="hidden-480">
             Back
    </span>
</a>
<?php endif;?>
<?php if($action!='insert'):?>
<a href="<?php echo make_admin_url('category','insert','insert');?>" class="btn default blue-stripe">
    <i class="icon-plus"></i>
    <span class="hidden-480">
             Add New Category
    </span>
</a>
<?php endif;?>
