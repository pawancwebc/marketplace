<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-sitemap"></i> Manage Service Categories</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>      
                        
                         <li class="last">
                                List Categories
                                <i class="icon-angle-right"></i>
                        </li>
                        
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Categories</div>
                <div class="actions">
                   <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('category', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >	
                      <table class="table table-striped table-bordered table-hover" id="datatable_without_sorting">
                            <thead>
                                 <tr>
                                    <th class="hidden-480">Sr. No.</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th class="hidden-480">Position</th>
                                    <th class='text-right'>Action</th>
                                </tr>
                            </thead>
                            <?php if(!empty($categories)): $sr='1';?>
                                <tbody>
                                    <?php foreach($categories as $kk=>$object):?>
                                    <tr class="odd gradeX">
                                            <td class="hidden-480">
                                               <?=$sr?>.
                                            </td>
                                            <td class="clickable"><?php echo $object->name?></td>
                                           
                                            <td class="center hidden-480">
                                                <div class="btn-group mini_buttons on_off_button" id='on_off_button<?php echo $object->id?>'>
                                                    <div module="category" class="btn status_on mini_buttons <?php echo ($object->is_active=='1')?'active':'';?>" on="<?php echo $object->id;?>" rel="<?php echo ($object->is_active=='1')?'on':'off';?>" >ON</div>
                                                    <div module="category" class="btn status_off mini_buttons button_right <?php echo ($object->is_active=='0')?'active':'';?>" off="<?php echo $object->id;?>" rel="<?php echo ($object->is_active=='1')?'off':'on';?>">OFF</div>
                                                </div>
                                            </td>
											<td class="clickable"><?php echo $object->position?></td>
                                            <td class='text-right'>
                                                <a id="do_action" href="<?php echo make_admin_url('category', 'update', 'update', 'id='.$object->id)?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i> &nbsp;</a>
                                                <a href="<?php echo make_admin_url('category', 'delete', 'list', 'id='.$object->id)?>" onclick="return confirm('Are you sure? You are deleting this record.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i> &nbsp;</a>
                                            </td>
                                    </tr>
                                    <?php	$sr++; 
										endforeach;?>
                                </tbody>
                               
                           <?php endif;?>  
                        </table>
                </form>    
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>
 <script src="assets/scripts/category_listing.js" type="text/javascript"></script>