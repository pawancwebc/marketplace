<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-sitemap"></i> Manage Categories</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                            List Trashed Categories
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Trashed Categories</div>
                <div class="actions">
                   <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('category', 'update_multiple_thrash', 'thrash');?>" method="post" id="form_data" name="form_data" >	
                      <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                 <tr>
                                    <th style="width:1%;" class="hidden-480"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th>
                                    <th>Sr. No</th>
                                    <th>Name</th>
                                    <th class="hidden-480">Position</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <?php if($QueryObj->GetNumRows()!=0):?>
                                <tbody>
                                <?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):?>
                                    <tr class="odd gradeX">
                                        <td class="hidden-480">
                                            <input class="checkboxes" id="multiopt[<?php echo $object->id ?>]" name="multiopt[<?php echo $object->id ?>]" type="checkbox" />
                                        </td>
                                        <td><?php echo $sr?>.</td>
                                        <td><?php echo $object->name?></td>
                                        <td class="hidden-480"><?php echo $object->position?></td>
                                        <td>
                                            <a href="<?php echo make_admin_url('category', 'restore', 'thrash', 'id='.$object->id)?>"  title="click here to restore this record" class="btn btn-xs default"><i class="icon-undo"></i> Restore</a>
                                            <a href="<?php echo make_admin_url('category', 'permanent_delete', 'thrash', 'id='.$object->id)?>" onclick="return confirm('Are you sure? You are deleting this record.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i> Delete</a>
                                        </td>
                                    </tr>
                                <?php $sr++; endwhile;?>
                                </tbody>
                                <tfoot>
                                    <tr class="odd gradeX hidden-480">
                                        <td colspan="3">
                                            <div style=" width:220px;float:left">
                                                <select name="multiopt_action" style="width:150px;float: left;" class="left_align form-control">
                                                    <option value="restore">Restore</option>
                                                    <option value="delete">Permanent Delete</option>
                                                </select>
                                                <input style="float:right" type="submit" class="btn green large" name="multiopt_go" value="Go"  onclick="return confirm('Are you sure?');" />
                                            </div>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr> 
                                </tfoot>
                           <?php endif;?>  
                        </table>
                </form>    
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>