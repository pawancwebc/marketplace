<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-sitemap"></i> Manage Categories</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a>  
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-sitemap"></i>              
                <a href="<?php echo make_admin_url('category', 'list', 'list'); ?>">List Categories</a> 
                <i class="icon-angle-right"></i>                         
            </li>
            <li class="last">
                New Category
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form class="form-horizontal" action="<?php echo make_admin_url('category', 'insert', 'insert') ?>" method="POST"  enctype="multipart/form-data" id="validation">
        <!-- / Box -->
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Add New Category</div>
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">
                    <div class="row-fluid">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="name">Name<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" name="name"  value="" id="name" class="form-control m-wrap validate[required]" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="description">Description</label>
                                <div class="col-md-8">
                                    <textarea name="description" rows='5' id="description" value="" class="form-control m-wrap"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="image">Upload Image</label>
                                <div class="col-md-8">
                                    <input type="file" name="image" multiple class="form-control m-wrap" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="is_active">Status</label>
                                <div class="col-md-6">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="is_active" id="is_active" value="1" checked/> Publish </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_active" id="is_active" value="0"/> Pending Review </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">

                                <label class="col-md-2 control-label border-left-2px" for="position">Position</label>
                                <div class="col-md-2">
                                    <input type="number" name="position"  value="" id="position" class="form-control" />
                                </div>
                            </div>


                        </div>

                    </div>
                    <div class="form-actions fluid">
                        <div class="offset2">
                            <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                            <button class="btn blue" type="submit" name="submit" value="SubmitClose"><i class="icon-ok"></i> Save and Close</button> 
                            <a href="<?php echo make_admin_url('category', 'list', 'list'); ?>" class="btn" name="cancel"> Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>