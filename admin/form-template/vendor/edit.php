<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-group"></i> Manage Vendors</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a>  
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-group"></i>
                <a href="<?php echo make_admin_url('vendor', 'list', 'list'); ?>">List Vendors</a> 
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                Edit Vendor
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form class="form-horizontal" action="<?php echo make_admin_url('vendor', 'update', 'update', 'id=' . $id) ?>" method="POST" id="validation">
        <!-- / Box -->
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Edit Vendor</div>
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">   
                    <div class="form-group">
                        <label class="span2 control-label" for="username">Email<span class="required">*</span></label>
                        <div class="span8">
                            <input type="text" readonly value="<?php echo $values->username; ?>" id="username" class="span6 form-control validate[required,custom[email]]" />
                        </div>
                    </div>	
                    <div class="form-group">
                        <label class="span2 control-label" for="firstname">First Name<span class="required">*</span></label>
                        <div class="span8">
                            <input type="text" name="firstname"  value="<?php echo $values->firstname; ?>" id="firstname" class="span6 form-control m-wrap validate[required]" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="span2 control-label" for="lastname">Last Name<span class="required">*</span></label>
                        <div class="span8">
                            <input type="text" name="lastname"  value="<?php echo $values->lastname; ?>" id="lastname" class="span6 form-control validate[required]" />
                        </div>
                    </div>	
                    <div class="form-group">
                        <label class="span2 control-label" for="phone">Phone<span class="required">*</span></label>
                        <div class="span8">
                            <input type="text" name="phone" id="phone"  value="<?php echo $values->phone; ?>" class="span6 form-control validate[required]" />
                        </div>
                    </div>	
                    <div class="form-group">
                        <label class="span2 control-label" for="firm_name">Firm Name</label>
                        <div class="span8">
                            <input type="text" name="firm_name" id="firm_name"  value="<?php echo $values->firm_name; ?>" class="span6 form-control" />
                        </div>
                    </div>	

                    <div class="form-group">
                        <label class="span2 control-label" for="address">Address 1<span class="required">*</span></label>
                        <div class="span8">
                            <textarea name="address" rows='3' id="address"  class="span6 form-control validate[required]"><?php echo $values->address; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="span2 control-label" for="address1">Address 2</label>
                        <div class="span8">
                            <textarea name="address1" rows='3' id="address1" class="span6 form-control"><?php echo $values->address1; ?></textarea>
                        </div>
                    </div>		

                    <div class="form-group">
                        <label class="span2 control-label" for="country_id">Country</label>
                        <div class="span4">
                            <select name="country_id" id="country_id" class="form-control">
                                <option value="">Select Country</option>
                                <?php foreach (getCountries(true) as $kk => $vv): ?>
                                    <option value="<?php echo $vv['id']; ?>" <?php echo ($vv['id'] == $values->country_id) ? "selected" : ""; ?>><?php echo $vv['short_name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span id="country_error" style="display:none;color:red"><i>Please select the country first.</i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="span2 control-label" for="state_id">State</label>
                        <div class="span4">
                            <select name="state_id" id="state_id" class="form-control">
                                <option value="">Select State</option>
                                <?php if (!empty($states)): ?>
                                    <?php foreach ($states as $kk => $vv): ?>
                                        <option value="<?php echo $vv['id']; ?>" <?php echo ($vv['id'] == $values->state_id) ? "selected" : ""; ?>><?php echo $vv['name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                            <span id="state_error" style="display:none;color:red"><i>Please select the state.</i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="span2 control-label" for="city_id">City</label>
                        <div class="span4">
                            <select name="city_id" id="city_id" class="form-control">
                                <option value="">Select City</option>
                                <?php if (!empty($cities)): ?>
                                    <?php foreach ($cities as $kk => $vv): ?>
                                        <option value="<?php echo $vv['id']; ?>" <?php echo ($vv['id'] == $values->city_id) ? "selected" : ""; ?>><?php echo $vv['name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                            <span id="state_error" style="display:none;color:red"><i>Please select the city.</i></span>
                        </div>
                    </div>
                    <input type='hidden' id="is_city" value="1"/>

                    <div class="form-group">
                        <label class="span2 control-label" for="zip_code">Post Code</label>
                        <div class="span8">
                            <input type="text" name="zip_code"  value="<?php echo $values->zip_code; ?>" id="zip_code" class="span6 form-control m-wrap" />
                        </div>
                    </div>								

                    <div class="form-group">
                        <label class="span2 control-label" for="is_active">Active</label>
                        <div class="span1">
                            <input type="checkbox" name="is_active" <?php echo ($values->is_active) ? "checked" : ""; ?> id="is_active" class="form-control" />
                        </div>

                    </div>
                    <div class="form-actions fluid">
                        <div class="offset2">
                            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('vendor', 'list', 'list'); ?>" class="btn" name="cancel"> Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<script src="assets/scripts/state_country.js" type="text/javascript"></script>