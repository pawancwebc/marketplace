<?php if($action!='list'):?>
<a href="<?php echo make_admin_url('content','list','list');?>" class="btn default blue-stripe">
    <i class="icon-angle-left"></i>
    <span class="hidden-480">
             Back
    </span>
</a>
<?php endif;?>
<?php if($action!='insert'):?>
<a href="<?php echo make_admin_url('content','insert','insert');?>" class="btn default blue-stripe">
    <i class="icon-plus"></i>
    <span class="hidden-480">
             Add New Page
    </span>
</a>
<?php endif;?>
<?php if($action!='thrash'):?>
<a href="<?php echo make_admin_url('content','thrash','thrash');?>" class="btn default blue-stripe">
    <i class="icon-trash"></i>
    <span class="hidden-480">
             Trash
    </span>
</a>
<?php endif;?>