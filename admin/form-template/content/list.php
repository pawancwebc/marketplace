<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-text-width"></i> Manage Content Pages</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                            List Content Pages
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Pages</div>
                <div class="actions">
                   <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('content', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >	
                      <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                 <tr>
                                    <th style="width:10%;" class="hidden-480">Sr. No.</th>
                                    <th>Page Name</th>
                                    <!---<th class="hidden-480 sorting_disabled">Urlname</th>--->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <?php if($QueryObj->GetNumRows()!=0):?>
                                <tbody>
                                <?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):?>
                                    <tr class="odd gradeX">
                                            <td class="hidden-480">
												<?=$sr?>.
                                            </td>
                                            <td class="clickable"><?php echo $object->name?></td>
                                            <!---<td class="hidden-480 clickable"><?php //echo $object->urlname?></td>--->
                                            <td>
                                                <a id="do_action" href="<?php echo make_admin_url('content', 'update', 'update', 'id='.$object->id)?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i></a>
                                               
                                                    <a href="<?php echo make_admin_url('content', 'delete', 'list', 'id='.$object->id)?>" onclick="return confirm('Are you sure? You are deleting this page.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i></a>
                                            
                                            </td>
                                    </tr>
                                <?php $sr++; endwhile;?>
                                </tbody>
                                
                           <?php endif;?>  
                        </table>
                </form>    
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>