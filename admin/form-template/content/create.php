<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title"><i class="icon-text-width"></i> Manage Content Pages</h3>
            <ul class="page-breadcrumb breadcrumb">
                    <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                            <i class="icon-angle-right"></i>
                    </li>                                  
                    <li>
                        <i class="icon-text-width"></i>
                               <a href="<?php echo make_admin_url('content', 'list', 'list');?>">List Pages</a>
                         <i class="icon-angle-right"></i>                                       
                    </li>
                    <li class="last">
                        New Page
                    </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
       <form class="form-horizontal" action="<?php echo make_admin_url('content', 'insert', 'insert')?>" method="POST" enctype="multipart/form-data" id="validation">
              <!-- / Box -->
              <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                 <div class="portlet">
                        <div class="portlet-title">
                                <div class="caption">Edit Page</div>
                                <div class="actions">
                                    <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                                </div>
                        </div>
                        <div class="portlet-body form form-body">      
                                <div class="form-group">
                                        <label class="col-md-2 control-label" for="name">Name<span class="required">*</span></label>
                                        <div class="col-md-8">
                                           <input type="text" name="name"  value="" id="name" class="form-control m-wrap validate[required]" />
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="col-md-2 control-label" for="name">Urlname</label>
                                        <div class="col-md-8">
                                           <input type="text" name="urlname"  value="" id="name" class="form-control" />
                                           <div class="clearfix"></div>
                                           <span class="help-block">leave empty for automatic create urlname<br/>must be unique if entered</span>
                                        </div>
                                </div>											
                                <div class="form-group">
                                        <label class="col-md-2 control-label" for="description">Page Content</label>
                                        <div class="col-md-8">
                                            <textarea id="page" class="form-control ckeditor m-wrap" name="page" rows="6"></textarea>
                                    </div>
                                 </div> 
                                <h4 class="form-section hedding_inner">SEO Information</h4>
                                
                                <div class="clearfix"></div>
                                <hr/>
                                <div class="form-group">
                                       <label class="col-md-2 control-label" for="page_name">Meta Title</label>
                                       <div class="col-md-8">
                                           <input type="text" name="page_name" id="page_name" rel="title" class="form-control meta_info" value=""> 
                                           <div class="clearfix"></div>
                                           <span class="help-block characterLeftTitle" id="characterLeft"></span>
                                        </div>  
                                </div>  
                               <div class="form-group">
                                        <label class="col-md-2 control-label" for="meta_keyword">Meta Keywords</label>
                                        <div class="col-md-8">
                                            <input type="text" name="meta_keyword" id="meta_keyword" class="form-control m-wrap" value="">
                                        </div>
                                </div>          
                               <div class="form-group">
                                        <label class="col-md-2 control-label" for="meta_description">Meta Description</label>
                                        <div class="col-md-8">
                                            <textarea rows="3" class="form-control meta_info" rel="desc" id="meta_description" name="meta_description"></textarea>
                                            <div class="clearfix"></div>
                                            <span class="help-block characterLeftDesc" id="characterLeft"></span>
                                        </div>
                               </div>       
      
                                <div class="form-actions fluid">
                                    <div class="offset2">
                                         <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                         <a href="<?php echo make_admin_url('content', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                    </div>
                                </div>
                  </div>
                </div>
            </div>
         </form>
         <div class="clearfix"></div>
 </div>
<div class="clearfix"></div>