<div class="row-fluid">
    <div class="span12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-pushpin"></i>Store Overview
                </div>
                <div class="actions">
                <a href="<?php echo make_admin_url('stats','list','list','print=1');?>" class="btn default btn-sm">
                        <i class="icon-print"></i> Print Store Overview
                </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Active Users</th>
                                <th>Total Orders</th>
                                <th>Total Sales</th>
                                <th>Active Products</th>
                                <th>Active Brands</th>
                                <th>Active Suppliers</th>
                                <th>Active Manufacturers</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd gradeX">
                                <td><?php echo $totalusers;?></td>
                                <td><?php echo $totalorders;?></td>
                                <td><?php echo show_price($totalsales);?></td>
                                <td><?php echo $totalproducts;?></td>
                                <td><?php echo $totalbrands;?></td>
                                <td><?php echo $totalsuppliers;?></td>
                                <td><?php echo $totalmanufacturers;?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <!-- Begin: life time stats -->
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-pushpin"></i>Popular Products
                </div>
                <div class="actions">
                <a href="<?php echo make_admin_url('report','top_products','top_products');?>" class="btn default btn-sm">
                        <i class="icon-list"></i> View All
                </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Product Name
                                </th>
                                <th>
                                    SKU #
                                </th>
                                <th>
                                    Quantity
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($records)):?>
                                <?php $sr=1;foreach($records as $kk=>$vv):?>
                                    <tr class="odd gradeX">
                                            <td class="hidden-480"><?php echo $vv['product_id'];?></td>
                                            <td>
                                                <a href="<?php echo make_admin_url('product','update','update','id='.$vv['product_id']);?>">
                                                    <?php 
                                                        echo $vv['product_name'];
                                                        if($vv['variant_name']!='')
                                                            echo " (".$vv['variant_name'].")";
                                                    ?>
                                                </a>
                                            </td>
                                            <td class="hidden-480"><?php echo $vv['product_sku'];?></td>
                                            <td><?php echo $vv['total_sale'];?></td>
                                    </tr>
                                <?php $sr++; endforeach;?>
                           <?php endif;?>  
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="span6">
        <!-- Begin: life time stats -->
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bolt"></i>Low Stock Products
                </div>
                <div class="actions">
                <a href="<?php echo make_admin_url('low_stock','list','list');?>" class="btn default btn-sm">
                        <i class="icon-list"></i> View All
                </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Product Name
                                </th>
                                <th>
                                    SKU #
                                </th>
                                <th>
                                    Remaining
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($low_stock_products)):?>
                                <?php $sr=1;foreach($low_stock_products as $kk=>$vv):?>
                                    <tr class="odd gradeX">
                                            <td class="hidden-480"><?php echo $vv['id'];?></td>
                                            <td>
                                                <a href="<?php echo make_admin_url('product','update','update','id='.$vv['id']);?>">
                                                    <?php 
                                                        echo $vv['name'];
                                                        if(isset($vv['variant_name']) && $vv['variant_name']!='')
                                                            echo " (".$vv['variant_name'].")";
                                                    ?>
                                                </a>
                                            </td>
                                            <td class="hidden-480"><?php echo $vv['sku'];?></td>
                                            <td><?php echo ($vv['variant_id'])?$vv['variant_quantity']:$vv['quantity'];?></td>
                                    </tr>
                                <?php $sr++; endforeach;?>
                           <?php endif;?>  
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <!-- Begin: life time stats -->
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-user"></i>Top Customers
                </div>
                <div class="actions">
                <a href="<?php echo make_admin_url('user','list','list');?>" class="btn default btn-sm">
                        <i class="icon-list"></i> View All Customers
                </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    User
                                </th>
                                <th>
                                    Orders
                                </th>
                                <th>
                                    Sales
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($top_users)):?>
                                <?php $sr=1;foreach($top_users as $kk=>$vv):?>
                                    <tr class="odd gradeX">
                                            <td><?php echo $vv->user_id;?></td>
                                            <td>
                                                <a href="<?php echo make_admin_url('user','update','update','id='.$vv->user_id);?>">
                                                    <?php 
                                                        echo $vv->firstname." ".$vv->lastname;
                                                    ?>
                                                </a>
                                            </td>
                                            <td><?php echo $vv->orders;?></td>
                                            <td><?php echo show_price($vv->sale);?></td>
                                    </tr>
                                <?php $sr++; endforeach;?>
                           <?php endif;?>  
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="span6">
        <!-- Begin: life time stats -->
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-user"></i> New Customers
                </div>
                <div class="actions">
                <a href="<?php echo make_admin_url('user','list','list');?>" class="btn default btn-sm">
                        <i class="icon-list"></i> View All
                </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th class="hidden-480">Gender</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($users)):?>
                                <?php $sr=1;foreach($users as $kk=>$vv):?>
                                    <tr class="odd gradeX">
                                            <td><?php echo $vv['firstname'];?></td>
                                            <td><?php echo $vv['lastname'];?></td>
                                            <td><?php echo $vv['username'];?></td>
                                            <td class="hidden-480"><?php echo ucfirst($vv['gender']);?></td>
                                            <td>
                                                <a class="btn btn-xs default" href="<?php echo make_admin_url('user','update','update','id='.$vv['id']);?>">
                                                    <i class="icon-pencil"></i> Edit
                                                </a>
                                            </td>
                                    </tr>
                                <?php $sr++; endforeach;?>
                           <?php endif;?>  
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <!-- Begin: life time stats -->
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-shopping-cart"></i> New Orders
                </div>
                <div class="actions">
                    <a href="<?php echo make_admin_url('order','list','list');?>" class="btn default btn-sm">
                        <i class="icon-list"></i> View All
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th class="hidden-480"> ID </th>
                                <th> Customer </th>
                                <th class="hidden-480"> Quantity </th>
                                <th> Price </th>
                                <th> Status </th>
                                <th> View </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($orders)):?>
                                <?php $sr=1;foreach($orders as $kk=>$vv):?>
                                    <tr class="odd gradeX">
                                            <td class="hidden-480"><?php echo $vv['order_id'];?></td>
                                            <td><?php echo $vv['billing_firstname']." ".$vv['billing_lastname'];?></td>
                                            <td class="hidden-480"><?php echo $vv['totalquantity'];?></td>
                                            <td><?php echo show_price($vv['grand_total']);?></td>
                                            <td><?php echo $vv['current_state'];?></td>
                                            <td>
                                                <a class="btn btn-xs default" href="<?php echo make_admin_url('order','update','update','id='.$vv['id']);?>">
                                                    <i class="icon-pencil"></i> Edit
                                                </a>
                                            </td>
                                    </tr>
                                <?php $sr++; endforeach;?>
                           <?php endif;?>  
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>