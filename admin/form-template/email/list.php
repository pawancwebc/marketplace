<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-envelope"></i> Manage Email Templates</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                            List Email Templates
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Email Templates</div>
                <div class='actions'>
                    <a class='btn blue-stripe' href='<?php echo make_admin_url('email','update','update','id='.COMMON_HEADER);?>'>
                        Header Template
                    </a>
                    <a class='btn blue-stripe' href='<?php echo make_admin_url('email','update','update','id='.COMMON_FOOTER);?>'>
                        Footer Template
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <form id="form_data" name="form_data" >	
                      <table class="table table-striped table-bordered table-hover" id="sample">
                            <thead>
                                 <tr>
                                    <th>Email Name</th>
                                    <th>Email Subject</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <?php if($QueryObj->GetNumRows()!=0):?>
                                <tbody>
                                <?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):?>
                                    <tr class="odd gradeX">
                                            <td class="clickable"><?php echo $object->email_name?></td>
                                            <td class="clickable"><?php echo $object->email_subject?></td>
                                            <td>
                                                <a id="do_action" href="<?php echo make_admin_url('email', 'update', 'update', 'id='.$object->id)?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i></a>
                                            </td>
                                    </tr>
                                <?php $sr++; endwhile;?>
                                </tbody>
                           <?php endif;?>  
                        </table>
                </form>    
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>