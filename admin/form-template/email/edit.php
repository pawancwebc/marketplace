<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title"><i class="icon-envelope"></i> Manage Email Templates</h3>
            <ul class="page-breadcrumb breadcrumb">
                    <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                            <i class="icon-angle-right"></i>
                    </li>                                  
                    <li>
                        <i class="icon-envelope"></i>
                               <a href="<?php echo make_admin_url('email', 'list', 'list');?>">List Email Templates</a>
                         <i class="icon-angle-right"></i>                                       
                    </li>
                    <li class="last">
                        Edit Email Template
                    </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
       <form class="form-horizontal" action="<?php echo make_admin_url('email', 'update', 'update','id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
              <!-- / Box -->
              <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                 <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">Edit Email Template (<?php echo $values->email_name?>)</div>
                                <div class="actions">
                                    <a href="<?php echo make_admin_url('email','list','list');?>" class="btn default blue-stripe">
                                        <i class="icon-angle-left"></i>
                                        <span class="hidden-480">
                                                 Back
                                        </span>
                                    </a>
                                </div>
                        </div>
                        <div class="portlet-body form form-body">    
                                <!--
                                <div class="form-group">
                                        <label class="span2 control-label" for="email_name">Email Name</label>
                                        <div class="span8">
                                           <input type="text" disabled value="<?php echo $values->email_name?>" class="form-control m-wrap" />
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="span2 control-label" for="email_fields">Available Constants</label>
                                        <div class="span8">
                                           <textarea rows="3" disabled class="form-control m-wrap"><?php echo $values->email_fields?></textarea>
                                        </div>
                                </div>-->
                                <div class="form-group">
                                        <label class="span2 control-label" for="from_name">From Name</label>
                                        <div class="span8">
                                           <input type="text" name="from_name"  value="<?php echo $values->from_name;?>" id="from_name" class="form-control m-wrap" />
                                           <div class="clearfix"></div>
                                           <span class="help-block">by default {SITE_NAME} <BR/> Site Name : <?php echo SITE_NAME;?></span>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="span2 control-label" for="email_subject">Email Subject<span class="required">*</span></label>
                                        <div class="span8">
                                           <input type="text" name="email_subject"  value="<?php echo $values->email_subject?>" id="email_subject" class="form-control m-wrap validate[required]" />
                                        </div>
                                </div>	
                               <div class="form-group">
                                        <label class="span2 control-label" for="email_text">Email Text</label>
                                        <div class="span8">
                                            <textarea id="email_text" class="form-control ckeditor m-wrap" name="email_text" rows="15">
                                            <?php echo html_entity_decode($values->email_text);?>
                                            </textarea>
                                            <div class="clearfix"></div>
                                            <span class="help-block">Available Values : <?php echo $values->email_fields?></span>
                                    </div>
                                 </div> 
                                <div class="form-actions fluid">
                                    <div class="offset2">
                                         <input type="hidden" name="id" value="<?php echo $values->id?>" tabindex="7" />
                                         <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                                         <button class="btn blue" type="submit" name="submit" value="SubmitClose"><i class="icon-ok"></i> Save and Close</button> 
                                         <a href="<?php echo make_admin_url('email', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                    </div>
                                </div>
                  </div>
                </div>
            </div>
         </form>
         <div class="clearfix"></div>
 </div>
<div class="clearfix"></div>