<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-shopping-cart"></i> Manage Orders</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                            List Archived Orders
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Archived Orders</div>
                <div class="actions">
                   <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                </div>
            </div>
             <div class="portlet-body">
                    <div class="table-container">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                            <tr role="row" class="heading">
                                    <th width="5%">
                                            #
                                    </th>
                                    <th width="5%">
                                             Order&nbsp;#
                                    </th>
                                    <th width="15%">
                                             Purchased&nbsp;On
                                    </th>
                                    <th width="15%">
                                             Customer
                                    </th>
                                    <th width="10%">
                                             Product&nbsp;Quantity
                                    </th>
                                    <th width="10%">
                                             Payment&nbsp;Method
                                    </th>
                                    <th width="10%">
                                             Order&nbsp;Price(<?php echo CURRENCY_SYMBOL;?>)
                                    </th>
                                    <th width="10%">
                                             Status
                                    </th>
                                    <th width="10%">
                                             Actions
                                    </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr role="row" class="filter">
                                    <td>
                                    </td>
                                    <td>
                                            <input type="text" class="form-control form-filter input-sm" name="order_id">
                                    </td>
                                    <td>
                                            <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                    <input type="text" class="form-control form-filter input-sm" readonly name="order_date_from" placeholder="From">
                                                    <span class="input-group-btn">
                                                            <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                                    </span>
                                            </div>
                                            <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                    <input type="text" class="form-control form-filter input-sm" readonly name="order_date_to" placeholder="To">
                                                    <span class="input-group-btn">
                                                            <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                                    </span>
                                            </div>
                                    </td>
                                    <td>
                                            <input type="text" class="form-control form-filter input-sm" name="order_customer_name">
                                    </td>
                                    <td>
                                            <input type="text" class="form-control form-filter input-sm" name="order_ship_to">
                                    </td>
                                    <td>
                                            <div class="margin-bottom-5">
                                                    <input type="text" class="form-control form-filter input-sm" name="order_base_price_from" placeholder="From"/>
                                            </div>
                                            <input type="text" class="form-control form-filter input-sm" name="order_base_price_to" placeholder="To"/>
                                    </td>
                                    <td>
                                            <div class="margin-bottom-5">
                                                    <input type="text" class="form-control form-filter input-sm margin-bottom-5 clearfix" name="order_purchase_price_from" placeholder="From"/>
                                            </div>
                                            <input type="text" class="form-control form-filter input-sm" name="order_purchase_price_to" placeholder="To"/>
                                    </td>
                                    <td>
                                            <select name="order_status" class="form-control form-filter input-sm">
                                                    <option value="">Select...</option>
                                                    <option value="pending">Pending</option>
                                                    <option value="closed">Closed</option>
                                                    <option value="hold">On Hold</option>
                                                    <option value="fraud">Fraud</option>
                                            </select>
                                    </td>
                                    <td>
                                            <div class="margin-bottom-5">
                                                    <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
                                            </div>
                                            <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
                                    </td>
                            </tr>
                             <?php if(!empty($orders)):?>
                             <?php $sr=1;foreach($orders as $kk=>$vv):?>
                                 <tr>
                                     <td><?php echo $sr;?></td>
                                     <td><?php echo $vv['order_id'];?></td>
                                     <td><?php echo date("d/m/Y",strtotime($vv['date_add']));?></td>
                                     <td><?php echo $vv['billing_firstname'];?></td>
                                     <td><?php echo $vv['totalquantity'];?></td>
                                     <td><?php echo $vv['grand_total'];?></td>
                                     <td><?php echo $vv['payment_method_name'];?></td>
                                     <td><span class="label label-sm label-<?php echo $status_array[$vv['current_state']];?>"><?php echo $vv['current_state'];?></span></td>
                                     <td><a href="<?php echo make_admin_url('order','update','update','id='.$vv['id']);?>" class="btn btn-xs default btn-editable"><i class="fa fa-search"></i> View</a></td>
                                 </tr>
                            <?php $sr++; endforeach;?>
                             <?php endif;?>  
                            </tbody>
                            </table>
                    </div>
            </div>

            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>