<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title"><i class="icon-globe"></i> Manage Brands</h3>
            <ul class="page-breadcrumb breadcrumb">
                    <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a>  
                            <i class="icon-angle-right"></i>
                    </li>                                  
                    <li>
                        <i class="icon-globe"></i>
                               <a href="<?php echo make_admin_url('brand', 'list', 'list');?>">List Brands</a> 
                         <i class="icon-angle-right"></i>                                       
                    </li>
                    <li class="last">
                        New Brand
                    </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
       <form class="form-horizontal form-row-seperated" action="<?php echo make_admin_url('brand', 'insert', 'insert')?>" method="POST" enctype="multipart/form-data" id="validation">
              <!-- / Box -->
              <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                 <div class="portlet">
                        <div class="portlet-title">
                                <div class="caption">Add New Brand</div>
                                <div class="actions">
                                    <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                                </div>
                        </div>
                        <div class="portlet-body form form-body">      
                                <div class="form-group">
                                        <label class="span2 control-label" for="name">Name<span class="required">*</span></label>
                                        <div class="span8">
                                           <input type="text" name="name"  value="" id="name" class="form-control m-wrap validate[required]" />
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="span2 control-label" for="urlname">Urlname</label>
                                        <div class="span8">
                                           <input type="text" name="urlname"  value="" id="urlname" class="form-control" />
                                        </div>
                                </div>	
                                <div class="form-group">
                                        <label class="span2 control-label" for="website">Website</label>
                                        <div class="span8">
                                           <input type="text" name="website"  value="" id="website" class="form-control" />
                                        </div>
                                </div>	
                                <div class="form-group">
                                        <label class="span2 control-label" for="is_active">Active</label>
                                        <div class="span1">
                                           <input type="checkbox" name="is_active"  value="" id="is_active" class="form-control" />
                                        </div>
                                        <label class="span1 control-label" for="position">Position</label>
                                        <div class="span1">
                                           <input type="text" name="position"  value="" id="position" class="form-control" />
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class=" span2 control-label">Image Upload</label>
                                    <div class="span6">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="<?php echo DIR_WS_SITE_ADMIN.'assets/img/noimage.gif' ?>" alt="" />
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                    <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                    <span class="fileupload-exists">Change</span>
                                                    <input type="file" class="default" name="image"/></span>
                                                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label class="span2 control-label" for="description">Description</label>
                                        <div class="span8">
                                            <textarea id="description" class="form-control ckeditor" name="description" rows="6"></textarea>
                                    </div>
                                 </div> 
                                <h4 class="form-section hedding_inner">SEO Information</h4>
                                <div class="form-group">
                                       <label class="span2 control-label" for="meta_title">Meta Title</label>
                                       <div class="span8">
                                           <input type="text" name="meta_title" id="meta_title" class="form-control m-wrap" value=""> 
                                        </div>  
                                </div>  
                               <div class="form-group">
                                        <label class="span2 control-label" for="meta_keyword">Meta Keywords</label>
                                        <div class="span8">
                                            <input type="text" name="meta_keyword" id="meta_keyword" class="form-control m-wrap" value=""/>
                                        </div>
                                </div>          
                               <div class="form-group">
                                        <label class="span2 control-label" for="meta_desc">Meta Description</label>
                                        <div class="span8">
                                            <textarea rows="3" class="form-control m-wrap" style=" height: 82px;" id="meta_desc" name="meta_desc"></textarea>
                                        </div>
                               </div>                                  
                                <div class="form-actions fluid">
                                    <div class="offset2">
                                         <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                         <a href="<?php echo make_admin_url('brand', 'list', 'list');?>" class="btn" name="cancel"> Cancel</a>
                                    </div>
                                </div>
                  </div>
                </div>
            </div>
         </form>
         <div class="clearfix"></div>
 </div>
<div class="clearfix"></div>