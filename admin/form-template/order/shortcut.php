<?php if($action=='update' && $redirect=='user'):?>
<a href="<?php echo make_admin_url('user','order','order','id='.$order->user_id);?>" class="btn default blue-stripe">
    <i class="icon-angle-left"></i>
    <span class="hidden-480">
             Back
    </span>
</a>
<?php elseif($action!='list'):?>
<a href="<?php echo make_admin_url('order','list','list');?>" class="btn default blue-stripe">
    <i class="icon-angle-left"></i>
    <span class="hidden-480">
             Back
    </span>
</a>
<?php endif;?>
<?php if($action!='archive' && $action!='update'):?>
<a href="<?php echo make_admin_url('order','archive','archive');?>" class="btn default blue-stripe">
    <i class="icon-trash"></i>
    <span class="hidden-480">
             Archived
    </span>
</a>
<?php endif;?>