<?php include_once(DIR_FS_SITE . 'include/functionClass/countryClass.php');
      include_once(DIR_FS_SITE . 'include/functionClass/stateClass.php');
      include_once(DIR_FS_SITE . 'include/functionClass/cityClass.php');  
?>
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-shopping-cart"></i> Manage Orders</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-globe"></i>
                <a href="<?php echo make_admin_url('order', 'list', 'list'); ?>">List Orders</a>
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                View Order
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- Begin: life time stats -->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-shopping-cart"></i>Order #<?php echo $order->order_id; ?>
                    <span class="hidden-480"> | <?php echo $order->date_add; ?></span>
                </div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    <!--<div class="btn-group">
                        <a href="<?php echo make_admin_url('order','download_invoice','update','invoice='.$order->invoice_no);?>" class="btn default blue-stripe">
                            <i class="icon-print"></i>
                            <span class="hidden-480">
                                     Invoice
                            </span>
                        </a>
                    </div>-->
                    <!--<div class="btn-group">
                        <a href="<?php echo make_admin_url('order','packing_slip','update','id='.$id);?>" class="btn default blue-stripe">
                            <i class="icon-print"></i>
                            <span class="hidden-480">
                                     Packing Slip
                            </span>
                        </a>
                    </div>-->
                    <?php if($order->shipping_date!='0000-00-00 00:00:00'):?>
                     <div class="btn-group">
                        <a href="<?php echo make_admin_url('order','shipping_label','update','id='.$id);?>" class="btn default blue-stripe">
                            <i class="icon-print"></i>
                            <span class="hidden-480">
                                     Shipping Label
                            </span>
                        </a>
                    </div>   
                    <?php endif;?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-lg">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab">
                                Details
                            </a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab">
                                Order Details
                            </a>
                        </li>
                        <!--<li>
                            <a href="#tab_4" data-toggle="tab">
                                Shipments / Payment
                            </a>
                        </li>-->
                        <!--<li>
                            <a href="#tab_5" data-toggle="tab">History</a>
                        </li>-->
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <!------ new questions box -------->
                            <div class="well">
                                <form method="post" id="validate1" action="<?php echo make_admin_url('order','update','update','id='.$id);?>" name="change_current_state">
                                <div class="span2">
                                <select class="form-control validate[required]" name="state" id="state">
                                    <?php foreach (order_status::getOrderStatus() as $kk=>$vv): ?>
                                            <option value="<?php echo $vv->name;?>" <?php echo ($vv->name==$order->current_state)?"selected":"";?>><?php echo $vv->name;?></option>
                                    <?php endforeach;?>
                                </select>
                                </div>
                                <div class="span4">
                                    <textarea class="form-control" name="comment" placeholder="Enter Comment"></textarea>
                                </div>
                                <div class="span4">
                                <button type="submit" name="change_state_btn" class="btn btn-sm green"><i class="icon-check"></i> Submit</button>
                                </div>
                                </form>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix margin-bottom-20"></div>
                            <!-------------- list of questions ---------------------->
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="portlet grey box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-cogs"></i>Order Status History
                                            </div>
                                            <div class="actions">

                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                           <table class="table table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Status</th>
                                                    <!--<th>SMS Sent</th>
                                                    <th>Email Sent</th>-->
                                                    <th>Comment</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if(!empty($statatus)):?>
                                                    <?php foreach($statatus as $kk=>$vv): ?>
                                                        <tr>
                                                            <td><?php echo $vv->date_add;?></td>
                                                            <td><?php echo $vv->status;?></td>
                                                            <!--<td><?php echo ($vv->is_sms_sent)?"Yes":"No";?></td>
                                                            <td><?php echo ($vv->is_email_sent)?"Yes":"No";?></td>-->
                                                            <td><?php echo $vv->comment;?></td>
                                                        </tr>
                                                    <?php endforeach;?>
                                                <?php else:?>
                                                        <tr>
                                                            <td colspan='5'>No Status found.</td>
                                                        </tr>
                                                <?php endif;?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6">
                                    <div class="portlet grey box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-cogs"></i>Order Details
                                            </div>
                                            <div class="actions">

                                            </div>
                                        </div>
                                        
                                        <div class="portlet-body">
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Order #:</div>
                                                <div class="span7 value">
                                                    #<?php echo $order->order_id;?>
                                                </div>
                                            </div>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Order Date & Time:</div>
                                                <div class="span7 value"><?php echo $order->date_add;?></div>
                                            </div>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Order Status:</div>
                                                <div class="span7 value"><?php echo $order->current_state;?></div>
                                            </div>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Grand Total</div>
                                                <div class="span7 value"><?php show_price($order->grand_total);?></div>
                                            </div>
                                            <?php if($order->transaction_id!=0)
                                                {?>
                                                <div class="row-fluid static-info">
                                                    <div class="span5 name">Transaction Id:</div>
                                                    <div class="span7 value"><?php echo $order->transaction_id;?></div>
                                                </div>
                                                <?php
                                            } ?>

                                            <?php if($order->paypal_business_email!=0 || $order->paypal_business_email!="")
                                                {?>
                                                <div class="row-fluid static-info">
                                                    <div class="span5 name">Paypal admin account:</div>
                                                    <div class="span7 value"><?php echo $order->paypal_business_email;?></div>
                                                </div>
                                                <?php
                                            } ?>

                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Payment Method:</div>
                                                <?php if($order->current_state=='Payment Complete'){?>
                                                <div class="span7 value"><?php echo $order->payment_method_name;?></div>
                                                <?php } ?>
                                            </div>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Payment Status:</div>
                                                <div class="span7 value"><?php echo $order->current_state;?></div>
                                            </div>
                                            <?php if($order->i_will_pay==1){ ?>
                                            <div class="row-fluid static-info">
                                                <div class="span12 name"><h4>Payment To be paid by Practitioner</h4> </div>
                                                
                                            </div>
                                            <?php } ?>
                                            <?php if($order->patient_will_pay==1 || $order->patient_will_pay_vouch==1){ ?>
                                            <div class="row-fluid static-info">
                                                <div class="span12 name"><h4>Payment To be paid by Practitioner</h4> </div>
                                                
                                            </div>
                                            <?php } ?>
                                            <?php if($order->current_state=='Payment Complete'){?>
                                            <div class="row-fluid static-info">
                                                <div class="span12 name"><h4>Payment Paid By</h4> </div>
                                                
                                            </div>
                                            <?php if($order->patient_will_pay==1 || $order->patient_will_pay_vouch==1){ ?>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Patient Name: </div>
                                                <div class="span7 value"><?php echo $order->billing_firstname;?> <?php echo $order->billing_lastname;?></div>
                                            </div>
                                            <?php } ?>
                                            <?php if($order->i_will_pay==1){ 
                                            $QueryObj_user = new user();
                                            $order_user=$QueryObj_user->getUser($order->user_id);
                                            
                                            ?>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Practitioner Name: </div>
                                                <div class="span7 value"><?php echo $order_user->firstname;?> <?php echo $order_user->lastname;?></div>
                                            </div>
                                            <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="row-fluid">
                                <div class="span4">
                                    <div class="portlet grey box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-cogs"></i>Customer Information
                                            </div>
                                            <div class="actions">
                                                
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <?php if(isset($order->user) && !empty($order->user)): ?>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Customer Name:</div>
                                                <div class="span7 value"><?php echo $order->user->firstname." ".$order->user->lastname;?></div>
                                            </div>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Gender/Age:</div>
                                                <div class="span7 value"><?php echo $order->user->gender." / ".$order->user->age;?></div>
                                            </div>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Email:</div>
                                                <div class="span7 value"><span title="<?php echo $order->user->username;?>" class="tooltips"><?php echo substr($order->user->username,0,15);?>...</span></div>
                                            </div>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Phone Number:</div>
                                                <div class="span7 value"><?php echo $order->user->phone;?></div>
                                            </div>
                                            <?php else: ?>
                                            <h4>Guest User</h4>
                                            <hr/>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Customer Name: </div>
                                                <div class="span7 value"><?php echo $order->billing_firstname." ".$order->billing_lastname;?></div>
                                            </div>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Email: </div>
                                                <div class="span7 value"><span title="<?php echo $order->billing_email;?>" class="tooltips"><?php echo substr($order->billing_email,0,15);?>...</span></div>
                                            </div>
                                            <div class="row-fluid static-info">
                                                <div class="span5 name">Phone Number: </div>
                                                <div class="span7 value"><?php echo $order->billing_phone;?></div>
                                            </div>
                                            
                                            <?php endif;?>
                                            
                                        </div>
                                    </div>
                                </div>
                                 <?php
                                 if($order->billing_address_id!='0'){
                                     $QueryObj = new user_address();
        $address_detail=$QueryObj->getAddress($order->billing_address_id);
        
                                     ?>
                                <div class="span4">
                                    <div class="portlet grey box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-cogs"></i>Billing Address
                                            </div>
                                            <div class="actions">
                                                
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row-fluid static-info">
                                                <div class="span12 value">
                                                    Name:- <?php echo $address_detail->firstname ?> <?php echo $address_detail->lastname ?><br/>
                                                    Email:- <?php echo $address_detail->email ?><br/>
                                                    Phone:- <?php echo $address_detail->phone ?><br/>
                                                    Address 1:- <?php echo $address_detail->address1 ?><br/>
                                                    Address 2:- <?php echo $address_detail->address2 ?><br/>
                                                    <?php
                                                    $QueryObj = new country();
                                                          $address_detail_country=$QueryObj->getnewCountry($address_detail->country_id);
                                                    $QueryObj = new state();
                                                          $address_detail_state=$QueryObj->getnewState($address_detail->state_id);     
                                                    $QueryObj = new city();
                                                          $address_detail_city=$QueryObj->getnewCity($address_detail->city_id);
                                                          
                                                          ?>
                                                    
                                                    City:- <?php echo $address_detail_city->name; ?> <br/>Province/State:- <?php echo $address_detail_state->name; ?><br/>
                                                    Postal/Zip code:- <?php echo $address_detail->zip_code; ?> <br/>Country:- <?php echo $address_detail_country->short_name; ?><br/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <?php } ?>
                                 <?php
                                 if($order->billing_address_id=='0'){
                                     ?>
                                <div class="span4">
                                    <div class="portlet grey box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-cogs"></i>Billing Address
                                            </div>
                                            <div class="actions">
                                                
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row-fluid static-info">
                                                <div class="span12 value">
                                                    Name:- <?php echo $order->billing_firstname ?> <?php echo $order->billing_lastname ?><br/>
                                                    Email:- <?php echo $order->billing_email ?><br/>
                                                    Phone:- <?php echo $order->billing_phone ?><br/>
                                                    Address 1:- <?php echo $order->billing_address1 ?><br/>
                                                    Address 2:- <?php echo $order->billing_address2 ?><br/>
                                                    City:- <?php echo city_name($order->billing_city); ?><br/> Province/State:- <?php echo state_name($order->billing_state); ?><br/>
                                                    Postal/Zip code:- <?php echo $order->billing_zip ?><br/>Country:- <?php echo country_name($order->billing_country); ?><br/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <?php } ?>
                                 <?php
                                 if($order->shipping_address_id!='0'){
                                     $QueryObj = new user_address();
        $address_detail_shipping=$QueryObj->getAddress($order->shipping_address_id);
        
                                     ?>
                                <div class="span4">
                                    <div class="portlet grey box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-cogs"></i>Shipping Address
                                            </div>
                                            <div class="actions">
                                                
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row-fluid static-info">
                                                <div class="col-md-12 value">
                                                    Name:- <?php echo $address_detail_shipping->firstname ?> <?php echo $address_detail_shipping->lastname ?><br/>
                                                    Email:- <?php echo $address_detail_shipping->email ?><br/>
                                                    Phone:- <?php echo $address_detail_shipping->phone ?><br/>
                                                    Address 1:- <?php echo $address_detail_shipping->address1 ?><br/>
                                                    Address 2:- <?php echo $address_detail_shipping->address2 ?><br/>
                                                    <?php
                                                    $QueryObj = new country();
                                                          $address_detail_country1=$QueryObj->getnewCountry($address_detail_shipping->country_id);
                                                    $QueryObj = new state();
                                                          $address_detail_state1=$QueryObj->getnewState($address_detail_shipping->state_id);     
                                                    $QueryObj = new city();
                                                          $address_detail_city1=$QueryObj->getnewCity($address_detail_shipping->city_id);
                                                          
                                                          ?>
                                   
                                                    City:- <?php echo $address_detail_city1->name; ?> <br/>Province/State:- <?php echo $address_detail_state1->name; ?><br/>
                                                    Postal/Zip code:- <?php echo $address_detail_shipping->zip_code; ?> <br/>Country:- <?php echo $address_detail_country1->short_name; ?><br/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <?php } ?>
                                 <?php
                                 if($order->shipping_address_id=='0'){
                                     ?>
                                <div class="span4">
                                    <div class="portlet grey box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-cogs"></i>Shipping Address
                                            </div>
                                            <div class="actions">
                                                
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row-fluid static-info">
                                                <div class="col-md-12 value">
                                                    Name:- <?php echo $order->shipping_firstname ?> <?php echo $order->shipping_lastname ?><br/>
                                                    Email:- <?php echo $order->shipping_email ?><br/>
                                                    Phone:- <?php echo $order->shipping_phone ?><br/>
                                                    Address 1:- <?php echo $order->shipping_address1 ?><br/>
                                                    Address 2:- <?php echo $order->shipping_address2 ?><br/>
                                                    City:- <?php echo city_name($order->shipping_city); ?><br/> Province/State:- <?php echo state_name($order->shipping_state); ?><br/>
                                                    Postal/Zip code:- <?php echo $order->shipping_zip ?> <br/>Country:- <?php echo country_name($order->shipping_country); ?><br/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <?php } ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="table-container">
                               <div class="row-fluid">
                                <div class="span12">
                                    <div class="portlet grey box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-shopping-cart"></i>Order Detail
                                            </div>
                                            <div class="actions">
                                               
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered table-striped">
                                                    <!--<thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Product</th>
                                                            <th>SKU</th>
                                                            <!--<th>Variant</th>-->
                                                        <!--    <th>Quantity</th>
                                                            <th>Tax Amount</th>
                                                            <!--<th>Discount</th>
                                                            <th>Coupon</th>-->
                                                         <!--   <th>Price</th>
                                                        </tr>
                                                    </thead>-->
                                                    <tbody>
                                                        <?php if(isset($order->items) && !empty($order->items)):
                                                            $counter=1;
                                                            ?>
                                                            <?php foreach($order->items as $kk => $vv): ?>
                                                            
                                                         <thead>
                                                        <tr>
                                                            <th>Product-><?php echo $counter; ?></th>
                                                            <th>Product Name</th>
                                                            <th>Patient Name</th>
                                                            <th>SKU</th>
                                                            <!--<th>Variant</th>-->
                                                            <th>Quantity</th>
                                                            <th>Tax Amount</th>
                                                            <!--<th>Discount</th>-->
                                                            <th>Markup</th>
                                                            <th colspan=2>Price</th>
                                                        </tr>
                                                    </thead>
                                                        
                                                            <tr>
                                                                <td class="thumb"><img src="<?php product::getProductMainImageFeed($vv['product_id'], 'thumb', 'echo', true);?>"/></td>
                                                                <td>
                                                                    <!--<a href="<?php echo make_admin_url('product','update','update','id='.$vv['product_id']);?>">-->
                                                                        <?php echo $vv['product_name'];?>
                                                                    <!--</a>-->
                                                                </td>
                                                                <td><?php echo $vv['p_firstname'];?> <?php echo $vv['p_lastname'];?></td>
                                                                <td><?php echo $vv['product_sku'];?></td>
                                                                <!--<td><?php echo ($vv['product_variant_value']!='')?$vv['product_variant_value']:"No variant";?></td>-->
                                                                <td><?php echo $vv['quantity'];?></td>
                                                                <td><?php show_price($vv['total_tax']);?></td>
                                                                <!--<td><?php show_price($vv['total_discount']);?></td>-->
                                                                <td>$<?php echo $vv['markup_total'];?></td>
                                                                <td colspan=2><?php show_price($vv['total_price_tax_excl']);?></td>
                                                            </tr>
                                                            <?php $res=order_status::get_custom_detail($vv['product_id']); ?>
                                                            
                                                            <?php if($vv['product_type']=='custom_formula'){ ?>
                                                            <tr><td colspan="8"><h5 style="text-align:center;font-family:bold;">Herbs List</h5></td></tr>
                                                            <tr>
                                                            <th>Sr No</th>
                                                            <th>English Name</th>
                                                            <th>Pin Yin</th>
                                                            <th>Latin Name</th>
                                                            <th>Chinese Name</th>
                                                            <th>Type</th>
                                                            <th>Supplier Name</th>
                                                            <th>Quantity</th>
                                                            <!--<th>Discount</th>
                                                            <th>Coupon</th>-->
                                                        </tr>
                                                            <?php 
                                                               $herb_start=1; 
                                                            foreach (order_status::getCustomFormula_herbs($vv['product_id']) as $kk=>$vv2): ?>
                                                                <tr>
                                                                <td><?php echo $herb_start; ?></td>    
                                                                <td><?php echo $vv2['name']; ?></td>
                                                                <td><?php echo $vv2['pin_name']; ?></td>
                                                                <td><?php echo $vv2['latin_name']; ?></td>
                                                                <td><?php echo $vv2['chinese_name']; ?></td>
                                                                <td><?php echo $vv2['herb_type']; ?></td>
                                                                <td><?php echo $vv2['herb_supplier_id']; ?></td>
                                                                <td><?php echo $vv2['quantity']; ?></td>
                                                                </tr>    
                                                            <?php 
                                                            $herb_start++;
                                                            endforeach; ?>
                                                            <tr><td>All preparation instructions :</td><td colspan=7><?php echo $res->instruction_prepration; ?></td></tr>
                                                            <tr><td>All Notes for medicinary :</td><td colspan=7><?php echo $res->instruction_for_medicinary; ?></td></tr>
                                                            <tr><td>All dosage information :</td><td colspan=7><?php echo $res->dosage_quantity; ?> <?php echo $res->dosage_type; ?> x <?php echo $res->dosage_per_day; ?> times per day x <?php echo $res->dosage_noof_days; ?> = <?php echo $total=$res->dosage_quantity*$res->dosage_per_day*$res->dosage_noof_days; ?> <?php echo $res->dosage_type; ?> </td></tr>    
                                                            <tr><td colspan="6"></td><td colspan="2"><a id="do_action" href="<?php echo DIR_WS_SITE.ADMIN_FOLDER.'/ajax_control.php?Page=order&action=print_label&section=print_label&id='.$order->id.'&item_id='.$vv['id'];?>" class="btn btn-xs default btn-editable"> Print Label</a>|<a id="do_action" href="<?php echo DIR_WS_SITE.ADMIN_FOLDER.'/ajax_control.php?Page=order&action=print_info&section=print_info&id='.$order->id.'&item_id='.$vv['id'];?>" class="btn btn-xs default btn-editable"> Print Info</a></td></tr>
                                                                <?php } ?>
                                                            
                                                            <?php $counter++; endforeach;?>
                                                        <?php endif;?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span6">
                                </div>
                                <div class="span6">
                                    <div class="well">
                                        <div class="row-fluid static-info align-reverse">
                                            <div class="span8 name">Sub Total:</div>
                                            <div class="span3 value"><?php show_price($order->total_price_tax_excl);?></div>
                                        </div>
                                        <!--<div class="row-fluid static-info align-reverse">
                                            <div class="span8 name">Discount:</div>
                                            <div class="span3 value"><?php show_price($order->total_discount);?></div>
                                        </div>-->
                                        <?php if($order->total_coupon_amount!='0'): ?>
                                        <div class="row-fluid static-info align-reverse">
                                            <div class="span8 name">Coupon Amount:</div>
                                            <div class="span3 value"><?php show_price($order->total_coupon_amount);?></div>
                                        </div>
                                        <?php endif;?>
                                        <?php if($order->total_gift_wrapping_amount!='0'): ?>
                                        <div class="row-fluid static-info align-reverse">
                                            <div class="span8 name">Gift Wrapping Charges:</div>
                                            <div class="span3 value"><?php show_price($order->total_gift_wrapping_amount);?></div>
                                        </div>
                                        <?php endif;?>
                                        <div class="row-fluid static-info align-reverse">
                                            <div class="span8 name">Total Tax:</div>
                                            <div class="span3 value"><?php show_price($order->total_tax);?></div>
                                        </div>
                                        <div class="row-fluid static-info align-reverse">
                                            <div class="span8 name">Shipping Amount:</div>
                                            <div class="span3 value"><?php show_price($order->total_shipping_amount);?></div>
                                        </div>
                                        <div class="row-fluid static-info align-reverse">
                                            <div class="span8 name">Grand Total:</div>
                                            <div class="span3 value"><?php show_price($order->grand_total+$order->total_shipping_amount);?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_4">
                           <div class="row-fluid">
                                <div class="span6">
                                    <div class="portlet grey box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-plane"></i>Shipment
                                            </div>
                                            <div class="actions">
                                                    <a data-toggle="modal" data-target="#add_shipping" class="btn default btn-sm">
                                                            <i class="icon-pencil"></i> Shipping Details
                                                    </a>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <h5 class="section-title">Selected Shipping : <?php echo $order->shipping_method_name;?></h5>
                                            <table class="table table-condensed table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Shipping Date</th>
                                                        <th>Approx. Delivery</th>
                                                        <th>Carrier</th>
                                                        <th>Amount</th>
                                                        <th>Weight</th>
                                                        <th>Tracking Order</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><?php echo ($order->shipping_date!='0000-00-00 00:00:00')?$order->shipping_date:"";?></td>
                                                        <td><?php echo ($order->estimate_delivery_date!='0000-00-00')?$order->estimate_delivery_date:"";?></td>
                                                        <td><?php echo ($order->shipping_carrier!='')?$order->shipping_carrier:$shipping_method->carrier;?></td>
                                                        <td><?php show_price($order->total_shipping_amount);?></td>
                                                        <td><?php echo $order->total_order_weight." ".WEIGHT_DIMENTIONS;?></td>
                                                        <td><?php echo $order->shipping_tracking_no;?></td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6">
                                    <div class="portlet grey box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-money"></i>Payment
                                            </div>
                                            <div class="actions">
                                                 <a data-toggle="modal" data-target="#edit_payment" class="btn default btn-sm">
                                                    <i class="icon-pencil"></i> Payment Details
                                                 </a>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <h5 class="section-title">Selected Payment : <?php echo $order->payment_method_name;?></h5>
                                            <table class="table table-condensed table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Paid</th>
                                                            <th>Payment Date</th>
                                                            <th>Payment Method</th>
                                                            <th>Transection #</th>
                                                            <th>Amount</th>
                                                            <th>Invoice</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><?php echo ($order->is_payment_made)?"Yes":"No";?></td>
                                                            <td><?php echo ($order->payment_time!='0000-00-00 00:00:00')?$order->payment_time:"";?></td>
                                                            <td><?php echo $order->payment_method_name;?></td>
                                                            <td><?php echo ($order->transaction_id!='')?$order->transaction_id:"";?></td>
                                                            <td><?php show_price(($order->total_paid!='' && $order->total_paid!='0')?$order->total_paid:$order->grand_total);?></td>
                                                            <td>
                                                                <a href="<?php echo make_admin_url('order','download_invoice','update','invoice='.$order->invoice_no);?>" class="btn default btn-sm">
                                                                    <i class="icon-download"></i>  #<?php echo $order->invoice_no;?>
                                                                </a>   
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                            </table>
                                            <p>
                                                <strong>Payment Comments:</strong>
                                                <?php echo $order->payment_comment;?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_5">
                            <div class="table-container">
                                <table class="table table-striped table-bordered table-hover" id="datatable_history">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th width="25%">
                                                Datetime
                                            </th>
                                            <th width="55%">
                                                 Description
                                            </th>
                                            <th width="10%">
                                                 Notification
                                            </th>
                                            <th width="10%">
                                                 Actions
                                            </th>
                                        </tr>
                                        <tr role="row" class="filter">
                                            <td>
                                                <div class="input-group date datetime-picker margin-bottom-5" data-date-format="dd/mm/yyyy hh:ii">
                                                    <input type="text" class="form-control form-filter input-sm" readonly name="order_history_date_from" placeholder="From">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-sm default date-set" type="button"><i class="icon-calendar"></i></button>
                                                    </span>
                                                </div>
                                                <div class="input-group date datetime-picker" data-date-format="dd/mm/yyyy hh:ii">
                                                    <input type="text" class="form-control form-filter input-sm" readonly name="order_history_date_to" placeholder="To">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-sm default date-set" type="button"><i class="icon-calendar"></i></button>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control form-filter input-sm" name="order_history_desc" placeholder="To"/>
                                            </td>
                                            <td>
                                                <select name="order_history_notification" class="form-control form-filter input-sm">
                                                    <option value="">Select...</option>
                                                    <option value="pending">Pending</option>
                                                    <option value="notified">Notified</option>
                                                    <option value="failed">Failed</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="margin-bottom-5">
                                                    <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="icon-search"></i> Search</button>
                                                </div>
                                                <button class="btn btn-sm red filter-cancel"><i class="icon-times"></i> Reset</button>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
      <div id="add_shipping"  class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">  
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Shipping Details</h4>
            </div>
           <form method="post" name="add_shipping" action="" id="validation">
            <div class="modal-body">
                <div class="row-fluid">                    
                   <div class="form-group">
                           <label class="span3 control-label" for="shipping_date">Shipping Date</label>
                           <div class="span6">
                               <input type="text" name="shipping_date" id="shipping_date" class="form-control m-wrap form_datetime_sql validate[required]" value="<?php echo ($order->shipping_date!='0000-00-00 00:00:00')?$order->shipping_date:"";?>"/> 
                            </div>  
                    </div> <div class="clearfix"></div>
                    <div class="form-group">
                           <label class="span3 control-label" for="estimate_delivery_date">Approx Deliver Date</label>
                           <div class="span6">
                               <input type="text" name="estimate_delivery_date" id="estimate_delivery_date" class="form-control m-wrap sql_format_datepicker validate[required]" value="<?php echo ($order->estimate_delivery_date!='0000-00-00')?$order->estimate_delivery_date:"";?>"/> 
                            </div>  
                    </div> <div class="clearfix"></div>
                    <div class="form-group">
                           <label class="span3 control-label" for="shipping_carrier">Shipping Carrier</label>
                           <div class="span6">
                               <select name="shipping_carrier" id="shipping_carrier" class="form-control m-wrap validate[required]">
                                   <?php $existing_value = ($order->shipping_carrier!='')?$order->shipping_carrier:$shipping_method->carrier;?>
                                   <?php if($all_shipping_methods) :?>
                                   <?php foreach($all_shipping_methods as $kk=>$vv): ?>
                                         <option value="<?php echo $vv['carrier'];?>" <?php echo ($vv['carrier']==$existing_value)?"selected":"";?>><?php echo $vv['carrier'];?></option>
                                   <?php endforeach;?>
                                   <?php endif;?>
                               </select>
                            </div>  
                    </div> <div class="clearfix"></div>
                    <div class="form-group">
                           <label class="span3 control-label" for="shipping_tracking_no">Tracking Number</label>
                           <div class="span6">
                               <input type="text" name="shipping_tracking_no" id="shipping_tracking_no" class="form-control m-wrap validate[required]" value="<?php echo $order->shipping_tracking_no;?>"/> 
                            </div>  
                    </div> <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="submit_shipping" class="btn blue">Save changes</button>
            </div>
          </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
      <div id="edit_payment"  class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">  
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Payment Details</h4>
            </div>
           <form method="post" name="payment_details" action="" id="validation">
            <div class="modal-body">
                <div class="row-fluid">     
                    <div class="form-group">
                           <label class="span3 control-label" for="is_payment_made">Status</label>
                           <div class="span6">
                               <select name="is_payment_made" id="is_payment_made" class="form-control" value="<?php echo $order->shipping_date;?>">
                                   <option value="1" <?php echo ($order->is_payment_made)?"selected":"";?>>Yes</option>
                                   <option value="0" <?php echo (!$order->is_payment_made)?"selected":"";?>>No</option>
                               </select>
                            </div>  
                    </div> <div class="clearfix"></div>
                   <div class="form-group">
                           <label class="span3 control-label" for="payment_time">Payment Date</label>
                           <div class="span6">
                               <input type="text" name="payment_time" id="payment_time" class="form-control form_datetime_sql" value="<?php echo ($order->payment_time!='0000-00-00 00:00:00')?$order->payment_time:"";?>"/> 
                            </div>  
                    </div> <div class="clearfix"></div>
                    <div class="form-group">
                           <label class="span3 control-label" for="transaction_id">Transaction #</label>
                           <div class="span6">
                               <input type="text" name="transaction_id" id="transaction_id" class="form-control" value="<?php echo $order->transaction_id;?>"/> 
                            </div>  
                    </div> <div class="clearfix"></div>
                    <div class="form-group">
                           <label class="span3 control-label" for="total_paid">Total Paid (<?php echo CURRENCY_SYMBOL;?>)</label>
                           <div class="span6">
                               <input type="text" name="total_paid" id="total_paid" class="form-control" value="<?php echo ($order->total_paid!='' && $order->total_paid!='0')?$order->total_paid:$order->grand_total;?>"/> 
                            </div>  
                    </div> <div class="clearfix"></div>
                    <div class="form-group">
                           <label class="span3 control-label" for="payment_comment">Payment Details</label>
                           <div class="span6">
                               <textarea name="payment_comment" id="payment_comment" class="form-control"><?php echo $order->payment_comment;?></textarea> 
                            </div>  
                    </div> <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="submit_payment" class="btn blue">Save changes</button>
            </div>
          </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>
<!-- END PAGE CONTENT-->
</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
