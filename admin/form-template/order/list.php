<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-shopping-cart"></i> Manage Orders</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                            List Orders
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Orders</div>
                <div class="actions">
                   <?php //include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                </div>
            </div>
             <div class="portlet-body">
                    <div class="table-container">
                            <table class="table table-striped table-bordered table-hover" id="datatable_without_sorting">
                            <thead>
                            <tr role="row" class="heading">
                                    <th width="5%">
                                            #
                                    </th>
                                    <th width="5%">
                                             Order&nbsp;#
                                    </th>
                                    <th width="15%">
                                             Purchased&nbsp;On
                                    </th>
                                    <!--<th width="15%">
                                             Billing Customer
                                    </th>-->
                                    <th width="10%">
                                             Service
                                    </th>
                                    <th width="10%">
                                             Payment&nbsp;Method
                                    </th>
                                    <th width="10%">
                                             Order&nbsp;Price(<?php echo CURRENCY_SYMBOL;?>)
                                    </th>
                                    <th width="10%">
                                             Status
                                    </th>
                                    <th width="10%">
                                             Actions
                                    </th>
                            </tr>
                            </thead>
                            <tbody>

                             <?php if(!empty($orders)):
                                 //echo "<pre>";
                                 //print_r($orders);
                                 ?>
                             <?php $sr=1;foreach($orders as $kk=>$vv):?>
                                 <tr>
                                     <td><?php echo $sr;?></td>
                                     <td class="clickable"><?php echo $vv['order_id'];?></td>
                                     <td class="clickable"><?php echo date("d/m/Y",strtotime($vv['date_add']));?></td>
                                     <!--<td class="clickable"><?php echo $vv['billing_firstname'].' '.$vv['billing_lastname'];?></td>-->
                                     <td class="clickable"><?php echo $vv['totalquantity'];?></td>
                                     <td class="clickable"><?php echo $vv['payment_method_name'];?></td>
                                     <td class="clickable"><?php show_price($vv['grand_total']);?></td>
                                     <td class="clickable"><span class="label label-sm label-<?php echo $status_array[$vv['current_state']];?>"><?php echo $vv['current_state'];?></span></td>
                                     <td>
                                                                                                                                                                                                        
                                        <!--<a id="do_action" href="<?php echo make_admin_url('order','print_label','print_label','id='.$vv['id']);?>" class="btn btn-xs default btn-editable"> Print Label</a>|<a id="do_action" href="<?php echo make_admin_url('order','print_info','print_info','id='.$vv['id']);?>" class="btn btn-xs default btn-editable"> Print Info</a>|-->
                                                                                                                                                   
                                        <a id="do_action" href="<?php echo make_admin_url('order','update','update','id='.$vv['id']);?>" class="btn btn-xs default btn-editable">View Order<!--<i class="icon-pencil"></i>--> </a></td>
                                 </tr>
                            <?php $sr++; endforeach;?>
							
							 <?php endif;?>  
                            </tbody>
                            </table>
                    </div>
            </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>