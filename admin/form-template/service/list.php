<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-bullhorn"></i> Manage Service Services</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>      

            <li class="last">
                List Services
                <i class="icon-angle-right"></i>
            </li>

        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><?php if(!empty($uid)):  echo "<strong>".$user->firstname." ".$user->lastname."</strong> >> ";endif;?>List Services</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('service', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >	
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th style="width:1%;" class="hidden-480"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th>
                                <th>Service Title</th>
                                <th class="hidden-480">Service Categories</th>
                                <th class='text-center'>Images</th>
                                <th class="hidden-480">Status</th>
                                <th class='text-right'>Price (<?= CURRENCY_SYMBOL ?>)</th>
                                <th class='text-right'>Action</th>
                            </tr>
                        </thead>
                        <?php if (!empty($services)): ?>
                            <tbody>
                                <?php $sr = 1;
                                foreach ($services as $key => $object):
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="hidden-480">
                                            <input class="checkboxes" id="multiopt[<?php echo $object->id ?>]" name="multiopt[<?php echo $object->id ?>]" type="checkbox" />
                                        </td>
                                        <td class="clickable"><?php echo $object->name ?></td>
                                        <td class="hidden-480 clickable">
                                            <?php
                                            $QueryObj = new categoryService();
                                            $rec = $QueryObj->getServiceCatNames($object->id);
                                            if (!empty($rec)):
                                                echo implode(', ', $rec);
                                            endif;
                                            ?>
                                        </td>
                                        <td class='text-center' >
                                            <a href="<?= make_admin_url('service', 'images', 'images', 'id=' . $object->id) ?>" class='btn btn-xs btn-info bg-yellow'>
                                                View Images
                                            </a>
                                        </td>

                                        <td class="center hidden-480">
                                            <div class="btn-group mini_buttons on_off_button" id='on_off_button<?php echo $object->id ?>'>
                                                <div module="user" class="btn status_on mini_buttons <?php echo ($object->is_active == '1') ? 'active' : ''; ?>" on="<?php echo $object->id; ?>" rel="<?php echo ($object->is_active == '1') ? 'on' : 'off'; ?>" >ON</div>
                                                <div module="user" class="btn status_off mini_buttons button_right <?php echo ($object->is_active == '0') ? 'active' : ''; ?>" off="<?php echo $object->id; ?>" rel="<?php echo ($object->is_active == '1') ? 'off' : 'on'; ?>">OFF</div>
                                            </div>
                                        </td>
                                        <td class='text-right'><?= CURRENCY_SYMBOL ?><?php echo number_format($object->price, 2); ?></td>
                                        <td class='text-right'>
                                            <a id="do_action" href="<?php echo make_admin_url('service', 'update', 'update', 'id=' . $object->id) ?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i> &nbsp;</a>
                                            <a href="<?php echo make_admin_url('service', 'delete', 'list', 'id=' . $object->id) ?>" onclick="return confirm('Are you sure? You are deleting this record.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i> &nbsp;</a>
                                        </td>
                                    </tr>
                                    <?php $sr++;
                                endforeach;
                                ?>
                            </tbody>
                            <tfoot>
                                <tr class="odd gradeX hidden-480">
                                    <td colspan="3">
                                        <div style=" width:220px;float:left">
                                            <select name="multiopt_action" style="width:150px;float: left;" class="left_align form-control">
                                                <option value="delete">Delete</option>
                                            </select>
                                            <input style="float:right" type="submit" class="btn blue large" name="multiopt_go" value="Go"  onclick="return confirm('Are you sure?');" />
                                        </div>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr> 
                            </tfoot>
<?php endif; ?>  
                    </table>
                </form> 
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div class="clearfix"></div>
<script src="assets/scripts/category_listing.js" type="text/javascript"></script>