<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-bullhorn"></i> Manage Services</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-sitemap"></i>
                <a href="<?php echo make_admin_url('service', 'list', 'list'); ?>">List Services</a>
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                Edit Service Images
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <!-- / Box -->
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Add/Edit Service Images</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>
            </div>
            <div class="portlet-body form form-body">   
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet">
                            <form class="form-horizontal" action="<?php echo make_admin_url('service', 'images', 'images', 'id=' . $id) ?>" method="POST" enctype="multipart/form-data" id="validation">
                                <div class="portlet-body form form-body">      
                                    <div class="form-group">
                                        <label class="span2 control-label">Upload Images</label>
                                        <div class="span6">
                                            <input type="file" name="image[]" multiple class="form-control m-wrap" />
                                            <div class="clearfix"></div>
                                            <span class="help-block">add multiple files by using 'ctrl' key</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="span2 control-label">Upload from URL</label>
                                        <div class="span6">
                                            <input type="text" name="image_url" placeholder="Image URL.." class="form-control" title="Image URL"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="span2 control-label">&nbsp;</label>
                                        <div class="span4">
                                            <input type="hidden" name="id" value="<?php echo $values->id; ?>"/>
                                            <input type="hidden" name="name" value="<?php echo $values->name; ?>"/>
                                            <input class="btn blue" type="submit" name="submit_images" value="Submit"/>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                        <form action="<?php echo make_admin_url('service', 'update_image_position', 'update_image_position', 'id=' . $id); ?>" method="post" id="form_data" name="form_data" >	
                            <table class="table table-striped table-bordered table-hover" id="product_images">
                                <thead>
                                    <tr class="heading">
                                        <th class="hidden-480">#</th>
                                        <th>Image</th>
                                        <th>Cover Image</th>
                                        <th class="hidden-480">Position</th>
                                        <th class="text-right">Action &nbsp;</th>
                                    </tr>
                                </thead>
                                <?php if (!empty($values->images)): ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($values->images as $image):
                                            ?>
                                            <tr class="odd gradeX">
                                                <td style="vertical-align:middle;" class="hidden-480"><?php echo $sr ?>.</td>
                                                <td>
                                                    <a class="fancybox-button" data-rel="fancybox-button" href="<?php echo cwebc::getImage($image['image'], 'service', 'big'); ?>">
                                                        <img src="<?php echo cwebc::getImage($image['image'], 'service', 'thumb'); ?>" alt="<?php echo $values->name; ?>"/>
                                                    </a>
                                                </td>
                                                <td style="vertical-align:middle;">
                                                    <?php if ($image['is_main']): ?>
                                                        <i class="icon-<?php echo ($image['is_main'] == '1') ? "ok" : "ban-circle"; ?> icon-white btn btn-xs <?php echo ($image['is_main'] == '1') ? "green" : "red"; ?>"></i>
                                                    <?php else: ?>
                                                        <a href="<?php echo make_admin_url('service', 'cover_image', 'cover_image', 'id=' . $id . '&img=' . $image['id']) ?>" title="Make Cover Image" id="make_cover_image" data-img_id="<?php echo $image['id'] ?>" data-product_id="<?php echo $image['service_id'] ?>" class="btn btn-xs <?php echo ($image['is_main'] == '1') ? "green" : "red"; ?> icn-only"><i class="icon-<?php echo ($image['is_main'] == '1') ? "ok" : "ban-circle"; ?> icon-white"></i></a>
        <?php endif; ?>
                                                </td>
                                                <td style="vertical-align:middle;" class="hidden-480">
                                                    <input type="number" class="text-center" name="position[<?php echo $image['id']; ?>]" value="<?php echo $image['position']; ?>" size="5" style="width:50%;" />
                                                </td>
                                                <td style="vertical-align:middle;" class="text-right">
                                                    <a href="<?php echo make_admin_url('service', 'delete_image', 'update', 'id=' . $id . '&img=' . $image['id']) ?>" onclick="return confirm('Are you sure? You are deleting this image.');"  title="click here to delete this image" class="btn btn-xs default"><i class="icon-trash"></i> Delete</a> &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <?php
                                            $sr++;
                                        endforeach;
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr class="odd gradeX hidden-480">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="left" valign="middle" width="20%"><input type="submit" class="btn blue mt15 middle" name="submit_image_position" value="Update" /></td>
                                            <td></td>
                                        </tr> 
                                    </tfoot>
                                <?php else: ?> 
                                    <tbody><tr><td colspan="5">No Images found.</td></tr></tbody>
<?php endif; ?>  
                            </table>
                        </form> 


                    </div>

                </div>

            </div>
        </div>

        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>