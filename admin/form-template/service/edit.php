<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-bullhorn"></i> Manage Services</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-sitemap"></i>
                <a href="<?php echo make_admin_url('service', 'list', 'list'); ?>">List Services</a>
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                Edit Services
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form class="form-horizontal" action="<?php echo make_admin_url('service', 'update', 'update', 'id=' . $id) ?>" method="POST" enctype="multipart/form-data" id="validation">
        <!-- / Box -->
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Edit Service</div>
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">   
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="name">Service Title :<span class="required">*</span></label>
                                <div class="col-md-6">
                                    <input type="text" name="name"  value="<?php echo $values->name ?>" id="name" class="form-control m-wrap validate[required]" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="catgory_id">Category :<span class="required">*</span></label>
                                <div class="col-md-6">
                                    <select name="catgory_id[]" id='catgory_id' class="form-control m-wrap validate[required]" multiple >
                                        <?php
                                        if (!empty($categories)):
                                            foreach ($categories as $key => $value):
                                                ?>
                                                <option value="<?= $value->id ?>" <?= (in_array($value->id, $selected_cat)) ? 'selected' : '' ?>><?= $value->name ?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>	
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="vendor_id">Vendor :<span class="required">*</span></label>
                                <div class="col-md-6">
                                    <select class="form-control validate[required]" id="vendor_id" name="vendor_id">
                                        <option value="">Select Vendor</option>
                                        <?php
                                        if (!empty($allUser)):
                                            foreach ($allUser as $key => $value):
                                                ?>
                                                <option value="<?= $value->id ?>" <?= ($values->vendor_id == $value->id) ? 'selected' : '' ?>><?= $value->firstname . ' ' . $value->lastname ?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="description">Description :</label>
                                <div class="col-md-8">
                                    <textarea name="description" rows='5' id="description" value="" class="form-control m-wrapckeditor ckeditor "><?= $values->description ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="service_area">Service Area :<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <textarea name="service_area" rows='2' id="service_area" value="" class="form-control m-wrapckeditor validate[required]"><?= $values->service_area ?></textarea>
                                    <div class="hint"><i>Enter (Postal Codes) / separated by commas</i></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="price">Price :<span class="required">*</span></label>
                                <div class="col-md-6">
                                    <input type="text" name="price"  value="<?= $values->price ?>" id="price" class="form-control m-wrap validate[required,custom[number]]" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="is_active">Approve Status :</label>
                                <div class="col-md-6">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="is_active" id="is_active" value="1" <?= ($values->is_active == 1) ? 'checked' : '' ?>/> Publish </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_active" id="is_active" value="0" <?= ($values->is_active == 0) ? 'checked' : '' ?>/> Pending Review </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label border-left-2px" for="position">Position</label>
                                <div class="col-md-2">
                                    <input type="number" name="position"  value="" id="position" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="offset2">
                            <input type="hidden" name="id" id="service_id" value="<?php echo $values->id ?>" />
                            <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                            <button class="btn blue" type="submit" name="submit" value="SubmitClose"><i class="icon-ok"></i> Save and Close</button> 
                            <a href="<?php echo make_admin_url('service', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>