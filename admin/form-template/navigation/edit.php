<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-reorder"></i> Navigation</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                   
            <li>
                <i class="icon-reorder"></i>
                <a href="<?php echo make_admin_url('navigation', 'list', 'list'); ?>">List Navigations</a> 
                <i class="icon-angle-right"></i>
            </li>                                   
            <li class="last">
                Edit Navigation
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
$navigation = $navigations;
?>
<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Edit Navigation</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php') ?>
                </div>
            </div>
            <div class="portlet-body">
                <form class="form form-horizontal" method="post" action="<?= make_admin_url('navigation', 'update', 'update', 'id=' . $id) ?>" enctype="multipart/form-data" id="validation">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="span2 control-label" for="name">Name<span class="required">*</span></label>
                            <div class="span8">
                                <input type="text" name="name"  value="<?php echo $navigation->name ?>" id="name" class="span6 form-control m-wrap validate[required]" />
                            </div>
                        </div>
                        <!--<div class="form-group">
                                                    <label class="span2 control-label" for="position">Position</label>
                                                    <div class="span8">
                                                        <select class="form-control span6" id="position" name="na[position]">
                                                            <option value="top"<?php // echo $navigation->position == "top" ? ' selected' : '' ?>>Top</option>
                                                            <option value="bottom"<?php // echo $navigation->position == "bottom" ? ' selected' : '' ?>>Bottom</option>
                                                        </select>
                                                    </div>
                                                </div>
                        <div class="form-group">
                            <label class="span2 control-label" for="description">Description</label>
                            <div class="span8">
                                <textarea name="description" class="form-control span6" id="description" rows="4"><?php // echo $navigation->description ?></textarea>
                            </div>
                        </div>-->
                        <div class="form-group">
                            <label class="span2 control-label" for="is_active">Show on Website</label>
                            <div class="span8">
                                <input name="is_active" type="checkbox" <?php echo $navigation->is_active == 1 ? ' checked' : '' ?> class="form-control span6" id="is_active" value="1" />
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="offset2">
                                <input type="hidden" name="id"  value="<?= $navigation->id ?>" />
                                <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-ok"></i> Save</button> 
                                <a href="<?php echo make_admin_url('navigation', 'list', 'list'); ?>" class="btn" name="cancel"> Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
