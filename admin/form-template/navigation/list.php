<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-reorder"></i> Navigation</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>
            <li class="last">List Navigations</li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
$nav_obj = $QueryObj;
?>
<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Navigations</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php') ?>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th class="hidden-480">#</th>
                            <th>Name</th>
                            <th style="width:150px" class='text-center'>Manage</th>
                            <th style="width:150px">Position</th>
                            <th style="width:200px">Show on Website</th>
                            <th style="width:200px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($nav_obj->GetNumRows() != 0) { ?>
                            <?php $i = 1 ?>
                            <?php while ($navigation = $nav_obj->GetObjectFromRecord()) { ?>
                                <tr>
                                    <td class="hidden-480"><?php echo $i++ ?></td>
                                    <td><?php echo $navigation->name ?></td>
                                    <td style="text-align: center">
                                        <a class="btn grey" href="<?php echo make_admin_url('content_navigation', 'list', 'list', 'nav_id=' . $navigation->id); ?>">Menu Items</a>
                                    </td>
                                    <td><?php echo ucfirst($navigation->position) ?></td>
                                    <td class="center">
                                        <div class="btn-group mini_buttons on_off_button" id="on_off_button<?php echo $navigation->id ?>">
                                            <div module="navigations" class="btn status_on mini_buttons<?php echo $navigation->is_active ? ' active' : '' ?>" on="<?php echo $navigation->id ?>" rel="<?php echo $navigation->is_active ? 'on' : 'off' ?>">ON</div>
                                            <div module="navigations" class="btn status_off mini_buttons button_right<?php echo!$navigation->is_active ? ' active' : '' ?>" off="<?php echo $navigation->id ?>" rel="<?php echo $navigation->is_active ? 'on' : 'off' ?>">OFF</div>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="<?php echo make_admin_url('navigation', 'update', 'update', 'id=' . $navigation->id) ?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i> </a>
                                        <?php if ($navigation->id != '1' || $navigation->id != '2') { ?>
                                            <a href="<?php echo make_admin_url('navigation', 'delete', 'list', 'id=' . $navigation->id) ?>" onclick="return confirm('Are you sure? You are deleting this record.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i> </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>