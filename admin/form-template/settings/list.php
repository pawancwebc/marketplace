
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">

            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Preferences > <?php echo ucwords(str_replace('_', ' ', $sname)); ?>
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>Preferences</li>

            </ul>


            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span3 sidebar-content tabbable tabs-left">
            <ul class="nav nav-tabs" id="product_tabs">
                <li class="<?php echo ($sname == 'general') ? "active" : ""; ?>">
                    <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=general'); ?>">
                        <i class="icon-briefcase"></i> General
                    </a>
                    <?php echo ($sname == 'general') ? '<span class="after"></span>' : ""; ?>
                </li>
                
               
               <li class="<?php echo ($sname == 'email') ? "active" : ""; ?>">
                    <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=email'); ?>">
                        <i class="icon-envelope-alt"></i> Email
                    </a>
                    <?php echo ($sname == 'email') ? '<span class="after"></span>' : ""; ?>
                </li>
                <li class="<?php echo ($sname == 'SMTP') ? "active" : ""; ?>">
                    <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=SMTP'); ?>">
                        <i class="icon-trello"></i> SMTP
                    </a>
                    <?php echo ($sname == 'SMTP') ? '<span class="after"></span>' : ""; ?>
                </li>
                <li class="<?php echo ($sname == 'seo') ? "active" : ""; ?>">
                    <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=seo'); ?>">
                        <i class="icon-eye-open"></i> SEO
                    </a>
                    <?php echo ($sname == 'seo') ? '<span class="after"></span>' : ""; ?>
                </li>
                <li class="<?php echo ($sname == 'meta') ? "active" : ""; ?>">
                    <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=meta'); ?>">
                        <i class="icon-fullscreen"></i> Meta Information
                    </a>
                    <?php echo ($sname == 'meta') ? '<span class="after"></span>' : ""; ?>
                </li>
                <li class="<?php echo ($sname == 'social') ? "active" : ""; ?>">
                    <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=social'); ?>">
                        <i class="icon-twitter"></i> Social Links
                    </a>
                    <?php echo ($sname == 'social') ? '<span class="after"></span>' : ""; ?>
                </li>
               
               
               <li class="<?php echo ($sname == 'design') ? "active" : ""; ?>">
                    <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=design'); ?>">
                        <i class="icon-retweet"></i> Design
                    </a>
                    <?php echo ($sname == 'design') ? '<span class="after"></span>' : ""; ?>
                </li>
				<li class="<?php echo ($sname == 'homepage') ? "active" : ""; ?>">
                    <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=homepage'); ?>">
                        <i class="icon-list"></i> Homepage
                    </a>
                    <?php echo ($sname == 'design') ? '<span class="after"></span>' : ""; ?>
                </li>                
                <li class="<?php echo ($sname == 'address') ? "active" : ""; ?>">
                    <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=address'); ?>">
                        <i class="icon-road"></i> Address
                    </a>
                    <?php echo ($sname == 'address') ? '<span class="after"></span>' : ""; ?>
                </li>
               
            </ul>	
        </div>
        <div class="span9">
            <!-- / Box -->
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <?php if (isset($setting_type) && $setting_type == 'email'): ?>
                            <i class="icon-envelope-alt"></i>
                        <?php else: ?>
                            <i class="icon-cogs"></i>
                        <?php endif; ?>
                        <?php echo ucwords(str_replace('_', ' ', $sname)); ?> Settings </div>
                    <div class="tools">
                    </div>
                </div>
                <div class="portlet-body form form-body"> 
                    <?php if ($sname == 'password'): ?>
                        <div class="note note-info">
                            <p>Password must contain both letters and numbers</p>
                            <p>Minimum password length - <?php echo MINIMUM_LENGTH_ADMIN_PASSWORD;?></p>
                            <p>Minimum one upper case and one lower case character</p>
                        </div>
                        <form action="<?php echo make_admin_url('setting', 'change_password', 'list'); ?>" method="post" id="validation" name="form_data" class="form-horizontal">	
                            <div class="form-group">
                                <label class="control-label span3">New Password</label>
                                <div class="controls span6">
                                    <input type="password" name="passsword" class="form-control validate[required,minSize[<?php echo MINIMUM_LENGTH_ADMIN_PASSWORD;?>]]" id="password"/>
                                    <label class="checkbox">
                                        <input type="checkbox" name="show_password" id="show_password" value="1" onchange="ChangeToPassField()"/>
                                        Show Password
                                    </label> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label span3">Confirm Password</label>
                                <div class="controls span6">
                                    <input type="password" name="confirm_passsword" class="form-control validate[required,equals[password]]" id="confirm_passsword"/>
                                </div>
                            </div>
                            <div class="form-actions fluid">
                                <div class="offset2">
                                    <button class="btn green" type="submit" name="Submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>" class="btn" name="cancel"> Cancel</a>
                                </div>
                            </div>
                        </form> 
                    <?php elseif ($sname == 'robots'): ?>
                        <form action="<?php echo make_admin_url('setting', 'robots_edit', 'list'); ?>" method="post" id="validation" name="form_data" class="form-horizontal">	
                            <div class="form-group">
                                <label class="control-label span3">Robots.txt</label>
                                <div class="controls span8">
                                    <textarea rows="14" name="robots" class="form-control" id="robots"><?php echo $robots_value; ?></textarea>
                                </div>
                            </div>
                            <div class="form-actions fluid">
                                <div class="offset2">
                                    <button class="btn green" type="submit" name="Submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>" class="btn" name="cancel"> Cancel</a>
                                </div>
                            </div>
                        </form> 
                    <?php else: ?>
                        <?php if ($sname == 'xml_sitemap'): ?>
                            <a href="<?php echo make_admin_url('sitemap', 'list', 'list'); ?>" class="btn btn-default margin-bottom-15">
                                <i class="icon-file"></i>  Generate New Sitemap
                            </a>
                        <?php endif; ?>
                        <?php if ($sname == 'cache'): ?>
                            <a class="btn btn-default margin-bottom-15" onclick="javascript:r=confirm('Press &quot;OK&quot; if you want to proceed with deleting or press &quot;Cancel&quot; to go back.'); if(r==true){ window.location='<?php echo make_admin_url('setting', 'list', 'list','sname=cache&clearcache');?>'}">
                                <i class="icon-remove"></i> Clear All Cache
                            </a>
                        <?php endif; ?>
                        <form class="form-horizontal" action="<?php echo make_admin_url('setting', 'update', 'list'); ?>" method="POST" enctype="multipart/form-data" id="validation">
                            <?php foreach ($ob as $kk => $vv): ?>
                                <?php if($vv['key']=='SITEMAP_PATH' || $vv['key']=='LAST_SITEMAP_DATE' ): ?>
                                    <div class="form-group">
                                        <label class="control-label col-md-4"><?php echo get_setting_name($vv['title']); ?>:</label>
                                        <div class="controls col-md-8" style="padding-top: 8px;">
                                            <?php if($vv['value']!=''): ?>
                                                <code><?php echo $vv['value']; ?></code>
                                            <?php endif;?>
                                            <span class="clearfix"></span>
                                            <span class="help-block settings_help"><?php echo $vv['hint']; ?></span>
                                        </div>
                                    </div>
                                <?php else :?>
                                    <div class="form-group <?php echo $vv['key'];?>">
                                        <label class="control-label col-md-4"><?php echo get_setting_name($vv['title']); ?>:</label>
                                        <div class="controls col-md-8">
                                            <?php echo get_setting_control($vv['id'], $vv['type'], $vv['value'], $vv['options']); ?>
                                            <span class="clearfix"></span>
                                            <span class="help-block settings_help">
                                                <?php echo $vv['hint']; ?>
                                                <?php if($vv['key']=='TERMS_CONDITIONS_CHECKBOX'):?>
                                                <br/>Edit Terms and conditions page content 
                                                <a target="_blank" href="<?php echo make_admin_url('content','update','update','id='.TERMS_CONDITIONS_PAGE) ?>">here</a>.
                                                <?php endif;?>
                                            </span>
                                        </div>
                                    </div>
                                <?php endif;?>
                            <?php endforeach; ?>   
                            <div class="form-actions fluid">
                                <div class="offset2">
                                    <input  type="hidden" name="sname1" value="<?php echo $sname ?>"/>
                                    <input  type="hidden" name="setting_type" value="<?php echo $setting_type ?>"/>
                                    <button class="btn green" type="submit" name="Submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>" class="btn" name="cancel"> Cancel</a>
                                </div>
                            </div>
                        </form>
                    <?php endif; ?>
                </div>
            </div>                             
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    
<script type="text/javascript">
    function ChangeToPassField() {
        if(document.getElementById("show_password").checked==true){
            document.getElementById('password').type="text";
        }
        if(document.getElementById("show_password").checked==false){
            document.getElementById('password').type="password";
        }
    }
    
    $(document).ready(function(){
        var field_location_class = window.location.hash.replace('#','');
        if(field_location_class!=''){
            $('.'+field_location_class).attr('id','highlight');
            $('html, body').animate({
                scrollTop: $("."+field_location_class).offset().top-100
            }, 1000);
        }
    });
</script>