<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-reorder"></i> Navigation</h3>
        <ul class="page-breadcrumb breadcrumb">
            <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/breadcrumb.php') ?>
        </ul>
        <!-- END BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">New Link</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php') ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row-fluid">
                    <form class="form form-horizontal" id="validation" action="<?php // echo make_admin_url('content_navigation', 'insert', 'insert');           ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="parent_id" value="<?php echo isset($_GET['parent_id']) ? $_GET['parent_id'] : 0 ?>" />
                        <input type="hidden" name="nav_id" value="<?php echo isset($_GET['nav_id']) ? $_GET['nav_id'] : 0 ?>" />
                        <div class="form-body">
                            <div class="form-group">
                                <label class="span2 control-label" for="navigation_title">Title<span class="required">*</span></label>
                                <div class="span8">
                                    <input type="text" name="navigation_title" id="navigation_title" class="span6 form-control m-wrap validate[required]" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="span2 control-label" for="nav_class">HTML Class</label>
                                <div class="span8">
                                    <input type="text" name="nav_class" id="nav_class" class="span6 form-control m-wrap" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="span2 control-label" for="navigation_link">Select Module</label>
                                <div class="span8">
                                    <select data-trigger="module" class="span6 form-control m-wrap validate[required] chosen-select" name="navigation_link">
                                        <option value="">--Select a Module--</option>
                                        <?php foreach ($navigationPages as $_id => $_module) { ?>
                                            <option value="<?php echo $_id ?>"><?php echo ucfirst($_module[0]) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div id="ajax_output"><div class="offset2" style="font-weight: bold">Please Select a Module from Drop Down Menu...</div></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>