<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-reorder"></i> Navigation</h3>
        <ul class="page-breadcrumb breadcrumb">
            <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/breadcrumb.php') ?>
        </ul>
        <!-- END BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">New External/Blank Link</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php') ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row-fluid">
                    <form class="form form-horizontal" id="validation" action="<?php // echo make_admin_url('content_navigation', 'insert', 'insert');     ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="parent_id" value="<?php echo isset($_GET['parent_id']) ? $_GET['parent_id'] : 0 ?>" />
                        <input type="hidden" name="nav_id" value="<?php echo isset($_GET['nav_id']) ? $_GET['nav_id'] : 0 ?>" />
                        <input type="hidden" name="is_external" value="1" />
                        <div class="form-body">
                            <div class="form-group">
                                <label class="span2 control-label" for="navigation_title">Title<span class="required">*</span></label>
                                <div class="span8">
                                    <input type="text" name="navigation_title" id="navigation_title" class="span6 form-control m-wrap validate[required]" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="span2 control-label" for="nav_class">HTML Class</label>
                                <div class="span8">
                                    <input type="text" name="nav_class" id="nav_class" class="span6 form-control m-wrap" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="span2 control-label" for="navigation_link">Url:</label>
                                <div class="span8">
                                    <input class="span6 form-control" type="text" name="navigation_link" placeholder="http://" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="span2 control-label" for="open_in_new">Open in New Tab</label>
                                <div class="span8">
                                    <input class="" type="checkbox" name="open_in_new" id="open_in_new" value="1" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="span2 control-label" for="is_active">Active</label>
                                <div class="span8">
                                    <input class="" type="checkbox" name="is_active" id="is_active" value="1" checked />
                                </div>
                            </div>
                            <div class="form-actions fluid">
                                <div class="offset2">
                                    <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-ok"></i> Save</button>
                                    <a class="btn default" type="submit" href="<?php echo make_admin_url('content_navigation','list','list&nav_id='.$nav_id);?>">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>