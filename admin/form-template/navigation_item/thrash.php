<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-reorder"></i> Navigation</h3>
        <ul class="page-breadcrumb breadcrumb">
            <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/breadcrumb.php') ?>
        </ul>
        <!-- END BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Manage Trashed Items</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php') ?>
                </div>
            </div>
            <div class="portlet-body">
                <form method="post" id="form_data" name="form_data" action="<?php echo make_admin_url('content_navigation', 'update3', 'list'); ?>">
                    <div class="row-fluid">
                        <div class="span12">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th width="1%" class="readonly check">
                                            <?php if ($QueryObj->GetNumRows() != 0): ?>
                                                <input type="checkbox" name="selectall" />
                                            <?php endif; ?>
                                        </th>
                                        <th width="30%" align="left">Title</th>
                                        <th width="20%" align="center">Parent Menu</th>
                                        <th width="20%" align="center">Action</th>
                                    </tr>
                                </thead>
                                <?php if ($QueryObj->GetNumRows() != 0) { ?>
                                    <tbody>
                                        <?php $sr = 1 ?>
                                        <?php while ($navigation = $QueryObj->GetObjectFromRecord()) { ?>
                                            <tr>
                                                <td class="readonly check">
                                                    <input id="multiopt[<?php echo $navigation->id ?>]" name="multiopt[<?php echo $navigation->id ?>]" type="checkbox" /> 
                                                </td>  
                                                <td><?= $navigation->navigation_title; ?></td>
                                                <td>
                                                    <?php
                                                    $temp = new navigationItem();
                                                    $parent = $temp->getObject($navigation->parent_id);
                                                    echo isset($parent->navigation_title) ? '<a href="' . make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . $parent->id) . '&nav_id=' . $parent->nav_id . '">' . $parent->navigation_title . '</a>' : '<small style="color:grey">Main Menu Item</small>';
                                                    ?>
                                                </td>
                                                <td>
                                                    <a href="<?php echo make_admin_url('content_navigation', 'restore', 'restore', 'id=' . $navigation->id . '&parent_id=' . $navigation->parent_id . '&nav_id=' . $navigation->nav_id) ?>" class="btn btn-xs green" title="Restore Item"><i class="icon-rotate-left"></i></a>
                                                    <a class="btn btn-xs" href="<?php echo make_admin_url('content_navigation', 'permanent_delete', 'list', 'id=' . $navigation->id . '&parent_id=' . $navigation->parent_id . '&nav_id=' . $navigation->nav_id); ?>" onclick="return confirm('Are you sure? You are permanently deleting this Menu Item.');" title="Permanent Delete" ><i class="icon icon-trash"></i></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="6">
                                                <select class="form-control span2" name="multiopt_action">
                                                    <option value="restore">Restore</option>
                                                    <option value="delete">Delete</option>
                                                </select>
                                                <input type="submit" class="form-control span1 btn blue large" name="multiopt_go" value="Go" onclick="return confirm('This action might be performed on many items, which could be irreversible. Please confirm.');" />
                                            </td>
                                        </tr>
                                    </tfoot>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>