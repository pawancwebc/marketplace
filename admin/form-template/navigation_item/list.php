<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-reorder"></i> Navigation</h3>
        <ul class="page-breadcrumb breadcrumb">
            <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/breadcrumb.php') ?>
        </ul>
        <!-- END BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
//@pr($parent_info);
?>
<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Manage Menu Items</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php') ?>
                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('content_navigation', 'update2', 'list', 'parent_id=' . $parent_id . '&nav_id=' . $nav_id); ?>" method="post" id="form_data" name="form_data" >
                    <div class="row-fluid">
                        <div class="span12">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th width="1%" class="readonly check">
                                            <?php if ($QueryObj->GetNumRows() != 0): ?>
                                                <input type="checkbox" name="selectall" />
                                            <?php endif; ?>
                                        </th>
                                        <th width="30%" align="left">Title</th>
										<?php if ($parent_id == 0 || (isset($parent_info) && $parent_info->is_mega_menu && $parent_info->parent_id == 0)) { ?>
                                            <th width="15%" align="center">Sub Menu</th>
                                        <?php } ?>
                                        <th width="15%" class='text-center' align="center">Position</th>
                                        <th width="20%" align="center">Show on Website</th>
                                        <th width="20%" align="center">Action</th>
                                    </tr>
                                </thead>
                                <?php if ($QueryObj->GetNumRows() != 0) { ?>
                                    <tbody>
                                        <?php $colspan = 0 ?>
                                        <?php $colspan_calculated = FALSE ?>
                                        <?php $sr = 1 ?>
                                        <?php while ($navigation = $QueryObj->GetObjectFromRecord()) { ?>
                                            <tr>
                                                <td class="readonly check">
                                                    <input id="multiopt[<?php echo $navigation->id ?>]" name="multiopt[<?php echo $navigation->id ?>]" type="checkbox" /> 
                                                </td>
                                                <?php if ($colspan_calculated === FALSE) $colspan++; ?>
                                                <td><?= $navigation->navigation_title; ?></td>
                                                <?php if ($colspan_calculated === FALSE) $colspan++; ?>
                                                <?php if ($parent_id == 0 || (isset($parent_info) && $parent_info->is_mega_menu)) { ?>
                                                    <td width="15%" class="center" >
                                                        <a href="<?php echo make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . $navigation->id . '&nav_id=' . $navigation->nav_id); ?>" class="btn grey">Sub Menu</a>
                                                    </td>
                                                    <?php if ($colspan_calculated === FALSE) $colspan++; ?>
                                                <?php } ?>                                                
                                                <td class='text-center'>
                                                    <input module="navigation" type="number" name="position[<?php echo $navigation->id ?>]" row_id="<?php echo $navigation->id ?>" value="<?= $navigation->position; ?>" size="3" style="width:35%" class="position_update" />
                                                </td>
                                                <?php if ($colspan_calculated === FALSE) $colspan++; ?>

                                                <td class="center">
                                                    <div class="btn-group mini_buttons on_off_button" id="on_off_button<?php echo $navigation->id ?>">
                                                        <div module="navigation" class="btn status_on mini_buttons<?php echo $navigation->is_active ? ' active' : '' ?>" on="<?php echo $navigation->id ?>" rel="<?php echo $navigation->is_active ? 'on' : 'off' ?>">ON</div>
                                                        <div module="navigation" class="btn status_off mini_buttons button_right<?php echo!$navigation->is_active ? ' active' : '' ?>" off="<?php echo $navigation->id ?>" rel="<?php echo $navigation->is_active ? 'on' : 'off' ?>">OFF</div>
                                                    </div>
                                                </td>
                                                <?php if ($colspan_calculated === FALSE) $colspan++; ?>
                                                <td>
                                                    <a href="<?php echo make_admin_url('content_navigation', 'update', 'update', 'id=' . $navigation->id . '&parent_id=' . $navigation->parent_id . '&nav_id=' . $navigation->nav_id) ?>" class="btn btn-xs default" title="edit"><i class="icon-pencil"></i></a>
                                                    <a class="btn btn-xs default" href="<?php echo make_admin_url('content_navigation', 'delete', 'list', 'id=' . $navigation->id . '&parent_id=' . $navigation->parent_id . '&nav_id=' . $navigation->nav_id); ?>" onclick="return confirm('Are you sure? You are deleting this Menu Item.');" title="delete" ><i class="icon icon-trash"></i></a>
                                                </td>
                                                <?php if ($colspan_calculated === FALSE) { ?>
                                                    <?php $colspan++ ?>
                                                    <?php $colspan_calculated = TRUE ?>
                                                <?php } ?>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="<?php echo $colspan ?>">
                                                <select class="form-control span2" name="multiopt_action">
                                                    <option value="delete">Delete</option>
                                                </select>
                                                <input type="submit" class="form-control span1 btn blue large" name="multiopt_go" value="Go" onclick="return confirm('This action might be performed on many items, which could be irreversible. Please confirm.');" />
                                            </td>
                                        </tr>
                                    </tfoot>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
