<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-reorder"></i> Navigation</h3>
        <ul class="page-breadcrumb breadcrumb">
            <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/breadcrumb.php') ?>
        </ul>
        <!-- END BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Edit Item</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php') ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row-fluid">
                    <?php // pr($navigation) ?>
                    <form class="form form-horizontal" id="validation" method="post" enctype="multipart/form-data">

                        <input type="hidden" name="parent_id" value="<?php echo isset($_GET['parent_id']) ? $_GET['parent_id'] : 0 ?>" />
                        <input type="hidden" name="nav_id" value="<?php echo isset($_GET['nav_id']) ? $_GET['nav_id'] : 0 ?>" />
                        <?php if ($navigation->is_external) { ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="span2 control-label" for="navigation_title">Title<span class="required">*</span></label>
                                    <div class="span8">
                                        <input type="text" name="navigation_title" id="navigation_title" class="span6 form-control m-wrap validate[required]" value="<?php echo $navigation->navigation_title ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="span2 control-label" for="nav_class">HTML Class</label>
                                    <div class="span8">
                                        <input type="text" name="nav_class" id="nav_class" class="span6 form-control m-wrap" value="<?php echo $navigation->nav_class ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="span2 control-label" for="navigation_link">Url:</label>
                                    <div class="span8">
                                        <input class="span6 form-control" type="text" name="navigation_link" placeholder="http://" value="<?php echo $navigation->navigation_link ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="span2 control-label" for="open_in_new">Open in New Tab</label>
                                    <div class="span8">
                                        <input class="" type="checkbox" name="open_in_new" id="open_in_new" value="1"<?php echo $navigation->open_in_new ? ' checked' : '' ?> />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="span2 control-label" for="is_active">Active</label>
                                    <div class="span8">
                                        <input class="" type="checkbox" name="is_active" id="is_active" value="1"<?php echo $navigation->is_active ? ' checked' : '' ?> />
                                    </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="offset2">
                                        <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-ok"></i> Update</button>
                                        <button class="btn default" type="submit" name="cancel">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="span2 control-label" for="navigation_title">Title<span class="required">*</span></label>
                                    <div class="span8">
                                        <input type="text" name="navigation_title" id="navigation_title" class="span6 form-control m-wrap validate[required]" value="<?php echo $navigation->navigation_title ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="span2 control-label" for="nav_class">HTML Class</label>
                                    <div class="span8">
                                        <input type="text" name="nav_class" id="nav_class" class="span6 form-control m-wrap" value="<?php echo $navigation->nav_class ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="span2 control-label" for="navigation_link">Select Module</label>
                                    <div class="span8">
                                        <select data-trigger="module" class="span6 form-control m-wrap validate[required] chosen-select" name="navigation_link">
                                            <option value="">--Select a Module--</option>
                                            <?php foreach ($navigationPages as $_id => $_module) { ?>
                                                <option value="<?php echo $_id ?>"<?php echo $navigation->navigation_link == $_id ? ' selected' : '' ?>><?php echo ucfirst($_module[0]) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="ajax_output">
                                    <?php
                                    $module = $navigation->navigation_link;
                                    $nav_query = $navigation->navigation_query;
                                    if ($nav_query && strpos($nav_query, '=')) {
                                        $temp = explode("=", $nav_query);
                                        $table_id = $temp[1];
                                    }
                                    $show_submit = FALSE;
                                    if (array_key_exists($module, $navigationPages)) {
                                        $show_submit = TRUE;
                                        if ($navigationPages[$module][1] === TRUE) {
                                            switch ($module) {
                                                case 'blog':
                                                    @include_once '../include/functionClass/blogClass.php';
                                                    $blog = new blog();
                                                    $blog->listBlogs(1);
                                                    $data = array();
                                                    while ($row = $blog->GetObjectFromRecord()) {
                                                        $data[] = $row;
                                                    }
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="span2 control-label" for="navigation_query">Select Blog</label>
                                                        <div class="span8">
                                                            <select class="span6 form-control m-wrap validate[required] chosen-select" name="navigation_query">
                                                                <?php foreach ($data as $blog) { ?>
                                                                    <option value="id=<?php echo $blog->id ?>"<?php echo $blog->id == $table_id ? ' selected' : '' ?>><?php echo $blog->name ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    break;
                                                case 'product':
                                                    @include_once '../include/functionClass/productClass.php';
                                                    $object = new product();
                                                    $object->listProduct(true, '', false);
                                                    $data = array();
                                                    while ($row = $object->GetObjectFromRecord()) {
                                                        $data[] = $row;
                                                    }
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="span2 control-label" for="navigation_query">Select Product</label>
                                                        <div class="span8">
                                                            <select class="span6 form-control m-wrap chosen-select" name="navigation_query">
                                                                <?php foreach ($data as $info) { ?>
                                                                    <option value="id=<?php echo $info->id ?>"<?php echo $info->id == $table_id ? ' selected' : '' ?>><?php echo $info->name ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    break;
                                                case 'content':
                                                    @include_once '../include/functionClass/contentClass.php';
                                                    $object = new content();
                                                    $object->listPages(FALSE);
                                                    $data = array();
                                                    while ($row = $object->GetObjectFromRecord()) {
                                                        $data[] = $row;
                                                    }
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="span2 control-label" for="navigation_query">Select Content Page</label>
                                                        <div class="span8">
                                                            <select class="span6 form-control m-wrap chosen-select" name="navigation_query">
                                                                <?php foreach ($data as $info) { ?>
                                                                    <option value="id=<?php echo $info->id ?>"<?php echo $info->id == $table_id ? ' selected' : '' ?>><?php echo $info->name ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    break;
                                                case 'shop':
                                                    @include_once '../include/functionClass/categoryClass.php';
                                                    $object = new category();
                                                    $object->listAllCategories(1, '', FALSE);
                                                    $data = array();
                                                    while ($row = $object->GetObjectFromRecord()) {
                                                        $data[] = $row;
                                                    }
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="span2 control-label" for="navigation_query">Select Category Page</label>
                                                        <div class="span8">
                                                            <select class="span6 form-control m-wrap chosen-select" name="navigation_query">
                                                                <?php foreach ($data as $info) { ?>
                                                                    <option value="id=<?php echo $info->id ?>"<?php echo $info->id == $table_id ? ' selected' : '' ?>><?php echo $info->name ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    break;
                                                case 'brand':
                                                    @include_once '../include/functionClass/brandClass.php';
                                                    $object = new brand();
                                                    $object->listBrand(1, '', 1);
                                                    $data = array();
                                                    while ($row = $object->GetObjectFromRecord()) {
                                                        $data[] = $row;
                                                    }
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="span2 control-label" for="navigation_query">Select Brand</label>
                                                        <div class="span8">
                                                            <select class="span6 form-control m-wrap chosen-select" name="navigation_query">
                                                                <?php foreach ($data as $info) { ?>
                                                                    <option value="id=<?php echo $info->id ?>"<?php echo $info->id == $table_id ? ' selected' : '' ?>><?php echo $info->name ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                    }
                                    if ($show_submit === TRUE) {
                                        ?>
                                        <div class="form-group">
                                            <label class="span2 control-label" for="is_active">Active</label>
                                            <div class="span8">
                                                <input class="" type="checkbox" name="is_active" id="is_active" value="1"<?php echo $navigation->is_active == 1 ? ' checked' : '' ?> />
                                            </div>
                                        </div>
                                        <div class="form-actions fluid">
                                            <div class="offset2">
                                                <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-ok"></i> Update</button>
                                                <button class="btn default" type="submit" name="cancel">Cancel</button>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="offset2" style="font-weight: bold">Please Select a Module from Drop Down Menu...</div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>