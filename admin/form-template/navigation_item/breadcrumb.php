<li>
    <i class="icon-home"></i>
    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
    <i class="icon-angle-right"></i>
</li>                                   
<li>
    <i class="icon-reorder"></i>
    <a href="<?php echo make_admin_url('navigation', 'list', 'list'); ?>">List Navigations</a> 
    <i class="icon-angle-right"></i>
</li>
<?php if ($section == 'list' && (!isset($_GET['parent_id']) || $parent_id == 0)) { ?>
    <li class="last">
        <?php
        $main_menu = get_object('navigations', $nav_id);
        echo $main_menu->name;
        ?>
    </li>
<?php } elseif ($section == 'list' && isset($_GET['parent_id']) && $parent_id != 0) { ?>
    <li>
        <?php $main_menu = get_object('navigations', $nav_id); ?>
        <a href="<?php echo make_admin_url('content_navigation', 'list', 'list', 'nav_id=' . $nav_id); ?>"><?php echo $main_menu->name ?></a>
        <i class="icon-angle-right"></i>
    </li>
    <?php $navigation_object = get_object('navigation', $_GET['parent_id']) ?>
    <?php if ($navigation_object->parent_id != 0) { ?>
        <li class="last">... <i class="icon-angle-right"></i></li>
    <?php } ?>
    <li class="last">
        <?php echo $navigation_object->navigation_title ?>
    </li>
<?php } else { ?>
    <?php if ($section != 'thrash') { ?>
        <li>
            <?php $main_menu = get_object('navigations', $nav_id) ?>
            <a href="<?php echo make_admin_url('content_navigation', 'list', 'list', 'nav_id=' . $nav_id) ?>"><?php echo $main_menu->name ?></a>
            <i class="icon-angle-right"></i>
        </li>
    <?php } else { ?>
        <li class="last"><i class="icon-trash"></i> Trashed Navigation Items</li>
    <?php } ?>
    <?php if (($section == 'insert' || $section == 'insert_link') && isset($_GET['parent_id']) && $parent_id != 0) { ?>
        <?php $navigation_object = get_object('navigation', $_GET['parent_id']) ?>
        <?php if ($navigation_object->parent_id != 0) { ?>
            <li class="last">... <i class="icon-angle-right"></i></li>
        <?php } ?>
        <li>
            <a href="<?php echo make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . $parent_id . '&nav_id=' . $nav_id); ?>"><?php echo $navigation_object->navigation_title ?></a>
            <i class="icon-angle-right"></i>
        </li>
        <li class="last">New Link</li>
    <?php } elseif (($section == 'insert' || $section == 'insert_link') && (!isset($_GET['parent_id']) || $parent_id == 0)) { ?>
        <li class="last">New Link</li>
    <?php } elseif (($section == 'update') && isset($_GET['parent_id']) && $parent_id != 0) { ?>
        <?php $navigation_object = get_object('navigation', $_GET['parent_id']) ?>
        <?php if ($navigation_object->parent_id != 0) { ?>
            <li class="last">... <i class="icon-angle-right"></i></li>
        <?php } ?>
        <li>
            <a href="<?php echo make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . $parent_id . '&nav_id=' . $nav_id); ?>"><?php echo $navigation_object->navigation_title ?></a>
            <i class="icon-angle-right"></i>
        </li>
        <li class="last">Edit Item</li>
    <?php } elseif ($section == 'update') { ?>
        <li class="last">Edit Item</li>
    <?php } elseif ($section == 'insert_link' && isset($_GET['parent_id']) && $parent_id != 0) { ?>
        <?php $navigation_object = get_object('navigation', $_GET['parent_id']) ?>
        <?php if ($navigation_object->parent_id != 0) { ?>
            <li class="last">... <i class="icon-angle-right"></i></li>
        <?php } ?>
        <li>
            <a href="<?php echo make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . $parent_id . '&nav_id=' . $nav_id); ?>"><?php echo $navigation_object->navigation_title ?></a>
            <i class="icon-angle-right"></i>
        </li>
        <li class="last">New External/Blank Link</li>
    <?php } elseif ($section == 'insert_link') { ?>
        <li class="last">New External/Blank Link</li>
    <?php } ?>
<?php } ?>
        