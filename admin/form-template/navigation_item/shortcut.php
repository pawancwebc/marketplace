<?php if ($section != 'insert' && $section != 'insert_module' && $section != 'insert_link' && $section != 'update' && $section != 'thrash') { ?>
    <a title="Click to Add New Link" href="<?php echo make_admin_url('content_navigation', 'insert', 'insert', 'parent_id=' . $parent_id . '&nav_id=' . $nav_id); ?>" class="btn default blue-stripe"><i class="icon icon-align-justify"></i> <span class="hidden-480">New Link</span></a>
    <!--<a title="Click to Add New Module Link" href="<?php echo make_admin_url('content_navigation', 'list', 'insert_module', 'parent_id=' . $parent_id . '&nav_id=' . $nav_id); ?>" class="btn default blue-stripe"><i class="icon icon-puzzle-piece"></i> <span class="hidden-480">New Module Link</span></a>-->
    <a title="Click to Add New External Link" href="<?php echo make_admin_url('content_navigation', 'insert', 'insert_link', 'parent_id=' . $parent_id . '&nav_id=' . $nav_id); ?>" class="btn default blue-stripe"><i class="icon icon-external-link"></i> <span class="hidden-480">New External/Blank Link</span></a>
<?php } else { ?>
    <a title="Back" href="<?php echo make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . $parent_id . '&nav_id=' . $nav_id); ?>" class="btn default blue-stripe"><i class="icon icon-arrow-left"></i> <span class="hidden-480">Back</span></a>
<?php } ?>
<?php if ($section != 'thrash') { ?>
    <a title="Thrashed Navigation Items" href="<?php echo make_admin_url('content_navigation', 'thrash', 'thrash'); ?>" class="btn default blue-stripe"><i class="icon icon-trash"></i> <span class="hidden-480">Thrashed Navigation Items</span></a>
<?php } ?>
