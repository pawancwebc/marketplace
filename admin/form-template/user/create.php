<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title"><i class="icon-group"></i> Manage User</h3>
            <ul class="page-breadcrumb breadcrumb">
                    <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a>  
                            <i class="icon-angle-right"></i>
                    </li>                                  
                    <li>
                         <i class="icon-group"></i>
                         <a href="<?php echo make_admin_url('user', 'list', 'list');?>">List Users</a> 
                         <i class="icon-angle-right"></i>                                       
                    </li>
                    <li class="last">
                        New User
                    </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
       <form class="form-horizontal" action="<?php echo make_admin_url('user', 'insert', 'insert')?>" method="POST" id="validation">
              <!-- / Box -->
              <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                 <div class="portlet">
                        <div class="portlet-title">
                                <div class="caption">Add New User</div>
                                <div class="actions">
                                    <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                                </div>
                        </div>
                        <div class="portlet-body form form-body">      
                                <div class="form-group">
                                        <label class="span2 control-label" for="username">Email<span class="required">*</span></label>
                                        <div class="span8">
                                           <input type="text" name="username"  value="" id="username" class="span6 form-control validate[required,custom[email]]" />
                                        </div>
                                </div>	
                                <div class="form-group">
                                        <label class="span2 control-label" for="firstname">First Name<span class="required">*</span></label>
                                        <div class="span8">
                                           <input type="text" name="firstname"  value="" id="firstname" class="span6 form-control m-wrap validate[required]" />
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="span2 control-label" for="lastname">Last Name<span class="required">*</span></label>
                                        <div class="span8">
                                           <input type="text" name="lastname"  value="" id="lastname" class="span6 form-control validate[required]" />
                                        </div>
                                </div>	
                                <div class="form-group">
                                        <label class="span2 control-label" for="password">Password</label>
                                        <div class="span8">
                                           <input type="password" name="password"  value="" id="password" class="span6 form-control" />
                                        </div>
                                </div>	
                                <div class="form-group">
                                        <label class="span2 control-label" for="gender">Gender</label>
                                        <div class="span8">
                                           <select name="gender" id="gender" class="span6 form-control">
                                               <option value="male">Male</option>
                                               <option value="female">Female</option>
                                           </select>
                                        </div>
                                </div>	
                                <div class="form-group">
                                        <label class="span2 control-label" for="birthdate">Birth Date</label>
                                        <div class="span2">
                                           <input type="text" name="birthdate" id="birthdate" class="mask_mysql_date form-control" />
                                        </div>
                                </div>	
                                
                               	
                                <div class="form-group">
                                        <label class="span2 control-label" for="is_active">Active</label>
                                        <div class="span1">
                                           <input type="checkbox" name="is_active"  value="" id="is_active" class="form-control" />
                                        </div>
                                        
                                </div>
                                <div class="form-actions fluid">
                                    <div class="offset2">
                                         <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                         <a href="<?php echo make_admin_url('user', 'list', 'list');?>" class="btn" name="cancel"> Cancel</a>
                                    </div>
                                </div>
                  </div>
                </div>
            </div>
         </form>
         <div class="clearfix"></div>
 </div>
<div class="clearfix"></div>