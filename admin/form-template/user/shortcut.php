<?php if($action!='list' && $action!='address'):?>
<a href="<?php echo make_admin_url('user','list','list');?>" class="btn default blue-stripe">
    <i class="icon-angle-left"></i>
    <span class="hidden-480">
             Back
    </span>
</a>
<?php endif;?>
<?php if($action=='address'):?>
<a href="<?php echo make_admin_url('user','update','update','id='.$id);?>" class="btn default blue-stripe">
    <i class="icon-angle-left"></i>
    <span class="hidden-480">
             Back
    </span>
</a>
<?php endif;?>
<?php if($action=='list'):?>
<a href="<?php echo make_admin_url('user','insert','insert');?>" class="btn default blue-stripe">
    <i class="icon-plus"></i>
    <span class="hidden-480">
             Add User
    </span>
</a>
<?php endif;?>
