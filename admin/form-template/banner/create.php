<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-film"></i> Manage Banners</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a>  
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-film"></i>
                <a href="<?php echo make_admin_url('banner', 'list', 'list'); ?>">List Banners</a> 
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                New Banner
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form class="form-horizontal" action="<?php echo make_admin_url('banner', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">
        <!-- / Box -->
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Add New Banner</div>
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">      
                    <div class="form-group">
                        <label class="span2 control-label" for="name">Name<span class="required">*</span></label>
                        <div class="span4">
                            <input type="text" name="name"  value="" id="name" class="form-control validate[required]" />
                        </div>
                    </div>
                    <!--
                    <div class="form-group">
                            <label class="span2 control-label" for="code">Code</label>
                            <div class="span4">
                               <input type="text" name="code"  value="" id="code" class="form-control" />
                               <span class="help-block">unique code for each banner should be used (without spaces and special chars).</span>
                            </div>
                    </div>	
                    -->
                    <div class="form-group">
                        <label class="span2 control-label" for="url">Redirect Link</label>
                        <div class="span5">
                            <input type="text" name="url"  value="" id="url" class="form-control " />
                            <span class="help-block">full url on which user will be redirected after clicking on the banner.</span>
                        </div>
                    </div>	
 


                    <div class="form-group">
                        <label class="span2 control-label" for="is_active">Status</label>
                        <div class="span6">
                            <div class="radio-list">
                                <label class="radio-inline">
                                    <input type="radio" name="is_active" id="is_active" value="1"/> Publish </label>
                                <label class="radio-inline">
                                    <input type="radio" name="is_active" id="is_active" value="2"/> Pending Review </label>
                                <label class="radio-inline">
                                    <input type="radio" name="is_active" id="is_active" value="0" checked/> Draft </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="span2 control-label" for="open_in_new_tab">Open in new window</label>
                        <div class="span4">
                            <input type="checkbox" name="open_in_new_tab"  value="" id="open_in_new_tab" class="form-control iPhoneStyle" />
                        </div>
                    </div>
               	
					<div class="form-group">
                            <label class="span2 control-label" for="banner_title">Banner Title</label>
                            <div class="span4">
                               <input type="text" name="banner_title"  value="" id="banner_title" class="form-control" />
                               <span class="help-block">Banner Title will show on slider other then banner text detail.</span>
                            </div>
                    </div>	
					
                    <div class="form-group">
                        <label class="span2 control-label" for="text">Banner Text</label>
                        <div class="span4">
                            <textarea id="text" class="form-control" name="text" rows="3"></textarea>
                            <span class="help-block">text appears on banner if image not uploaded</span>
                        </div>
                    </div> 
           
                    <div class="form-group">
                        <label class=" span2 control-label">Image Upload</label>
                        <div class="span6">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="<?php echo DIR_WS_SITE_ADMIN . 'assets/img/noimage.gif' ?>" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                    <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                        <span class="fileupload-exists">Change</span>
                                        <input type="file" class="default" name="image"/></span>
                                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                            <!--<span class="help-block">if image will be uploaded then banner text and background color will not used.</span>-->
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="span2 control-label" for="image_alt_text">Image Alt Text</label>
                        <div class="span8">
                            <input type="text" style="width: 200px;" name="image_alt_text" title="Image Alt Text" placeholder="Image Alt Text"  value="" id="image_alt_text" class="form-control" />
                        </div>
                    </div>
     
                    <div class="form-actions fluid">
                        <div class="offset2">
                            <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                            <a href="<?php echo make_admin_url('banner', 'list', 'list'); ?>" class="btn" name="cancel"> Cancel</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>