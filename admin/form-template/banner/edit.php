<script type="text/javascript">
    jQuery(".edit_location").live("click", function () {
        var bid = $(this).attr("rel");
        jQuery(document.body).addClass('model-open');
        $("#edit_location_div").html('<div style="width:100%;text-align:center;padding-top:5px;"><br/><br/><img src="assets/img/ajax-loading.gif"/><br/><br/>Please Wait...<br/><br/><br/></div>');
        var dataString = 'bid=' + bid;

        $.ajax({
            type: "POST",
            url: "<?php echo make_admin_url_window('editbannerlocation'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                $("#edit_location_div").show();
                $("#edit_location_div").html(data);
                $("#edit_location_div").fadeIn(1500);
                App.init();
                FormComponents.init();
                FormSamples.init();
                ComponentsFormTools.init();
                ComponentsPickers.init();
            }
        });
    });
</script>
<div id="select_categories"  class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Select Categories</h4>
            </div>
            <form method="post" name="product_group_set" action="" id="validate">
                <div class="modal-body" id="model_category_banner">
                    <div class="row-fluid">  
                        <?php if (!empty($categories)): ?>
                            <table class="table" id="sample_3">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Category</th>
                                        <th>Status</th>
                                        <th>Products</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sr = 1;
                                    foreach ($categories as $kk => $vv):
                                        $QueryObj = new category();
                                        $categories = $QueryObj->listRecursiveCategoryTable($vv->id, 1, true);
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit_categories" data-dismiss="modal" name="submit_group_set" class="btn blue"><i class="icon-ok"></i> Add Categories & close</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="select_mega_menus"  class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Select Mega Menus</h4>
            </div>
            <form method="post" name="product_group_set" action="" id="validate">
                <div class="modal-body" id="model_category_banner">
                    <div class="row-fluid">  
                        <?php if (!empty($all_mega_menus)): ?>
                            <table class="table" id="sample_3">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Menu Title</th>
                                        <th>Navigation Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($all_mega_menus as $mega_menu) { ?>
                                        <tr>
                                            <td class="hidden-480"> 
                                                <input class="checkboxes mega_menusCheckBox" rel="<?php echo $mega_menu->id ?>" id="multiopt[<?php echo $mega_menu->id ?>]" name="<?php echo $mega_menu->navigation_title; ?>" type="checkbox" />
                                            </td>
                                            <td><?php echo $mega_menu->navigation_title ?></td>
                                            <td><?php echo $mega_menu->nav_id->name ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit_mega_menus" data-dismiss="modal" name="submit_group_set" class="btn blue"><i class="icon-ok"></i> Add Menus & close</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="clearfix"></div>
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-film"></i> Manage Banners</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a>  
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-film"></i>
                <a href="<?php echo make_admin_url('banner', 'list', 'list'); ?>">List Banners</a> 
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                Edit Banner
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <!-- / Box -->
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Edit Banner</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>
            </div>
            <div class="portlet-body form form-body">  
                <div class="tabbable-custom ">
                    <ul class="nav nav-tabs ">
                        <li class="active"><a href="#tab1" data-toggle="tab">Banner Information</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active">
                           
                            <form class="form-horizontal" action="<?php echo make_admin_url('banner', 'update', 'update', 'id=' . $id) ?>" method="POST" enctype="multipart/form-data" id="validation">
                                <div class="form-group">
                                    <label class="span2 control-label" for="name">Name<span class="required">*</span></label>
                                    <div class="span4">
                                        <input type="text" name="name"  value="<?php echo $values->name; ?>" id="name" class="form-control validate[required]" />
                                    </div>
                                </div>
                              
                                <div class="form-group">
                                    <label class="span2 control-label" for="url">Redirect Link</label>
                                    <div class="span5">
                                        <input type="text" name="url"  value="<?php echo $values->url; ?>" id="url" class="form-control " />
                                        <span class="help-block">full url on which user will be redirected after clicking on the banner.</span>
                                    </div>
                                </div>	

                                <div class="form-group">
                                    <label class="span2 control-label" for="is_active">Status</label>
                                    <div class="span6">
                                        <div class="radio-list">
                                            <label class="radio-inline">
                                                <input type="radio" name="is_active" id="is_active" <?php echo ($values->is_active == 1) ? "checked" : ""; ?> value="1"/> Publish </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="is_active" id="is_active" <?php echo ($values->is_active == 2) ? "checked" : ""; ?> value="2"/> Pending Review </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="is_active" id="is_active" <?php echo ($values->is_active == 0) ? "checked" : ""; ?> value="0"/> Draft </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="span2 control-label" for="open_in_new_tab">Open in new window</label>
                                    <div class="span1">
                                        <input type="checkbox" name="open_in_new_tab" id="open_in_new_tab" <?php echo ($values->open_in_new_tab) ? "checked" : ""; ?> class="form-control iPhoneStyle" />
                                    </div>
                                </div>
                              
								<div class="form-group">
										<label class="span2 control-label" for="banner_title">Banner Title</label>
										<div class="span4">
										   <input type="text" name="banner_title"  value="<?php echo $values->banner_title; ?>" id="banner_title" class="form-control" />
										   <span class="help-block">Banner Title will show on slider other then banner text detail.</span>
										</div>
								</div>	
					
                                <div class="form-group">
                                    <label class="span2 control-label" for="text">Banner Text</label>
                                    <div class="span4">
                                        <textarea id="text" class="form-control" name="text" rows="3"><?php echo $values->text; ?></textarea>
                                        <span class="help-block">text appears on banner if image not uploaded</span>
                                    </div>
                                </div> 
                      
                                <div class="form-group">
                                    <label class=" span2 control-label">Image Upload</label>
                                    <div class="span6 item">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <?php if ($values->image != ''): ?>
                                                    <?php if (file_exists(DIR_FS_SITE_UPLOAD . '/photo/banner/large/' . $values->image)): ?>
                                                        <a class="fancybox-button" data-rel="fancybox-button" href="<?php echo DIR_WS_SITE_UPLOAD . '/photo/banner/large/' . $values->image ?>">
                                                            <div class="zoom">
                                                                <img style="width: 190px;" src="<?php echo DIR_WS_SITE_UPLOAD . '/photo/banner/large/' . $values->image ?>" alt="<?php echo $values->image; ?>" />
                                                                <div class="zoom-icon"></div>
                                                            </div>
                                                        </a>
                                                        <div class="details">
                                                            <a href="<?php echo make_admin_url('banner', 'delete_image', 'delete_image', 'id=' . $values->id); ?>" onclick="return confirm('Image shall be permanently deleted. Are you sure?');" class="icon" style="color:#fff;">
                                                                <i class="icon-remove"></i> Remove
                                                            </a>    
                                                        </div>
                                                    <?php else: ?>
                                                        <img src="<?php echo DIR_WS_SITE_ADMIN . 'assets/img/noimage.gif' ?>" alt="" />
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <img src="<?php echo DIR_WS_SITE_ADMIN . 'assets/img/noimage.gif' ?>" alt="" />
                                                <?php endif; ?>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                    <span class="fileupload-exists">Change</span>
                                                    <input type="file" class="default" name="image"/></span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="span2 control-label" for="image_alt_text">Image Alt Text</label>
                                    <div class="span8">
                                        <input type="text" style="width: 200px;" name="image_alt_text" title="Image Alt Text" placeholder="Image Alt Text"  value="<?php echo $values->image_alt_text; ?>" id="image_alt_text" class="form-control" />
                                    </div>
                                </div>
                               
                                <div class="form-actions fluid">
                                    <div class="offset2">
                                        <input type="hidden" name="id" value="<?php echo $values->id; ?>"/>
                                        <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                                        <button class="btn blue" type="submit" name="submit" value="SubmitClose"><i class="icon-ok"></i> Save and Close</button> 
                                        <a href="<?php echo make_admin_url('banner', 'list', 'list'); ?>" class="btn" name="cancel"> Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!--update form div---->
    <div id="edit_location_div"  class="modal fade" role="dialog" aria-hidden="true"></div><!--ajax response in this div-->
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<script src="assets/scripts/banner_locations.js" type="text/javascript"></script>