<script type="text/javascript">
jQuery(".edit_location").live("click", function(){
    var bid=$(this).attr("rel");
    jQuery(document.body).addClass('model-open');
    $("#edit_location_div").html('<div style="width:100%;text-align:center;padding-top:5px;"><br/><br/><img src="assets/img/ajax-loading.gif"/><br/><br/>Please Wait...<br/><br/><br/></div>');
    var dataString = 'bid='+bid;

    $.ajax({
        type: "POST",
        url: "<?php echo make_admin_url_window('editbannerlocation');?>",
        data: dataString,
        success: function(data, textStatus) {
           $("#edit_location_div").show();
           $("#edit_location_div").html(data);
           $("#edit_location_div").fadeIn(1500);
           App.init();
           FormComponents.init();
           FormSamples.init();
           ComponentsFormTools.init();
           ComponentsPickers.init();
        }
    });
});
</script>
 <div id="select_categories"  class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">  
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Select Categories</h4>
            </div>
           <form method="post" name="product_group_set" action="" id="validate">
            <div class="modal-body" id="model_category_banner" >
                <div class="row-fluid">  
                    <?php if(!empty($categories)): ?>
                    <table class="table" id="sample_3">
                        <thead>
                             <tr>
                                <th></th>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Products</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $sr=1; foreach($categories as $kk=>$vv): ?>
                            <?php
                                $QueryObj= new category();
                                $categories = $QueryObj->listRecursiveCategoryTable($vv->id,1,true);
                            ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php endif;?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit_categories" data-dismiss="modal" name="submit_group_set" class="btn blue"><i class="icon-ok"></i> Add Categories & close</button>
            </div>
          </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
 <div class="clearfix"></div>
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title"><i class="icon-film"></i> Manage Banners</h3>
            <ul class="page-breadcrumb breadcrumb">
                    <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a>  
                            <i class="icon-angle-right"></i>
                    </li>                                  
                    <li>
                        <i class="icon-film"></i>
                               <a href="<?php echo make_admin_url('banner', 'list', 'list');?>">List Banners</a> 
                         <i class="icon-angle-right"></i>                                       
                    </li>
                    <li class="last">
                        Edit Banner
                    </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
              <!-- / Box -->
              <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                 <div class="portlet">
                        <div class="portlet-title">
                                <div class="caption">Edit Banner</div>
                                <div class="actions">
                                    <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                                </div>
                        </div>
                        <div class="portlet-body form form-body">  
                            <div class="tabbable-custom ">
                                <ul class="nav nav-tabs ">
                                        <li class="active">
                                                <a href="#tab1" data-toggle="tab">
                                                         Banner Information
                                                </a>
                                        </li>
                                        <li>
                                                <a href="#tab2" data-toggle="tab">
                                                         Banner Locations
                                                </a>
                                        </li>
                                </ul>
                                        <div class="tab-content">
                                                <div class="tab-pane active" id="tab1">
                                                    <div class="note note-info">
                                                        <p>Banner Hits : <?php echo $values->hits;?></p>
                                                        <p>Banner Views : <?php echo $values->views;?></p>
                                                    </div>
                                                    <form class="form-horizontal" action="<?php echo make_admin_url('banner', 'update', 'update','id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                                                            <div class="form-group">
                                                                    <label class="span2 control-label" for="name">Name<span class="required">*</span></label>
                                                                    <div class="span4">
                                                                       <input type="text" name="name"  value="<?php echo $values->name;?>" id="name" class="form-control validate[required]" />
                                                                    </div>
                                                            </div>
                                                            <!--
                                                            <div class="form-group">
                                                                    <label class="span2 control-label" for="code">Code</label>
                                                                    <div class="span4">
                                                                       <input type="text" name="code"  value="<?php echo $values->code;?>" id="code" class="form-control" />
                                                                       <span class="help-block">unique code for each banner should be used (without spaces and special chars).</span>
                                                                    </div>
                                                            </div>
                                                            -->
                                                            <div class="form-group">
                                                                    <label class="span2 control-label" for="url">Redirect Link</label>
                                                                    <div class="span5">
                                                                       <input type="text" name="url"  value="<?php echo $values->url;?>" id="url" class="form-control validate[required]" />
                                                                       <span class="help-block">full url on which user will be redirected after clicking on the banner.</span>
                                                                    </div>
                                                            </div>	
                                                            <div class="form-group">
                                                                    <label class="control-label span2">Valid Range</label>
                                                                    <div class="span5">
                                                                            <div class="input-group input-xlarge">
                                                                                    <input type="text" readonly class="form-control form_datetime_sql" name="from_date" value="<?php echo ($values->from_date=='0000-00-00 00:00:00')?date("Y-m-d H:i:s"):$values->from_date;?>"/>
                                                                                    <span class="input-group-addon">
                                                                                             to
                                                                                    </span>
                                                                                    <input type="text" readonly class="form-control form_datetime_sql" name="to_date" value="<?php echo ($values->to_date=='0000-00-00 00:00:00')?date("Y-m-d H:i:s" ,strtotime("+1 month")):$values->to_date;?>"/>
                                                                            </div>
                                                                            <!-- /input-group -->
                                                                            <span class="help-block">
                                                                                     date time range for which banner can be applied, default "One month"
                                                                            </span>
                                                                    </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="span2 control-label" for="is_active">Status</label>
                                                                <div class="span6">
                                                                    <div class="radio-list">
                                                                        <label class="radio-inline">
                                                                        <input type="radio" name="is_active" id="is_active" <?php echo ($values->is_active==1)?"checked":"";?> value="1"/> Publish </label>
                                                                        <label class="radio-inline">
                                                                        <input type="radio" name="is_active" id="is_active" <?php echo ($values->is_active==2)?"checked":"";?> value="2"/> Pending Review </label>
                                                                        <label class="radio-inline">
                                                                        <input type="radio" name="is_active" id="is_active" <?php echo ($values->is_active==0)?"checked":"";?> value="0"/> Draft </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                    <label class="span2 control-label" for="open_in_new_tab">Open in new window</label>
                                                                    <div class="span1">
                                                                       <input type="checkbox" name="open_in_new_tab" id="open_in_new_tab" <?php echo ($values->open_in_new_tab)?"checked":"";?> class="form-control iPhoneStyle" />
                                                                    </div>
                                                            </div>
                                                            <div class="form-group">
                                                                    <label class="span2 control-label" for="color">Background Color</label>
                                                                    <div class="span3">
                                                                       <input type="text" name="color"  value="<?php echo $values->color;?>" id="color" class="colorpicker-default form-control" />
                                                                       <span class="help-block">background color for banner. (i.e. #000000)</span>
                                                                    </div>
                                                            </div>	
                                                            <div class="form-group">
                                                                    <label class="span2 control-label" for="text">Banner Text</label>
                                                                    <div class="span4">
                                                                        <textarea id="text" class="form-control" name="text" rows="3"><?php echo $values->text;?></textarea>
                                                                        <span class="help-block">text appears on banner if image not uploaded</span>
                                                                    </div>
                                                             </div> 
                                                             <div class="form-group">
                                                                    <label class="span2 control-label" for=""></label>
                                                                    <div class="span4">
                                                                        <strong class="offset4">OR</strong>
                                                                        <span class="help-block">(either background image or background color and text can be used.)</span>
                                                                    </div>
                                                             </div> 
                                                             <div class="form-group">
                                                                <label class=" span2 control-label">Image Upload</label>
                                                                <div class="span6 item">
                                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                                                <?php if($values->image!=''): ?>
                                                                                    <?php if(file_exists(DIR_FS_SITE_UPLOAD.'/photo/banner/large/'.$values->image)): ?>
                                                                                        <a class="fancybox-button" data-rel="fancybox-button" href="<?php echo DIR_WS_SITE_UPLOAD.'/photo/banner/large/'.$values->image ?>">
                                                                                            <div class="zoom">
                                                                                            <img style="width: 190px;" src="<?php echo DIR_WS_SITE_UPLOAD.'/photo/banner/large/'.$values->image ?>" alt="<?php echo $values->image;?>" />
                                                                                            <div class="zoom-icon"></div>
                                                                                            </div>
                                                                                        </a>
                                                                                        <div class="details">
                                                                                            <a href="<?php echo make_admin_url('banner', 'delete_image', 'delete_image', 'id='.$values->id);?>" onclick="return confirm('Image shall be permanently deleted. Are you sure?');" class="icon" style="color:#fff;">
                                                                                                <i class="icon-remove"></i> Remove
                                                                                            </a>    
                                                                                        </div>
                                                                                   <?php else: ?>
                                                                                        <img src="<?php echo DIR_WS_SITE_ADMIN.'assets/img/noimage.gif' ?>" alt="" />
                                                                                    <?php endif; ?>
                                                                               <?php else: ?>
                                                                                    <img src="<?php echo DIR_WS_SITE_ADMIN.'assets/img/noimage.gif' ?>" alt="" />
                                                                               <?php endif; ?>
                                                                        </div>
                                                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                        <div>
                                                                                <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                                                <span class="fileupload-exists">Change</span>
                                                                                <input type="file" class="default" name="image"/></span>
                                                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                                        </div>
                                                                        <div class="clearfix push-down-10 margin-bottom-10"></div>
                                                                        <div class="offset2"><strong>or</strong></div>
                                                                        <div class="clearfix push-down-10 margin-bottom-10"></div>
                                                                        <input type="text" name="image_url" placeholder="Image URL.." class="form-control" title="Image URL"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="span2 control-label" for="image_alt_text">Image Alt Text</label>
                                                                <div class="span8">
                                                                <input type="text" style="width: 200px;" name="image_alt_text" title="Image Alt Text" placeholder="Image Alt Text"  value="<?php echo $values->image_alt_text;?>" id="image_alt_text" class="form-control" />
                                                                </div>
                                                            </div>
                                                             <div class="form-group">
                                                                    <label class="span2 control-label" for="description">Description</label>
                                                                    <div class="span4">
                                                                        <textarea id="description" class=" form-control" name="description" rows="3"><?php echo $values->description;?></textarea>
                                                                        <span class="help-block">for admin use only</span>
                                                                    </div>
                                                             </div>  
                                                            <div class="form-actions fluid">
                                                                <div class="offset2">
                                                                     <input type="hidden" name="id" value="<?php echo $values->id;?>"/>
                                                                     <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                                                                     <button class="btn blue" type="submit" name="submit" value="SubmitClose"><i class="icon-ok"></i> Save and Close</button> 
                                                                     <a href="<?php echo make_admin_url('banner', 'list', 'list');?>" class="btn" name="cancel"> Cancel</a>
                                                                </div>
                                                            </div>
                                                    </form>
                                                </div>
                                                <div class="tab-pane" id="tab2">
                                                     <form class="form-horizontal" action="<?php echo make_admin_url('banner', 'update', 'update','id='.$id)?>" method="POST" enctype="multipart/form-data" id="banner_location">
                                                        <table class="table table-striped table-bordered table-hover" id="product_images">
                                                            <thead>
                                                                 <tr class="heading">
                                                                    <th>Module</th>
                                                                    <th>Use Banner Date/Time</th>
                                                                    <th>Time Frame</th>
                                                                    <th>Position</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="odd gradeX">
                                                                        <td>
                                                                            <select name="module" id="module" class="form-control">
                                                                            <?php foreach($banner_pages_array as $k=>$v):?>
                                                                               <option value="<?php echo $k;?>"><?php echo ucwords($v);?></option>
                                                                            <?php endforeach; ?>
                                                                            </select>
                                                                            <div class="clearfix margin-bottom-5"></div>
                                                                            <div id="select_categoires_div" style="display:none;">
                                                                                <a data-toggle="modal" data-target="#select_categories" class="btn btn-sm blue-stripe">
                                                                                    <i class="icon-plus"></i> Select categories
                                                                                </a>                                                                                
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                            <div id="selected_categoires_div" style="display:none;">
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <input type="checkbox" name="same_date_time" id="same_date_time" class="form-control" checked/>
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-group input-large">
                                                                                <input type="text" disabled="disabled" readonly class="form-control form_datetime_sql" id="location_from_date" name="from_date" value="<?php echo ($values->from_date=='0000-00-00 00:00:00')?date("Y-m-d H:i:s"):$values->from_date;?>"/>
                                                                                <span class="input-group-addon">
                                                                                         to
                                                                                </span>
                                                                                <input type="text" disabled="disabled" readonly class="form-control form_datetime_sql" id="location_to_date" name="to_date" value="<?php echo ($values->to_date=='0000-00-00 00:00:00')?date("Y-m-d H:i:s" ,strtotime("+1 day")):$values->to_date;?>"/>
                                                                            </div>
                                                                       </td>
                                                                       <td>
                                                                            <select name="position" id="position" class="form-control">
                                                                                <?php foreach($banner_locations_on_each_page as $kk=>$vv):?>
                                                                                    <option value="<?php echo $kk;?>"><?php echo str_replace('_',' ',$kk);?></option>
                                                                                <?php endforeach;?>
                                                                            </select>
                                                                       </td>
                                                                    <td>
                                                                         <input type="hidden" value="<?php echo $id;?>" name="banner_id"/>
                                                                         <input type="submit" class="btn blue" name="submit_locations" value="Add New"/>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>          
                                                    </form>
                                                    <div class="clearfix margin-bottom-10"></div>
                                                   <table class="table table-striped table-bordered table-hover" id="product_images">
                                                    <thead>
                                                         <tr class="heading">
                                                            <th>Module</th>
                                                            <th>From Date</th>
                                                            <th>To Date</th>
                                                            <th>Position</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php $sr=1;foreach($locations_array as $ak=>$av):?>
                                                            <tr class="odd gradeX">
                                                                    <td>
                                                                         <a class="edit_location" rel="<?php echo $av->id;?>" data-toggle="modal" href="#edit_location_div" >
                                                                         <?php echo ucwords($banner_pages_array[$av->type]);?>
                                                                         <?php if($av->type=='shop'): ?>
                                                                            (<?php echo $av->name;?>)
                                                                         <?php endif;?>
                                                                         </a>
                                                                    </td>
                                                                    <td>
                                                                        <a class="edit_location" rel="<?php echo $av->id;?>" data-toggle="modal" href="#edit_location_div" >
                                                                         <?php echo $av->from_date;?>
                                                                        </a>
                                                                    </td>
                                                                    <td>
                                                                        <a class="edit_location" rel="<?php echo $av->id;?>" data-toggle="modal" href="#edit_location_div" >
                                                                         <?php echo $av->to_date;?>
                                                                        </a>
                                                                    </td>
                                                                    <td>
                                                                        <a class="edit_location" rel="<?php echo $av->id;?>" data-toggle="modal" href="#edit_location_div" >
                                                                         <?php echo str_replace('_', ' ', $av->position);?>
                                                                        </a>
                                                                    </td>
                                                                    <td style="vertical-align:middle;">
                                                                        <a class="btn btn-xs default edit_location" rel="<?php echo $av->id;?>" data-toggle="modal" href="#edit_location_div" title="click here to edit" ><i class="icon-pencil"></i></a>                                                     
                                                                        <a href="<?php echo make_admin_url('banner', 'delete_banner_location', 'update','id='.$id.'&bid='.$av->id)?>" title="click here to delete this relation" class="btn btn-xs default"><i class="icon-remove"></i></a>
                                                                    </td>
                                                            </tr>
                                                        <?php $sr++; endforeach;?>
                                                    </tbody>
                                                </table>
                                                </div>
                                        </div>
                                </div>
                       
                  </div>
                </div>
            </div>
        <div class="clearfix"></div>
        <!--update form div---->
        <div id="edit_location_div"  class="modal fade" role="dialog" aria-hidden="true"></div><!--ajax response in this div-->
        <div class="clearfix"></div>
 </div>
<div class="clearfix"></div>
<script src="assets/scripts/banner_locations.js" type="text/javascript"></script>