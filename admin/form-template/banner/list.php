<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-film"></i> Manage Banners</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                            List Banners
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Banners</div>
                <div class="actions">
                   <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('banner', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >	
                      <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                 <tr>
                                    <th style="width:1%;" class="hidden-480"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th>
                                    <th>Name</th>
									<th>Thumb</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <?php if($QueryObj->GetNumRows()!=0):?>
                                <tbody>
                                <?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):
									$image_obj = new imageManipulation();?>                                    
                                    <tr class="odd gradeX">
                                        <a href="<?php echo make_admin_url('banner', 'update', 'update', 'id='.$object->id)?>" >
                                            <td class="hidden-480">
                                                <input class="checkboxes" id="multiopt[<?php echo $object->id ?>]" name="multiopt[<?php echo $object->id ?>]" type="checkbox" />
                                            </td>
                                            <td class=" clickable"><?php echo $object->name;?></td>
                                            <td class=""><?php if($object->image): echo "<img src='"; $image_obj->get_image('banner', 'thumb', $object->image); echo "'/>"; endif;?></td>
                                            <td class="center">
                                                <select module="banner" name="is_active[<?php echo $object->id ?>]" row_id="<?php echo $object->id ?>" class="form-control status_change ">
                                                    <option <?php echo ($object->is_active==1)?"selected":"";?> value="1"/>Publish</option>
                                                    <option <?php echo ($object->is_active==2)?"selected":"";?> value="2"/>Pending Review</option>
                                                    <option <?php echo ($object->is_active==0)?"selected":"";?> value="0"/>Draft</option>
                                                </select>
                                            </td>
                                            <td>
                                                <a id="do_action" href="<?php echo make_admin_url('banner', 'update', 'update', 'id='.$object->id)?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i></a>
                                                <a href="<?php echo make_admin_url('banner', 'delete', 'list', 'id='.$object->id)?>" onclick="return confirm('Are you sure? You are deleting this record.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i></a>
                                            </td>
                                        </a>
                                    </tr>
                                <?php $sr++; endwhile;?>
                                </tbody>
                                <tfoot>
                                    <tr class="odd gradeX hidden-480">
                                        <td colspan="3">
                                            <div style=" width:220px;float:left">
                                                <select name="multiopt_action" style="width:150px;float: left;" class="left_align form-control">
                                                    <option value="delete">Delete</option>
                                                </select>
                                                <input style="float:right" type="submit" class="btn blue large" name="multiopt_go" value="Go"  onclick="return confirm('Are you sure?');" />
                                            </div>
                                        </td><td></td> <td></td>                                        
                                    </tr> 
                                </tfoot>
                           <?php endif;?>  
                        </table>
                </form>    
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>