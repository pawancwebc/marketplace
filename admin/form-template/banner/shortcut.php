<?php if($action!='list' && $action!='location'):?>
<a href="<?php echo make_admin_url('banner','list','list');?>" class="btn default blue-stripe">
    <i class="icon-angle-left"></i>
    <span class="hidden-480">
             Back
    </span>
</a>
<?php endif;?>
<?php if($action=='list'):?>
<a href="<?php echo make_admin_url('banner','insert','insert');?>" class="btn default blue-stripe">
    <i class="icon-plus"></i>
    <span class="hidden-480">
             Add Banner
    </span>
</a>
<?php endif;?>
<?php if($action=='location'):?>
<a href="<?php echo make_admin_url('banner','update','update','id='.$id);?>" class="btn default blue-stripe">
    <i class="icon-angle-left"></i>
    <span class="hidden-480">
             Back
    </span>
</a>
<?php endif;?>