<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title"><i class="icon-krw"></i> URL Rewrite Management</h3>
            <ul class="page-breadcrumb breadcrumb">
                    <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a>  
                            <i class="icon-angle-right"></i>
                    </li>                                  
                    <li>
                        <i class="icon-flag-alt"></i>
                               <a href="<?php echo make_admin_url('urlrewrite', 'list', 'list');?>">URL Rewrites</a> 
                         <i class="icon-angle-right"></i>                                       
                    </li>
                    <li class="last">
                        Edit URL
                    </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
       <form class="form-horizontal" action="<?php echo make_admin_url('urlrewrite', 'update', 'update','id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
              <!-- / Box -->
              <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                 <div class="portlet">
                        <div class="portlet-title">
                                <div class="caption">Edit URL</div>
                                <div class="actions"> 
                                    <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                                </div>
                        </div>
                        <div class="portlet-body form form-body"> 
                            <?php if(!URL_REWRITE):?>
                            <div class="note note-danger">
                                <p>Please switch on the URL rewrites<br/>
                               <a href="<?php echo make_admin_url('setting','list','list','sname=seo').'#URL_REWRITE';?>">Click here to enable.</a></p>
                            </div>
                            <?php endif;?>
                                <div class="form-group">
                                        <label class="span2 control-label" for="name">Module <span class="required">*</span></label>
                                        <div class="span4">
                                           <select disabled class="form-control">
                                                <?php foreach ($modules_for_url_rewrite as $kk=>$vv): ?>
                                                        <option value="<?php echo $vv;?>" <?php echo ($values->module==$vv)?"selected":"";?>><?php echo ucwords($kk);?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="span2 control-label" for="name">Target Path <span class="required">*</span></label>
                                        <div class="span4">
                                           <input type="text" readonly value="<?php echo $values->module.'/'.$values->module_id;?>" class="form-control" />
                                        </div>
                                </div> 
                                <div class="form-group">
                                        <label class="span2 control-label" for="urlname">Urlname<span class="required">*</span></label>
                                        <div class="span4">
                                           <input type="text" name="urlname"  value="<?php echo $values->urlname;?>" id="urlname" class="form-control validate[required]" />
                                           <div class="clearfix"></div>
                                           <span class="help-block"><?php echo make_url($values->page,'id='.$values->module_id); ?></span>
                                        </div>
                                </div>     
                                <div class="form-actions fluid">
                                    <div class="offset2">
                                         <input type="hidden" value="<?php echo $id;?>" name="id"/>
                                         <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                                         <button class="btn blue" type="submit" name="submit" value="SubmitClose"><i class="icon-ok"></i> Save and Close</button> 
                                         <a href="<?php echo make_admin_url('urlrewrite', 'list', 'list');?>" class="btn" name="cancel"> Cancel</a>
                                    </div>
                                </div>
                  </div>
                </div>
            </div>
         </form>
         <div class="clearfix"></div>
 </div>
<div class="clearfix"></div>