<?php if($action!='list'):?>
<a href="<?php echo make_admin_url('urlrewrite','list','list','type='.$values->module);?>" class="btn default blue-stripe">
    <i class="icon-angle-left"></i>
    <span class="hidden-480">
             Back
    </span>
</a>
<?php endif;?>
<?php if($action=='list'):?>
<a href="<?php echo make_admin_url('setting','list','list','sname=url_slug');?>" class="btn default blue-stripe">
    <i class="icon-angle-left"></i>
    <span class="hidden-480">
             URL Slug for default pages
    </span>
</a>
<?php endif;?>