<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-krw"></i> URL Rewrite Management</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                            URL Rewrites
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    URL Rewrites
                </div>
                <div class="actions">
                  
                </div>
            </div>
            <div class="well">
                <form method="post" id="validate1" action="<?php echo make_admin_url('order','update','update','id='.$id);?>" name="change_current_state">
                     <label class="span2">Select Module</label>
                    <div class="span3">
                    <select class="form-control" name="type" id="type_url_rewrite">
                        <?php foreach ($modules_for_url_rewrite as $kk=>$vv): ?>
                                <option value="<?php echo $vv;?>" <?php echo ($type==$vv)?"selected":"";?>><?php echo ucwords($kk);?></option>
                        <?php endforeach;?>
                        <input type="hidden" disabled id="url_page_base_url" value="<?php echo make_admin_url('urlrewrite','list','list','type=');?>"/>
                    </select>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix margin-bottom-20"></div>
            <div class="portlet-body">
               <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                         <tr>
                            <th>ID</th>
                            <th><?php echo ucwords($type);?></th>
                            <th>urlname</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                        <?php if(!empty($urlrewrites)):?>
                            <tbody>
                            <?php $sr=1;foreach($urlrewrites as $kk=>$vv):?>
                                <tr class="odd gradeX">
                                        <td class="clickable"><?php echo $vv->module_id;?></td>
                                        <td class="clickable"><?php echo $vv->name;?></td>
                                        <td class="clickable"><?php echo $vv->urlname;?></td>
                                        <td>
                                            <a id="do_action" href="<?php echo make_admin_url('urlrewrite', 'update', 'update', 'id='.$vv->id)?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i></a> 
                                        </td>
                                </tr>
                            <?php $sr++;endforeach;?>
                            </tbody>
                       <?php endif;?>  
                    </table>  
            </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>