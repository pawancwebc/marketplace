<?php if($action!='list'):?>
<a href="<?php echo make_admin_url('city','list','list');?>" class="btn default blue-stripe">
    <i class="icon-angle-left"></i>
    <span class="hidden-480">
             Back
    </span>
</a>
<?php endif;?>
<?php if($action!='insert'):?>
<a href="<?php echo make_admin_url('city','insert','insert');?>" class="btn default blue-stripe">
    <i class="icon-plus"></i>
    <span class="hidden-480">
             Add City
    </span>
</a>
<?php endif;?>