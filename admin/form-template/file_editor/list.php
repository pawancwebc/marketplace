<script type="text/javascript">
$(document).on('click', '#add_button', function(event){  

    var dir_path = $(this).attr('dir_path');
    
    var dataString = 'dir_path='+dir_path;
     
    $.ajax({
        type: "POST",
        url: "<?php echo make_admin_url_window('ajax_add_file');?>",
        data: dataString,
        success: function(data, textStatus) {
            
           $("#add_file_div").show();
           $("#add_file_div").html(data);
           $("#add_file_div").fadeIn(1500);
           App.init();
           FormComponents.init();
           FormSamples.init();
           TableManaged.init();
           ComponentsFormTools.init();
           ComponentsPickers.init();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert('asads');
        }
    });

});
</script>
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-edit"></i> Template Editor</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li class="btn-group">
                    <button data-toggle="modal" href="#help" class="btn" type="button">
                    <span>
                           <i class="icon-exclamation-sign"></i> Help
                    </span>
                    </button>

            </li>
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                   
            <li class="last">
                Template Editor
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-body">
                <div class="row-fluid" id="editor_main_section">
                    <div class="span4">
                        <div class="tabbable-custom" id="file_editor_tabs">
                                <ul class="nav nav-tabs">
                                    <li class="<?php echo ($module=='CSS')?"active":"";?>" id="CSS" path="../theme/default/css">
                                        <a href="#tab2" data-toggle="tab">CSS</a>
                                    </li>
                                    <li class="<?php echo ($module=='JS')?"active":"";?>" id="JS" path="../theme/default/javascript">
                                        <a href="#tab3" data-toggle="tab">JS</a>
                                    </li>
                                    <li class="<?php echo ($module=='Images')?"active":"";?>" id="Images" path="../theme/default/graphic">
                                        <a href="#tab4" data-toggle="tab">Images</a>
                                    </li>
                                    <li class="<?php echo ($module=='HTML')?"active":"";?>" id="HTML" path="../view/default">
                                        <a href="#tab1" data-toggle="tab">HTML</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane <?php echo ($module=='HTML')?"active":"";?>" id="tab1">
                                        <div  class="tree-demo fileeditor">
                                            <?php file::listFolderFiles('../view/default','HTML');?>
                                        </div>
                                    </div>
                                    <div class="tab-pane <?php echo ($module=='CSS')?"active":"";?>" id="tab2">
                                        <div  class="tree-demo fileeditor">
                                            <?php file::listFolderFiles('../theme/default/css','CSS');?>
                                        </div>
                                    </div>
                                    <div class="tab-pane <?php echo ($module=='JS')?"active":"";?>" id="tab3">
                                        <div  class="tree-demo fileeditor">
                                            <?php file::listFolderFiles('../theme/default/javascript','JS');?>
                                        </div>
                                    </div>
                                    <div class="tab-pane <?php echo ($module=='Images')?"active":"";?>" id="tab4">
                                        <div  class="tree-demo fileeditor">
                                            <?php file::listFolderFiles('../theme/default/graphic','Images');?>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="span8" id="editor_top">
                        <div id="fileEditSection">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet">
                                    <div class="portlet-title">
                                            <div class="caption">
                                                <span id="module_name"></span>
                                                <span id="title_file_name"></span>
                                            </div>
                                            <div class="tools">
                                                
                                            </div>
                                    </div>
                                    <div class="portlet-body" style="width:100%">
                                        <div class="emptydiv">
                                            <div class="whiteBack">
                                                <span class="focus">Open a file</span><span class="focuslight"> to get started.</span>
                                                <div class="clearfix"></div>
                                                <div id="create_file_outer" style="display:none;">
                                                    <button data-toggle="modal" href="#add_file_div" class="btn green" id="add_button" dir_path="" >
                                                        <i class="icon-plus"></i> Create File
                                                    </button>
                                                    <div class="clearfix"></div>
                                                    <span class="current_dir_name"></span>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div id="add_image_button_outer" style="display:none;">
                                                    <form method="POST" name="upload_image_form" id="upload_image_form" action="<?php echo make_admin_url('file_editor','upload','upload','module=Images');?>" enctype="multipart/form-data">
                                                        <div class="controls">
                                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                <span class="btn btn-file green">
                                                                <span class="fileupload-new"><i class="icon-upload"></i> Upload Image </span>
                                                                <span class="fileupload-exists">Change</span>
                                                                <input type="file" class="default" id="image_upload" name="image"/>
                                                                </span>
                                                                <span class="fileupload-preview"></span>
                                                                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <span class="current_dir_name"></span>
                                                        </div>
                                                        <input type="hidden" id="image_dir_path" name="dir_path" value=""/>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="code_editor_div"></div>
                                        <div id="loader" class="whiteBack">
                                            <img src="assets/img/loading_new.gif"/>
                                        </div>
                                    </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                    </div>
                </div>
                <div class="row-fluid" id="editor_hide_section">
                    <div class="note note-danger">
                    <p>Template Editor use requires a larger screen resolution. Resize your browser, or use a device with a larger screen (desktop or laptop). The editing capability is disabled on smaller widths (phone / tablet).</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div class="clearfix"></div>
<div id="add_file_div" class="modal fade" role="dialog" aria-hidden="true"></div><!--ajax response in this div-->
<div class="clearfix"></div>
<div id="help" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-body">
                    <h3>File Editor</h3>
                    <ul>
                        <li>For editing file select the file from <strong>file tree</strong> and code will appear on the right side.</li>
                        <li>You can create new file by selecting on the directory from file tree.</li>
                        <li>Select directory from images tab to add new image in the same directory.</li>
                        <li>After selecting file or image, there is a button called rename, press button if you want to rename the file.</li>
                        <li>Be very careful before editing any file, it will directly reflect the modifications on front end after saving.</li>
                        <li>Before editing file, try to save the basic code in any backup file.</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/ace/1.1.3/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="assets/scripts/fileeditor.js" type="text/javascript"></script>