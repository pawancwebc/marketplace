<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title"><i class="icon-flag-alt"></i> Manage States</h3>
            <ul class="page-breadcrumb breadcrumb">
                    <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a>  
                            <i class="icon-angle-right"></i>
                    </li>                                  
                    <li>
                        <i class="icon-flag"></i>
                               <a href="<?php echo make_admin_url('state', 'list', 'list');?>">List States</a> 
                         <i class="icon-angle-right"></i>                                       
                    </li>
                    <li class="last">
                        Edit State
                    </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
       <form class="form-horizontal" action="<?php echo make_admin_url('state', 'update', 'update','id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
              <!-- / Box -->
              <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                 <div class="portlet">
                        <div class="portlet-title">
                                <div class="caption">Edit State</div>
                                <div class="actions">
                                    <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                                </div>
                        </div>
                        <div class="portlet-body form form-body">      
                                <div class="form-group">
                                        <label class="span2 control-label" for="name">Name<span class="required">*</span></label>
                                        <div class="span4">
                                           <input type="text" name="name" value="<?php echo $values->name;?>" id="name" class="form-control m-wrap validate[required]" />
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="span2 control-label" for="country_id">Country</label>
                                        <div class="span4">
                                           <select name="country_id" id="country_id" class="form-control validate[required] chosen">
                                               <option value="">Select Country</option>
                                               <?php foreach(getCountries(true) as $kk=>$vv): ?>
                                                    <option value="<?php echo $vv['id'];?>" <?php echo ($vv['id']==$values->country_id)?"selected":"";?>><?php echo $vv['short_name'];?></option>
                                               <?php endforeach;?>
                                           </select>
                                            <span id="country_error" style="display:none;color:red"><i>Please select the country first.</i></span>
                                        </div>
                                </div>	
                                <div class="form-group">
                                        <label class="span2 control-label" for="iso_code">ISO Code</label>
                                        <div class="span2">
                                           <input type="text" name="iso_code"  value="<?php echo $values->iso_code;?>" id="iso_code" class="form-control" />
                                        </div>
                                </div>	
                                <div class="form-group">
                                        <label class="span2 control-label" for="is_active">Active</label>
                                        <div class="span1">
                                           <input type="checkbox" name="is_active" <?php echo ($values->is_active)?"checked":"";?> id="is_active" class="form-control iPhoneStyle" />
                                        </div>
                                </div>                             
                                <div class="form-actions fluid">
                                    <div class="offset2">
                                         <input type="hidden" value="<?php echo $id;?>" name="id"/>
                                         <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                                         <button class="btn blue" type="submit" name="submit" value="SubmitClose"><i class="icon-ok"></i> Save and Close</button> 
                                         <a href="<?php echo make_admin_url('state', 'list', 'list');?>" class="btn" name="cancel"> Cancel</a>
                                    </div>
                                </div>
                  </div>
                </div>
            </div>
         </form>
         <div class="clearfix"></div>
 </div>
<script>
$('#validation').on('submit', function(e) {
    var country_id = $('#country_id').val();
    if(country_id==''){
        $('#country_error').show();
        return false;
    }
});  
</script>
<div class="clearfix"></div>