<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-flag"></i> Manage Countries</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                            List Countries
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Countries</div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('country', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >	
                      <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                 <tr>
                                    <th style="width:1%;" class="hidden-480">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                    </th>
                                    <th>Flag</th>
                                    <th>Name</th>
                                    <th class="hidden-480">iso2</th>
                                    <th class="hidden-480">iso3</th>
                                    <th class="hidden-480">Calling Code</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <?php if($QueryObj->GetNumRows()!=0):?>
                                <tbody>
                                <?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):?>
                                    <tr class="odd gradeX">
                                            <td class="hidden-480">
                                                <input class="checkboxes" id="multiopt[<?php echo $object->id ?>]" name="multiopt[<?php echo $object->id ?>]" type="checkbox" />
                                            </td>
                                            <td>
                                                <?php if(file_exists("assets/img/flags/".$object->flag.".png")): ?>
                                                <img src="assets/img/flags/<?php echo $object->flag?>.png" alt="<?php echo $object->short_name?>"/>
                                                <?php endif;?>
                                            </td>
                                            <td><?php echo $object->short_name?></td>
                                            <td class="hidden-480"><?php echo $object->iso2;?></td>
                                            <td class="hidden-480"><?php echo $object->iso3;?></td>
                                            <td class="hidden-480"><?php echo $object->calling_code;?></td>
                                            <td class="center">
                                                <div class="btn-group mini_buttons on_off_button" id='on_off_button<?php echo $object->id?>'>
                                                    <div module="country" class="btn status_on mini_buttons <?php echo ($object->is_active=='1')?'active':'';?>" on="<?php echo $object->id;?>" rel="<?php echo ($object->is_active=='1')?'on':'off';?>" >ON</div>
                                                    <div module="country" class="btn status_off mini_buttons button_right <?php echo ($object->is_active=='0')?'active':'';?>" off="<?php echo $object->id;?>" rel="<?php echo ($object->is_active=='1')?'off':'on';?>">OFF</div>
                                                </div>
                                            </td>
                                    </tr>
                                <?php $sr++; endwhile;?>
                                </tbody>
                                <tfoot class="hidden-480">
                                    <tr class="odd gradeX ">
                                        <td colspan="3">
                                            <div style=" width:220px;float:left">
                                                <select name="multiopt_action" style="width:150px;float: left;" class="left_align form-control">
                                                    <option value="active">Active</option>
                                                    <option value="inactive">Inactive</option>
                                                </select>
                                                <input style="float:right" type="submit" class="btn blue large" name="multiopt_go" value="Go"  onclick="return confirm('Are you sure?');" />
                                            </div>
                                        </td>
                                        <td colspan="4"></td>
                                    </tr> 
                                </tfoot>
                           <?php endif;?>  
                        </table>
                </form>    
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>