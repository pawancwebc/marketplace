<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title"><i class="icon-flag-checkered"></i> Manage Cities</h3>
            <ul class="page-breadcrumb breadcrumb">
                    <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a>  
                            <i class="icon-angle-right"></i>
                    </li>                                  
                    <li>
                        <i class="icon-flag-alt"></i>
                               <a href="<?php echo make_admin_url('city', 'list', 'list');?>">List Cities</a> 
                         <i class="icon-angle-right"></i>                                       
                    </li>
                    <li class="last">
                        Edit City
                    </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
       <form class="form-horizontal" action="<?php echo make_admin_url('city', 'update', 'update','id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
              <!-- / Box -->
              <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                 <div class="portlet">
                        <div class="portlet-title">
                                <div class="caption">Edit City</div>
                                <div class="actions">
                                    <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                                </div>
                        </div>
                        <div class="portlet-body form form-body">      
                                <div class="form-group">
                                        <label class="span2 control-label" for="name">Name<span class="required">*</span></label>
                                        <div class="span4">
                                           <input type="text" name="name"  value="<?php echo $values->name;?>" id="name" class="form-control m-wrap validate[required]" />
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="span2 control-label" for="country_id">Country</label>
                                        <div class="span4">
                                           <select name="country_id" id="country_id" class="form-control validate[required]">
                                               <option value="">Select Country</option>
                                               <?php foreach(getCountries(true) as $kk=>$vv): ?>
                                                <option value="<?php echo $vv['id'];?>" <?php echo ($vv['id']==$values->country_id)?"selected":"";?>><?php echo $vv['short_name'];?></option>
                                               <?php endforeach;?>
                                           </select>
                                            <span id="country_error" style="display:none;color:red"><i>Please select the country first.</i></span>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="span2 control-label" for="state_id">State</label>
                                        <div class="span4">
                                           <select name="state_id" id="state_id" class="form-control validate[required]">
                                               <option value="">Select State</option>
                                               <?php if(!empty($states)):?>
                                                <?php foreach($states as $kk=>$vv): ?>
                                                 <option value="<?php echo $vv['id'];?>" <?php echo ($vv['id']==$values->state_id)?"selected":"";?>><?php echo $vv['name'];?></option>
                                                <?php endforeach;?>
                                               <?php endif;?>
                                           </select>
                                            <span id="state_error" style="display:none;color:red"><i>Please select the state.</i></span>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="span2 control-label" for="is_active">Active</label>
                                        <div class="span1">
                                           <input type="checkbox" name="is_active" <?php echo ($values->is_active)?"checked":"";?> id="is_active" class="form-control iPhoneStyle" />
                                        </div>
                                </div>                             
                                <div class="form-actions fluid">
                                    <div class="offset2">
                                         <input type="hidden" value="<?php echo $id;?>" name="id"/>
                                         <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                         <a href="<?php echo make_admin_url('city', 'list', 'list');?>" class="btn" name="cancel"> Cancel</a>
                                    </div>
                                </div>
                  </div>
                </div>
            </div>
         </form>
         <div class="clearfix"></div>
 </div>
<div class="clearfix"></div>
<script src="assets/scripts/state_country.js" type="text/javascript"></script>