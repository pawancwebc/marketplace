<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-flag-checkered"></i> Manage Cities</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                            List Cities
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Cities</div>
                <div class="actions">
                   <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('city', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >	
                      <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                 <tr>
                                    <th style="width:1%;" class="hidden-480"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th>
                                    <th>City</th>
                                    <th class="hidden-480">State</th>
                                    <th class="hidden-480">Country</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <?php if(!empty($cities)):?>
                                <tbody>
                                <?php $sr=1;foreach($cities as $city):?>
                                    <tr class="odd gradeX">
                                            <td class="hidden-480">
                                                <input class="checkboxes" id="multiopt[<?php echo $city['id'];?>]" name="multiopt[<?php echo $city['id'];?>]" type="checkbox" />
                                            </td>
                                            <td class="clickable"><?php echo $city['name'];?></td>
                                            <td class="clickable"><?php echo $city['state'];?></td>
                                            <td class="clickable"><?php echo $city['country'];?></td>
                                            <td class="center">
                                                <div class="btn-group mini_buttons on_off_button" id='on_off_button<?php echo $city['id'];?>'>
                                                    <div module="city" class="btn status_on mini_buttons <?php echo ($city['is_active']=='1')?'active':'';?>" on="<?php echo $city['id'];?>" rel="<?php echo ($city['is_active']=='1')?'on':'off';?>" >ON</div>
                                                    <div module="city" class="btn status_off mini_buttons button_right <?php echo ($city['is_active']=='0')?'active':'';?>" off="<?php echo $city['id'];?>" rel="<?php echo ($city['is_active']=='1')?'off':'on';?>">OFF</div>
                                                </div>
                                            </td>
                                            <td>
                                                <a id="do_action" href="<?php echo make_admin_url('city', 'update', 'update', 'id='.$city['id'])?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i> </a>
                                                <a href="<?php echo make_admin_url('city', 'delete', 'list', 'id='.$city['id'])?>" onclick="return confirm('Are you sure? You are deleting this record.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i> </a>
                                            </td>
                                    </tr>
                                <?php $sr++; endforeach;?>
                                </tbody>
                                <tfoot>
                                    <tr class="odd gradeX hidden-480">
                                        <td colspan="3">
                                            <div style=" width:220px;float:left">
                                                <select name="multiopt_action" style="width:150px;float: left;" class="left_align form-control">
                                                    <option value="active">Active</option>
                                                    <option value="inactive">Inactive</option>
                                                </select>
                                                <input style="float:right" type="submit" class="btn blue large" name="multiopt_go" value="Go"  onclick="return confirm('Are you sure?');" />
                                            </div>
                                        </td>
                                        <td colspan="3"></td>
                                    </tr> 
                                </tfoot>
                           <?php endif;?>  
                        </table>
                </form>    
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>