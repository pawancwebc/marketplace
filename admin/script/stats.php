<?php
include_once(DIR_FS_SITE.'include/functionClass/productClass.php');
include_once(DIR_FS_SITE.'include/functionClass/attributeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/brandClass.php');
include_once(DIR_FS_SITE.'include/functionClass/supplierClass.php');
include_once(DIR_FS_SITE.'include/functionClass/manufacturerClass.php');


isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['print'])?$print=$_GET['print']:$print='0';

$modName = 'stats';

$records_on_dashboard = 6;
#handle actions here.
switch ($action):
	case'list':

                $usersObj = new user();
                $totalusers = $usersObj->countTotalRecords(true);
                
                $productObj = new product();
                $totalproducts = $productObj->countTotalRecords(true);
                
                $brandObj = new brand();
                $totalbrands = $brandObj->countTotalRecords(true);
                
                $supplierObj = new supplier();
                $totalsuppliers = $supplierObj->countTotalRecords(true);
                
                $manufacturerObj = new manufacturer();
                $totalmanufacturers = $manufacturerObj->countTotalRecords(true);
            
                $orderObj = new order();
                $totalorders = $orderObj->countTotalOrders();
                
                $orderPriceObj = new order();
                $totalsales = $orderPriceObj->countTotalOrdersPrice();
                
                if($print=='1'):
                    $output = array(array(
                        'Total Users' => $totalusers,
                        'Total Products' => $totalproducts,
                        'Total Brands' => $totalbrands,
                        'Total Suppliers' => $totalsuppliers,
                        'Total Manufacturer' => $totalmanufacturers,
                        'Total Orders' => $totalorders,
                        'Total Sales' => number_format($totalsales,2),
                    ));

                    download_csv_file($output,'Stats');  
                    Redirect(make_admin_url('stats','list','list'));
                endif;
               /*
                * get list of top ordered products 
                */
               $Query = new order_detail();
               $records = $Query -> getTopOrderedProducts('','',$records_on_dashboard); 
               
               /*
                *  get list of low stock products
                */
               $Query = new product();
               $low_stock_products = $Query -> getLowStockProducts($records_on_dashboard);
               if($low_stock_products && !empty($low_stock_products)):
                  foreach($low_stock_products as $kk=>$vv):
                     if($vv['variant_id']!='' && $vv['variant_id']!='0'):
                        $query = new product_variant();
                        $low_stock_products[$kk]['variant_name'] = $query ->getVariantName($vv['variant_id']);
                     endif;
                  endforeach;
                endif;
                /*
                 * get top customers
                 */
                $QueryUser = new order();
                $top_users = $QueryUser -> getTopCustomersbyOrderCount($records_on_dashboard);
                
                /*
                * get latest orders
                */
                $QueryObj = new order();
                $orders = $QueryObj ->listOrders(false,'array',$records_on_dashboard);
                
                /*
                 * get new customers
                 */
                $QueryObj= new user();
                $users = $QueryObj->listLatestUsers($records_on_dashboard);
               break;
	default:break;
endswitch;
?>
