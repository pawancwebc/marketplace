<?php 
set_time_limit(300);
include_once(DIR_FS_SITE.'include/functionClass/productClass.php');
include_once(DIR_FS_SITE.'include/functionClass/categoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/brandClass.php');
include_once(DIR_FS_SITE.'include/functionClass/contentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/supplierClass.php');
include_once(DIR_FS_SITE.'include/functionClass/manufacturerClass.php');

//$sitemap_dir = DIR_FS_SITE;
//if (!is_dir($sitemap_dir)) {
//   @mkdir($sitemap_dir,0755);
//}

global $handle, $counter, $file_number, $filename;

$counter = 0;
$file_number = 0;

/*static content pages*/
$content_page_array=array('home','shop','brand','new_products','top_products','on_sale','contact');
foreach($content_page_array as $content):
    $url_content = make_url($content);
    file::write_xml($url_content,CONTENT_PAGES_PRIORITY,CONTENT_PAGES_FREQUENCY);
endforeach;

/*content pages from admin*/
$query_content = new content();
$all_content_pages = $query_content->getSitemapRecords(false,'include');
foreach($all_content_pages as $kk=>$vv):
    $priority = '';
    $url_content = make_url('content','id='.$vv->id);
    $priority = ($vv->sitemap_priority!='global')?$vv->sitemap_priority:CONTENT_PAGES_PRIORITY;
    file::write_xml($url_content,$priority,CONTENT_PAGES_FREQUENCY);
endforeach;

if(INCLUDE_CONTENT_PAGES_SITEMAP=='yes'):
    $query_content = new content();
    $all_content_pages = $query_content->getSitemapRecords(false,'global');
    foreach($all_content_pages as $kk=>$vv):
        $priority = '';
        $url_content = make_url('content','id='.$vv->id);
        $priority = ($vv->sitemap_priority!='global')?$vv->sitemap_priority:CONTENT_PAGES_PRIORITY;
        file::write_xml($url_content,$priority,CONTENT_PAGES_FREQUENCY);
    endforeach;  
endif;

/* products */
$query = new product();
$all_products = $query->getSitemapRecords(true,'include');
foreach($all_products as $k=>$v):
   $priority = '';
   $url_products = make_url('product','id='.$v->id);
   $priority = ($vv->sitemap_priority!='global')?$vv->sitemap_priority:PRODUCT_PAGES_PRIORITY;
   file::write_xml($url_products,$priority,PRODUCT_PAGES_FREQUENCY);
endforeach;
if(INCLUDE_PRODUCT_XML_SITEMAP=='yes'):
    $query = new product();
    $all_products = $query->getSitemapRecords(true,'global');
    foreach($all_products as $k=>$v):
       $priority = '';
       $url_products = make_url('product','id='.$v->id);
       $priority = ($vv->sitemap_priority!='global')?$vv->sitemap_priority:PRODUCT_PAGES_PRIORITY;
       file::write_xml($url_products,$priority,PRODUCT_PAGES_FREQUENCY);
    endforeach;
endif;

/* categories */
$query1 = new Category();
$all_cats = $query1 ->getSitemapRecords(true,'include');
foreach($all_cats as $kk=>$vv):
   $priority = '';
   $url_cats = make_url('shop','id='.$vv->id);
   $priority = ($vv->sitemap_priority!='global')?$vv->sitemap_priority:CATEGORY_PAGES_PRIORITY;
   file::write_xml($url_cats,$priority,CATEGORY_PAGES_FREQUENCY);
endforeach;
if(INCLUDE_CATEGORY_PAGES_SITEMAP=='yes'):
    $query1 = new Category();
    if(INCLUDE_ROOT_CATEGORY_PAGES_ONLY_SITEMAP=='yes'):
        $all_cats = $query1 ->getSitemapRecords(true,'global',true);
    else:
        $all_cats = $query1 ->getSitemapRecords(true,'global');
    endif;
    foreach($all_cats as $kk=>$vv):
       $priority = '';
       $url_cats = make_url('shop','id='.$vv->id);
       $priority = ($vv->sitemap_priority!='global')?$vv->sitemap_priority:CATEGORY_PAGES_PRIORITY;
       file::write_xml($url_cats,$priority,CATEGORY_PAGES_FREQUENCY);
    endforeach;
endif;

/* brand */
$QueryObj = new brand();
$all_brands = $QueryObj->getSitemapRecords(true,'include');
foreach($all_brands as $kkk=>$vvv):
   $priority = '';
   $url_blogs = make_url('brand','id='.$vvv->id);
   $priority = ($vv->sitemap_priority!='global')?$vv->sitemap_priority:BRAND_PAGES_PRIORITY;
   file::write_xml($url_blogs,$priority,BRAND_PAGES_FREQUENCY);
endforeach;
if(INCLUDE_BRAND_PAGES_SITEMAP=='yes'):
    $QueryObj = new brand();
    $all_brands = $QueryObj->getSitemapRecords(true,'global');
    foreach($all_brands as $kkk=>$vvv):
       $priority = '';
       $url_blogs = make_url('brand','id='.$vvv->id);
       $priority = ($vv->sitemap_priority!='global')?$vv->sitemap_priority:BRAND_PAGES_PRIORITY;
       file::write_xml($url_blogs,$priority,BRAND_PAGES_FREQUENCY);
    endforeach;
endif;

file::write_xml('END');
Redirect1(make_admin_url('setting','list','list','sname=sitemap'));
exit;
?>
