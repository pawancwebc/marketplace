<?php

include_once(DIR_FS_SITE . 'include/functionClass/serviceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

$modName = 'service';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;
isset($_GET['uid']) ? $uid = $_GET['uid'] : $uid = 0;
isset($_GET['img']) ? $img_id = $_GET['img'] : $img_id = '0';

if (!empty($uid)):
    /* get user contents */
    $Query_obj = new user();
    $user = $Query_obj->getUser($uid);
endif;

/* List All Vendor */
$QueryObj = new user();
$allUser = $QueryObj->getLimitedInfo('vendor');


/* List All Category */
$QueryObj = new category();
$categories = $QueryObj->listCategory();

/* Handle actions here. */
switch ($action):
    case 'list':
        if (!empty($uid)):
            $QueryObj = new service();
            $services = $QueryObj->listUserService($uid);
        else:
            $QueryObj = new service();
            $services = $QueryObj->listService();
        endif;
        break;



    case 'update':
        /* update Category */
        if (isset($_POST['submit'])):
            $QueryObj = new service();
            $new_id = $QueryObj->saveService($_POST);

            /* Delete All Previous Service Rel */
            $QueryObj = new categoryService();
            $QueryObj->deleteServiceCat($new_id);

            /* Save Category Services Rel Services */
            if (!empty($_POST['catgory_id']) && !empty($_POST['catgory_id'])):
                $QueryObj = new categoryService();
                $QueryObj->saveServiceCat($new_id, $_POST['catgory_id']);
            endif;

            if ($_POST['submit'] == 'SubmitClose'):
                Redirect(make_admin_url('service', 'list', 'list'));
            else:
                Redirect(make_admin_url('service', 'update', 'update', 'id=' . $new_id));
            endif;

        endif;

        /* get Category contents */
        $Query_obj = new service();
        $values = $Query_obj->getService($id);

        if (!is_object($values)):
            $admin_user->set_error();
            $admin_user->set_pass_msg('Something went wrong.');
            Redirect(make_admin_url('service', 'list', 'list'));
        endif;

        $QueryObj = new categoryService();
        $selected_cat = $QueryObj->getServiceCat($id);

        break;

    case 'update2':
        if (isset($_POST['multiopt_go']) && $_POST['multiopt_go'] == 'Go'):
            if ($_POST['multiopt_action'] == 'delete'):
                if (count($_POST['multiopt'])):
                    foreach ($_POST['multiopt'] as $k => $v):
                        /* Delete All Previous Service Rel */
                        $QueryObj = new categoryService();
                        $QueryObj->deleteServiceCat($k);

                        $content = new service();
                        $content->purgeObject($k);
                    endforeach;
                else:
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                    Redirect(make_admin_url('service', 'list', 'list'));
                endif;
            endif;
        endif;

        $admin_user->set_pass_msg('Operation has been performed successfully');
        Redirect(make_admin_url('service', 'list', 'list'));
        break;


    case 'images':

        if (isset($_POST['submit_images'])):
            if (isset($_FILES['image']) && $_FILES['image']['tmp_name']['0'] != ''):
                $files = serializeUploadArray($_FILES, 'image');
                $QueryObj = new simage();
                $QueryObj->uploadServiceImages($id, $files, 'image');
                $admin_user->set_pass_msg('Image uploaded successfully.');
                Redirect(make_admin_url('service', 'images', 'images', 'id=' . $id) . '#images');
            endif;

            if (isset($_POST['image_url']) && $_POST['image_url'] != ''):
                $image_arr = explode('/', $_POST['image_url']);
                $image_name = end($image_arr);

                $rand = rand(0, 99999999);
                $image_obj = new imageManipulation();

                if ($image_obj->upload_photo_from_url('service', $_POST['image_url'], $image_name, $rand)):
                    $queryObj = new simage();
                    $queryObj->insertServiceImage($id, $image_obj->makeFileName($image_name, $rand));

                    $admin_user->set_pass_msg('Image uploaded successfully.');
                    Redirect(make_admin_url('service', 'images', 'images', 'id=' . $id) . '#images');
                endif;

            endif;

            $admin_user->set_error();
            $admin_user->set_pass_msg('Error occurred while uploading new image.');
            Redirect(make_admin_url('service', 'update', 'images', 'id=' . $id) . '#images');


        endif;


        /* get service contents */
        $Query_obj = new service();
        $values = $Query_obj->getService($id);

        if (!is_object($values)):
            $admin_user->set_error();
            $admin_user->set_pass_msg('Something went wrong.');
            Redirect(make_admin_url('service', 'list', 'list'));
        endif;

        /* get service Images */
        $Query_obj = new simage();
        $images = $Query_obj->getServiceImages($id);

        $values->images = $images;

        break;

    case 'delete_image':
        if ($img_id) {
            $QueryObj = new simage();
            if ($QueryObj->deleteServiceImage($img_id)):
                $admin_user->set_pass_msg('Image has been successfully deleted.');
            else:
                $admin_user->set_error();
                $admin_user->set_pass_msg('service deleting image. Please try again.');
            endif;
            Redirect(make_admin_url('service', 'images', 'images', 'id=' . $id) . '#images');
        }
        Redirect(make_admin_url('service', 'list', 'list'));
        break;

    case 'cover_image':
        if ($img_id) {
            $QueryObj = new simage();
            if ($QueryObj->updateCoverImage($img_id, $id)):
                $admin_user->set_pass_msg('Cover Image has been set successfully.');
            else:
                $admin_user->set_error();
                $admin_user->set_pass_msg('Please try again.');
            endif;
            Redirect(make_admin_url('service', 'images', 'images', 'id=' . $id) . '#images');
        }
        Redirect(make_admin_url('service', 'list', 'list'));
        break;


    case 'update_image_position':
        if (isset($_POST['submit_image_position'])):
            foreach ($_POST['position'] as $k => $v):
                $Query_obj = new simage();
                $Query_obj->updatePosition($v, $k);
            endforeach;
            $admin_user->set_pass_msg('Position has been updated.');
            Redirect(make_admin_url('service', 'images', 'images', 'id=' . $id) . '#images');
        endif;
        Redirect(make_admin_url('service', 'list', 'list'));
        break;


    case 'insert':
        if (isset($_POST['submit'])):
            /* server side validation */
            $validation = new user_validation();
            $validation->add('name', 'req');
            $validation->add('name', 'reg_words');

            $valid = new valid();

            if ($valid->validate($_POST, $validation->get())):
                $error = 0;
            else:
                $error = 1; /* set error */
                $error_obj->errorAddArray($valid->error);
            endif;

            if ($error != '1'): /* if there is no error */
                $Category = new service();
                $new_id = $Category->saveService($_POST);

                /* Delete All Previous Service Rel */
                $QueryObj = new categoryService();
                $QueryObj->deleteServiceCat($new_id);

                /* Save Category Services Rel Services */
                if (!empty($_POST['catgory_id']) && !empty($_POST['catgory_id'])):
                    $QueryObj = new categoryService();
                    $QueryObj->saveServiceCat($new_id, $_POST['catgory_id']);
                endif;

                if ($_POST['submit'] == 'SubmitClose'):
                    Redirect(make_admin_url('service', 'list', 'list'));
                else:
                    Redirect(make_admin_url('service', 'update', 'update', 'id=' . $new_id));
                endif;
            endif;
        endif;
        break;

    case 'delete':
        /* Delete All Previous Service Rel */
        $QueryObj = new categoryService();
        $QueryObj->deleteServiceCat($id);

        $content = new service();
        $content->purgeObject($id);

        $admin_user->set_pass_msg('Service has been deleted successfully.');
        Redirect(make_admin_url('service', 'list', 'list'));
        break;


    default:break;
endswitch;
?>