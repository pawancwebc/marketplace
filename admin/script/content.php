<?php
include_once(DIR_FS_SITE.'include/functionClass/contentClass.php');

$modName='content';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id=0;

/*Handle actions here.*/
switch ($action):
	case 'list':
                $QueryObj= new content();
                $QueryObj->listPages();
		break;
	case 'update':
                /* update page */
		if(isset($_POST['submit'])):
						$QueryObj = new content();    
						$QueryObj ->savePage($_POST);
                       
            
                        $admin_user->set_pass_msg('The page has been updated successfully.');
                        Redirect(make_admin_url('content', 'update', 'update', 'id='.$id));
		endif;
		
                /* get page contents */
                $content_obj= new content();
                $page_content=$content_obj->getPage($id);
		break;

         case 'update2':
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                    if($_POST['multiopt_action']=='delete'):
                        if(count($_POST['multiopt'])):
                            foreach($_POST['multiopt'] as $k=>$v):                            
                                
                                    $query= new query('content');
                                    $query->Data['id']="$k";
                                    $query->Data['is_deleted']='1';
                                    $query->Update();                                   

                               
                            endforeach;
                         else:
                            $admin_user->set_error();   
                            $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                            Redirect(make_admin_url('content', 'list', 'list'));
                         endif;   
                    endif;
                endif;
            
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('content', 'list', 'list'));
                break;   
        
        case 'update_multiple_thrash': 
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                     if(count($_POST['multiopt'])):
                            if($_POST['multiopt_action']=='delete'):
                                    foreach($_POST['multiopt'] as $k=>$v):
                                            $query= new query('content');
                                            $query->id="$k";
                                            $query->Delete();

                                    endforeach;
                            endif;
                            if($_POST['multiopt_action']=='restore'):
                                    foreach($_POST['multiopt'] as $k=>$v):
                                            $query= new query('content');
                                            $query->Data['id']="$k";
                                            $query->Data['is_deleted']='0';
                                            $query->Update();

                                    endforeach;
                            endif;
                     else:
                        $admin_user->set_error();   
                        $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                        Redirect(make_admin_url('content', 'thrash', 'thrash'));
                     endif;   
                endif;
            
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('content', 'thrash', 'thrash'));
                break;
                
	case 'insert':
                /* create page*/
                if(isset($_POST['submit'])):
                    $content= new content();    
                    $new_id = $content->savePage($_POST);
                    
            
                    $admin_user->set_pass_msg('The page has been inserted successfully.');
                    Redirect(make_admin_url('content', 'update', 'update', 'id='.$new_id));
                endif;
                break;
        case 'delete':
                   
                        $content= new content(); 
                        $content->deletePage($id);


                        $admin_user->set_pass_msg('The page has been deleted successfully.');
                        Redirect(make_admin_url('content', 'list', 'list'));
                   
                break;
         case'permanent_delete':
		$QueryObj = new content();
		$QueryObj->purgeObject($id);

                
		$admin_user->set_pass_msg('Page has been deleted successfully');
		Redirect(make_admin_url('content', 'thrash', 'thrash'));
        	break;
         
          case'restore':
		$QueryObj = new content();
		$QueryObj->restoreObject($id);

        
		$admin_user->set_pass_msg('Page has been restored successfully');
		Redirect(make_admin_url('content', 'thrash', 'thrash'));
        	break;
            
            case'thrash':
		$QueryObj = new content();
		$QueryObj->getThrash();
		break;
    default:break;
endswitch;
?>