<?php
include_once(DIR_FS_SITE.'include/functionClass/cityClass.php');
include_once(DIR_FS_SITE.'include/functionClass/stateClass.php');

$modName='city';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id=0;

/*Handle actions here.*/
switch ($action):
	case 'list':
                $QueryObj= new city();
                $cities = $QueryObj->listCity();
		break;
	case 'update':
                /* update city */
		if(isset($_POST['submit'])):
                        /*server side validation*/
                        $validation=new user_validation();
                        $validation->add('name', 'req');
                        $validation->add('name', 'reg_words');
                        
                        $validation->add('country_id', 'req');
                        $validation->add('country_id', 'num');
                        
                        $validation->add('state_id', 'req');
                        $validation->add('state_id', 'num');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                            $QueryObj = new city();    
                            $QueryObj ->saveCity($_POST);
                            
                            $admin_user->set_pass_msg('City has been updated successfully.');
                            Redirect(make_admin_url('city', 'update', 'update', 'id='.$id));
                        endif;
		endif;
		
                /* get city contents */
                $Query_obj= new city();
                $values=$Query_obj->getCity($id);
                if(!is_object($values)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something went wrong.');
                    Redirect(make_admin_url('city', 'list', 'list'));
                endif;
                
                /*get state with country id*/ 
                $states=array();
                $QueryObj1 = new state();
                $states=$QueryObj1->getStateNameByCountry($values->country_id);

		break;

         case 'update2':
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                     if(count($_POST['multiopt'])):
                        if($_POST['multiopt_action']=='active'):
                            foreach($_POST['multiopt'] as $k=>$v):
                                   $deleteObj= new city(); 
                                   $deleteObj->setActive($k);
                            endforeach;
                        endif;
                        if($_POST['multiopt_action']=='inactive'):
                            foreach($_POST['multiopt'] as $k=>$v):
                                   $deleteObj= new city(); 
                                   $deleteObj->setInactive($k);
                            endforeach;
                        endif;
                     else:
                        $admin_user->set_error();   
                        $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                        Redirect(make_admin_url('city', 'list', 'list'));
                     endif;   
                endif;
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('city', 'list', 'list'));
                break;   

	case 'insert':
                /* create city*/
                if(isset($_POST['submit'])):
                    /*server side validation*/
                        $validation=new user_validation();
                
                        $validation->add('name', 'req');
                        $validation->add('name', 'reg_words');
                        
                        $validation->add('country_id', 'req');
                        $validation->add('country_id', 'num');
                        
                        $validation->add('state_id', 'req');
                        $validation->add('state_id', 'num');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                            $city= new city();    
                            $new_id = $city->saveCity($_POST);

                            $admin_user->set_pass_msg('City has been inserted successfully.');
                            Redirect(make_admin_url('city', 'update', 'update', 'id='.$new_id));
                        endif;
                endif;
                break;
                
        case 'delete':
                $content= new city(); 
                $content->id=$id;
                $content->SoftDelete();
                        
                $admin_user->set_pass_msg('City has been deleted successfully.');
                Redirect(make_admin_url('city', 'list', 'list'));
                break;

    default:break;
endswitch;
?>