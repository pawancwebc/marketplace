<?php

include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');

$modName = 'category';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;
isset($_GET['pid']) ? $pid = $_GET['pid'] : $pid = 0;

$parent_id = 0;


/* Handle actions here. */
switch ($action):
    case 'list':
        $QueryObj = new category();
        $categories = $QueryObj->listCategory();

        break;
    case 'update':
        /* update Category */
        if (isset($_POST['submit'])):
            $QueryObj = new category();
            $getId = $QueryObj->saveCategorys($_POST);

            if ($_POST['submit'] == 'SubmitClose'):
                Redirect(make_admin_url('category', 'list', 'list'));
            else:
                Redirect(make_admin_url('category', 'update', 'update', 'id=' . $id));
            endif;

        endif;

        /* get Category contents */
        $Query_obj = new category();
        $values = $Query_obj->getCategory($id);

        if (!is_object($values)):
            $admin_user->set_error();
            $admin_user->set_pass_msg('Something went wrong.');
            Redirect(make_admin_url('category', 'list', 'list'));
        endif;

        break;



    case 'insert':

        if (isset($_POST['submit'])):
            
            /* server side validation */
            $validation = new user_validation();
            $validation->add('name', 'req');
            $validation->add('name', 'reg_words');

            $valid = new valid();

            if ($valid->validate($_POST, $validation->get())):
                $error = 0;
            else:
                $error = 1; /* set error */
                $error_obj->errorAddArray($valid->error);
            endif;

            if ($error != '1'): /* if there is no error */
                $Category = new category();
                $new_id = $Category->saveCategorys($_POST);

                if ($_POST['submit'] == 'SubmitClose'):
                    Redirect(make_admin_url('category', 'list', 'list'));
                else:
                    Redirect(make_admin_url('category', 'update', 'update', 'id=' . $id));
                endif;
            endif;
        endif;
        break;
    case 'delete':
        $content = new category();
        $content->purgeObject($id);


        $admin_user->set_pass_msg('Category has been deleted successfully.');
        Redirect(make_admin_url('category', 'list', 'list'));
        break;


    default:break;
endswitch;
?>