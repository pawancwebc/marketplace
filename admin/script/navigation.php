<?php
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/navigationClass.php');
$modName='navigation';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;
switch ($action):
    case'list':
             $QueryObj = new navigation();
	     $QueryObj->ListNavigations();
         break;
    case'insert':
            if(isset($_POST['submit'])):
                $QueryObj = new navigation();
                $QueryObj->saveObject($_POST);
                $admin_user->set_pass_msg('The navigation has been added.');
                Redirect(make_admin_url('navigation', 'list', 'list'));
             elseif(isset($_POST['cancel'])):
                    $admin_user->set_pass_msg('The operation has been cancelled');
                     Redirect(make_admin_url('navigation', 'list', 'list'));     
             endif;
           break;
    case'update':
                $QueryObj = new navigation();
		$navigations=$QueryObj->getObject($id);
		if(isset($_POST['submit'])):
                    $QueryObj = new navigation();
                    $QueryObj->saveObject($_POST);
                    $admin_user->set_pass_msg('The navigation has been updated');
                    Redirect(make_admin_url('navigation', 'list', 'list', 'id=' . $navigations->id));
                 elseif(isset($_POST['cancel'])):
                    $admin_user->set_pass_msg('The operation has been cancelled');
                     Redirect(make_admin_url('navigation', 'list', 'list'));     
                 endif;
        break;
    case'delete':
                $QueryObj = new navigation();
		$QueryObj->deleteObject($id);
		$admin_user->set_pass_msg('Navigation has been deleted successfully');
	        Redirect(make_admin_url('navigation', 'list', 'list', 'id=' . $id));
        break;
    default:break;
endswitch;
?>