<?php
include_once(DIR_FS_SITE.'include/functionClass/userClass.php');

$modName='user';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id=0;
isset($_GET['aid'])?$aid=$_GET['aid']:$aid=0;

/*Handle actions here.*/
switch ($action):
	case 'list':
                $QueryObj= new user();
                $QueryObj->listUsers();
		break;
            
            
	case 'update':
                 /* create user*/
                if(isset($_POST['submit'])):
                    /*server side validation*/
                        $validation=new user_validation();
                        
                        $validation->add('firstname', 'req');
                        $validation->add('firstname', 'reg_words');
                        
                        $validation->add('lastname', 'req');
                        $validation->add('lastname', 'reg_words');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                                $user= new user();    
                                $new_id = $user->updateUserbyAdmin($_POST);
                                if($new_id):
                                    $admin_user->set_pass_msg('User has been updated successfully.');
                                else:
                                    $admin_user->set_error();
                                    $admin_user->set_pass_msg('Error occurred while updating new customer.');
                                endif;
                            Redirect(make_admin_url('user', 'update', 'update', 'id='.$id));
                        endif;
                endif;
		
                /* get user contents */
                $Query_obj= new user();
                $values=$Query_obj->getUser($id);
				
                if(!is_object($values)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something went wrong.');
                    Redirect(make_admin_url('user', 'list', 'list'));
                endif;
		break;

         case 'update2':
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                    if($_POST['multiopt_action']=='delete'):
                        if(count($_POST['multiopt'])):
                            foreach($_POST['multiopt'] as $k=>$v):
                                    $content= new user(); 
                                    $content->id=$k;
                                    $content->SoftDelete();
                            endforeach;
                         else:
                            $admin_user->set_error();   
                            $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                            Redirect(make_admin_url('user', 'list', 'list'));
                         endif;   
                    endif;
                endif;
            
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('user', 'list', 'list'));
                break;   
        
        case 'update_multiple_thrash': 
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                     if(count($_POST['multiopt'])):
                            if($_POST['multiopt_action']=='delete'):
                                foreach($_POST['multiopt'] as $k=>$v):
                                    $deleteObj= new user(); 
                                    $deleteObj->deleteUser($k);
                                endforeach;
                            endif;
                            if($_POST['multiopt_action']=='restore'):
                                foreach($_POST['multiopt'] as $k=>$v):
                                    $QueryObj = new user();
                                    $QueryObj->restoreObject($k);
                                endforeach;
                            endif;
                     else:
                        $admin_user->set_error();   
                        $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                        Redirect(make_admin_url('user', 'thrash', 'thrash'));
                     endif;   
                endif;
            
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('user', 'thrash', 'thrash'));
                break;
                
	case 'insert':
                /* create user*/
                if(isset($_POST['submit'])):
                    /*server side validation*/
                        $validation=new user_validation();
                        
                        $validation->add('firstname', 'req');
                        $validation->add('firstname', 'reg_words');
                        
                        $validation->add('lastname', 'req');
                        $validation->add('lastname', 'reg_words');
                        
                        $validation->add('username', 'req');
                        $validation->add('username', 'email');
                        
                        $validation->add('password', 'req');
                        $validation->add('password', 'reg_words');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                            $user_obj = new user();
                            $user = $user_obj->checkUserEmailExists($_POST['username']);
                            if(!$user):
                                $user= new user();    
                                $new_id = $user->updateUserbyAdmin($_POST);
                                if($new_id):
                                    /*send email*/
                                    $email_array=array(
                                        'firstname'=>$_POST['firstname'],
                                        'lastname'=>$_POST['lastname'],
                                        'password'=>$_POST['password']
                                     );

                                   
                                    $admin_user->set_pass_msg('User has been added successfully.');
                                    Redirect(make_admin_url('user', 'update', 'update', 'id='.$new_id));
                                else:
                                    $admin_user->set_error();
                                    $admin_user->set_pass_msg('Error occurred while adding new customer.');
                                endif;
                            else:
                                $admin_user->set_error();
                                $admin_user->set_pass_msg('Email id already used.');
                            endif;
                            Redirect(make_admin_url('user', 'insert', 'insert'));
                        endif;
                endif;
                break;
        case 'delete':
                $content= new user(); 
                $content->id=$id;
                $content->SoftDelete();
                        
                $admin_user->set_pass_msg('User has been deleted successfully.');
                Redirect(make_admin_url('user', 'list', 'list'));
                break;

            
        case'permanent_delete':
		$QueryObj = new user();
		$QueryObj->purgeObject($id);
                        
		$admin_user->set_pass_msg('User has been deleted successfully');
		Redirect(make_admin_url('user', 'thrash', 'thrash'));
        	break;
         
          case'restore':
		$QueryObj = new user();
		$QueryObj->restoreObject($id);
                        
		$admin_user->set_pass_msg('User has been restored successfully');
		Redirect(make_admin_url('user', 'thrash', 'thrash'));
        	break;
            
          case'thrash':
		$QueryObj = new user();
		$QueryObj->getThrash();
		break;
    default:break;
endswitch;
?>