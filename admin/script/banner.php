<?php
include_once(DIR_FS_SITE . 'include/functionClass/bannerClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/navigationClass.php');

$modName = 'banner';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;
isset($_GET['bid']) ? $bid = $_GET['bid'] : $bid = 0;

/* Handle actions here. */
switch ($action):
    case 'list':
        $QueryObj = new banner();
        $QueryObj->listBanner();
        break;

    case 'update':
        /* update banner */
        if (isset($_POST['submit'])):
            /* server side validation */
            $validation = new user_validation();
            $validation->add('name', 'req');
           
            $valid = new valid();

            if ($valid->validate($_POST, $validation->get())):
                $error = 0;
            else:
                $error = 1; /* set error */
                $error_obj->errorAddArray($valid->error);
            endif;

            if ($error != '1'): /* if there is no error */
                $QueryObj = new banner();
                if ($QueryObj->saveBanner($_POST)):
                    $admin_user->set_pass_msg('Banner has been updated successfully.');
                    if ($_POST['submit'] == 'SubmitClose'):
                        Redirect(make_admin_url('banner', 'list', 'list'));
                    endif;
                else:
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Error occurred while updating banner.');
                endif;
                Redirect(make_admin_url('banner', 'update', 'update', 'id=' . $id));
            endif;
        endif;

        if (isset($_POST['submit_locations'])):
            /* get banner contents */
            $Query_obj = new banner();
            $values = $Query_obj->getBanner($id);

            if (isset($_POST['same_date_time'])):
                $_POST['from_date'] = $values->from_date;
                $_POST['to_date'] = $values->to_date;
            endif;
            $_POST['type'] = $_POST['module'];
            if ($_POST['module'] == 'shop' && isset($_POST['categories_id']) && $_POST['categories_id'] != ''):
                $cat_ids = explode(',', $_POST['categories_id']);
                foreach ($cat_ids as $kk => $vv):
                    $_POST['type'] = 'shop';
                    $_POST['type_id'] = $vv;
                    $SaveCatObj = new banner_location();
                    if ($SaveCatObj->saveBannerLocation($_POST['banner_id'], $_POST)):
                        $admin_user->set_pass_msg('Banner Locations has been updated successfully.');
                    else:
                        $admin_user->set_error();
                        $admin_user->set_pass_msg('Error occurred, while adding new banner location.');
                    endif;
                endforeach;

            elseif ($_POST['module'] == 'mega_menus' && isset($_POST['mega_menus_id']) && $_POST['mega_menus_id'] != ''):
                $cat_ids = explode(',', $_POST['mega_menus_id']);
                foreach ($cat_ids as $kk => $vv):
                    $_POST['type'] = 'mega_menus';
                    $_POST['type_id'] = $vv;
                    $SaveCatObj = new banner_location();
                    if ($SaveCatObj->saveBannerLocation($_POST['banner_id'], $_POST)):
                        $admin_user->set_pass_msg('Banner Locations has been updated successfully.');
                    else:
                        $admin_user->set_error();
                        $admin_user->set_pass_msg('Error occurred, while adding new banner location.');
                    endif;
                endforeach;

            else:
                $_POST['type_id'] = '0';
                $SaveCatObj = new banner_location();
                if ($SaveCatObj->saveBannerLocation($_POST['banner_id'], $_POST)):
                    $admin_user->set_pass_msg('Banner Locations has been updated successfully.');
                else:
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Error occurred, while adding new banner location.');
                endif;
            endif;


            Redirect(make_admin_url('banner', 'update', 'update', 'id=' . $id) . '#tab2');
        endif;

        /* get banner contents */
        $Query_obj = new banner();
        $values = $Query_obj->getBanner($id);

        if (!is_object($values)):
            $admin_user->set_error();
            $admin_user->set_pass_msg('Something went wrong.');
            Redirect(make_admin_url('banner', 'list', 'list'));
        endif;


        break;

    case 'update2':
        if (isset($_POST['multiopt_go']) && $_POST['multiopt_go'] == 'Go'):
            if ($_POST['multiopt_action'] == 'delete'):
                if (count($_POST['multiopt'])):
                    foreach ($_POST['multiopt'] as $k => $v):
                        $deleteObj = new banner();
                        $deleteObj->deleteBanner($k);
                    endforeach;
                else:
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                    Redirect(make_admin_url('banner', 'list', 'list'));
                endif;
            endif;
        endif;

        $admin_user->set_pass_msg('Operation has been performed successfully');
        Redirect(make_admin_url('banner', 'list', 'list'));
        break;

    case 'insert':
        /* create banner */
        if (isset($_POST['submit'])):
            /* server side validation */
            $validation = new user_validation();
            $validation->add('name', 'req');
            $validation->add('name', 'reg_words');

            $valid = new valid();

            if ($valid->validate($_POST, $validation->get())):
                $error = 0;
            else:
                $error = 1; /* set error */
                $error_obj->errorAddArray($valid->error);
            endif;

            if ($error != '1'): /* if there is no error */
                $banner = new banner();
                if ($new_id = $banner->saveBanner($_POST)):
                    $admin_user->set_pass_msg('Banner has been inserted successfully.');
                    Redirect(make_admin_url('banner', 'update', 'update', 'id=' . $new_id) . '#tab2');
                else:
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Error occurred while adding new banner.');
                    Redirect(make_admin_url('banner', 'insert', 'insert'));
                endif;
            endif;
        endif;
        break;

    case 'delete':
        $content = new banner();
        $content->deleteBanner($id);

        $admin_user->set_pass_msg('Banner has been deleted successfully.');
        Redirect(make_admin_url('banner', 'list', 'list'));
        break;

    case 'delete_image':
        if ($id) {
            $QueryObj = new banner();
            $object = $QueryObj->getBanner($id);

            $QueryObj1 = new banner();
            $mobile = FALSE;
            if (isset($_GET['mobile'])) {
                $mobile = TRUE;
            }
            $QueryObj1->removeImage($id, $mobile);

            #delete images from all folders
            $image_obj = new imageManipulation();
            if (isset($_GET['mobile'])) {
                $image_obj->DeleteImagesFromAllFolders('banner', $object->mobile_image);
            } else {
                $image_obj->DeleteImagesFromAllFolders('banner', $object->image);
            }
            $admin_user->set_pass_msg('Image has been successfully deleted.');
            Redirect(make_admin_url('banner', 'update', 'update', 'id=' . $id));
        }
        break;

    case 'location':
        if (isset($_POST['submit'])):
            if (!empty($_POST['position'])):
                $sr = 1;
                foreach ($_POST['position'] as $kk => $vv):
                    if (count($vv) == '2'):
                        if ($sr == '1'):
                            $query = new banner_location();
                            $query->resetBannerLocations($id);
                        endif;

                        $query1 = new banner_location();
                        $query1->saveBannerLocation($id, $vv);
                        $sr++;
                    endif;
                endforeach;
                $admin_user->set_pass_msg('Locations has been updated successfully.');
                Redirect(make_admin_url('banner', 'location', 'location', 'id=' . $id));
            endif;
            $admin_user->set_error();
            $admin_user->set_pass_msg('Error occurred. Please try again.');
            Redirect(make_admin_url('banner', 'location', 'location', 'id=' . $id));
        endif;

        /* get banner contents */
        $Query_obj = new banner();
        $values = $Query_obj->getBanner($id);

        if (!is_object($values)):
            $admin_user->set_error();
            $admin_user->set_pass_msg('Something went wrong.');
            Redirect(make_admin_url('banner', 'list', 'list'));
        endif;

        $Query1 = new banner_location();
        $locations_array = $Query1->getBannerLocations($id);
        $locations = array();
        if (!empty($locations_array)):
            foreach ($locations_array as $kk => $vv):
                $locations[$vv['type']] = $vv['position'];
            endforeach;
        endif;

        $QueryCat = new category();
        $categories = $QueryCat->listRecursiveCategory();

        break;

    case 'delete_banner_location':
        $content = new banner_location();
        $content->id = $bid;
        if ($content->Delete()):
            $admin_user->set_pass_msg('Banner has been deleted successfully.');
        else:
            $admin_user->set_error();
            $admin_user->set_pass_msg('Error occurred while deleting banner location.');
        endif;
        Redirect(make_admin_url('banner', 'update', 'update', 'id=' . $id) . '#tab2');
        break;

    case 'update_location':
        if (isset($_POST['submit_edit_location'])):
            $QuryUPdtLoca = new banner_location();
            if ($QuryUPdtLoca->updateBannerLocation($_POST['id'], $_POST)):
                $admin_user->set_pass_msg('Banner location updated successfully.');
            else:
                $admin_user->set_error();
                $admin_user->set_pass_msg('Error occurred while updating banner location.');
            endif;
            Redirect(make_admin_url('banner', 'update', 'update', 'id=' . $id) . '#tab2');
        endif;
        Redirect(make_admin_url('banner', 'list', 'list'));
        break;
    default:break;
endswitch;
?>