<?php
include_once('../../include/config/config.php');

if (isset($_GET['act'])) {
    extract($_GET);
    if ($act == 'get_submenus') {
        $show_submit = FALSE;
        if (array_key_exists($module, $navigationPages)) {
            $show_submit = TRUE;
            if ($navigationPages[$module][1] === TRUE) {
                switch ($module) {

                    case 'content':
                        @include_once '../../include/functionClass/contentClass.php';
                        $object = new content();
                        $object->listPages(FALSE);
                        $data = array();
                        while ($row = $object->GetObjectFromRecord()) {
                            $data[] = $row;
                        }
                        ?>
                        <div class="form-group">
                            <label class="span2 control-label" for="navigation_query">Select Content Page</label>
                            <div class="span8">
                                <select class="span6 form-control m-wrap chosen-select" name="navigation_query">
                                    <?php foreach ($data as $info) { ?>
                                        <option value="id=<?php echo $info->id ?>"><?php echo $info->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php
                        break;

                    default:
                        break;
                }
            }
        }
        if ($show_submit === TRUE) {
            ?>
            <div class="form-group">
                <label class="span2 control-label" for="is_active">Active</label>
                <div class="span8">
                    <input class="" type="checkbox" name="is_active" id="is_active" value="1" checked />
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="offset2">
                    <button class="btn green" type="submit" name="submit" value="Submit"><i class="icon-ok"></i> Save</button>
                    <button class="btn default" type="submit" name="cancel">Cancel</button>
                </div>
            </div>
            <?php
        } else {
            ?>
            <div class="offset2" style="font-weight: bold">Please Select a Module from Drop Down Menu...</div>
            <?php
        }
    }

}
if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'make_mega_menu') {
        @include_once '../../include/functionClass/navigationClass.php';
        $obj = new navigationItem;
        $data = array('id' => $menu_id, 'is_mega_menu' => $value);
        $obj->Data = $data;
        $obj->Update();
    }
}