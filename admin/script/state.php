<?php
include_once(DIR_FS_SITE.'include/functionClass/stateClass.php');

$modName='state';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id=0;

/*Handle actions here.*/
switch ($action):
	case 'list':
                $QueryObj= new state();
                $states = $QueryObj->listState();
		break;
	case 'update':
                /* update state */
		if(isset($_POST['submit'])):
                        /*server side validation*/
                        $validation=new user_validation();
                
                        $validation->add('name', 'req');
                        $validation->add('name', 'reg_words');
                        
                        $validation->add('country_id', 'req');
                        $validation->add('country_id', 'num');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                            $QueryObj = new state();    
                            if($QueryObj ->saveState($_POST)):
                                $admin_user->set_pass_msg('State has been updated successfully.');
                                if($_POST['submit']=='SubmitClose'):
                                    Redirect(make_admin_url('state', 'list', 'list'));
                                endif;
                            else:
                                $admin_user->set_error();
                                $admin_user->set_pass_msg('Error occurred while updating.');
                            endif;
                            Redirect(make_admin_url('state', 'update', 'update', 'id='.$id));
                        endif;
		endif;
		
                /* get state contents */
                $Query_obj= new state();
                $values=$Query_obj->getState($id);
                if(!is_object($values)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something went wrong.');
                    Redirect(make_admin_url('state', 'list', 'list'));
                endif;
		break;

         case 'update2':
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                     if(count($_POST['multiopt'])):
                        if($_POST['multiopt_action']=='active'):
                            foreach($_POST['multiopt'] as $k=>$v):
                                   $deleteObj= new state(); 
                                   $deleteObj->setActive($k);
                            endforeach;
                        endif;
                        if($_POST['multiopt_action']=='inactive'):
                            foreach($_POST['multiopt'] as $k=>$v):
                                   $deleteObj= new state(); 
                                   $deleteObj->setInactive($k);
                            endforeach;
                        endif;
                     else:
                        $admin_user->set_error();   
                        $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                        Redirect(make_admin_url('state', 'list', 'list'));
                     endif;   
                endif;
            
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('state', 'list', 'list'));
                break;   
                
	case 'insert':
            
                /* create state*/
                if(isset($_POST['submit'])):
                    /*server side validation*/
                        $validation=new user_validation();
                        $validation->add('name', 'req');
                        $validation->add('name', 'reg_words');
                        
                        $validation->add('country_id', 'req');
                        $validation->add('country_id', 'num');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                            $state= new state();    
                            $new_id = $state->saveState($_POST);
                            if($new_id):
                                $admin_user->set_pass_msg('New state has been inserted successfully.');
                                if($_POST['submit']=='SubmitClose'):
                                    Redirect(make_admin_url('state', 'list', 'list'));
                                endif;
                            else:
                                $admin_user->set_error();
                                $admin_user->set_pass_msg('Error occurred while updating.');
                            endif;
                            Redirect(make_admin_url('state', 'update', 'update', 'id='.$new_id));
                            
                        endif;
                endif;
                break;
        case 'delete':
                $content= new state(); 
                $content->id=$id;
                $content->SoftDelete();
                        
                $admin_user->set_pass_msg('State has been deleted successfully.');
                Redirect(make_admin_url('state', 'list', 'list'));
                break;

    default:break;
endswitch;
?>