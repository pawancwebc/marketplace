<?php

include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/navigationClass.php');

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;
isset($_GET['parent_id']) ? $parent_id = $_GET['parent_id'] : $parent_id = 0;
isset($_GET['nav_id']) ? $nav_id = $_GET['nav_id'] : $nav_id = 1;

switch ($action):
    case'list':
        $QueryObj = new navigationItem();
        $QueryObj->setParentId($parent_id);
        $QueryObj->setNavId($nav_id);
        $QueryObj->getAllItems();
        if ($parent_id != 0) {
            $new_obj = new navigationItem();
            $parent_info = $new_obj->getObject($parent_id);
        }
        break;
    case 'insert':
        if (isset($_POST['submit'])) {
            $QueryObj = new navigationItem();
            /* server side validation */
            $validation = new user_validation();
            $validation->add('navigation_title', 'req');
            $validation->add('navigation_title', 'reg_words');

            $valid = new valid();
            if ($valid->validate($_POST, $validation->get())) {
                $error = 0;
            } else {
                $error = 1; /* set error */
                $error_obj->errorAddArray($valid->error);
//                echo $section;die;
                Redirect(make_admin_url('content_navigation', 'insert', $section, 'parent_id=' . (isset($_GET['parent_id']) ? $_GET['parent_id'] : 0) . '&nav_id=' . (isset($_GET['nav_id']) ? $_GET['nav_id'] : 0)));
            }
            unset($_POST['submit']);
            $_POST['date_add'] = date("Y-m-d H:i:s");
            if ($QueryObj->saveObject($_POST)) {
                $admin_user->set_pass_msg('Navigation Item has been inserted successfully.');
                Redirect(make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . (isset($_GET['parent_id']) ? $_GET['parent_id'] : 0) . '&nav_id=' . (isset($_GET['nav_id']) ? $_GET['nav_id'] : 0)));
            } else {
                $error = 1; /* set error */
                $error_obj->errorAddArray(array('Some error occoured, Please try again...'));
                Redirect(make_admin_url('content_navigation', 'insert', $section, 'parent_id=' . (isset($_GET['parent_id']) ? $_GET['parent_id'] : 0) . '&nav_id=' . (isset($_GET['nav_id']) ? $_GET['nav_id'] : 0)));
            }
        } elseif (isset($_POST['cancel'])) {
            $error = 1;
            $error_obj->errorAddArray(array('The operation has been cancelled.'));
            Redirect(make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . (isset($_GET['parent_id']) ? $_GET['parent_id'] : 0) . '&nav_id=' . (isset($_GET['nav_id']) ? $_GET['nav_id'] : 0)));
        }
        break;
    case'update':
        $QueryObj = new navigationItem();
        $navigation = $QueryObj->getObject($id);
        if (isset($_POST['submit'])) {
            /* server side validation */
            $validation = new user_validation();
            $validation->add('navigation_title', 'req');
            $validation->add('navigation_title', 'reg_words');

            $valid = new valid();
            if ($valid->validate($_POST, $validation->get())) {
                $error = 0;
            } else {
                $error = 1; /* set error */
                $error_obj->errorAddArray($valid->error);
//                echo $section;die;
                Redirect(make_admin_url('content_navigation', 'update', 'update', 'parent_id=' . (isset($_GET['parent_id']) ? $_GET['parent_id'] : 0) . '&nav_id=' . (isset($_GET['nav_id']) ? $_GET['nav_id'] : 0) . '&id=' . (isset($_GET['id']) ? $_GET['id'] : 0)));
            }
            unset($_POST['submit']);
            $_POST['id'] = $_GET['id'];
            $_POST['is_active'] = isset($_POST['is_active']) ? $_POST['is_active'] : 0;
            $_POST['open_in_new'] = isset($_POST['open_in_new']) ? $_POST['open_in_new'] : 0;
			if(!isset($_POST['navigation_query'])){
				$_POST['navigation_query']='';
			}
			
//            pr($_POST);die;
            if ($QueryObj->saveObject($_POST)) {
                $admin_user->set_pass_msg('Navigation Item has been Updated successfully.');
                Redirect(make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . (isset($_GET['parent_id']) ? $_GET['parent_id'] : 0) . '&nav_id=' . (isset($_GET['nav_id']) ? $_GET['nav_id'] : 0)));
            } else {
                $error = 1; /* set error */
                $error_obj->errorAddArray(array('Some error occoured, Please try again...'));
                Redirect(make_admin_url('content_navigation', 'update', 'update', 'parent_id=' . (isset($_GET['parent_id']) ? $_GET['parent_id'] : 0) . '&nav_id=' . (isset($_GET['nav_id']) ? $_GET['nav_id'] : 0) . '&id=' . (isset($_GET['id']) ? $_GET['id'] : 0)));
            }
        } elseif (isset($_POST['cancel'])) {
            $admin_user->set_pass_msg('The operation has been cancelled');
            Redirect(make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . $navigation->parent_id . '&nav_id=' . $navigation->nav_id));
        }
        break;

    case 'update2':
        if (isset($_POST['update_position'])):
            foreach ($_POST['position'] as $k => $v):
                $Q = new query('navigation');
                if ($v == ''):
                    $Q->Data['position'] = 0;
                else:
                    $Q->Data['position'] = $v;
                endif;
                $Q->Data['id'] = $k;
                $Q->Update();
            endforeach;
        endif;

        /*         * ************for listing page multiple delete option************************** */

        if (isset($_POST['multiopt_go']) && $_POST['multiopt_go'] == 'Go'):
            if ($_POST['multiopt_action'] == 'delete'):
                foreach ($_POST['multiopt'] as $k => $v):
                    $QueryObj = new query('navigation');
                    $QueryObj->Data['is_deleted'] = '1';
                    $QueryObj->Data['id'] = $k;
                    $QueryObj->Update();
                endforeach;
            endif;
            $admin_user->set_pass_msg('Operation has been performed successfully');
        endif;

        /*         * *******************for listing page multiple delete option********************************* */

        Redirect(make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . $parent_id . '&nav_id=' . $nav_id));
        break;

    case 'delete':
        $QueryObj = new navigationItem();
        $QueryObj->deleteObject($id);
        $admin_user->set_pass_msg('Navigation Item has been deleted successfully');
        Redirect(make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . $parent_id . '&nav_id=' . $nav_id));

        break;
    case'thrash':
        $QueryObj = new navigationItem();
        $QueryObj->getThrash();
        break;
    case 'permanent_delete':
        $QueryObj = new navigationItem();
        $QueryObj->purgeObject($id);
        $admin_user->set_pass_msg('Navigation Item has been deleted successfully');
        Redirect(make_admin_url('content_navigation', 'thrash', 'thrash'));
//        Redirect(make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . $parent_id . '&nav_id=' . $nav_id));
        break;

    case 'restore':
        $QueryObj = new navigationItem();
        $QueryObj->restoreObject($id);
        $admin_user->set_pass_msg('Navigation Item has been restored successfully');
        Redirect(make_admin_url('content_navigation', 'thrash', 'thrash'));
//        Redirect(make_admin_url('content_navigation', 'list', 'list', 'parent_id=' . $parent_id . '&nav_id=' . $nav_id));
        break;


    /*     * **************************for thrash changes******************************************* */

    case 'update3':
        if (isset($_POST['multiopt_go']) && $_POST['multiopt_go'] == 'Go'):
            if ($_POST['multiopt_action'] == 'delete'):
                foreach ($_POST['multiopt'] as $k => $v):
                    $QueryObj = new query('navigation');
                    $QueryObj->id = $k;
                    $QueryObj->Delete();
                endforeach;
            elseif ($_POST['multiopt_action'] == 'restore'):
                foreach ($_POST['multiopt'] as $k => $v):
                    $QueryObj = new query('navigation');
                    $QueryObj->Data['is_deleted'] = '0';
                    $QueryObj->Data['id'] = $k;
                    $QueryObj->Update();
                endforeach;
            endif;
            $admin_user->set_pass_msg('Operation has been performed successfully');
        endif;
        Redirect(make_admin_url('content_navigation', 'thrash', 'thrash'));
        break;

    /*     * ************************thrash page changes ends here*************************************** */


    default:break;

endswitch;
?>