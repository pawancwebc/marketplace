<?php

include_once(DIR_FS_SITE . 'include/functionClass/navigationClass.php');
//include_once(DIR_FS_SITE.'include/functionClass/attributeClass.php');

$action = isset($_GET['action']) ? $_GET['action'] : 'list';
$section = isset($_GET['section']) ? $_GET['section'] : 'list';

//this variable value should be the name of the folder which is to be accessed in form-template folder
$modName = 'navigation';

//$records_on_dashboard = 6;
#handle actions here.
switch ($action):
    case 'list':
        $nav_obj = new navigation();
        $nav_obj->ListNavigations();
//        $nav_obj->
        break;
    case 'insert':
        if (isset($_POST['submit'])) {
            /* server side validation */
            $validation = new user_validation();
            $validation->add('name', 'req');
            $validation->add('name', 'reg_words');

            $valid = new valid();
            if ($valid->validate($_POST['na'], $validation->get())) {
                $error = 0;
            } else {
                $error = 1; /* set error */
                $error_obj->errorAddArray($valid->error);
            }
            if (!$error) {
                $nav_obj = new navigation();
                if ($id = $nav_obj->saveNavigation($_POST['na'])) {
                    $admin_user->set_pass_msg('Navigation has been Added successfully. Please Add Menus to Navigation.');
                    Redirect(make_admin_url('navigation', 'manage', 'nav', 'id=' . $id));
                }
            }
            Redirect(make_admin_url('navigation', 'insert', 'insert'));
        }
        break;
    case 'update':
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            if (!is_numeric($id)) {
                $error = 1;
                $error_obj->errorAddArray(array('Invalid id supplied, Try Again.'));
                Redirect(make_admin_url('navigation', 'list', 'list'));
            }
            $nav_obj = new navigation;
            if (!$nav_obj->validNavigation($id)) {
                $error = 1;
                $error_obj->errorAddArray(array('Navigation not found, It might be deleted.'));
                Redirect(make_admin_url('navigation', 'list', 'list'));
            }
            $navigation = $nav_obj->getNavigation($id);
        } else {
            $error = 1;
            $error_obj->errorAddArray(array('id must be specified to View the Page.'));
            Redirect(make_admin_url('navigation', 'list', 'list'));
        }
        break;
    default:break;
endswitch;
?>
