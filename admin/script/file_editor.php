<?php

$modName='file_editor';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['module'])?$module=$_GET['module']:$module='CSS';

/*Handle actions here.*/
switch ($action):
	case 'list':
                
		break;
	case 'update':
                if(isset($_POST['save_file'])):
                    if($_POST['file_name']!='' && $_POST['file_path']!=''):
                       $file_full_path = $_POST['file_path'].'/'.$_POST['file_name'];

                       if(file_exists($file_full_path)):
                           if(file::fileWrite($file_full_path,$_POST['file_text'])):
                               $admin_user->set_pass_msg('File has been successfully updated.');
                               Redirect(make_admin_url('file_editor', 'list', 'list','module='.$_POST['module']));
                           else:
                               $admin_user->set_error();   
                               $admin_user->set_pass_msg('Error occurred while editing file.');
                               Redirect(make_admin_url('file_editor', 'list', 'list','module='.$_POST['module']));
                           endif;
                       endif;
                    endif;
                    $admin_user->set_error();   
                    $admin_user->set_pass_msg('Wrong file path');
                    Redirect(make_admin_url('file_editor', 'list', 'list','module='.$_POST['module']));
                endif;
                
                Redirect(make_admin_url('file_editor','list','list','module='.$module));
		break;
                
	case 'upload':
                if(isset($_FILES['image']) && $_FILES['image']['name']!='' && isset($_POST['dir_path']) && $_POST['dir_path']!=''):
                
                    $image_obj=new imageManipulation();
                
                    $base_dir_path = DIR_FS_SITE.substr($_POST['dir_path'],3).'/';
                    $new_file_path = $base_dir_path.$_FILES['image']['name'];
                
                    if(file_exists($new_file_path)):
                        $rand=rand(0, 99999999);
                        $image_name = $image_obj->makeImageName($_FILES['image']['name'], $rand);
                        $new_file_path = $base_dir_path.$image_name;
                    endif;
                    
                    if (in_array($_FILES['image']['type'], $conf_allowed_photo_mime_type)):
                        if(is_writable($base_dir_path)) :
                            if (move_uploaded_file($_FILES['image']['tmp_name'], $new_file_path)):
                                $admin_user->set_pass_msg('New image uploaded successfully.');
                            else:
                                $admin_user->set_error();   
                                $admin_user->set_pass_msg('Error occurred while adding new image.');
                            endif;
                        else:
                            $admin_user->set_error();   
                            $admin_user->set_pass_msg('Folder not writable.');
                        endif;
                    else:
                        $admin_user->set_error();   
                        $admin_user->set_pass_msg('Invalid file type.');
                    endif;
                endif;
                Redirect(make_admin_url('file_editor','list','list','module=Images'));
                break;

    default:break;
endswitch;
?>