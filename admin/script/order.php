<?php

$modName='order';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id=0;
isset($_GET['item_id'])?$item_id=$_GET['item_id']:$item_id=0;
isset($_GET['redirect'])?$redirect=$_GET['redirect']:$redirect='order';
isset($_GET['invoice'])?$invoice=$_GET['invoice']:$invoice='';

//filter parameters
$order_id = isset($_GET['oid'])?$_GET['oid']:'';
$date_from = isset($_GET['date_from'])?$_GET['date_from']:'';
$date_to = isset($_GET['date_to'])?$_GET['date_to']:'';
$customer = isset($_GET['customer'])?$_GET['customer']:'';
$p_quan = isset($_GET['p_quan'])?$_GET['p_quan']:'';
$payment_method = isset($_GET['payment_method'])?$_GET['payment_method']:'';
$price_from = isset($_GET['price_from'])?$_GET['price_from']:'';
$price_to = isset($_GET['price_to'])?$_GET['price_to']:'';
$status = isset($_GET['status'])?$_GET['status']:''; 


$status_array = array(
            "Pending Payment" => "info",
            "Pending" => "info",
            "Payment Failed"  => "warning",
            "Confirmed"       => "info",
            "Cancelled"       => "danger",
            "On hold"         => "warning",
            "Completed"       => "success",
            "Order Shipped"   => 'info',
            "Suspected Fraud" => 'danger',
            "Payment Received"=> 'success',
            "Payment Complete"=> 'success',
            "Payment Refund"  => 'warning',
            "Order Invoiced"  => 'info',
            "Download Permissions Granted" => "success"
        );
/*Handle actions here.*/
switch ($action):
	case 'list':
          
		break;
	case 'update':
                if(isset($_POST['change_state_btn']) && !empty($_POST['state'])):
                    $Qury = new order();
                    if($Qury -> updateOrderStatus($id,$_POST['state'])):
                        
                        $status_obj = new order_status();
                        $new_status = $status_obj -> getStatusDetails($_POST['state']);
                        
                        /* get order contents */
                        $Query_obj= new order();
                        $order = $Query_obj->getOrder($id);
                       
                         /* get order shipping method name */
                        $Query_obj1 = new shipping();
                        $shipping_method = $Query_obj1->getShippingMethod($order->shipping_method_id);
                           
                        $email_template = '';
                        $email_array = array(
                            'CUSTOMER'=>$order->billing_firstname.' '.$order->billing_lastname,
                            'ORDER_ID' => $order->order_id,
                            'CURRENT_STATE' => $order->current_state,
                            'SHIPPING_DATETIME'=>$order->shipping_date,
                            'APPROX_DELIVERY_DATE'=>$order->estimate_delivery_date,
                            'SHIPPING_CARRIER' => $order->shipping_carrier,
                            'TRACKING_NUMBER' => $order->shipping_tracking_no,
                            'TRACKING_URL' => $shipping_method->tracking_url,
                            'PAYMENT_STATUS' => ($order->is_payment_made)?"Yes":"No",
                            'TRANSECTION_ID' => $order->transaction_id,
                            'PAYMENT_METHOD' => $order->payment_method_name,
                            'GRAND_TOTAL' => $order->grand_total,
                            'INVOICE_NUMBER' => $order->invoice_no,
                            'COMMENT' => $_POST['comment'],
                            'CONTACT_US_URL' => make_url('contact'),
                            'PRODUCT_TABLE' => order::getOrderItemsTableEmail($order->id),
                            'SHIPPING_ADDRESS' => order::getOrderShippingAddressEmail($order->id)
                         );
                        if($new_status->email_template && $new_status->email_template!=''):
                            $email_template = send_order_status_email($new_status->email_template,$order->billing_email,$email_array);
                        else:
                            $email_template = '';
                        endif;
                        
                        if($email_template):
                            $_POST['is_email_sent'] =  '1';
                            $_POST['email'] =  $email_template;
                        endif;
                        $_POST['status'] =  $order->current_state;
                        $_POST['date_add'] = date("Y-m-d H:i:s");
                        
                        $qury_status_history = new order_status_history();
                        $qury_status_history -> addOrderStatusHistory($id,$_POST);

                        $admin_user->set_pass_msg('Order Status updated successfully.');
                    else:
                        $admin_user->set_error();
                        $admin_user->set_pass_msg('Something went wrong.');
                    endif;
                    Redirect(make_admin_url('order', 'update', 'update','id='.$id));
                endif;
                
                if(isset($_POST['submit_shipping'])):
                    if($_POST['shipping_date']!='' && $_POST['shipping_carrier']!='' && $_POST['shipping_tracking_no']!=''):
                        $Qury = new order();
                        if($Qury -> updateOrderShippingDetails($id,$_POST)):
                            /* get order contents */
                            $Query_obj= new order();
                            $order = $Query_obj->getOrder($id);
                            
                            /* get order shipping method name */
                            $Query_obj1 = new shipping();
                            $shipping_method = $Query_obj1->getShippingMethod($order->shipping_method_id);
                            
                            $email_array=array(
                                'USER'=>$order->shipping_firstname.' '.$order->shipping_lastname,
                                'ORDER_ID' => $order->order_id,
                                'SHIPPING_DATETIME'=>$_POST['shipping_date'],
                                'APPROX_DELIVERY_DATE'=>$_POST['estimate_delivery_date'],
                                'SHIPPING_CARRIER' => $_POST['shipping_carrier'],
                                'TRACKING_NUMBER' => $_POST['shipping_tracking_no'],
                                'TRACKING_URL' => $shipping_method->tracking_url,
                                'PRODUCT_TABLE' => order::getOrderItemsTableEmail($order->id),
                                'SHIPPING_ADDRESS' => order::getOrderShippingAddressEmail($order->id)
                             );
                            /*** user email **/
                            send_db_email_content(EMAIL_ORDER_SHIPPING_USER, $order->shipping_email,$email_array,ORDER_DEPARTMENT_EMAIL);
                             
                            $admin_user->set_pass_msg('Shipping details updated successfully.');
                        else:
                            $admin_user->set_error();
                            $admin_user->set_pass_msg('Something went wrong.');
                        endif;
                        Redirect(make_admin_url('order', 'update', 'update','id='.$id).'#tab_4');
                    endif;
                endif;
                
                if(isset($_POST['submit_payment'])):
                       $Query = new order();
                        if($Query -> updateOrderPaymentDetails($id,$_POST)):
                            $admin_user->set_pass_msg('Payment details updated successfully.');
                        else:
                            $admin_user->set_error();
                            $admin_user->set_pass_msg('Something went wrong.');
                        endif;
                        Redirect(make_admin_url('order', 'update', 'update','id='.$id).'#tab_4');
                endif;
                /* get order contents */
                $Query_obj= new order();
                $order = $Query_obj->getOrder($id);
                 
                //echo "<pre>";
                  //      print_r($order);
                    //    exit;
                if(!is_object($order)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something went wrong.');
                    Redirect(make_admin_url('order', 'list', 'list'));
                endif;
                
                /* get order shipping method name */
                $Query_obj1 = new shipping();
                $shipping_method = $Query_obj1->getShippingMethod($order->shipping_method_id);

                /* get all shipping methods */
                $Query_obj2 = new shipping();
                $all_shipping_methods = $Query_obj2 -> listShipping(true,'array');
                
                /*  get all order status */
                $OrderStatusObj = new order_status_history();
                $statatus = $OrderStatusObj -> listAllOrderStatus($id);
                
		break;

           case 'archive':
		$QueryObj = new order();
                $orders = $QueryObj ->listOrders(true,'array');
		break;
            
           case 'download_invoice':
                $file_pdf_name='invoice_'.$invoice;
                $post_url=make_url('pdf_print','invoice='.$invoice.'&section=invoice');
                download_pdf_file($file_pdf_name,$post_url);
                break;
           
           case 'packing_slip':
                $file_pdf_name='packingslip_'.$id;
                $post_url=make_url('pdf_print','section=packing_slip&id='.$id); 
                download_pdf_file($file_pdf_name,$post_url);
                break;
            
           case 'shipping_label':
                $file_pdf_name='shipping_label_'.$id;
                $post_url=make_url('pdf_print','section=shipping_label&id='.$id); 
                download_pdf_file($file_pdf_name,$post_url,'','A4','landscape');
                break;
            
            case 'print_label':
		$QueryObj = new order();
                $order_detail = $QueryObj ->orderdetail_single($id,$item_id);
                //echo "<pre>";
                //print_r($order_detail);
		break;
            case 'print_info':
		$QueryObj = new order();
                $order_detail = $QueryObj ->orderdetail_single($id,$item_id);
		break;
            
            
    default:break;
endswitch;
?>