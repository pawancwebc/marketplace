<?php
        require_once("../include/config/config.php");

        $function=array('url_rewrite','url', 'cart','input', 'admin', 'users', 'gallery','database','email');
	include_functions($function);
?>
<?php
if (isset($_POST['submit'])):
 isset($_POST['email']) ? $email = $_POST['email'] : $email = '';
 $QueryObj = new query('admin_user');
 $QueryObj->Where="where email='$email'";
 $user=$QueryObj->DisplayOne();
   if($QueryObj->GetNumRows()):
            $name=$user->username;
            $password=$user->password;
            $header='Forgot Password Request';
            $center_content='';
            $subject=SITE_NAME.': Forgot Password Request';
            $footer='Best Regards, '."<br/>".SITE_NAME;
            $center_content.= "Dear ".ucfirst($user->username).",<br/>";
            $center_content.= "You have requested for your password for the site ".SITE_NAME."<br/><br/>";
            $center_content.= "Your Username is : ".$name."<br/>";
            $center_content.= "Your Password is : " .$password."<br/><br/>";
            $center_content.= "You may change your password for further security and remeberance"."<br/><br/>";
            include_once(DIR_FS_SITE.'include/email/general.php');
            $content=ob_get_contents();
            SendEmail($subject, $email, ADMIN_EMAIL, SITE_NAME, $content, USER_DEPARTMENT_EMAIL, 'html');
            $admin_user->set_pass_msg('Your Login Details has been successfully sent to your Email Id');
            Redirect(DIR_WS_SITE.'index.php');
    else:
       $admin_user->set_pass_msg('Sorry, User with this Email Address does not exist!!');
       Redirect(DIR_WS_SITE.'forgot_password.php');
   endif;
endif;
?>
<?php include_once(DIR_FS_SITE.'control/include/forgot_header.php');?>
    <!-- / Start main -->
<div id="main" class="container_12 clearfix" role="main">
 <?php display_message(1); ?>
<!-- / Start main -->
              <form action="" method="POST" enctype="multipart/form-data" id="validation">
                       <!-- / Box -->
                      <div class="box twothirds">
                            <div class="boxheading clearfix"><h3>Forgot Password</h3><a class="move"></a></div>
                              <section>

                                  <div class="row">
                                        <label for="email">Email Address:</label>
                                        <input type="text" name="email" id="email" class="validate[required,custom[email]]" />
                                 </div>
                                 <div  class="row">
                                     <a href="<?php echo DIR_WS_SITE_CONTROL.'index.php'?>" style="text-decoration:none;" class="right_align btn red submit mt15" name="cancel" > Cancel</a>
                                     <input class="right_align btn green submit mt15" type="submit" name="submit" value="Submit" tabindex="7" />
                                     <div class="clear"></div>
                                 </div>

                           </section>
                      </div>

                   <div class="box onethird">
                     <div class="boxheading clearfix"><h3>Guidelines</h3><a class="move"></a></div>
                       <section>
                               <div  class="row">
                                  <ul>
                                        <li>Please enter your email address to receive your login details. </li>
                                        <li>After submitting this form you will get your Login Details in a email in your email id.</li>
                                 </ul>
                                 <div class="clear"></div>
                              </div>
                      </section>
                  </div>
           </form>
   </div><!--Main div  ending here-->
     <footer role="contentinfo" class="clearfix">
            <section class="container_12"><!-- / Using 960's grid container_12 to standartize paddings/margins -->
                    <div class="lower clearfix">

                            <a href="#" id="top" class="top">top</a>
                    </div>
                    <div class="smallest">
                            <div class="l">
                                 Copyright 2012 cWeb Consultants
                            </div>
                            <div class="r">
                                    Copyright 2012 cWeb Consultants
                            </div>
                    </div>
            </section>
     </footer>

 </body>

</html>