<!-- section coding -->
<?php

/* sections */
switch ($section):
    case 'list':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/list.php');
            break;
    case 'update':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/edit.php');
            break;  
    case 'update_herbs':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/edit_herbs.php');
            break;  
    case 'insert':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/create.php');
            break;         
    case 'insert_group':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/create_group.php');
            break;    
    default:break;
endswitch;
?>




