<?php
$dir_path = isset($_POST['dir_path']) ?  $_POST['dir_path'] :  '';
?>
<div class="modal-dialog">
        <div class="modal-content">
            <?php if($dir_path!=''): ?>
                <div class="modal-body">
                    <div class="note note-info">
                        <p>Put file name including the file extention.<br/> i.e. 'test.tpl'</p>
                    </div>
                    <label>File Name <small>(including file extention)</small></label>
                    <input type="text" id="new_file_name" value="" class="form-control"/>
                </div> 
                <div class="modal-footer">
                    <input type="hidden" id="base_dir_path" value="<?php echo $dir_path;?>"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="add_new_file" id="add_new_file" class="btn blue">Add File</button>
                </div>
            <?php else: ?>
                <div class="modal-body">
                    Wrong Directory path, Please select the directory in which you want to create new file.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            <?php endif; ?>
        </div>
</div>
<div class="clearfix"></div>